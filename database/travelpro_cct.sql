-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 20-12-2016 a las 18:10:52
-- Versión del servidor: 5.5.31
-- Versión de PHP: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `travelpro_cct`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_app_config`
--

CREATE TABLE IF NOT EXISTS `ospos_app_config` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ospos_app_config`
--

INSERT INTO `ospos_app_config` (`key`, `value`) VALUES
('address', 'Venustiano Carranza#381'),
('company', 'Brontobyte'),
('currency_symbol', ''),
('default_tax_1_name', 'Sales Tax'),
('default_tax_1_rate', ''),
('default_tax_2_name', 'Sales Tax 2'),
('default_tax_2_rate', ''),
('default_tax_rate', '8'),
('email', 'adrian@brontobytemx.com'),
('fax', ''),
('language', 'spanish'),
('phone', '3221584600'),
('print_after_sale', '0'),
('return_policy', 'Test'),
('timezone', 'America/New_York'),
('website', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_cobros`
--

CREATE TABLE IF NOT EXISTS `ospos_cobros` (
  `cobro_id` int(255) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `nom_cliente` varchar(20) NOT NULL,
  `concepto` varchar(255) NOT NULL,
  `estado` varchar(15) NOT NULL DEFAULT 'Pendiente',
  `cantidad` double(10,2) NOT NULL DEFAULT '0.00',
  `tipo` varchar(15) NOT NULL DEFAULT 'Efectivo',
  `recurso_id` int(10) NOT NULL,
  `sale_id` int(10) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`cobro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_customers`
--

CREATE TABLE IF NOT EXISTS `ospos_customers` (
  `person_id` int(10) NOT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `taxable` int(1) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `company` varchar(255) DEFAULT NULL,
  UNIQUE KEY `account_number` (`account_number`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ospos_customers`
--

INSERT INTO `ospos_customers` (`person_id`, `account_number`, `taxable`, `deleted`, `company`) VALUES
(3, NULL, 0, 0, '0'),
(4, NULL, 0, 0, '0'),
(5, NULL, 0, 0, '0'),
(6, NULL, 0, 0, '0'),
(7, NULL, 0, 0, '0'),
(8, NULL, 0, 0, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_employees`
--

CREATE TABLE IF NOT EXISTS `ospos_employees` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `username` (`username`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ospos_employees`
--

INSERT INTO `ospos_employees` (`username`, `password`, `person_id`, `deleted`) VALUES
('klara', 'dad8fc3b2ba7dbb10a1f6a587fac918f', 2, 0),
('NaN', '21232f297a57a5a743894a0e4a801fc3', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_gastos`
--

CREATE TABLE IF NOT EXISTS `ospos_gastos` (
  `gasto_id` int(255) NOT NULL AUTO_INCREMENT,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `concepto` varchar(255) NOT NULL,
  `recurso_id` int(10) NOT NULL,
  `estado` varchar(15) NOT NULL DEFAULT 'Pendiente',
  `cantidad` double(10,2) NOT NULL DEFAULT '0.00',
  `tipo` varchar(15) NOT NULL DEFAULT 'Efectivo',
  PRIMARY KEY (`gasto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `ospos_gastos`
--

INSERT INTO `ospos_gastos` (`gasto_id`, `fecha`, `concepto`, `recurso_id`, `estado`, `cantidad`, `tipo`) VALUES
(1, '2016-10-12 19:38:00', 'agua ', 1, 'Hecho', 50.00, 'Efectivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_giftcards`
--

CREATE TABLE IF NOT EXISTS `ospos_giftcards` (
  `giftcard_id` int(11) NOT NULL AUTO_INCREMENT,
  `giftcard_number` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `value` double(15,2) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`giftcard_id`),
  UNIQUE KEY `giftcard_number` (`giftcard_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_inventory`
--

CREATE TABLE IF NOT EXISTS `ospos_inventory` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_items` int(11) NOT NULL DEFAULT '0',
  `trans_user` int(11) NOT NULL DEFAULT '0',
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text NOT NULL,
  `trans_inventory` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`trans_id`),
  KEY `ospos_inventory_ibfk_1` (`trans_items`),
  KEY `ospos_inventory_ibfk_2` (`trans_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Volcado de datos para la tabla `ospos_inventory`
--

INSERT INTO `ospos_inventory` (`trans_id`, `trans_items`, `trans_user`, `trans_date`, `trans_comment`, `trans_inventory`) VALUES
(1, 2, 1, '2016-10-15 22:04:52', 'Edición Manual de Cantidad', 0),
(2, 3, 1, '2016-10-15 22:05:29', 'Edición Manual de Cantidad', 0),
(3, 4, 1, '2016-10-16 10:10:53', 'Edición Manual de Cantidad', 0),
(4, 5, 1, '2016-10-16 10:11:39', 'Edición Manual de Cantidad', 0),
(5, 3, 1, '2016-10-19 20:33:37', 'Edición Manual de Cantidad', 0),
(6, 5, 1, '2016-10-19 20:33:50', 'Edición Manual de Cantidad', 0),
(7, 6, 1, '2016-10-19 21:36:21', 'Edición Manual de Cantidad', 0),
(8, 7, 1, '2016-10-19 22:19:48', 'Edición Manual de Cantidad', 4),
(9, 8, 2, '2016-10-22 12:09:31', 'Edición Manual de Cantidad', 0),
(10, 9, 2, '2016-10-22 12:12:00', 'Edición Manual de Cantidad', 0),
(11, 10, 2, '2016-10-28 15:11:19', 'Edición Manual de Cantidad', 0),
(12, 10, 2, '2016-10-28 15:15:05', 'Edición Manual de Cantidad', 0),
(13, 11, 2, '2016-10-28 15:16:24', 'Edición Manual de Cantidad', 0),
(14, 12, 2, '2016-11-01 14:05:52', 'Edición Manual de Cantidad', 0),
(15, 13, 2, '2016-11-01 15:06:15', 'Edición Manual de Cantidad', 0),
(16, 14, 2, '2016-11-01 15:12:02', 'Edición Manual de Cantidad', 0),
(17, 15, 2, '2016-11-01 15:40:33', 'Edición Manual de Cantidad', 0),
(18, 15, 2, '2016-11-01 15:41:38', 'Edición Manual de Cantidad', 0),
(19, 16, 2, '2016-11-01 15:55:48', 'Edición Manual de Cantidad', 0),
(20, 16, 2, '2016-11-01 15:56:29', 'Edición Manual de Cantidad', 0),
(21, 16, 2, '2016-11-01 15:58:49', 'Edición Manual de Cantidad', 0),
(22, 17, 2, '2016-11-01 16:44:17', 'Edición Manual de Cantidad', 0),
(23, 17, 2, '2016-11-01 16:46:25', 'Edición Manual de Cantidad', 0),
(24, 17, 2, '2016-11-01 16:47:10', 'Edición Manual de Cantidad', 0),
(25, 18, 2, '2016-11-01 17:04:21', 'Edición Manual de Cantidad', 0),
(26, 18, 2, '2016-11-01 17:04:44', 'Edición Manual de Cantidad', 0),
(27, 19, 2, '2016-12-12 11:40:31', 'Edición Manual de Cantidad', 0),
(28, 20, 2, '2016-12-12 12:31:23', 'Edición Manual de Cantidad', 0),
(29, 21, 2, '2016-12-14 13:18:20', 'Edición Manual de Cantidad', 0),
(30, 22, 2, '2016-12-14 13:36:30', 'Edición Manual de Cantidad', 0),
(31, 23, 2, '2016-12-14 13:37:44', 'Edición Manual de Cantidad', 0),
(32, 24, 2, '2016-12-14 13:48:55', 'Edición Manual de Cantidad', 0),
(33, 25, 2, '2016-12-14 13:51:17', 'Edición Manual de Cantidad', 0),
(34, 26, 2, '2016-12-14 14:09:37', 'Edición Manual de Cantidad', 0),
(35, 27, 2, '2016-12-14 14:45:50', 'Edición Manual de Cantidad', 0),
(36, 28, 2, '2016-12-14 14:56:57', 'Edición Manual de Cantidad', 0),
(37, 29, 2, '2016-12-14 18:43:17', 'Edición Manual de Cantidad', 0),
(38, 30, 2, '2016-12-14 18:45:20', 'Edición Manual de Cantidad', 0),
(39, 31, 2, '2016-12-15 13:16:26', 'Edición Manual de Cantidad', 0),
(40, 32, 2, '2016-12-15 14:55:43', 'Edición Manual de Cantidad', 0),
(41, 33, 2, '2016-12-15 14:59:14', 'Edición Manual de Cantidad', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_items`
--

CREATE TABLE IF NOT EXISTS `ospos_items` (
  `name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `item_number` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `cost_price` double(15,2) NOT NULL DEFAULT '0.00',
  `unit_price` double(15,2) NOT NULL DEFAULT '0.00',
  `other_price` double(15,2) NOT NULL DEFAULT '0.00',
  `quantity` double(15,2) NOT NULL DEFAULT '0.00',
  `reorder_level` double(15,2) NOT NULL DEFAULT '0.00',
  `location` varchar(255) NOT NULL DEFAULT 'Principal',
  `item_id` int(10) NOT NULL AUTO_INCREMENT,
  `allow_alt_description` tinyint(1) NOT NULL DEFAULT '0',
  `is_serialized` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `publicar` tinyint(1) NOT NULL DEFAULT '0',
  `promocionar` tinyint(4) NOT NULL DEFAULT '0',
  `brand` varchar(255) DEFAULT NULL,
  `modelo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `item_number` (`item_number`),
  KEY `ospos_items_ibfk_1` (`supplier_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Volcado de datos para la tabla `ospos_items`
--

INSERT INTO `ospos_items` (`name`, `category`, `supplier_id`, `item_number`, `description`, `cost_price`, `unit_price`, `other_price`, `quantity`, `reorder_level`, `location`, `item_id`, `allow_alt_description`, `is_serialized`, `deleted`, `publicar`, `promocionar`, `brand`, `modelo`) VALUES
('Adulto Orlando', 'USA', NULL, NULL, '', 500.00, 10000.00, 10000.00, 0.00, 15.00, '', 2, 0, 0, 0, 0, 0, '0', '0'),
('Menor Orlando', 'USA', NULL, NULL, '', 2000.00, 5000.00, 5000.00, 0.00, 15.00, '', 3, 0, 0, 0, 0, 0, '0', '0'),
('Adulto Tailandia', 'Asia', NULL, NULL, '', 5000.00, 1000.00, 1000.00, 0.00, 15.00, '', 4, 0, 0, 0, 0, 0, '0', '0'),
('Menor Tailandia', 'Asia', NULL, NULL, '', 7000.00, 12000.00, 12000.00, 0.00, 15.00, '', 5, 0, 0, 0, 0, 0, '0', '0'),
('Adulto San Paolo', 'SudAmerica', NULL, NULL, '', 500.00, 600.00, 600.00, 0.00, 8.00, '', 6, 0, 0, 0, 0, 0, '0', '0'),
('Adulto Osaca', 'Japon', NULL, NULL, '', 5000.00, 6000.00, 5500.00, 4.00, 15.00, '', 7, 0, 0, 0, 0, 0, '0', '0'),
('Adulto Puerto Vallarta', 'Mexico', NULL, NULL, '', 600.00, 800.00, 800.00, 0.00, 4.00, '', 8, 0, 0, 0, 0, 0, '0', '0'),
('Menor Puerto Vallarta ', 'Mexico', NULL, NULL, '', 600.00, 800.00, 800.00, 0.00, 4.00, '', 9, 0, 0, 0, 0, 0, '0', '0'),
('Adulto Esencia Europea', 'Europa', NULL, NULL, '', 2000.00, 2735.00, 2735.00, 0.00, 10.00, '', 10, 0, 0, 0, 0, 0, '0', '0'),
('Menor esencia europea', 'Europa', NULL, NULL, '', 550.00, 600.00, 600.00, 0.00, 10.00, '', 11, 0, 0, 0, 0, 0, '0', '0'),
('Adulto Canada', 'CANADA', NULL, NULL, '', 600.00, 999.00, 999.00, 0.00, 9.00, '', 12, 0, 0, 0, 1, 0, '0', '0'),
('Adulto Peru', 'SudAmerica', NULL, NULL, '', 600.00, 999.00, 999.00, 0.00, 0.00, '', 13, 0, 0, 1, 0, 0, '0', '0'),
('Adulto Peru', 'SudAmerica', NULL, NULL, '', 600.00, 900.00, 999.00, 0.00, 8.00, '', 14, 0, 0, 0, 1, 0, '0', '0'),
('Adulto Londres y Paris ', 'Europa', NULL, NULL, '', 600.00, 999.00, 999.00, 0.00, 8.00, '', 15, 0, 0, 0, 1, 0, '0', '0'),
('Adulto Semana Santa', 'Europa', NULL, NULL, '', 1000.00, 1199.00, 1199.00, 0.00, 10.00, '', 16, 0, 0, 0, 1, 0, '0', '0'),
('Adulto Punta Cana', 'Luna de Miel ', NULL, NULL, '', 999.00, 1105.00, 1105.00, 0.00, 5.00, '', 17, 0, 0, 0, 1, 0, '0', '0'),
('Adulto Cuba', 'Lunas de Miel ', NULL, NULL, '', 500.00, 865.00, 865.00, 0.00, 8.00, '', 18, 0, 0, 0, 1, 0, '0', '0'),
('Adulto Cancun', 'Mexico', NULL, NULL, '', 12000.00, 12808.00, 12808.00, 0.00, 5.00, '', 19, 0, 0, 0, 1, 0, '0', '0'),
('Adulto Chiapas ', 'Mexico', NULL, NULL, '', 12560.00, 12570.00, 12570.00, 0.00, 6.00, '', 20, 0, 0, 0, 1, 0, '0', '0'),
('Adulto Triangulo del este ', 'Estados Unidos ', NULL, NULL, '', 999.00, 999.00, 999.00, 0.00, 8.00, '', 21, 0, 0, 0, 1, 0, '0', '0'),
('circuito del oeste', 'USA', NULL, NULL, '', 999.00, 999.00, 999.00, 0.00, 8.00, '', 22, 0, 0, 0, 1, 0, '0', '0'),
('circuito del oeste', 'USA', NULL, NULL, '', 999.00, 999.00, 999.00, 0.00, 8.00, '', 23, 0, 0, 0, 1, 0, '0', '0'),
('Las Vegas', 'USA', NULL, NULL, '', 359.00, 359.00, 359.00, 0.00, 4.00, '', 24, 0, 0, 0, 1, 0, '0', '0'),
('Adulto Las Vegas ', 'USA', NULL, NULL, '', 359.00, 359.00, 359.00, 0.00, 4.00, '', 25, 0, 0, 0, 1, 0, '0', '0'),
('Adulto La Habana', 'Cuba y Caribe', NULL, NULL, '', 1169.00, 1169.00, 1169.00, 0.00, 10.00, '', 26, 0, 0, 0, 1, 0, '0', '0'),
('Adulto Barrancas', 'Mexico', NULL, NULL, '', 15379.00, 15379.00, 15379.00, 0.00, 5.00, '', 27, 0, 0, 0, 1, 0, '0', '0'),
('Adulto Barriloche', 'Lunas de Miel', NULL, NULL, '', 1928.00, 1928.00, 1928.00, 0.00, 9.00, '', 28, 0, 0, 0, 1, 0, '0', '0'),
('Adulto Crucero Alaska', 'Cruceros', NULL, NULL, '', 19279.00, 19279.00, 19279.00, 0.00, 6.00, '', 29, 0, 0, 0, 1, 0, '0', '0'),
('Adulto Crucero Caribe', 'Crucero ', NULL, NULL, '', 12000.00, 12808.00, 12808.00, 0.00, 7.00, '', 30, 0, 0, 0, 1, 0, '0', '0'),
('Adulto crucero Europa', 'Cruceros', NULL, NULL, '', 15000.00, 17350.00, 17350.00, 0.00, 7.00, '', 31, 0, 0, 0, 1, 0, '0', '0'),
('Canada Clasico', 'CANADA', NULL, NULL, '', 1519.00, 1519.00, 1519.00, 0.00, 9.00, '', 32, 0, 0, 1, 1, 0, '0', '0'),
('Adulto Canada Clasico', 'CANADA', NULL, NULL, '', 1519.00, 1519.00, 1519.00, 0.00, 9.00, '', 33, 0, 0, 0, 1, 0, '0', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_items_taxes`
--

CREATE TABLE IF NOT EXISTS `ospos_items_taxes` (
  `item_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `percent` double(15,2) NOT NULL,
  PRIMARY KEY (`item_id`,`name`,`percent`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_item_kits`
--

CREATE TABLE IF NOT EXISTS `ospos_item_kits` (
  `item_kit_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `kit_number` varchar(255) DEFAULT NULL,
  `category` varchar(255) NOT NULL,
  `kit_price` double(10,2) NOT NULL DEFAULT '0.00',
  `publicar` tinyint(4) NOT NULL DEFAULT '0',
  `expiracion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`item_kit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Volcado de datos para la tabla `ospos_item_kits`
--

INSERT INTO `ospos_item_kits` (`item_kit_id`, `name`, `description`, `kit_number`, `category`, `kit_price`, `publicar`, `expiracion`) VALUES
(1, 'BARILOCHE Y BUENOS AIRES ', '0', NULL, 'Lunas de Miel', 1928.00, 1, '2020-07-30 00:00:00'),
(2, 'Orlando Maravilloso', '', NULL, 'USA', 779.00, 1, '2017-04-14 00:00:00'),
(3, 'Triangulo del Este ', '', NULL, 'USA', 999.00, 1, '2017-07-19 00:00:00'),
(4, 'Super Canada ', '0', NULL, 'Canada', 999.00, 1, '2017-02-10 00:00:00'),
(5, 'Canada y Ballenas ', '', NULL, 'Canada', 1629.00, 1, '2017-12-09 00:00:00'),
(6, 'Londres y Paris Promocional', '', NULL, 'Europa', 999.00, 1, '2017-12-09 00:00:00'),
(7, 'Peru', '', NULL, 'SudAmerica', 899.00, 1, '2017-01-01 00:00:00'),
(8, 'Semana Santa ', '', NULL, 'Europa', 1199.00, 1, '2017-12-09 00:00:00'),
(9, 'Circuito del Oeste ', '', NULL, 'USA', 999.00, 1, '2017-11-25 00:00:00'),
(10, 'Mega Canada ', '', NULL, 'Canada', 999.00, 1, '2017-08-05 00:00:00'),
(13, 'Punta Cana', '0', NULL, 'Lunas de Miel', 1105.00, 1, '2016-11-14 00:00:00'),
(14, 'Aromas y Colores de Cuba', '', NULL, 'Lunas de Miel', 865.00, 1, '2017-12-09 00:00:00'),
(15, 'Egipto', '', NULL, 'Medio Oriente', 969.00, 1, '2017-11-14 00:00:00'),
(16, 'Cancun', '0', NULL, 'Mexico', 12808.00, 1, '2017-03-10 00:00:00'),
(17, 'A tu medida', '', NULL, 'SudAmerica', 438.00, 1, '2016-11-01 00:00:00'),
(18, 'Europa', '', NULL, 'Michoacan ', 500.00, 0, '2016-12-27 00:00:00'),
(19, 'Chiapas Semana Santa ', '', NULL, 'Mexico', 12570.00, 1, '2017-04-18 00:00:00'),
(20, 'Argentina', '0', NULL, 'SudAmerica', 899.00, 1, '2017-06-29 00:00:00'),
(23, 'Cancun', '', NULL, 'Mexico', 12808.00, 1, '2016-12-10 00:00:00'),
(24, 'Crucero Alaska', '', NULL, 'Cruceros', 999.00, 1, '2017-05-17 00:00:00'),
(25, 'Crucero al Caribe', '', NULL, 'Cruceros', 13359.00, 1, '2017-05-07 00:00:00'),
(26, 'Crucero a Europa', '', NULL, 'Cruceros', 17530.00, 1, '2017-05-20 00:00:00'),
(27, 'Crucero a Canada', '', NULL, 'Cruceros', 999.00, 1, '2017-12-10 00:00:00'),
(28, 'Turquia', '', NULL, 'Medio Oriente', 499.00, 1, '2017-12-10 00:00:00'),
(29, 'Dubai', '', NULL, 'Medio Oriente', 999.00, 1, '2017-12-10 00:00:00'),
(30, 'Egipto Especial ', '', NULL, 'Medio Oriente', 969.00, 1, '2017-03-10 00:00:00'),
(31, 'Santo Domingo ', '', NULL, 'Cuba y Caribe', 525.00, 1, '2017-12-10 00:00:00'),
(32, 'Harmony of the seas ', '', NULL, 'Cruceros', 0.00, 1, '2017-01-10 00:00:00'),
(33, '0', '0', NULL, '0', 0.00, 0, '0000-00-00 00:00:00'),
(34, '0', '0', NULL, '0', 0.00, 0, '0000-00-00 00:00:00'),
(35, 'Las Vegas ', '', NULL, 'USA', 359.00, 1, '2017-04-20 00:00:00'),
(37, 'La Habana', '', NULL, 'Cuba y Caribe', 1169.00, 1, '2017-03-30 00:00:00'),
(38, 'Barrancas del cobre', '', NULL, 'Mexico', 15379.00, 1, '2017-04-19 00:00:00'),
(39, 'Crucero a las Bahamas', '', NULL, 'Cruceros', 7939.00, 1, '2017-05-08 00:00:00'),
(40, '0', '0', NULL, '0', 0.00, 0, '0000-00-00 00:00:00'),
(41, 'Canada Clasico', '', NULL, 'Canada', 1519.00, 1, '2017-04-10 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_item_kit_items`
--

CREATE TABLE IF NOT EXISTS `ospos_item_kit_items` (
  `item_kit_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` double(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`item_kit_id`,`item_id`,`quantity`),
  KEY `ospos_item_kit_items_ibfk_2` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ospos_item_kit_items`
--

INSERT INTO `ospos_item_kit_items` (`item_kit_id`, `item_id`, `quantity`) VALUES
(2, 2, 779.00),
(4, 12, 999.00),
(5, 12, 1629.00),
(10, 12, 1303.00),
(30, 12, 999.00),
(7, 13, 999.00),
(6, 16, 1199.00),
(8, 16, 1199.00),
(13, 17, 1105.00),
(14, 18, 865.00),
(16, 19, 12808.00),
(19, 20, 1.00),
(3, 21, 999.00),
(9, 22, 999.00),
(35, 25, 359.00),
(37, 26, 1169.00),
(38, 27, 15379.00),
(1, 28, 1928.00),
(24, 29, 999.00),
(25, 30, 13359.00),
(26, 31, 17350.00),
(41, 33, 1519.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_locations`
--

CREATE TABLE IF NOT EXISTS `ospos_locations` (
  `nombre_location` varchar(255) NOT NULL,
  `item_id` int(20) NOT NULL,
  `cantidad` double(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_modules`
--

CREATE TABLE IF NOT EXISTS `ospos_modules` (
  `name_lang_key` varchar(255) NOT NULL,
  `desc_lang_key` varchar(255) NOT NULL,
  `sort` int(10) NOT NULL,
  `module_id` varchar(255) NOT NULL,
  PRIMARY KEY (`module_id`),
  UNIQUE KEY `desc_lang_key` (`desc_lang_key`),
  UNIQUE KEY `name_lang_key` (`name_lang_key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ospos_modules`
--

INSERT INTO `ospos_modules` (`name_lang_key`, `desc_lang_key`, `sort`, `module_id`) VALUES
('module_cobros', 'module_cobros_desc', 120, 'cobros'),
('module_config', 'module_config_desc', 100, 'config'),
('module_customers', 'module_customers_desc', 10, 'customers'),
('module_employees', 'module_employees_desc', 80, 'employees'),
('module_gastos', 'module_gastos_desc', 130, 'gastos'),
('module_giftcards', 'module_giftcards_desc', 90, 'giftcards'),
('module_items', 'module_items_desc', 20, 'items'),
('module_item_kits', 'module_item_kits_desc', 30, 'item_kits'),
('module_pedidos', 'module_pedidos_desc', 140, 'pedidos'),
('module_receivings', 'module_receivings_desc', 60, 'receivings'),
('module_recursos', 'module_recursos_desc', 150, 'recursos'),
('module_reports', 'module_reports_desc', 50, 'reports'),
('module_sales', 'module_sales_desc', 70, 'sales'),
('module_services', 'module_services_desc', 110, 'services'),
('module_suppliers', 'module_suppliers_desc', 40, 'suppliers'),
('module_vehicles', 'module_vehicles_desc', 160, 'vehicles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_pedidos`
--

CREATE TABLE IF NOT EXISTS `ospos_pedidos` (
  `pedido_id` int(255) NOT NULL AUTO_INCREMENT,
  `pedido_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `customer_id` int(10) NOT NULL,
  `payment_type` varchar(512) NOT NULL,
  `total` double(15,2) NOT NULL,
  `estado` varchar(15) NOT NULL DEFAULT 'Pendiente',
  PRIMARY KEY (`pedido_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `ospos_pedidos`
--

INSERT INTO `ospos_pedidos` (`pedido_id`, `pedido_time`, `customer_id`, `payment_type`, `total`, `estado`) VALUES
(1, '2016-12-15 14:41:48', 8, 'efectivo', 0.00, 'Pendiente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_pedidos_items`
--

CREATE TABLE IF NOT EXISTS `ospos_pedidos_items` (
  `pedido_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) NOT NULL,
  `serialnumber` varchar(30) NOT NULL,
  `line` int(3) NOT NULL,
  `quantity_purchased` double(15,2) NOT NULL,
  `item_cost_price` double(15,2) NOT NULL,
  `item_unit_price` double(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL,
  PRIMARY KEY (`pedido_id`,`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_people`
--

CREATE TABLE IF NOT EXISTS `ospos_people` (
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address_1` varchar(255) NOT NULL,
  `address_2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `person_id` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `ospos_people`
--

INSERT INTO `ospos_people` (`first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES
('John', 'Doe', '555-555-5555', 'admin@pappastech.com', 'Address 1', '', '', '', '', '', '', 1),
('Klara', 'Rivera', '', '', '', '', '', '', '', '', '', 2),
('Adrian', 'Ramirez', '3221584600', 'adrian@brontobytemx.com', 'Puerto la Paz#179, Ramblases', '0', 'Puerto Vallarta', '0', '0', '', '0', 3),
('Adrian', 'Ramirez', '3221584600', 'adrian@brontobytemx.com', 'Puerto la Paz#179, Ramblases', '0', 'Puerto Vallarta', '0', '0', '', '0', 4),
('CLARA ', 'rivera ', '4621303174', 'clara@travelpro.mx', 'bilbao ', '0', 'Irapuato, Guanajuato', '0', '0', '', '0', 5),
('Adrian', 'Ramirez', '3221584600', 'adrian@brontobytemx.com', 'Puerto la Paz#179, Ramblases', '0', 'Puerto Vallarta', '0', '0', '', '0', 6),
('Adrian', 'Ramirez', '3221584600', 'adrian@brontobytemx.com', 'Puerto la Paz#179, Ramblases', '0', 'Puerto Vallarta', '0', '0', '', '0', 7),
('Adrian', 'Ramirez', '3221584600', 'adrian@brontobytemx.com', 'Puerto la Paz#179, Ramblases', '0', 'Puerto Vallarta', '0', '0', '', '0', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_permissions`
--

CREATE TABLE IF NOT EXISTS `ospos_permissions` (
  `module_id` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL,
  PRIMARY KEY (`module_id`,`person_id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ospos_permissions`
--

INSERT INTO `ospos_permissions` (`module_id`, `person_id`) VALUES
('cobros', 1),
('config', 1),
('customers', 1),
('employees', 1),
('gastos', 1),
('giftcards', 1),
('items', 1),
('item_kits', 1),
('pedidos', 1),
('receivings', 1),
('recursos', 1),
('reports', 1),
('sales', 1),
('services', 1),
('suppliers', 1),
('vehicles', 1),
('cobros', 2),
('config', 2),
('customers', 2),
('employees', 2),
('gastos', 2),
('giftcards', 2),
('items', 2),
('item_kits', 2),
('pedidos', 2),
('receivings', 2),
('recursos', 2),
('reports', 2),
('sales', 2),
('services', 2),
('suppliers', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_receivings`
--

CREATE TABLE IF NOT EXISTS `ospos_receivings` (
  `receiving_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `receiving_id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(20) DEFAULT NULL,
  `total` double(15,2) NOT NULL,
  `modo` varchar(20) NOT NULL DEFAULT 'Entrada',
  PRIMARY KEY (`receiving_id`),
  KEY `supplier_id` (`supplier_id`),
  KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_receivings_items`
--

CREATE TABLE IF NOT EXISTS `ospos_receivings_items` (
  `receiving_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL,
  `quantity_purchased` int(10) NOT NULL DEFAULT '0',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` double(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`receiving_id`,`item_id`,`line`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_recursos`
--

CREATE TABLE IF NOT EXISTS `ospos_recursos` (
  `recurso_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `recurso` double(15,2) NOT NULL,
  PRIMARY KEY (`recurso_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `ospos_recursos`
--

INSERT INTO `ospos_recursos` (`recurso_id`, `nombre`, `recurso`) VALUES
(1, 'Caja Chica', 1950.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_sales`
--

CREATE TABLE IF NOT EXISTS `ospos_sales` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `sale_id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`sale_id`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_sales_items`
--

CREATE TABLE IF NOT EXISTS `ospos_sales_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` double(15,2) NOT NULL DEFAULT '0.00',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` double(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_id`,`line`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_sales_items_taxes`
--

CREATE TABLE IF NOT EXISTS `ospos_sales_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `percent` double(15,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_sales_item_kits`
--

CREATE TABLE IF NOT EXISTS `ospos_sales_item_kits` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_kit_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` double(15,2) NOT NULL DEFAULT '0.00',
  `kit_price` double(15,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`item_kit_id`,`line`),
  KEY `item_kit_id` (`item_kit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_sales_payments`
--

CREATE TABLE IF NOT EXISTS `ospos_sales_payments` (
  `sale_id` int(10) NOT NULL,
  `payment_type` varchar(40) NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL,
  `total` double(15,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`payment_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_sales_suspended`
--

CREATE TABLE IF NOT EXISTS `ospos_sales_suspended` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `sale_id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`sale_id`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_sales_suspended_items`
--

CREATE TABLE IF NOT EXISTS `ospos_sales_suspended_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` double(15,2) NOT NULL DEFAULT '0.00',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` double(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_id`,`line`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_sales_suspended_items_taxes`
--

CREATE TABLE IF NOT EXISTS `ospos_sales_suspended_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `percent` double(15,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_sales_suspended_payments`
--

CREATE TABLE IF NOT EXISTS `ospos_sales_suspended_payments` (
  `sale_id` int(10) NOT NULL,
  `payment_type` varchar(40) NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`payment_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_services`
--

CREATE TABLE IF NOT EXISTS `ospos_services` (
  `service_id` int(255) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `nom_cliente` varchar(50) NOT NULL,
  `nom_asignado` varchar(50) NOT NULL,
  `motivo` varchar(255) NOT NULL,
  `accion` text NOT NULL,
  `reporte` text NOT NULL,
  `estado` varchar(100) NOT NULL DEFAULT 'Pendiente',
  `duracion` double(10,1) NOT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_sessions`
--

CREATE TABLE IF NOT EXISTS `ospos_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ospos_sessions`
--

INSERT INTO `ospos_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('006dd1b604575db3a6a7853cb94ed7be', '66.102.6.114', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478727639, NULL),
('00a4a3737166fa68560878a48dcff24d', '54.80.114.81', 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv', 1479295552, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('010193199572a63ab7c0990cce24f33f', '189.187.197.58', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1476313282, 'a:2:{s:9:"person_id";s:1:"2";s:15:"customer_cobros";s:2:"-1";}'),
('01a2fd3ecc116cc580dc6f4dfce3c673', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481747651, 'a:3:{s:11:"captchaWord";s:3:"101";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('01c62fbea03ed01983dcc5187b15df31', '93.150.81.44', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480794363, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('01ce18264df8a231fb9c1664f3b9e3b9', '66.249.65.171', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480185551, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0206ead120c17a2032622b4882c8e2fd', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479670585, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0218a0575796c995eaa76786afdd9ad7', '66.249.64.176', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481883685, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('02416e8e46f2910e58c4aa7cc5e76f5b', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481746004, 'a:3:{s:11:"captchaWord";s:3:"767";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0283ac6345882bd81f402c928972723b', '187.216.136.86', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0_2 like Mac', 1477153781, 'a:4:{s:11:"captchaWord";s:3:"932";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"1";}'),
('029ba2992d315c1f03067197e0ee37da', '189.164.111.62', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479342581, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('030bb0fdff4161a40ac2bcfc79cb7002', '189.177.226.244', 'WhatsApp/2.16.225 A', 1480362983, NULL),
('033401bbaba1bdf1839b03d88e8c6519', '66.249.65.171', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479292360, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('037a815f93ac141cc2a9595cc1b47eba', '66.249.91.124', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1479933012, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('039abbea90e3f82732a22d6e9eeec8af', '66.249.90.78', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1479587932, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('03be169173be224812424661d565a58c', '187.144.112.193', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1482003113, 'a:3:{s:11:"captchaWord";s:3:"750";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('03e9ad9ff67571592a9980abe0ad3709', '210.163.39.79', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1481659979, 'a:3:{s:11:"captchaWord";s:3:"432";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('040a8275258fe8617ad04afa41fc0c78', '189.187.219.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479517597, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('04151362986c9644792f13d62644a4c7', '200.68.128.81', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1479408335, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('041a0889e621a99eb1d28d57deca55eb', '189.187.156.194', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480791228, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('045b88b3f2c091f05305eff3c7d38ecb', '54.92.251.73', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1479792020, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('04a1872f0c36c8b36c0b6c91dfe68f96', '66.249.73.150', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1481913052, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('04c6333f590b4e9a3bf249933ee7d156', '187.193.19.171', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481846367, 'a:5:{s:11:"captchaWord";s:3:"887";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"2";s:8:"position";s:6:"inside";}'),
('05484d332c3a3135f5e4b66ec337c180', '189.177.226.244', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1479927651, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('058739a0794b1679cdcde25ec5e7cb1b', '157.55.39.25', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS', 1479507022, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('058864589e6313b1c014b73b62a8c783', '66.249.69.243', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479927342, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('05a3c0e7dce15b8a4973931b203a1150', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482205542, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0669f6d281d5876a4eb6777ff4d5f067', '64.246.165.140', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en; rv:1.', 1481639451, 'a:3:{s:11:"captchaWord";s:3:"809";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('068b0c5941e68ce59265b303e52d3cdc', '157.55.39.107', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481809534, 'a:3:{s:11:"captchaWord";s:3:"135";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('06b1ba76c64e1340f930680e687ede9d', '189.187.91.125', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1478023179, 'a:7:{s:9:"person_id";s:1:"2";s:8:"position";s:6:"inside";s:9:"data_save";a:1:{i:1;a:11:{s:7:"item_id";s:2:"12";s:4:"line";i:1;s:4:"name";s:13:"Adulto Canada";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";d:1001;s:8:"discount";i:0;s:5:"price";s:6:"999.00";}}s:4:"kits";a:1:{i:2;a:11:{s:7:"item_id";s:13:"Super Canada ";s:4:"line";i:2;s:4:"name";s:13:"Super Canada ";s:11:"item_number";N;s:11:"description";s:239:"Salidas Especiales\nEnero: 15, 29 \nFebrero: 12, 26\n Marzo: 12, 18 y 25\n Abril: 02, 08, 09, 10, 15, 16, 23, 30\nMayo: 14, 21, 28 \nJunio: 04, 11, 18, 25 \nJulio: 09, 15, 16, 21, 22, 23, 24, 26, 27\n Julio: 28, 29, 30, 31\n Agosto: 01, 02, 03, 04\n";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:6:"999.00";}}s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('071323e1ff6f566d29a9e7f64bb4b3e5', '207.46.13.82', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480945806, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('071c76292dc6ce5501cb2190719112f8', '66.249.80.20', 'Mozilla/5.0 (Linux; Android 6.0; XT1063 Build/MPB2', 1481563641, 'a:3:{s:11:"captchaWord";s:3:"546";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('071e084fa5927df117de3bca47b07800', '187.144.112.193', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1482003134, 'a:3:{s:11:"captchaWord";s:3:"426";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('073297fa13b85972aee356d95eaeda61', '66.102.7.154', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478539425, 'a:3:{s:11:"captchaWord";s:3:"238";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('079ae8d18952ae4b12eb52f96792f757', '157.55.39.90', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480827313, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('07e26d2e51c9e564990a140d112d6c0c', '187.192.179.167', 'Mozilla/5.0 (iPad; CPU OS 10_0_2 like Mac OS X) Ap', 1479750466, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('08131cd362a9c6f07509dc68b1a30822', '187.144.112.193', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1482238012, 'a:3:{s:11:"captchaWord";s:3:"822";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0824c8dca9bb18d691bbc63d4896ce1e', '199.59.150.183', 'Twitterbot/1.0', 1480055185, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('084ea8dfa40452fdd596a72ee5f36a65', '189.216.199.250', 'Mozilla/5.0 (Linux; Android 6.0.1; SAMSUNG SM-G925', 1481717340, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('08522d35a0a149faf3b41fa5e2532e50', '189.187.1.49', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_3_5 like Mac ', 1481759000, 'a:3:{s:11:"captchaWord";s:3:"596";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('085baa0ba68742cec621c040d1553efe', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481415255, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('08a2a7a5ec152ba8724c0ba016bf706d', '211.123.206.74', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1480803250, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0976a2f5c55b5faf5ffbef43dd8cced6', '187.254.154.152', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1476927675, 'a:6:{s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";s:11:"captchaWord";s:3:"663";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";s:6:"cart_p";a:0:{}}'),
('0978ea14f3ec2f8e6f52643ec9298030', '189.158.85.175', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-G935F Build/', 1479968733, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0991ceb01983ccecfe52f1ffe77a2493', '66.249.65.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481982361, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('09cbc9d49b914aabcbf61a930dded8c9', '157.55.39.135', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480436540, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('09eca0166af5d60b3542da6ac1353677', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481044993, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('09f31780f6f18c8cec22c7c71263b5f5', '189.217.201.161', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_2_1 like Mac ', 1480266400, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0a02f3388e3aa86b4567bc566294bba1', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479291031, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0a043ce4ab5dfa8bde55b3599e833d61', '157.55.39.135', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480436541, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0a4a78dba50c86cdeeb455c0ec57b94b', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239578, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0a687aefd670c97b179dad3710e9c977', '66.102.7.150', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478793528, 'a:3:{s:11:"captchaWord";s:3:"108";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0a822f6b89fa528bdd9376f6083b8ab1', '187.237.231.37', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1479499519, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0a951b55b5f0ae39ae1574e2a4ea8f0c', '66.249.65.66', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482089493, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0aabced42581822a32f55e70c9012585', '66.249.65.148', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479680944, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0af52b448cd66c70e1ff45a5aba05968', '66.249.65.174', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479382469, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0b0b2b95cd3e5b1290631e59f34481bb', '189.166.35.200', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1479848361, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0b7298438f826f07a617828d3a7fd583', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481045002, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0b93ec814de1b267ca959cb4b6b446d0', '189.177.239.107', 'WhatsApp/2.16.225 A', 1476919166, 'a:3:{s:11:"captchaWord";s:3:"172";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('0bab1e54bd017ad33a9e562db3e31585', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481764859, 'a:3:{s:11:"captchaWord";s:3:"812";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0bba47fb1703f84fbbf73dc61c885df6', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481414957, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0bc3a14fbbf0e38cd61f03eb862f08d6', '199.58.86.209', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481574987, 'a:3:{s:11:"captchaWord";s:3:"295";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0bed2b735f01b7f2d331173b238256f4', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612370, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0c38351889c1b5f349d934a7e8233517', '187.193.3.186', 'Mozilla/5.0 (Linux; Android 4.4.2; SM-G355M Build/', 1480910065, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0c74a0e0727f6cfdb45b770f34455727', '54.146.58.46', 'CheckMarkNetwork/1.0 (+http://www.checkmarknetwork', 1479942440, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0cbf8cb591160aae7dfa16be2345fb87', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481963731, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0d07725073d599b83edbdaffe922fb06', '54.166.69.236', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1480998495, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0d55140ffe300b8f8d46587944111d26', '189.187.63.5', 'Mozilla/5.0 (iPad; CPU OS 9_3_5 like Mac OS X) App', 1480542067, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0d91aa6f74c6619b6287088ef06accad', '189.177.239.107', 'WhatsApp/2.16.225 A', 1478050924, 'a:3:{s:11:"captchaWord";s:3:"426";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0daba7fa6e553928b4dcfc6269b076d5', '189.187.108.161', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481071119, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0de5142643d5e4184190398423492902', '66.249.65.156', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480096114, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0e2b1301d3bebb6de58610b84a83646d', '189.187.22.85', 'Mozilla/5.0 (iPad; CPU OS 10_1_1 like Mac OS X) Ap', 1479410680, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0e6d20f57b75273dcc95bd2a5f598fa6', '66.249.65.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482058123, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0edd492cd2c73efd3ff3a44bea88cad3', '211.6.121.143', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1479499944, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0f657b274edd61c4c40a7b9178544b51', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480066076, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0f6d98fffb488f671c9de93ae3544559', '40.77.167.4', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS', 1480552272, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('0fdd06397565cbe31a16c9a594521105', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480187446, 'a:5:{s:9:"person_id";s:1:"2";s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";s:8:"position";s:6:"inside";}'),
('0fe6c7fcf67b9554285e75f695dd40cd', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239576, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('104666b0d79a8f86e3dab624ac65ed69', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480378647, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1078521027e5bdfb4571f280d6dd1a54', '52.4.244.57', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) App', 1481070466, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('11004905d499ee9bf95ed78c5f3d108d', '23.96.208.137', 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1', 1481914755, 'a:3:{s:11:"captchaWord";s:3:"708";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('113972118d3a8c4c667c5c49f33521da', '91.215.152.226', 'Mozilla/5.0 (Windows NT 6.1; rv:20.0) Gecko/201001', 1481878419, 'a:3:{s:11:"captchaWord";s:3:"690";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1144b5883932d6a198d0a0438075d030', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481518243, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1196f64a5497db7402010704dff9fb04', '66.249.65.106', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480820746, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('11c0ae14d616933210925b265003de02', '66.249.90.78', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1480628109, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('11f03baceb9b56483fd2a8d36d9fa046', '199.59.150.182', 'Twitterbot/1.0', 1481567032, 'a:3:{s:11:"captchaWord";s:3:"276";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('11f513c3983aa20249796691a3cb020c', '66.249.90.132', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1480894824, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1204f9aef0503ea80b082467756c2614', '87.206.241.37', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480368596, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('12054ccbb343b4854c13d2523596a1ec', '162.223.11.137', 'Mozilla/4.0 (compatible; MSIE 6.0b; Windows NT 5.0', 1481143207, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1285be87aeab1b92b2b308629737c5f3', '189.187.115.79', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479230106, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('128cca38f3b9b09badd24c68c3e6ddfa', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612357, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('12a3b57490c1d9a8c74d92f876a9d070', '66.249.65.55', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482205851, 'a:3:{s:11:"captchaWord";s:3:"902";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('12c76b81faca93d0a61d86f51b89ce8d', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480168001, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('12e8e60efc38fa0af9af3e1d32f72e7a', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481747650, 'a:3:{s:11:"captchaWord";s:3:"190";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('132beb77769d5e7904c471e0ef3c857c', '66.249.91.124', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1479933013, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('139890a449ef810a407e40124c3767e3', '189.187.51.160', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480134903, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('139f575177db8e15a9536e7b7253fcc3', '157.55.39.195', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480436544, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('13a6541c3ad68693ab2bfe8130485e2c', '66.249.65.55', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482123342, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('13d7e8324cc3b7fa27204bd7503c7d9b', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651304, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('14008de27fb71f840a54695d3fa43dee', '157.55.39.213', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481809520, 'a:3:{s:11:"captchaWord";s:3:"183";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('14099ba4bc18270aad52169cce634c77', '66.102.7.194', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1481814577, 'a:3:{s:11:"captchaWord";s:3:"108";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('142a12b9669a2b8f2e994953d1b5f541', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481738926, 'a:3:{s:11:"captchaWord";s:3:"514";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('143daeef2e33b117ad63125f88ac1e0b', '66.249.65.174', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479499638, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1460b725f19df8f84c9351ff736ed6e3', '104.209.188.207', 'Mozilla/5.0 (Windows NT 6.1; WOW64) SkypeUriPrevie', 1479342221, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('148220575cc3746029fc485414d9b369', '66.249.90.55', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1481403155, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('14e0381f75f0236e6b9fee645c5986b1', '66.249.65.70', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482150374, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('14e318e51f013e7e74fc8c3fcca48ebf', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239576, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1577ecb19341dacea87983ac5407885c', '189.187.219.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479930576, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('158a3a73c2be9f50eb35e7c8e14f0a54', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651278, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('15959437a718d0725ab879ea17c0b184', '66.249.64.166', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481903022, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('16113fe63762a22df9872430bc1d526c', '189.163.157.253', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1481861563, 'a:3:{s:11:"captchaWord";s:3:"883";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1644f317b213f4f4c873c24f8e96410f', '157.55.39.109', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481809525, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('16ae91c9a43584c07fe9347a78252b0f', '157.55.39.55', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481414836, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('16c5ef325b21cf0dfede42007286fb9f', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480987692, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('16f45708b48c008ff13e563c2b876454', '66.249.90.132', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1481069723, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('16fa421f523822a68c1260b05a2bb293', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651247, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('171703f863e11a46591d29efccee510d', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481975010, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1761a2651147dcbe5a3393be83a5db59', '201.141.85.70', 'Mozilla/5.0 (iPad; CPU OS 10_1_1 like Mac OS X) Ap', 1480476349, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('17d83da8671dd630170b9a12ec8b653a', '64.246.165.210', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en; rv:1.', 1481633568, 'a:3:{s:11:"captchaWord";s:3:"182";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('17df53ec2eba2699e8a1fd01c37ebcd9', '189.187.89.119', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1479851757, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('181edb9e9d0222badbc068330c7cfd4c', '177.233.11.210', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1479842202, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('182bce6109aad241a2988c44b5002142', '66.102.7.154', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479065663, NULL),
('185fbb89ecfc47719e991a338913ed84', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482189908, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('18af04ebaeb8dac2a8eb774a57d9a672', '157.55.39.25', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS', 1479345082, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('18bc760153878ce7d558d3b79001d23a', '189.177.239.107', 'WhatsApp/2.16.225 A', 1478050928, NULL),
('18ca351ea06ab81acf7e2355df9a32ea', '66.249.65.174', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479594783, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('192cb3737b8b0474a5c4842ddd6f70ff', '177.248.151.124', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0)', 1481770478, 'a:3:{s:11:"captchaWord";s:3:"515";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('19e5fe237666cb8724ae959a471e2bdb', '189.187.156.104', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) Ap', 1479845212, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1a0372ac2d408ba6432adb7d4f073eb7', '189.187.23.11', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) Ap', 1479408388, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1a52d440ebd5777245113075d1656a05', '157.55.39.107', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481809534, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1a703a6b3e46a224d944f3ddec9077b6', '189.177.239.107', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1477608221, 'a:9:{s:11:"captchaWord";s:3:"Un9";s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";s:9:"data_save";a:2:{i:-1;a:11:{s:7:"item_id";s:1:"4";s:4:"line";i:-1;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";d:30;s:8:"discount";i:0;s:5:"price";s:7:"1000.00";}i:-2;a:11:{s:7:"item_id";s:1:"5";s:4:"line";i:-2;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";d:32;s:8:"discount";i:0;s:5:"price";s:8:"12000.00";}}s:4:"kits";a:3:{i:1;a:11:{s:7:"item_id";s:9:"Tailandia";s:4:"line";i:1;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:2;s:8:"discount";i:0;s:5:"price";s:8:"15300.00";}i:3;a:11:{s:7:"item_id";s:9:"Tailandia";s:4:"line";i:3;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:8:"15300.00";}i:2;a:11:{s:7:"item_id";s:9:"Tailandia";s:4:"line";i:2;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:2;s:8:"discount";i:0;s:5:"price";s:8:"15300.00";}}s:6:"cart_p";a:3:{i:1;a:11:{s:7:"item_id";s:1:"4";s:4:"line";i:1;s:4:"name";s:16:"Adulto Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:1:"2";s:8:"discount";i:0;s:5:"price";s:7:"1000.00";}i:2;a:11:{s:7:"item_id";s:1:"5";s:4:"line";i:2;s:4:"name";s:15:"Menor Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:1:"2";s:8:"discount";i:0;s:5:"price";s:8:"12000.00";}i:3;a:11:{s:7:"item_id";s:9:"Tailandia";s:4:"line";i:3;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:8:"15300.00";}}s:4:"tots";s:1:"0";s:8:"customer";s:2:"-1";s:10:"payments_p";s:0:"";}'),
('1aa6008f99df140702194675ac297882', '66.249.65.200', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481582413, 'a:3:{s:11:"captchaWord";s:3:"299";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1ab8f87226e7458cabaa522c79f68b59', '66.249.65.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482067873, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1b1953e9322e1249b9ec3fa70b0c4357', '66.102.7.152', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479752569, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1b29b038d2fb144ffbc8807078d7540f', '158.69.225.34', 'Mozilla/5.0 (compatible; Dataprovider; https://www', 1481695470, 'a:3:{s:11:"captchaWord";s:3:"93Q";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1b491fb1548397ce2224dffb38877a03', '69.30.234.74', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko', 1481555118, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1c06d12c18f98a6b98c1018d119c9043', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481222815, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1c0a33da68b9ed5668261b17ce73fde2', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480358316, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1c171c1e44e6d155ffba767747be1896', '189.244.204.55', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1479579053, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1c1b68e644816ff376c722b6e924f9c1', '189.166.52.5', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480609275, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1c6b436bf0058f410657c8010e714f14', '66.249.90.76', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1480191597, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1c8cd96a1f83d3992bc98b7873303508', '66.102.6.146', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1477764558, 'a:3:{s:11:"captchaWord";s:3:"586";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1d1e40212da408fbcda8c83855d1297c', '66.249.69.243', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480052239, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1d8670681e1f1420674dca5703bfd122', '66.249.65.156', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480080589, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1d944b1d8eeb190063099b1af0874429', '189.187.219.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479496444, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1dd592e085e9f6b17a031d0b0ce4be85', '66.249.65.148', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479606702, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1e36d6d60d7da36c21f37093e32d4e5e', '66.249.85.136', 'Mozilla/5.0 (Linux; Android 4.4.2; LG-D331 Build/K', 1476320934, 'a:3:{s:11:"captchaWord";s:3:"724";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('1e481fa588eecae899a3c91760b382c2', '66.102.7.152', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478627713, NULL),
('1ec064102ed4fd4a51c58ee27261bf84', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612340, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1ef3c84b9495c501d75eae79cf0d6f4a', '66.249.92.139', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1480366083, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1f03ccfd0f87a6e7c71b9b0618007082', '66.249.90.76', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1480106620, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1f045be1853046beb7660d78256c6bdb', '157.55.39.136', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481538547, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1f21929940759b09d5f014ab5e2d557a', '189.177.239.107', 'WhatsApp/2.16.225 A', 1476920876, 'a:2:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('1f5ab551399ee7a71fff7f09fdde45a6', '189.187.219.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479933678, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1fa126055985e9e37491b44eda66fa3d', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482141261, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1facda4fd0f953545c6b97dabb437b8e', '92.241.162.3', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1480512145, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1fcb0564da201640e0f2c9cd0d285a0a', '187.144.134.51', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480883314, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1fd81d4135571f540a48a2d5d297ad54', '157.55.39.1', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480436560, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1fe3564c3b36324e82430d8639ba49e0', '66.249.64.176', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481894472, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('1fee3c24521ff4d7c5a80f60ee5c89bb', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651252, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('200f32bc42108dba968633ed7423b249', '66.249.79.110', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481743293, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2056c85e48e8c4ed25454df175043ed0', '66.249.65.156', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480164936, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('20cbe46a2f3e8d696522bde938807b0a', '207.46.13.28', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1482085069, 'a:3:{s:11:"captchaWord";s:3:"286";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('20d56bb93cdc3fed590b69e464ef5072', '74.85.220.99', '=Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) A', 1479562185, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('21223b87b454ac19e233a400b7d96231', '108.171.133.170', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481149135, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('216cf5113b1d2725a5df863add3d9f86', '66.249.65.177', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479692851, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('21797fdc6a1f9892e40acd72f2aa1deb', '207.46.13.64', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480945794, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2192ba071b0169cc6993ca3236df7d08', '66.249.88.62', 'Mozilla/5.0 (Linux; Android 6.0.1; LG-H840 Build/M', 1481395097, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('21c877fef6321481f7acd63aa4fd9db4', '210.232.15.29', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1479564771, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('21e9c7d534a77b03ee2586d2f5e594cb', '66.249.65.177', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479318137, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('21f9c7071436698f149c93a9e4f2de57', '117.214.160.241', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479273380, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('22296c8962e9da79301b1044f06aa782', '5.189.146.46', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1479625862, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('228528bdee3e1b632a471eb5cdfdf99d', '52.71.64.37', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) App', 1480116397, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('22a018968af9aeca30dea44a910c934b', '66.249.65.70', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482213526, NULL),
('22a9443e5edfc428178ea8c856889a2c', '189.177.212.195', 'Mozilla/5.0 (Linux; Android 4.4.2; LG-D331 Build/K', 1478786040, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('22b46ca8459914c7d8e557e906463364', '157.55.39.213', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481809521, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('22cd306274c2f43f38bc3afd9a7b5aa5', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239576, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2315451ffc585c4224f366ad0ea2c57d', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612390, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('240f92c2b8ba1d5fbe6422943a9bc483', '189.187.203.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481661012, 'a:3:{s:11:"captchaWord";s:3:"641";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('24182b1da342b2cb97c1baa2d7252afc', '66.249.65.66', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482025083, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('245533856fe2a78edb461dbb3111816d', '189.187.102.113', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481308700, 'a:3:{s:11:"captchaWord";s:3:"337";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('247c9389aa12c97b67debbe1046fcdf5', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651270, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('248215c8d9e97f0d92bdbe9ae39030b1', '66.249.91.124', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1480039961, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2498b060ab8a5747421a4bcc09c7233d', '189.187.203.112', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA', 1481673370, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('249c2a171a0975cab8b5a44ab9d0b08f', '66.249.65.174', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480113015, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('25083e32ab3fa7331c9f9776b7c5b53e', '66.102.7.150', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1480378818, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2519b05172912fbac8e198265825e1ce', '187.144.112.193', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481862786, 'a:3:{s:11:"captchaWord";s:3:"575";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2579ac24f0065f72188961aa0c6e3a8c', '189.187.173.20', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481657201, 'a:3:{s:11:"captchaWord";s:3:"159";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('260f49dfdd3d696308558137768f26b3', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481219667, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('26378ab14608eb6d72fd966a2170d537', '104.193.88.245', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1479447048, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('271a8225823338318cf19fa2e5b9d79e', '199.59.150.181', 'Twitterbot/1.0', 1481914674, 'a:3:{s:11:"captchaWord";s:3:"249";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2726761f837ef6d5380b7571d5146190', '207.46.13.28', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481924683, 'a:3:{s:11:"captchaWord";s:3:"222";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('272ed8f4745ac546cec5f3ebf1deeff3', '66.249.65.196', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481117161, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('276dfae8f52b661b6bbc0f2937cb9af2', '66.249.90.74', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1480581575, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2775ede87ad301782ad3fb3dde8401b7', '189.187.108.161', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481071859, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('279589ba804012816e1582926cf245d9', '66.249.66.118', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480318570, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('283f198cbfba01e93495034869adf9b3', '52.4.244.57', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) App', 1480116397, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('28c47fa65d27f480946220a9f94af62b', '189.166.52.5', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480609259, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('28e24850d8c4609f1cea033a36767446', '54.197.104.219', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1482231631, 'a:3:{s:11:"captchaWord";s:3:"494";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('29057518f14a2331bca11c575d53543d', '54.221.114.39', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:29', 1482033801, 'a:3:{s:11:"captchaWord";s:3:"220";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('294c0266c538500812c0b20e28dd7303', '54.92.252.123', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1480385626, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2968b2f8c55503222ebfcb17f6c56857', '189.166.37.53', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480086458, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('29703a1e174c9d97ba64e37c92e0d3c4', '66.249.65.156', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479469747, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('297e435bff48cabe464699be68763c5f', '72.14.199.152', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1481754789, 'a:3:{s:11:"captchaWord";s:3:"134";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('297eb023303d3e4f758930883005de06', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612351, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('29d0f388bf913c2c450a183ab425c4f1', '189.177.239.107', 'WhatsApp/2.16.225 A', 1476927557, NULL),
('2aa154b0d15f1b889cd074a3183c2238', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480376175, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2aa2d024685543d2746c1a51097683e5', '66.102.6.148', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1477095351, 'a:3:{s:11:"captchaWord";s:3:"334";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('2ab137b75c3eb16125d821dc0458c5f3', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651318, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2ac501e8a3e7144a1da9f255737362b9', '189.177.239.107', 'WhatsApp/2.16.225 A', 1476919191, NULL),
('2acef576030c8ce581248602e5b9f1a9', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481219652, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2add52e19f553abc16f0684ce1fe80db', '52.71.155.178', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) Ap', 1480649634, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2b0e70935ecb995ee4450145a58aedb5', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481314528, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2b389fd520109fb5582c68fa497fddae', '207.248.61.226', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebK', 1479996387, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2b9830a0d53e99b129826179e26baca3', '108.171.133.170', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; r', 1481297162, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2b9d8ac9cac8b33b2f86b2d8e2147307', '192.154.111.82', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1481059356, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2bc3e636762fcb8d72c8f3d2110fac18', '66.249.69.247', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481360492, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2c45ade339c757b6270f3442f2d19933', '104.193.88.244', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1479546088, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2c58a85d34203692bc7c7a08cc8dda63', '207.46.13.193', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480236405, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2c8d9d624c6ce6eccf60e0395571af65', '66.249.69.239', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480062364, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2cc169497d41347fde397817b5e5253a', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482055363, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2d15694ae5f8f58f290b479e7da99aea', '189.166.37.53', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480086457, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2d7316bc7efbdc9daf10adeac01cb541', '66.249.83.74', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0_2 like Mac', 1480818118, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2d8f012863a38a6b34f439fd0421fb96', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479701713, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2d971b65fbc7749645b878ab5b2c5aa5', '201.151.45.198', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479398910, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2db2b4610d42008636ab0cda60397c9d', '189.177.226.244', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1481570928, 'a:3:{s:11:"captchaWord";s:3:"117";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2de035fcdbfab6b24cfc172a501689ca', '66.249.65.57', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482018719, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2df6396709e3d7498eceddb8359752a7', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481415176, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2e2f40b48e64d02d307c4a4d52829ca5', '187.134.86.211', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1479610878, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2e305189b0a38b05310ecfd2c880a3f2', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612416, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2e3b85cc3e07ef68ddab8965e8ee7669', '66.249.90.128', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1480709964, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2e4089d613e5d6a9d585a916fc523d84', '66.249.65.66', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482044623, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2e41eb5286c315a86fef5a93209a255a', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480377369, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2e76e342ada169ab8973253e72bea256', '157.55.39.107', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481809534, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2e8e20d2d4facd190bc9795ed63f427d', '66.102.6.118', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1480117402, 'a:3:{s:11:"captchaWord";s:3:"319";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2f0aae9f4a651e3fcce3e0fc6b2924b2', '189.177.239.107', 'WhatsApp/2.16.225 A', 1477156531, 'a:2:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('2f0e321a96ab9cb2c9540bfa8676b20a', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482073982, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2f0e97ef95aa7aafc3f1c5d5e80536b5', '66.249.80.18', 'Mozilla/5.0 (Linux; Android 4.4.2; LG-D331 Build/K', 1476627312, 'a:11:{s:11:"captchaWord";s:3:"861";s:4:"cart";a:1:{i:1;a:11:{s:7:"item_id";s:1:"8";s:4:"line";i:1;s:4:"name";s:22:"Adulto Puerto Vallarta";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:6:"800.00";}}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";s:8:"customer";s:2:"-1";s:9:"sale_mode";s:4:"sale";s:8:"payments";a:0:{}s:9:"data_save";a:3:{i:1;a:11:{s:7:"item_id";s:1:"8";s:4:"line";i:1;s:4:"name";s:22:"Adulto Puerto Vallarta";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:6:"800.00";}i:-1;a:11:{s:7:"item_id";s:1:"4";s:4:"line";i:-1;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:4:"1.00";s:8:"discount";i:0;s:5:"price";s:7:"1000.00";}i:-2;a:11:{s:7:"item_id";s:1:"5";s:4:"line";i:-2;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:4:"2.00";s:8:"discount";i:0;s:5:"price";s:8:"12000.00";}}s:4:"kits";a:1:{i:1;a:11:{s:7:"item_id";s:9:"Tailandia";s:4:"line";i:1;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:8:"15300.00";}}s:6:"cart_p";a:1:{i:1;a:11:{s:7:"item_id";s:9:"Tailandia";s:4:"line";i:1;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:8:"15300.00";}}}'),
('2f1580df06ccae88574747a2322b3fab', '66.249.65.171', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479708155, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2f30168f3beef89dbde36a8893c07571', '87.206.241.37', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480367674, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2fa095a0dfccaa1aa158bccf7c0e8145', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239396, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('2fa9dfa6f23d1006d904f5e76b987def', '66.249.85.134', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1477416996, 'a:3:{s:11:"captchaWord";s:3:"568";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('2fad55a198327ffb29f7ad4b2b3f3c51', '66.249.65.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482173879, 'a:3:{s:11:"captchaWord";s:3:"544";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}');
INSERT INTO `ospos_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('2fb531ccce68869db9d0896ee6dc1c8b', '187.144.112.193', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1482185026, 'a:3:{s:11:"captchaWord";s:3:"595";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('30055432c7df0cf2df69e720962eeba6', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480183740, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('300d05aed6a5e3112c6eb5ad8cedc454', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651326, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3024c9798aa71223741a0c784cf0a6d8', '189.166.35.200', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1479781160, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('30422a4bd9367d44e8aa4f08c5b0383a', '177.231.81.47', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_2 like Mac O', 1479422853, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('304c94ae59a62df9d353425dfb6861d2', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481740302, 'a:3:{s:11:"captchaWord";s:3:"406";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3071f4e04702318bc8700f2c8b541009', '157.55.39.66', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480054800, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3082a225ce905bcdf525eccb2d6dc898', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480261489, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('30e55a60e63df91c277ebe35e30d8249', '189.216.199.250', 'Mozilla/5.0 (Linux; Android 6.0.1; SAMSUNG SM-G925', 1481717315, 'a:3:{s:11:"captchaWord";s:2:"38";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('310e4647ce52080896b2a3be8783531e', '189.187.104.103', 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/53', 1480025670, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('312b3536a75c1915f8dbaab249dbdafd', '200.68.128.76', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_2_1 like Mac ', 1480654723, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('314fc32711fb1b521aa0ad9b16ab6682', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481920465, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('317b75de55e8dee7942269cbd2ec17d0', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481937677, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3181c60bcfcfe8391cb57d2c0d9f591e', '66.249.65.70', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482028743, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('31878e3aad41b4b9d489908ef3337729', '72.30.14.94', 'Mozilla/5.0 (compatible; Yahoo! Slurp; http://help', 1481914800, 'a:3:{s:11:"captchaWord";s:3:"803";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('31bd60c3eab7adf5f49df6ebcd0737e9', '195.53.106.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1479911766, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3258e92bcf2813fbeac313ce96b9fd29', '187.189.68.128', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1480284355, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('32716fba64d8241cf18363e2fee0ed63', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651244, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3295bc8162760cc4243b63647f809493', '189.205.241.74', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; r', 1480705026, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('32bd176644830089e12b977f656eb238', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480239552, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('332d32011aebd63e16037efba0236d93', '204.79.180.209', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0;', 1482232047, 'a:3:{s:11:"captchaWord";s:3:"824";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3345d05dea8a4302116ee79e0427072c', '189.187.203.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481672061, 'a:3:{s:11:"captchaWord";s:3:"710";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3356b8e123c50cc8797bf4fabd158cbf', '66.102.6.146', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478188881, NULL),
('3374f54203a3927f1f0a6e53907e8825', '54.204.138.179', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1480437928, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3375fef9bf05085994a1404964531575', '66.249.90.76', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1479761512, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('33b567389f0171110b2a815c9da89c94', '66.249.65.156', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480103876, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('33d53905439f37f4b5628d4de1a53c05', '189.177.239.107', 'WhatsApp/2.16.225 A', 1477156534, 'a:2:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('33e13136435b77156da83ce565878adb', '66.249.65.56', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482137202, 'a:3:{s:11:"captchaWord";s:3:"164";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3405cd75340e2e37920fba9c9464d96c', '187.193.8.233', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480444377, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('34340b8c0866c70f3f1e15bfe17e1553', '66.249.65.152', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480228782, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3434b9f047567cadb7787e6ec0d5661d', '187.144.134.51', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480885705, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('344c11431d4d8dbf414b350277ec6bf3', '148.235.92.34', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1479419338, 'a:3:{s:11:"captchaWord";s:3:"206";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('34688680fa1e10291787903428b19456', '189.187.219.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479934249, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3476ae17ffe7da4ac4d69395e40b5470', '187.134.86.211', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1479610938, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('34dd9db8beec501b46ae3a2fa1004e87', '187.144.134.51', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480883259, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('34f83e10af2516c68ccca7aaab7223bc', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481990865, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3506e8f30ac5ab835bdd7a565ca3de04', '66.249.65.70', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482033293, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('350c7c5c06cbb0dadcd331efff4fc65d', '187.144.112.193', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1482210338, 'a:3:{s:11:"captchaWord";s:3:"374";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('35af536cf1c4219e0bd98caf5ec52a16', '189.187.203.112', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA', 1481673572, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('35b866dbb7d95463287912b3d7cefe0d', '189.187.203.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481661946, 'a:3:{s:11:"captchaWord";s:3:"149";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('35d5ff3aea8f8f7622a8c1e52d8c4e10', '189.177.239.107', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1477759124, 'a:8:{s:11:"captchaWord";s:3:"0n6";s:9:"data_save";a:2:{i:-1;a:11:{s:7:"item_id";s:2:"10";s:4:"line";i:-1;s:4:"name";s:16:"Esencia Europea ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:4:"2.00";s:8:"discount";i:0;s:5:"price";s:7:"2735.00";}i:-2;a:11:{s:7:"item_id";s:2:"11";s:4:"line";i:-2;s:4:"name";s:16:"Esencia Europea ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:4:"2.00";s:8:"discount";i:0;s:5:"price";s:6:"600.00";}}s:4:"kits";a:1:{i:1;a:11:{s:7:"item_id";s:16:"Esencia Europea ";s:4:"line";i:1;s:4:"name";s:16:"Esencia Europea ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:7:"2735.00";}}s:8:"customer";s:2:"-1";s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";s:6:"cart_p";a:1:{i:1;a:11:{s:7:"item_id";s:12:"¡PROMOCION!";s:4:"line";i:1;s:4:"name";s:12:"¡PROMOCION!";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:7:"1699.00";}}s:4:"tots";s:1:"0";}'),
('35ee683f75f731fa7a466d7eebe21199', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651259, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('35f3763da5bfa66645589f219bab64ba', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612405, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('360839a3e37cc25eb105625cb99981eb', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481746474, 'a:3:{s:11:"captchaWord";s:3:"408";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('361f5dec9fd1f8982a0f1e924dfb8670', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482224063, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('363fcc93200f0cbc6bdceae8237373d2', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481920284, 'a:3:{s:11:"captchaWord";s:3:"102";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('365957622f6fed25b3a754fe19078c1c', '66.249.90.74', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1480581506, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('365f18506b1fe30ff85d63035663760b', '189.177.226.244', 'WhatsApp/2.16.225 A', 1481590498, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('368457596a2fc6b2b55c8016118383b3', '66.249.79.102', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481739664, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('36a7c83f7b80decd94dfcae29c8fc41e', '66.249.90.86', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1479742890, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('36ae258992028370bf160cffd99c7ea1', '66.249.64.184', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1481866467, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('36b73642e798f951391cdc5cb8cb339f', '189.187.231.162', 'Mozilla/5.0 (Linux; Android 6.0.1; D6603 Build/23.', 1479409699, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('36d9ed11cad8ea7881d520dc51251c64', '157.55.39.1', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480436559, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('371e9c80541c7a73ef00b4cf68b4af69', '189.166.35.200', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1479781159, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('37363d8349fb6c7997d41398deb18b70', '52.213.197.0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) Ap', 1481914690, 'a:3:{s:11:"captchaWord";s:3:"861";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3738af4c87bdbba7cd155701a52f656f', '189.187.203.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481673382, 'a:3:{s:11:"captchaWord";s:3:"883";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('37396ed603719561778dfff54283ad3a', '213.136.78.176', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1481967796, 'a:3:{s:11:"captchaWord";s:3:"157";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('374eb37a90fbb264d749d65fd2d217e2', '66.102.6.148', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478031095, NULL),
('3752fdc2df186879ce7bc7e2bf0b514c', '50.18.94.121', 'Google-HTTP-Java-Client/1.17.0-rc (gzip)', 1481914675, 'a:3:{s:11:"captchaWord";s:3:"666";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('377af2b03a4215787ea25cc2f505ffa9', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612330, 'a:3:{s:11:"captchaWord";s:3:"D9u";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3797f2990f2e852b9751d9bc69dd3b27', '40.77.167.64', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481414831, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('37bae6478aaa9d5ce4dcb0fd2fcc8c66', '187.144.134.51', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480885705, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('38177893e05acf745c13aa79457dbaf1', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480530921, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3829d05b31bf69cf3cf54b2a83ab0109', '54.204.138.179', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1480411539, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('38333c99c57f323c1945c6a908ff14e9', '157.55.39.243', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1479357894, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('38410759c89b3867a6a9be8d58c1be4b', '66.249.65.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481414901, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('38f60b958aaef9d1171763fb4e3f9228', '187.144.134.51', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480885260, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('38f8d2fc413e95da68e01062082f2d9f', '66.102.6.24', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_1 like Mac ', 1479603050, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('38fe1b897013f824e5bff60fdb4ec60b', '66.249.90.128', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1481153342, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3927a2717189c7613ec2a96c0f9f54ec', '66.249.65.192', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480896875, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3955f9e3ee9a28f7a7b1592264fc8f4b', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481314525, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('396d47f6de2d94eaec56144490600526', '187.193.49.93', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1481396248, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3971730c39c5d35dc4ba95edd7d72f79', '189.177.112.57', 'WhatsApp/2.16.225 A', 1477174793, NULL),
('3972d136650dfd6030d855825b140e4a', '66.102.6.150', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478031096, NULL),
('39a4d7fac5fb7704cf5a260e408c2bfc', '187.193.19.171', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebK', 1482193451, 'a:3:{s:11:"captchaWord";s:3:"287";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('39e14908921625ba90a4e2c1b7a22625', '189.177.233.214', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1477004044, 'a:6:{s:4:"cart";a:0:{}s:11:"captchaWord";s:3:"VOb";s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";s:9:"data_save";a:4:{i:-1;a:11:{s:7:"item_id";s:1:"4";s:4:"line";i:-1;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:4:"1.00";s:8:"discount";i:0;s:5:"price";s:7:"1000.00";}i:-2;a:11:{s:7:"item_id";s:1:"5";s:4:"line";i:-2;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:4:"2.00";s:8:"discount";i:0;s:5:"price";s:8:"12000.00";}i:-3;a:11:{s:7:"item_id";s:2:"10";s:4:"line";i:-3;s:4:"name";s:16:"Esencia Europea ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:4:"2.00";s:8:"discount";i:0;s:5:"price";s:7:"2735.00";}i:-4;a:11:{s:7:"item_id";s:2:"11";s:4:"line";i:-4;s:4:"name";s:16:"Esencia Europea ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:4:"2.00";s:8:"discount";i:0;s:5:"price";s:6:"600.00";}}s:4:"kits";a:2:{i:1;a:11:{s:7:"item_id";s:9:"Tailandia";s:4:"line";i:1;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:8:"15300.00";}i:2;a:11:{s:7:"item_id";s:16:"Esencia Europea ";s:4:"line";i:2;s:4:"name";s:16:"Esencia Europea ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:7:"2735.00";}}}'),
('3a121e2440fdbea976c273593407d369', '66.102.6.146', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1477606913, 'a:3:{s:11:"captchaWord";s:3:"766";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3a972a495f5b0132bd6807468540ee58', '157.55.39.213', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481809521, NULL),
('3abd4eb2d543df01e041d88253b998f5', '66.249.85.134', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1477756165, 'a:3:{s:11:"captchaWord";s:3:"143";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3b0fcd4b92891de41e6d04e130a9ddc3', '66.249.65.66', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482107130, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3b34527774eeae0fe55607912d9bafa2', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612356, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3b56336c390b35910ed342fe3afd60a9', '66.249.65.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481414526, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3b8ff8a528b6c97bd4a0d8520bc19ff2', '66.249.90.132', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1481315582, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3b91521310fd3274bab373c853ca5890', '157.55.39.221', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480705603, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3b92c600fa091b3b460ea132528da60d', '66.249.65.152', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480606360, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3ba1dbd1e50a772206af8ea5763eeb0d', '66.102.7.152', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478793529, NULL),
('3ba54f8ba44848e14bb319eaf20722b5', '66.249.90.132', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1480983891, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3bba988633d535dfd5345920afa4bff5', '187.144.134.51', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480885280, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3be56eefacca86db5cc7eb5393cfbf24', '198.240.85.161', '=Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) A', 1482094524, 'a:3:{s:11:"captchaWord";s:3:"918";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3c0edd93ebd7b6723baeec13d62603c5', '66.249.69.243', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479960461, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3c33c0ecf604445413d7cf72095929fc', '66.249.79.116', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479887360, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3c6540995f292ce0ddc4788a9ecf91b6', '66.249.90.74', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1480515323, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3c9b85aae57028660a1fe6516df6c206', '187.210.230.48', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1481390542, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3d245b0fbe018b32140f8b15bad881f0', '200.76.83.212', 'Mozilla/5.0 (Linux; Android 5.0.2; SM-G530H Build/', 1479406780, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3d465bb3a8d96d80bf5e2524fa287dc6', '188.244.47.93', 'Mozilla/4.0 (Windows NT 6.2) AppleWebKit/537.17 (K', 1480721618, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3d5cb722320249a56374be06e9d08aea', '66.249.85.138', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1477520966, 'a:3:{s:11:"captchaWord";s:3:"639";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3d6ab2e52ff600d7e10e9c2ebc6df05f', '66.249.69.239', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479932141, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3d9a381f8109f02c02cd495b8ed52bad', '208.90.57.196', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:31.0) Gecko', 1480504014, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3d9de1152657eaefb44fe7a9897bcec0', '66.102.7.150', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479947254, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3dde63dab7f1beede701bfbb22a50a69', '187.144.112.193', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481929516, 'a:3:{s:11:"captchaWord";s:3:"624";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3e3e3bd55d09e47e8ed92970aa6215b3', '168.195.181.34', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480193573, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3edfb0c47fb678ebc32bf6eacdd73d2d', '66.249.65.70', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482026904, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3f216530cffa7bcb30ea94daf17bb27e', '66.249.90.118', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1480729824, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3fbd68d765970dfd006ec5b5b8e4ceb3', '66.102.7.182', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478212317, 'a:3:{s:11:"captchaWord";s:3:"463";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3fc35f2f53f4714f19283816dc97f516', '66.249.90.57', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1481315722, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3fcc36bdddc38ed66c32aa4c2e5dd24e', '66.249.65.156', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479291676, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('3fd0f45cef18fd7aa4f04e812a3d33cb', '199.59.148.211', 'Twitterbot/1.0', 1479401423, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4002ba730ccd0115c4104c0deccb0a53', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481415138, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('402e4c38c88056087fc48b6fbb639377', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481745690, 'a:3:{s:11:"captchaWord";s:2:"63";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('40688ae8c11208f2f857aa9f78c2f562', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481752200, 'a:3:{s:11:"captchaWord";s:3:"685";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('407029c0847ce1ebd63c567156a06908', '66.249.90.154', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1479245387, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('407e61d480f511d6be3cc3dff1d39b39', '66.249.65.56', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482015354, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4084f9692ea9a6d2b1a4718342426d17', '157.55.39.142', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS', 1480183979, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('40c2cf833517c51c518690a374b52ea5', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479864240, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('40e65457e9a5f94c25d0cbe555709645', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481415197, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('40f833a289c6eaa6d34462a921c6afa5', '189.177.239.107', 'WhatsApp/2.16.225 A', 1477156523, 'a:2:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('40fcea28ab6af72216a10b883a26c11b', '66.249.64.176', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1481903632, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('40ff4868a037c0efd22a5dbd503e6e4d', '187.193.8.85', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479411119, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('41886e767f4116853ff2d38923598f37', '66.102.7.194', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1481911119, 'a:3:{s:11:"captchaWord";s:3:"258";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('419d6629a752b7d111f2fb504e1ffcc7', '66.249.65.56', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481999140, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('41f3d7c9bdf37f323c9b1cb965277f27', '173.252.90.166', 'facebookexternalhit/1.1', 1479846628, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('42003854887516a3ff5ef808945c031d', '157.55.39.252', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS', 1481579891, 'a:3:{s:11:"captchaWord";s:3:"516";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('425ec65baeb0b2e6dfaac23948db5c31', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480152476, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4277594417a27e929b5bece4074c6fda', '189.187.182.216', 'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; r', 1479696764, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('42c615e19ba7c90246e93c40496cd7d2', '66.102.7.154', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478710098, NULL),
('42d0758fa0994baae8bd9835b2b40d65', '189.177.239.107', 'WhatsApp/2.16.225 A', 1476920878, 'a:2:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('436b1b2400aa4a77924a7869cc0b0290', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480182851, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('438249802cae30f86e6565b7410aa961', '50.18.94.121', 'Google-HTTP-Java-Client/1.17.0-rc (gzip)', 1481914675, 'a:3:{s:11:"captchaWord";s:3:"666";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('43d59b08945608e1d93ed010b77ccd97', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480114230, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4432b71cbb9544edc0371da727ed5acf', '66.249.65.192', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480781718, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('445cb62dfd5c787313338bc0d86a8173', '66.249.69.239', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481360788, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('44b0e99d345dfaf1014d1fcbbff8a3b1', '207.46.13.30', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480945790, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4507cdfd4d97f1c1e3c5f74e82f7eb53', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651328, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4564476f041d7f6e84d2cf9c456ba9be', '54.172.64.175', 'python-requests/2.9.1', 1481916569, 'a:3:{s:11:"captchaWord";s:3:"528";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('457c836071407c41e755f6e77dac59a8', '66.249.90.74', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1479418999, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('45b9c80be3dd6d8258a3e4022800687e', '66.249.90.56', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1482186201, 'a:3:{s:11:"captchaWord";s:3:"960";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('46060412a4a5c2240304dd36e79f4257', '210.163.39.77', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1479528707, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('462af7280d3f329a52bd1811bd06e98c', '187.144.134.51', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480885259, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4632ccec8758c17442e52e682c422aa4', '200.188.143.130', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1479852010, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4665887326ea0a60fb7226f6384bea09', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481518244, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('466d8370ae624f28c039896d49c0f394', '207.46.13.193', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480157173, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4683aeba292c6d8f107b403843a055bb', '66.249.65.106', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480951868, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('469961d1f590ed7a6954d9497839b94c', '66.249.90.57', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1482092712, 'a:3:{s:11:"captchaWord";s:3:"927";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('46c45ebb75dd4ceb4ed80d6c25836209', '52.70.48.63', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) App', 1481828508, 'a:3:{s:11:"captchaWord";s:3:"667";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('46df056c9f887753ac653b150545737c', '66.249.90.128', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1480894824, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('46fd884e6e2749e16552ea9bbc7527b8', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480188157, 'a:5:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"2";s:8:"position";s:6:"inside";}'),
('4740e8d882defc998885f5092370aed6', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481314529, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('477865ffe9ce88b7c443500018843e0a', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651288, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('478ccc21836d392fd62525cb6500d782', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479291567, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('479868a63184196eab8839f986a15712', '177.248.151.124', 'Mozilla/5.0 (Linux; Android 5.1; XT1032 Build/LPBS', 1481767498, 'a:3:{s:11:"captchaWord";s:3:"770";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('479a8866ce5c74bb295a371fda4995a1', '66.249.65.156', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480310906, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('47aa995168a7c9c4892c2739c69ee245', '66.249.69.239', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481360575, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('47bc07c0de5d08527c01a3e6f994ec52', '157.55.39.155', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481038324, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('47da6bd5509a7152235f5d6409a51945', '66.102.8.156', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; r', 1481386822, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('48147e05caa1d267b18e82b9a952b977', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612396, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4855043fe857bb1814dec0ed9a897729', '149.202.72.198', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:50.0) G', 1481542248, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('485e68b50d6cbb624bbbc1a3871fc547', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481222823, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('48754fb6e82ef57ae12ff40573874b7f', '187.144.134.51', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480883258, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4905fe224d163c8376f0cb3b84eb4162', '66.249.65.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481952286, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('49561f4792a2eb2612a2827d2868ecb9', '52.70.48.63', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) App', 1480471789, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4957fc18e1a2f02ac354a616a0091d07', '189.187.35.43', 'Mozilla/5.0 (iPad; CPU OS 9_3_5 like Mac OS X) App', 1479683409, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('495d3c4e2a6fc4bdc025fee2136c632b', '66.249.65.177', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479301576, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('498fe5e8be366a3e247605b8e911ad63', '187.193.8.233', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480444185, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('49bef532097e2bdb91c36309d1e023f8', '66.249.88.62', 'Mozilla/5.0 (Linux; Android 5.0.2; LG-D693n Build/', 1480642159, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('49ce72425569717c450efad688818336', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481222823, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('49ed6dabbcebe3ff5c1245448916a6db', '54.197.104.219', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1482216536, 'a:3:{s:11:"captchaWord";s:3:"611";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('49eebe7bef3d4a18c3427bd793e61b84', '66.249.85.185', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1481731300, 'a:3:{s:11:"captchaWord";s:3:"400";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('49f5886e6633480c3f451fbe1d0c6a7a', '66.249.73.166', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481618941, 'a:3:{s:11:"captchaWord";s:3:"124";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4a116fe840593b5653d30d5ed1269d52', '66.249.85.134', 'Mozilla/5.0 (Linux; Android 5.1.1; SM-J111M Build/', 1477417041, 'a:4:{s:11:"captchaWord";s:3:"C8B";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";s:6:"cart_p";a:0:{}}'),
('4a36fc7b4d0dfef8c3f9035c05e64114', '189.166.35.200', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1479781203, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4a3bb706099ce12529b74d8f9458a47b', '189.187.200.52', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480099846, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4a544e10387988fe7f958f51bd7c1c15', '187.193.31.93', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-G935F Build/', 1480114450, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4a5b31381c2b35ee58e2480cbc229edc', '66.249.90.76', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1479761529, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4a5d5041a2c8a925642c8d12dd0cb425', '66.249.65.174', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479352073, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4a62e23ba8a66ec58d3d9c0bf2027f72', '187.190.167.100', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1481142014, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4a7bc46e421ddefab9cce83a9ca74472', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651286, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4a8030e8e6f2eee597a21f2b563eecce', '187.144.120.30', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1479253546, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4a94a227b8ba4cd9096fb33ae544dca4', '187.144.112.193', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1482184987, 'a:3:{s:11:"captchaWord";s:3:"482";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4aae31e12543ddfb7e504416f11835bd', '207.46.13.176', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1479448806, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4abed588d85b4d8311f65567e2c20445', '187.140.26.138', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1481900472, 'a:3:{s:11:"captchaWord";s:3:"242";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4acfdcb080b5a58c3e29e454e3837a96', '187.144.112.193', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1482123914, 'a:3:{s:11:"captchaWord";s:3:"713";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4ae06785a44eaf4ec5a7217d05fa232d', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239576, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4b16aadfb6b1e27b7b31d8d1383fb34b', '66.249.65.196', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481650743, 'a:3:{s:11:"captchaWord";s:3:"261";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4b314a9d5a4695c7b2624d5c2b31f0f3', '187.193.8.233', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480443056, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4b643b28dd132e16f87fa4fed6279de1', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480110964, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4b76e054737fedad7a3218ed3bf84eb3', '189.187.107.227', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1479563287, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4b8170be3c6195c36f5007d3c93304bf', '66.249.79.110', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481736791, NULL),
('4ba0896dd2bfe98deef5bb0ce6938ead', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612335, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4ba18e80364395bd7009c18bb96e8156', '157.55.39.29', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS', 1481798999, 'a:3:{s:11:"captchaWord";s:3:"719";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4ba9d20d526c957c4f5de0a283b190e6', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480378839, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4bc26954a5adc787924c2a428f59e0cd', '189.187.156.194', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480787436, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4bdda53fe791a716a9000fdcc31b026c', '207.46.13.28', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481958060, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4c116ac1b927a1366aa8005ed1f22541', '69.171.228.122', 'facebookexternalhit/1.1', 1479237901, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4c16f966a5c97be835f567f57d1cceb2', '187.144.112.193', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481929499, 'a:3:{s:11:"captchaWord";s:3:"603";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4c1aaf7e8c3c86d9b9f231b2e2269180', '66.249.79.102', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481740772, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4c47eb819734fb97062939b57fc4d2bf', '62.210.105.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479247765, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4c5b911be9d17bf043773d5a6e12d134', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239577, NULL),
('4c70247558fea2dad9ea0194574b75fd', '198.240.81.11', '=Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) A', 1479994716, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4c7c773af17b850f7b508f62a81db3db', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482161443, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4cae63b8e6f5599ee61b78ca84db961c', '66.249.65.156', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480074176, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4cb2e775942dc2ae57905f556517d2d0', '66.249.65.196', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481696871, 'a:3:{s:11:"captchaWord";s:3:"621";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4cce0f0c9b418221e3f9c38c804707e4', '66.249.65.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482140738, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4cd979bdf750a9ba41dc8b3a4d7a4d75', '189.187.173.20', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481648680, 'a:3:{s:11:"captchaWord";s:3:"584";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4ce0ce454969a0aa37dd2e0669fcac96', '66.249.65.196', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480678304, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4ce1dfbc3da37b68babcb68ead105cd9', '157.55.39.195', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480436545, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4ceefa8a673cad5487ce645fae87114b', '66.249.65.57', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482146322, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4d1921613712d854f5635c70a1765b9f', '66.102.6.118', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479141985, NULL),
('4d2fbf92ad1e2f8594989ab1e49f6c9f', '211.0.145.106', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1481361295, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4d45cee3e36ce14561cde258b36c17ed', '189.187.173.20', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481657116, 'a:3:{s:11:"captchaWord";s:3:"706";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4d671974e28ed5d0acd2fd3aeffbb7ee', '184.88.28.14', 'Mail/3251 CFNetwork/807.1.3 Darwin/16.1.0 (x86_64)', 1479922641, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4e3b9c0210f7e62f1fecc93522326fbe', '184.88.28.14', 'Mail/3251 CFNetwork/807.1.3 Darwin/16.1.0 (x86_64)', 1481052774, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4e608f17a68c6546cb5f1971a3b0859c', '66.249.65.125', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482024783, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4e80f743f5cbdf68403eaa5849b4d887', '66.102.6.198', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1480463002, 'a:3:{s:11:"captchaWord";s:3:"784";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4eca5059738cc1d130945f3532366ed6', '200.76.83.0', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_3_2 like Mac ', 1478376994, 'a:4:{s:11:"captchaWord";s:3:"301";s:6:"cart_p";a:1:{i:1;a:11:{s:7:"item_id";s:6:"Cancun";s:4:"line";i:1;s:4:"name";s:6:"Cancun";s:11:"item_number";s:0:"";s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:0:"";}}s:4:"tots";s:1:"0";s:4:"kits";a:1:{i:1;a:11:{s:7:"item_id";s:6:"Cancun";s:4:"line";i:1;s:4:"name";s:6:"Cancun";s:11:"item_number";s:0:"";s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:0:"";}}}'),
('4ed1ad4ba69f513f3fe4c4c8e2e0acc3', '157.55.39.80', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1479546441, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4f2986366d5f2f1da87a3ddf2a7d57d7', '187.193.8.233', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480441574, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4f2cf53e16573e3a549dbca45d15b914', '184.88.28.14', 'Mail/3251 CFNetwork/807.1.3 Darwin/16.1.0 (x86_64)', 1480709735, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4f5101dd2bbe2eb0ed49aeec6f5e9df5', '189.177.239.107', 'WhatsApp/2.16.225 A', 1478050908, 'a:3:{s:11:"captchaWord";s:3:"317";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4f7bd32c30a20590038c7b2232d85826', '66.249.80.18', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1481640703, 'a:3:{s:11:"captchaWord";s:3:"979";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4f8c19ab2fb94c28c34707b3b957f48b', '201.175.158.210', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1479231564, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4f99ca50d6915e7623d1f3f8d7246014', '66.249.65.57', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482175820, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4fc3a1177d74a57d78cfeaad9bf43169', '189.187.115.79', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479238131, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4fd20889f528e6488c35e815829d498a', '189.166.52.5', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480445349, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('4ff20ce72479a955355651d2bbea5d3d', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480990704, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5029a56ee598d7db0308501445767ccf', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481747557, 'a:3:{s:11:"captchaWord";s:3:"775";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('505044d6eb423e186c5ad5c3d87984c0', '54.162.255.176', 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv', 1479949588, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('506ccb19c6480cc77ddb9a75b57b2717', '66.249.90.57', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1482008028, 'a:3:{s:11:"captchaWord";s:2:"16";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('50d2b13353454c09039e2f0b6693e1ac', '66.249.90.57', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1482186142, 'a:3:{s:11:"captchaWord";s:2:"89";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5150dba2c80c0578b2bf4743cded0cf4', '157.55.39.29', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS', 1481957108, 'a:3:{s:11:"captchaWord";s:3:"707";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('516018b064e906323f0ade238cd96330', '66.249.65.102', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480797179, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('516b6ad9dd3574de0836794325aadd11', '207.46.13.30', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480945790, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5195165d1c0d946d346e07b1ebef50dd', '154.16.47.215', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480101378, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('51ad656efd40afdc624549797bd83f14', '187.144.112.193', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1482238037, 'a:3:{s:11:"captchaWord";s:3:"154";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('51daae25536075d9ff4d07ce565fc9c6', '189.177.233.214', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1476987543, 'a:2:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('51e6f0865b5d43b1cdf1bbceb508e16f', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651239, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5224e2fcef2b9842ecbbdc6449266508', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239577, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('52726799f643290f5785950c3525042e', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480953821, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5297224e7d8468ba2c891f98865ebc77', '174.135.100.180', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-G930V Build/', 1478050391, 'a:5:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"2";s:8:"position";s:6:"inside";}'),
('52ccc5d0a656bf33863d32a924c4ea27', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651280, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('52dbce55992542bf9eed8d14686810a8', '189.177.239.107', 'WhatsApp/2.16.225 A', 1477153865, NULL),
('53c9f156bccfa2a44cb0c2ef20739d9f', '187.193.46.44', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481564888, 'a:3:{s:11:"captchaWord";s:3:"vEX";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('53cbd284ef14a3b6653e3f523c50c899', '66.102.6.150', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1477428337, 'a:3:{s:11:"captchaWord";s:3:"347";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('53d5e7466514d549634bb1397676804b', '189.203.217.206', 'WhatsApp/2.16.225 A', 1477692029, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('53e35c02d187a2188aa1edf281f969e9', '181.167.136.43', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) Ap', 1480228481, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('53e7f9fd017d63a1ab8c5273c05d8b4f', '174.115.35.103', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) Ap', 1482253449, 'a:3:{s:11:"captchaWord";s:3:"476";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('541f0671441c57b542a08be9a5ea86f4', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651298, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5420fec64374a793dd9180de6d6760d5', '204.79.180.231', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0;', 1481322055, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5424898ad1bd60736d1fda9ad96e3180', '189.177.239.107', 'WhatsApp/2.16.225 A', 1476919190, NULL),
('54aac7bf24bf68f2aeaea5c589982926', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480953477, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('54c68fccf7bc0c4718081b78aadafc7a', '187.144.126.231', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1479840575, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('54d6fe1ff7ba8ae388a6575e695b2c61', '187.253.126.88', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1481080912, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('54f4d05e477e3f6951e663f366b05112', '189.187.201.81', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481136399, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5515615ac80f1f903d42c706d4ec1c7e', '189.187.113.206', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481660562, 'a:3:{s:11:"captchaWord";s:3:"253";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}');
INSERT INTO `ospos_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('5520d7ee6bd4961fe0f570c27221de33', '66.249.75.80', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481394531, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('553a79c319197908ad58d68d35ee5577', '189.187.115.79', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479232382, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('559fe2d45b22aa2ff3e69f4a8ecc7b47', '187.193.96.54', 'Mozilla/5.0 (Linux; Android 6.0.1; LG-H840 Build/M', 1480774828, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('55d118479b751e11450e1ea6fc6f80cf', '66.249.65.174', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479425390, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('55e5898cf8909bee0c1f86d42bdc9f2f', '66.249.73.174', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481619190, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('55efd1fa0c8503422c0b8e22c4ddb3cd', '66.249.66.85', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480346441, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5600ffb0d0a9de22d728d5f38a56a6c4', '207.46.13.61', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480064364, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('56023e64e12eaeb7572cea6e703e0b15', '189.187.159.133', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481317570, 'a:5:{s:11:"captchaWord";s:3:"744";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"2";s:8:"position";s:6:"inside";}'),
('56184da1631941345c6be20f95ba654c', '154.243.84.44', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480185381, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5639fa9f28d4cb70fc4dcc075b1273a2', '187.237.14.231', 'Mozilla/5.0 (Linux; Android 4.4.4; D6603 Build/23.', 1482083667, 'a:3:{s:11:"captchaWord";s:3:"153";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5650ea8334652ddef2ff44a1658b8f9b', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481601722, 'a:3:{s:11:"captchaWord";s:3:"878";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5679377ee9717662291eb4365cde8805', '189.187.203.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481677468, 'a:3:{s:11:"captchaWord";s:3:"123";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('56a70bc829d5055ccb497cd928cc8d4e', '189.187.108.161', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481136492, 'a:3:{s:11:"captchaWord";s:3:"396";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('56aaa538b027ab1a02c3f6dc369e3d95', '66.249.79.102', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481738063, NULL),
('56bb190f9e100fae61bec6a84305ef0c', '66.249.65.177', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479326856, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('56c3e70501e04eb1869b868437e1aee6', '66.249.65.174', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479436479, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('56dfefd544e077ce976e03ad23ff39a3', '91.227.68.136', 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1;', 1479832787, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5731f066d775175cee554fc4a5e57b69', '66.102.7.152', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479065663, NULL),
('575c3d055496b4906874cae992e3e00e', '207.46.13.193', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480173024, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5789323e3365e27a298f99d059fe0aec', '66.102.6.146', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478037889, NULL),
('5799510469d66befdd538dd3a4ee6167', '66.102.8.154', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; Trident/7', 1479562158, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('57b71e5d2559c060801863b35bf397f8', '187.144.112.193', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1482123914, 'a:3:{s:11:"captchaWord";s:3:"713";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('57d56e3657d6c8c816588b0489b6ae18', '66.249.65.106', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481117774, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5805e4fd305304a3644a66b7b9cb7c85', '199.16.157.183', 'Twitterbot/1.0', 1481567061, 'a:3:{s:11:"captchaWord";s:3:"758";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5832d90f18d5c009aaf25a272a78c9d8', '189.166.52.5', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480445334, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5874a16218b63685a7004f8cb6d37a42', '189.187.91.125', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1478023047, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('58a636740f9d30274ac298e8f809c486', '66.249.90.76', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1479864790, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('58d846571d9e6e8c5b2394b87f9bb317', '66.249.65.70', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1481962511, NULL),
('5927ef7a9f1829c92ba86c11cd4e01fd', '66.249.65.174', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479713916, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('598a3a90a2eb8474be16ec2b9ce29c1b', '66.249.65.174', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479474860, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('59a99963167f516b19557a8265b591fa', '189.187.18.68', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) Ap', 1481301088, 'a:3:{s:11:"captchaWord";s:3:"249";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('59dfc18dce0d7b4290bd9bd1115b0db6', '187.144.82.150', 'Mozilla/5.0 (iPad; CPU OS 10_1_1 like Mac OS X) Ap', 1479246676, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('59f6aad4486d185e67eccf1db01c68a7', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479291071, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('59fad9137c145bc75ca0fa538a4e6334', '189.187.178.167', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1481909623, 'a:3:{s:11:"captchaWord";s:3:"306";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5a4637c104612b4a41b80efd9b117e62', '189.163.157.253', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1481863856, 'a:3:{s:11:"captchaWord";s:3:"567";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5a5aace64adb9d341a53b2614fabb1f8', '187.237.14.238', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1480221317, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5a5d9515043a00723819dac51942a3d8', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480483170, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5a735bf7023d5371df208951efbbc1d7', '189.166.35.200', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1479848326, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5a80d15ab03a6dc77368a824ae69d2cd', '66.249.65.156', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479328277, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5b0556f4ad9fc1558872a93e9c360e0a', '66.102.6.202', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1480462999, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5b1621a21bfd8504e3fa5985d261aa2e', '157.55.39.155', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481183703, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5b1e54adfaa188a4c9882cb893f86c13', '198.204.240.42', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1481141118, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5b29516fa464dff253934deaf7e5bc3b', '66.249.69.239', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481387404, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5b4602eb626386cd073ca0c74d1b6c18', '189.187.161.210', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) Ap', 1478375677, 'a:5:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:2:{i:1;a:11:{s:7:"item_id";s:2:"12";s:4:"line";i:1;s:4:"name";s:13:"Adulto Canada";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:1:"2";s:8:"discount";i:0;s:5:"price";s:6:"999.00";}i:2;a:11:{s:7:"item_id";s:13:"Super Canada ";s:4:"line";i:2;s:4:"name";s:13:"Super Canada ";s:11:"item_number";N;s:11:"description";s:239:"Salidas Especiales\nEnero: 15, 29 \nFebrero: 12, 26\n Marzo: 12, 18 y 25\n Abril: 02, 08, 09, 10, 15, 16, 23, 30\nMayo: 14, 21, 28 \nJunio: 04, 11, 18, 25 \nJulio: 09, 15, 16, 21, 22, 23, 24, 26, 27\n Julio: 28, 29, 30, 31\n Agosto: 01, 02, 03, 04\n";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:6:"999.00";}}s:4:"tots";s:1:"0";s:9:"data_save";a:1:{i:1;a:11:{s:7:"item_id";s:2:"12";s:4:"line";i:1;s:4:"name";s:13:"Adulto Canada";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";d:1001;s:8:"discount";i:0;s:5:"price";s:6:"999.00";}}s:4:"kits";a:1:{i:2;a:11:{s:7:"item_id";s:13:"Super Canada ";s:4:"line";i:2;s:4:"name";s:13:"Super Canada ";s:11:"item_number";N;s:11:"description";s:239:"Salidas Especiales\nEnero: 15, 29 \nFebrero: 12, 26\n Marzo: 12, 18 y 25\n Abril: 02, 08, 09, 10, 15, 16, 23, 30\nMayo: 14, 21, 28 \nJunio: 04, 11, 18, 25 \nJulio: 09, 15, 16, 21, 22, 23, 24, 26, 27\n Julio: 28, 29, 30, 31\n Agosto: 01, 02, 03, 04\n";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:6:"999.00";}}}'),
('5b488157f4c6ec9225cbdfa4728f24e2', '189.166.52.5', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480609107, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5b4f7ce6516bd70959c0df62cfff9253', '66.249.90.130', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1480710041, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5b54178b3882ef74b5e8b1fc8e5d7dbd', '200.52.141.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1481862212, 'a:3:{s:11:"captchaWord";s:3:"674";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5b7aba9fa1be882fdfa19935c7b81b3f', '189.231.169.116', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480486536, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5bb5a6af18b30c287a42760a7ff93b6f', '189.187.108.161', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481072712, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5bba039e2a8ea568278e11978149ea6f', '187.144.112.193', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481929517, 'a:3:{s:11:"captchaWord";s:2:"30";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5c0ca0099ec00ef75de7e0ae552d8556', '189.177.239.107', 'WhatsApp/2.16.225 A', 1477156531, 'a:2:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('5c9f79e28c79630a7976a85dca98b1e0', '54.92.251.73', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1479773905, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5cabe64924c7f5acb13de2772b7ab769', '187.193.41.69', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebK', 1480453411, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5cdf1e25a1af54b5a13a810f6ed0c313', '66.102.7.150', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479862814, 'a:3:{s:11:"captchaWord";s:3:"196";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5cf6c60e7b9213b9485d6dee38d2efc0', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479291084, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5d1f302d0aa51dde1044ad059ccd111e', '200.76.83.227', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-G920I Build/', 1481606791, 'a:3:{s:11:"captchaWord";s:3:"755";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5d547f23eff1194d34f97e92d2a3e70c', '189.187.35.43', 'Mozilla/5.0 (iPad; CPU OS 9_3_5 like Mac OS X) App', 1479754172, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5d7c95352ed32551bce21a308a0be73e', '187.188.19.155', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Geck', 1480033188, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5da97c84678b6f4a399b2c9be32f961c', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612403, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5dd94968ff28e8b6f2f87e2675a445da', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481746004, 'a:3:{s:11:"captchaWord";s:3:"767";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5ddb744e8c57f459e5176e38dd1dbdc1', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612349, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5dfecedd95db23aad669f9da702d5f7e', '189.177.239.107', 'WhatsApp/2.16.225 A', 1476927555, NULL),
('5e01b8ee92b2cd652599de4461f593a0', '187.189.155.210', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481079491, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5e04877f192c1787aa840a9ea41153a1', '66.249.90.55', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1481402953, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5e46e1168b240036dbcf2428f4f23f70', '66.102.6.148', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1477095110, 'a:3:{s:11:"captchaWord";s:3:"934";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('5e48d50ad9d5f89632971047c78b4088', '189.177.184.117', 'WhatsApp/2.16.225 A', 1477402884, NULL),
('5e61675e9971fdcd4d0279880f14669b', '189.177.226.244', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1478890835, 'a:3:{s:11:"captchaWord";s:3:"554";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5e6acd6822c6f9151816c0d8307cc5bc', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612381, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5ebcf28e293635cf624386a5513c06ba', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481518257, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5ec191f1837270751ee9216c95cc55f5', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481239246, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5ece1a4a3db5de4c00887523706c114c', '66.249.64.184', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1481860021, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5ef3f668eb4c661b981d3de277bfcceb', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480380052, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5f118a20acd054f68111c043062c1640', '184.88.28.14', 'Mail/3251 CFNetwork/807.1.3 Darwin/16.1.0 (x86_64)', 1479316853, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5f15ee818dc0a2c820b650aea250991a', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651315, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5f19e89c43c2217d105a3d9a8199795c', '66.249.65.156', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479291008, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5f2de34cbd8080e1aad5d4ddfba73a1a', '201.175.134.5', 'Mozilla/5.0 (Linux; U; Android 2.3.6; es-us; XT621', 1479316564, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5f421cf778e528b56074d8d276581277', '157.55.39.115', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481231138, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5f85971a4b55924a6ff884a0cb506a3e', '187.144.120.30', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1479419774, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5f9d8ca630e14fdbc27a4dc28af1b411', '189.177.233.214', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1478021614, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5fafc0e097917a6d3a7a5f5912eb5fed', '189.166.37.53', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480086460, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('5fd5dfe5cbda7678855b386c36815d1d', '66.102.7.150', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479752570, 'a:3:{s:11:"captchaWord";s:2:"75";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('603b6d21d6bf75a54a89002f048bb993', '66.249.65.171', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479377679, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('604cfd0d5be2047cc406e2c17f1471b0', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480109264, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('60925e50b5a01f32cb1fd1da555fd9e7', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480990688, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('609b398c40ad42dec52674ff814ef343', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480262164, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('60b56efb2078beedc445c7cc8230ab42', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651291, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('60ee307ebdb321dea60f37e91be92cf4', '66.220.146.28', 'facebookexternalhit/1.1 (+http://www.facebook.com/', 1481562533, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6116091d50e31e1bb28d7e31727a9580', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479403905, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6148ee71f045e219cbd2f9fb2ff16535', '66.249.79.106', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481756288, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('61b5f4b59b747ef62a29a591b717e709', '189.187.96.176', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480999543, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('61c36e87845cdd825f809aba8220a54e', '66.249.90.76', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1480191597, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('61e1e00f6d955b035230b911fb6199f8', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239577, NULL),
('61f160652f7e996c8690487af027e74d', '66.249.65.196', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480727305, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('61f16350dbd3b91c6236cf6554538898', '100.36.54.111', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1479810656, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('62274432643c8fa00b252bf8995d9ef7', '66.249.90.132', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1481666973, 'a:3:{s:11:"captchaWord";s:3:"981";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('622eda3ad7c2f38a2e65f8672dd0e00c', '66.249.80.18', 'Mozilla/5.0 (Linux; Android 4.4.2; LG-D331 Build/K', 1476626675, 'a:4:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";}'),
('6244c2166c2f1ed4382a42a5a9f531e2', '66.249.80.18', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1476626860, 'a:2:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('624d71461ced192cfb3ab42a392728c7', '52.22.16.190', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:33.0) Gecko', 1481914688, 'a:3:{s:11:"captchaWord";s:3:"536";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6277b80f09a36fc81b051003e723b72f', '104.193.88.244', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1479789993, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6282715acb9f92c4d6b3f12e560f2e38', '66.249.88.61', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; r', 1480170031, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('62a73583b8fc8df53a94923c2f5ce664', '157.55.39.55', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481414836, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('62a9ed3864b6b7e95d615e2c127a65ee', '200.52.141.103', 'Mozilla/5.0 (iPad; CPU OS 9_3_5 like Mac OS X) App', 1481691289, 'a:3:{s:11:"captchaWord";s:3:"331";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('62f5e6a5e327d5f77f1b9147d9280287', '189.187.201.81', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481073895, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('631bbdd366c2ecda43a61e46dccb6bd3', '54.172.64.175', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:16.0.1', 1481916614, 'a:3:{s:11:"captchaWord";s:3:"117";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('632fd23aca7535f1a1c7c662dbe946f5', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481762503, 'a:3:{s:11:"captchaWord";s:3:"108";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('63699fc3bb8e6193b67805459ed57185', '66.249.69.239', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479954701, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6386a5cdc5f50f32eda0432ff89c385b', '138.197.46.52', 'Mozilla/5.0 (compatible; NetcraftSurveyAgent/1.0; ', 1481475057, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('639896220a0066ab92f23015388b4db0', '189.187.203.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481672356, 'a:3:{s:11:"captchaWord";s:3:"568";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('63aed6cce66b14557734c9e2aff442a7', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651254, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('63c7c4e7fc000c67030e4a4b10e365bf', '157.55.39.195', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480436545, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('63db3fea96e4824b6717917e5f806a36', '66.249.90.128', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1481921411, 'a:3:{s:11:"captchaWord";s:3:"643";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('63e9c8a2f86e9914597697f29ffbdfef', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480075189, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('64278ba2ba670da01f69687fd2a500af', '54.86.189.171', 'CheckMarkNetwork/1.0 (+http://www.checkmarknetwork', 1481526036, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('646462b8b49cb546f46cad19a80b1e46', '177.248.151.124', 'Mozilla/5.0 (Linux; Android 5.1; XT1032 Build/LPBS', 1481768115, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6474cf0a4ca96659fe76ff1f4c7ae204', '54.172.64.175', 'python-requests/2.9.1', 1481916588, 'a:3:{s:11:"captchaWord";s:3:"878";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('64aba954e0d83536bc8d0c863bcb2ac3', '66.249.66.79', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480386297, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('64eee00e787c2dc043b6ee742398ab6e', '189.187.67.189', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1479236301, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6512bd352151615483a7fd1a2c6b488f', '157.55.39.135', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480436540, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6514a96ddb6360a1e3ea210d78b56e6b', '66.249.65.148', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480169516, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('65296f55ac2a7e2c7a36330cfaba2766', '66.249.85.183', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1482165355, 'a:3:{s:11:"captchaWord";s:3:"483";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('652b0ae3ffe26543ee6acd0e3933ea0e', '66.249.65.171', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479786169, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6543c6396dc202637c0b0b83ccc49e6b', '66.102.6.194', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; Trident/7', 1480778554, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('658f40c1f26621c1833805448ae7e132', '189.187.156.194', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481228235, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('65af46e6dc3cd2fdc81e43a098de72ae', '54.211.192.237', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1481588390, 'a:3:{s:11:"captchaWord";s:3:"749";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('65c30db234fac78170232aed942e66e8', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612401, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('65f3ce907cc7ab24f292a7b9239918d5', '66.249.69.247', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479939821, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('65fba81938a0dffe6a4d1876d80e59e3', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651310, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6608b7258f4f7f14f5468b3b3f2786a5', '187.163.61.86', 'EMail Exractor', 1480099818, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('66180c6567a8d9a92923b18c7e713a93', '66.249.65.148', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479846703, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6625d66125265da7fabf183f7f9909bd', '187.193.25.128', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1479966710, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('66573c08f57e0391404caa21f7198c60', '66.102.6.24', 'Mozilla/5.0 (Linux; Android 6.0.1; SAMSUNG SM-G930', 1481426413, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('666acc835c7f7a3746b8c44799bf4129', '211.6.122.120', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1479535979, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('66cae66528bb871510d86345c99922bd', '189.166.52.5', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480609253, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('66e490b98f91aa82fd77637f4c130878', '189.177.245.196', 'WhatsApp/2.16.225 A', 1481849335, 'a:3:{s:11:"captchaWord";s:3:"294";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('679591f765f7e74cdef724e991063961', '52.70.48.63', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) App', 1480663413, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('67afb531c0f3c58c43c8676281733355', '66.249.79.112', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1481743023, 'a:3:{s:11:"captchaWord";s:3:"881";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('67b108a699a9f23c235b00ed9c1e6e81', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481745878, 'a:3:{s:11:"captchaWord";s:3:"718";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('67f54b458ce252833f23a02c18eb6460', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482059289, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('68153c62dea2e2a0c20dcc606f502e15', '187.193.8.233', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480443290, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6817dd2d625b4c1615344fdf7331f387', '104.45.18.178', 'Mozilla/5.0 (Windows NT 6.1; WOW64) SkypeUriPrevie', 1479892843, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('681ae3e9b9b9adabbbe186f4bc258804', '157.55.39.109', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481809525, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('682bfac8b7ee3ed969aade14ac1c5810', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482183350, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('68c644a3cc62276b282aa9727cae4dd4', '66.249.64.170', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481891732, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('68f627ca52214940c3f1f19b99703fc9', '66.249.90.76', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1479502537, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6943f0a4f6cac945ec945f5911941192', '189.187.173.20', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481656979, 'a:3:{s:11:"captchaWord";s:3:"192";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('69684f0ccba7d092d15e9afa109e6008', '189.177.112.57', 'WhatsApp/2.16.225 A', 1477174793, NULL),
('696c94444132962e2f4563520a92999c', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479373668, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('69e9f823d4d5042a6aba9e29200db8b3', '178.52.249.124', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480747739, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6a2bd874b24126bdbb40335c5421776f', '66.249.85.136', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1477340960, 'a:3:{s:11:"captchaWord";s:3:"539";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('6a36becc46441e0d3461f37a749b20ce', '66.249.80.22', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1476626941, 'a:2:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('6a69fc127eb23775dbbf2406cd9851b3', '189.187.170.55', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480006433, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6a84fd38860741fd16879a6721e12b9f', '199.16.157.183', 'Twitterbot/1.0', 1480788889, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6a9c4d78998b2cb714418ad8008557f9', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479377440, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6ab44e6962174e361c9aba79830051c1', '187.193.41.69', 'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; r', 1480453188, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6abbefc8296303939e7047c5230b020e', '66.249.65.174', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479524920, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6af9100861054469aebfe39ac476b6f4', '66.249.65.177', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479508142, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6b10011a6b8e767e8931b634ce95214a', '46.248.189.244', 'Mozilla/5.0 (Windows; U; Windows NT 6.0; pl; rv:1.', 1482090169, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6b1fa60b72969719c7fbd3f6ffb495b5', '189.187.219.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479503385, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6ba4a5b542f5a792ca20985e01a71a02', '207.46.13.128', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481414845, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6bae4e8c98f2a98e61d055266357b0fc', '207.46.13.128', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481414845, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6bc32197e0918447fe16aa3740b23f56', '66.102.6.146', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478017339, 'a:3:{s:11:"captchaWord";s:3:"277";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6bd8fee0c3d7e4321ec6b1f2353af1b0', '187.210.117.2', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1481050464, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6bf0ca4224334516aa3240ac23d10ca2', '66.249.91.124', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1480039774, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6bf9be5b7cefda7e550a3cbf21f55602', '211.123.208.22', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1482063175, 'a:3:{s:11:"captchaWord";s:3:"406";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6c03cc793be8004ec16c2b38640c8c9a', '66.249.65.177', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480276001, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6c07054c9d671ede26e6322f0cd0e51f', '66.249.65.70', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482016573, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6c20136453a5621fb3b45aa0878d2576', '66.249.65.152', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479765346, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6c2edba6f72141d8e8363a77eeae1e3c', '189.166.35.200', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1479960042, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6c36039c8c1d5b2a075a3132ecc449eb', '66.249.90.78', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1479356098, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6c451ffd7552d772a5027593a2bec7c7', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482141244, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6c70fa11f20dbab83ebeb0674f63821b', '212.129.5.119', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1481569033, 'a:3:{s:11:"captchaWord";s:3:"843";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6d17165f3a63b2cc93bfd812d8111027', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239578, NULL),
('6d677153487c851f722acc5d234a613e', '66.249.90.78', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1479501834, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6d7375116646ea752b7c839453b66161', '187.144.136.145', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480356198, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6d9bf349f3e6f170941d0a8464a0a8ef', '66.102.7.194', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1481647331, 'a:3:{s:11:"captchaWord";s:3:"341";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6e429fb33c9bae621dc12d84b5e8f2bd', '189.187.156.104', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1479845230, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6e88bcf1aae1dd3d51c02fc0210cacb7', '187.193.19.171', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1482167219, 'a:3:{s:11:"captchaWord";s:3:"367";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6ee7ccae626eda98bf7a096e314c2ce9', '66.249.79.110', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1481743833, NULL),
('6eedccd2e48622374aa63ea80974de0e', '66.249.65.156', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480247651, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6f25b733db99a0b1ea43cc52551fe6b7', '66.102.6.114', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479141985, NULL),
('6f5c634b54b810197b703165e79244bf', '187.193.28.204', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1482195062, 'a:3:{s:11:"captchaWord";s:3:"452";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6f94e79e03e22930a1233d469e414d56', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479631176, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('6fb31c5ee2e1d297d0baedac75a9e1cd', '200.94.102.2', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1479405564, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7000b4b9d3e2e3f253936ed41774802d', '54.211.192.237', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1481601779, 'a:3:{s:11:"captchaWord";s:3:"921";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7053c089f61ea63c03aba38564257207', '54.145.241.82', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1482175928, 'a:3:{s:11:"captchaWord";s:3:"436";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('706f1e665ddaa5f7986410aec24b8f2d', '138.108.39.10', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; r', 1481306355, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('70ad49f655c52072267ca4051c58c0cd', '189.177.226.244', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1479923569, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('70c6373d66da53da9f2f9680796c67d3', '207.46.13.2', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1479723790, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('70cfb59d9c7fbefae253cf49015d80fe', '66.249.65.196', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481632006, NULL),
('70de35a8a6ae2ad14621f47e659138f3', '54.67.65.103', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) Ap', 1480662464, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('70ec23aee21ba0705869aba303b6be11', '66.249.65.174', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479600285, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('70fe97ebfd63fcc90f632f9a4903888b', '52.71.155.178', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) Ap', 1480644027, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('710981d79b4ca0062cfd18806a9c4798', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239574, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('711533f273dbdd59e62036f28ea57533', '189.211.181.181', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1479485125, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('71452c71a6af51deb9334500f7ded7bf', '66.249.65.177', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479748047, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7163bb840ef7401d49cbb5b9d56cc586', '189.164.111.62', 'Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/201', 1479342585, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7187b575fbdae20292952bf6020c98e4', '187.193.34.197', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) App', 1481517877, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('721c1fa24e8e4fdad8c361222775c7cf', '66.102.6.212', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1480709111, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('72e266bf276cdf045b2fc3661a887c3a', '187.144.134.51', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480881760, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('73579f344ac7858c50fcea5ae6761d08', '66.249.85.138', 'Mozilla/5.0 (Linux; Android 4.4.2; LG-D331 Build/K', 1477624214, 'a:7:{s:11:"captchaWord";s:3:"197";s:9:"data_save";a:1:{i:1;a:11:{s:7:"item_id";s:1:"4";s:4:"line";i:1;s:4:"name";s:16:"Adulto Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";d:10;s:8:"discount";i:0;s:5:"price";s:7:"1000.00";}}s:4:"kits";a:2:{i:2;a:11:{s:7:"item_id";s:9:"Tailandia";s:4:"line";i:2;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:2;s:8:"discount";i:0;s:5:"price";s:8:"15300.00";}i:1;a:11:{s:7:"item_id";s:9:"Tailandia";s:4:"line";i:1;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:8:"15300.00";}}s:6:"cart_p";a:1:{i:1;a:11:{s:7:"item_id";s:9:"Tailandia";s:4:"line";i:1;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:8:"15300.00";}}s:4:"tots";s:1:"0";s:8:"customer";s:2:"-1";s:10:"payments_p";s:0:"";}'),
('737f58950bd97494c07d3f96d5ae9e0c', '66.249.65.200', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481628843, 'a:3:{s:11:"captchaWord";s:3:"402";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('738e36f994f8b1d2a1f84c923ca89ede', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480364308, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('739e3c3d26de8a833428036daee0c01d', '187.193.51.130', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1479413096, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('73c3ca612060626732997750b8c8c52e', '189.187.90.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1482035233, 'a:3:{s:11:"captchaWord";s:3:"462";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('744be401412260db1be4cbb2b2c67b56', '66.249.65.125', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482027836, 'a:3:{s:11:"captchaWord";s:3:"Ydo";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('74d62c95e16052ed2f95137bfbce1540', '189.177.239.107', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1477526168, 'a:7:{s:11:"captchaWord";s:3:"170";s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";s:4:"kits";a:1:{i:1;a:11:{s:7:"item_id";s:1:"4";s:4:"line";i:1;s:4:"name";s:1:"4";s:11:"item_number";s:0:"";s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:0;s:8:"discount";i:0;s:5:"price";s:0:"";}}s:6:"cart_p";a:1:{i:1;a:11:{s:7:"item_id";s:9:"Tailandia";s:4:"line";i:1;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:8:"15300.00";}}s:4:"tots";s:1:"0";s:9:"data_save";a:2:{i:-1;a:11:{s:7:"item_id";s:1:"4";s:4:"line";i:-1;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:4:"1.00";s:8:"discount";i:0;s:5:"price";s:7:"1000.00";}i:-2;a:11:{s:7:"item_id";s:1:"5";s:4:"line";i:-2;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:4:"2.00";s:8:"discount";i:0;s:5:"price";s:8:"12000.00";}}}'),
('74dfcf6f96957655f5812a0483375cf7', '189.162.165.28', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1480690199, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('74fe8fecdfcd620180be633a9344ebaf', '187.144.120.30', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1479334126, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('752d7dbe5535c22305a67781f2db1f4d', '157.55.39.109', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481809525, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7539bab215d4fdbd8ac166e045401656', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482097252, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('759e555cf4d4b3b15fca80bde74ca83c', '66.102.6.148', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478037889, NULL),
('75a807347f8da933ee24f91020f3ab7a', '187.144.134.51', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480885281, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('760361978503c63d16f84327393b392f', '158.69.225.34', 'Mozilla/5.0 (Linux; Android 4.05; Galaxy Nexus Bui', 1481695487, 'a:3:{s:11:"captchaWord";s:3:"922";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('761b29ca8555f8cb664e501e024600be', '40.77.167.64', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481414831, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('76293d581321b545c4c4b0774efe06a8', '66.249.65.174', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479794976, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('76483ee4db3facbe6d6bd61c4e4a2ee4', '200.68.128.81', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1479408343, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7703666e67e31f4abea85b580cff0331', '66.102.7.150', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478878532, NULL),
('770481b04aa589dde8c3b611f7ce36fb', '66.249.65.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482250848, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('771230748980aeda73c1834cc1e1a364', '66.249.65.174', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479376772, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('77bfd65ca97d4e3546cf8f45eb5ad30c', '189.177.233.214', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1476313335, 'a:4:{s:11:"captchaWord";s:3:"596";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"1";}'),
('7811156083423d16ac2492f3b10d13c1', '66.249.85.138', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1477843221, 'a:3:{s:11:"captchaWord";s:3:"362";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('782844a2500a0cf776a03e25d5046776', '211.6.121.140', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1482041681, 'a:3:{s:11:"captchaWord";s:3:"760";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('782d7976e5226ec4d23724d58bf179e6', '66.249.65.152', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479378253, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('78741ab5aeedbe43128dada328e1fcf7', '66.102.6.148', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1477685925, 'a:3:{s:11:"captchaWord";s:3:"206";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7889d712177014a163dbd77a68d52886', '189.231.169.116', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480486536, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('78a3c8c748f964cae758b63a2055b1c8', '187.162.95.97', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1481651192, 'a:3:{s:11:"captchaWord";s:3:"386";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('78a77e0386ad55bca5bf3913dd02940f', '189.177.226.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479931542, 'a:9:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"2";s:8:"position";s:6:"inside";s:8:"customer";s:2:"-1";s:4:"cart";a:0:{}s:9:"sale_mode";s:4:"sale";s:8:"payments";a:0:{}}'),
('78a8d55d498f0fceb57d5c1e4dec0672', '66.102.6.24', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_3_5 like Mac ', 1482034746, 'a:3:{s:11:"captchaWord";s:2:"77";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('78b31a0ec202865010d36dd6ab29f5f3', '66.249.65.57', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482139530, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('78f023752fc6a095d6098d7712c809eb', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482055667, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('791c739b76f58e7702436e854c837d7e', '66.249.88.59', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1480798907, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7921d88cc82b3c9894baa6441c1c7b47', '157.55.39.221', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480678957, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('792b7032919bd53f9cb28336d13d30c5', '66.249.90.128', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1480810067, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('794249eed83dd3ac5f1a9d1dd4a33add', '69.171.230.115', 'facebookexternalhit/1.1 (+http://www.facebook.com/', 1482169407, 'a:3:{s:11:"captchaWord";s:3:"770";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('79542822ada791a5c100e6af3920579c', '40.77.167.48', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS', 1481260576, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('79cf705dc9e105d80bf4508853825f76', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481964331, NULL),
('7a9de1238bf614e2c89136c610426c33', '148.244.118.125', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) App', 1481754647, 'a:3:{s:11:"captchaWord";s:3:"979";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7ad0712aad877236f71c6fbf6579b5af', '187.193.19.171', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481844920, 'a:3:{s:11:"captchaWord";s:3:"571";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7ad81a5d8f445002fb69520758f00b2d', '189.187.173.20', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481659153, 'a:3:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";s:11:"captchaWord";s:3:"755";}'),
('7b3b8773224a562d1279d5c2f5988975', '54.145.241.82', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1482188543, 'a:3:{s:11:"captchaWord";s:3:"884";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7b5aea9e894a9bf58bfd6846e09c266f', '66.249.65.171', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479554538, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7bbc8f714a843ce64d2463708cf53623', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651241, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7bcc92de33d6ab9b5f2cf2c2c15843a3', '187.144.126.231', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1479840416, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7be1121c4bcfa82076b2a33e3afd75f5', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651249, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7bed563d495798d08837ad6a15f63304', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481990590, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7bf249b93b3ec470cd72ec409d7f5b2c', '189.187.108.161', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481127614, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7c2d8417e2801c41241079aa1ae2dc65', '199.59.150.183', 'Twitterbot/1.0', 1481656760, 'a:3:{s:11:"captchaWord";s:3:"731";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7c354959aa4ace9030814e76334eed76', '66.102.7.154', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478627714, NULL),
('7c47aab35c1facdf68de66928664ce1b', '207.46.13.148', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS', 1481052355, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}');
INSERT INTO `ospos_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('7c7330b67c1cc5e236b9cfac2e67d6f7', '189.187.108.161', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481071509, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7d7d4697bdb4e073a00ce4f8fd3ad715', '66.249.65.125', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482136287, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7d7e5bec369e67763cd240188f7fcde0', '189.187.108.161', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481127309, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7dd4cf45e9202d7e4ec05dbbf67bef50', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480108401, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7de6168be61a0fbb1efe3e0aa7d331d0', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651301, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7de942ef139464884e891a2c70eeeffb', '187.193.85.246', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1480382522, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7e027c0165c6bcf237fed3a81a37719b', '66.249.65.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482059023, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7e7c20a6baa27f6551300d82000ea859', '66.249.65.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482074584, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7e89c5d35bd3bc3ce20190d17b5ae947', '189.177.184.117', 'WhatsApp/2.16.225 A', 1477402884, NULL),
('7ee3dd3d607dd3497bddd81438dcdac9', '54.162.50.31', 'Ruby/Curb', 1481905746, 'a:3:{s:11:"captchaWord";s:3:"310";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7f36c9259b9f2a853938a10cf53bb288', '187.191.6.65', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1479410120, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7f426f3d5473504f51b2d8838feb7e82', '192.154.106.250', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1481083724, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7f4c02dd88423915433d27be53b0a619', '189.177.239.107', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1478204509, 'a:3:{s:11:"captchaWord";s:3:"160";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7f713fea45e2dbbeea553bae1d13502d', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481239245, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7fb546c37c9e09833b3266c1d6a244ec', '189.187.203.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481672408, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7fca3f0218f0db57c1b7a866490085d2', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482189551, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7fe1a4851273f62ee3144bf6078d175a', '66.249.65.192', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480886124, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('7fea3dfaa2ad800d90c1d532ea7de011', '66.249.65.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482062393, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8021a956e402f8354590629ece96a66e', '66.249.69.243', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481360694, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('80370e31a74bbfe97d407dd1142c4663', '40.77.167.53', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480945780, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('805382991c5fe6451f11d21ff2f7f839', '187.191.7.140', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1479404160, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('805c64968515bd0d22cb6f7b49aa8378', '104.193.88.245', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1479858198, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8084a47c37d4e7c7551b2bcf4b474a64', '189.187.219.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479844238, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('80d711b9ff5015d77b1566bb0e6beb07', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651267, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('812aaaeed02be679db41b7fc03946a4a', '54.157.223.13', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1479779250, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('815a55a2d9b207007d3d8c1ed14eb85e', '66.249.80.22', 'Mozilla/5.0 (Linux; Android 4.4.2; LG-D331 Build/K', 1476626991, 'a:2:{s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";}'),
('821bd6cbc6bfbdf052bbaf954afca4fd', '66.249.79.102', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479898296, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('822c83edcdd9bdd47f965bf8ff06717e', '207.46.13.128', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481414845, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('824c9524e1d20b74c88b4d463476075f', '172.98.200.219', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;', 1481441577, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('824d4994f001218303a3bd77989668cf', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481242970, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('82cc9c48f97a68a031967404326058be', '66.249.64.174', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1481853976, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('82de3d86a43eaed5155b31b116907cf7', '89.163.208.238', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479370203, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('82f120e115c4ec94158a1207baa84ae5', '66.249.65.57', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482197391, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('833ddf6a800e6ad257ac843198d7da40', '211.6.121.138', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1481159631, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('834525c3db1fbd7e9800095df8121b92', '66.249.90.130', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1481666819, 'a:3:{s:11:"captchaWord";s:3:"869";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('838892a0236d00fa755ceb1f623af9f3', '189.177.239.107', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1477682677, 'a:7:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:3:{i:1;a:11:{s:7:"item_id";s:2:"10";s:4:"line";i:1;s:4:"name";s:22:"Adulto Esencia Europea";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:1:"3";s:8:"discount";i:0;s:5:"price";s:7:"2735.00";}i:2;a:11:{s:7:"item_id";s:2:"11";s:4:"line";i:2;s:4:"name";s:21:"Menor esencia europea";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:1:"5";s:8:"discount";i:0;s:5:"price";s:6:"600.00";}i:3;a:11:{s:7:"item_id";s:16:"Esencia Europea ";s:4:"line";i:3;s:4:"name";s:16:"Esencia Europea ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:7:"2735.00";}}s:4:"tots";s:1:"0";s:9:"data_save";a:2:{i:1;a:11:{s:7:"item_id";s:2:"10";s:4:"line";i:1;s:4:"name";s:22:"Adulto Esencia Europea";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";d:5;s:8:"discount";i:0;s:5:"price";s:7:"2735.00";}i:2;a:11:{s:7:"item_id";s:2:"11";s:4:"line";i:2;s:4:"name";s:21:"Menor esencia europea";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";d:7;s:8:"discount";i:0;s:5:"price";s:6:"600.00";}}s:4:"kits";a:1:{i:3;a:11:{s:7:"item_id";s:16:"Esencia Europea ";s:4:"line";i:3;s:4:"name";s:16:"Esencia Europea ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:7:"2735.00";}}s:8:"customer";s:2:"-1";s:10:"payments_p";s:0:"";}'),
('839cce549892806f96132e13aa68994f', '187.144.120.30', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1479253526, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('83e2818fea4342209b184e9447d09580', '189.177.239.107', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1476556880, 'a:10:{s:11:"captchaWord";s:3:"209";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";s:6:"cart_p";a:1:{i:1;a:11:{s:7:"item_id";s:16:"Esencia Europea ";s:4:"line";i:1;s:4:"name";s:16:"Esencia Europea ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:2;s:8:"discount";i:0;s:5:"price";s:7:"2735.00";}}s:9:"data_save";a:2:{i:-1;a:11:{s:7:"item_id";s:2:"10";s:4:"line";i:-1;s:4:"name";s:16:"Esencia Europea ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";d:4;s:8:"discount";i:0;s:5:"price";s:7:"2735.00";}i:-2;a:11:{s:7:"item_id";s:2:"11";s:4:"line";i:-2;s:4:"name";s:16:"Esencia Europea ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";d:4;s:8:"discount";i:0;s:5:"price";s:6:"600.00";}}s:4:"kits";a:1:{i:1;a:11:{s:7:"item_id";s:16:"Esencia Europea ";s:4:"line";i:1;s:4:"name";s:16:"Esencia Europea ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:2;s:8:"discount";i:0;s:5:"price";s:7:"2735.00";}}s:8:"customer";s:2:"-1";s:10:"payments_p";s:0:"";}'),
('8405956a40b55050176a9ff68012b117', '108.168.185.13', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1481915152, 'a:3:{s:11:"captchaWord";s:3:"199";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8422339e5b2d818cc75565c580789887', '66.249.85.136', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1477968735, 'a:3:{s:11:"captchaWord";s:3:"147";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('843b59ef5eb4a86ece83e48228d19bc7', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479854579, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('844d6578074ee2f5dff0a6d8d7ec4dc6', '189.177.239.107', 'WhatsApp/2.16.225 A', 1478050923, 'a:3:{s:11:"captchaWord";s:2:"11";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('84dd4c24107cd5729298da7a6b124e92', '66.249.65.196', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481733392, NULL),
('84e6f5d84e7e72e4d318f4cfe82b1715', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480990686, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('85282d2338d34e402a367653db45b7dd', '66.249.79.120', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481736691, 'a:3:{s:11:"captchaWord";s:3:"788";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8537012e481c6d859a9b3be157b05e68', '66.249.65.174', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480262839, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8560e086c3a1afa9966491098e4af5eb', '189.166.35.200', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1479960019, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('856690e22fd2c9aac25a8880a8454f9e', '187.144.112.193', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1482210348, 'a:3:{s:11:"captchaWord";s:3:"997";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('85b79d746a60feb70e4d7781961e2459', '66.249.65.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481967381, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8606cd5eca15e13709b4b78bdc7bdc11', '66.249.65.55', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482206275, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('863ea1e59503881083b1745575b14ca5', '157.55.39.213', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481809521, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('866d5a1ae934c9874242b711a2ba4f08', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480953821, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8686290bf557497bd81b7b6c8a45edf9', '177.248.151.124', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0)', 1481770571, 'a:3:{s:11:"captchaWord";s:3:"796";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8686c2bf19d77bd1ba43f7ae5623ea4d', '200.68.128.39', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1479364295, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('86a8b4ea91dfc3115e21dd6e166a3a68', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481415373, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('86e609e2534a530dc1cac51c095e8aa1', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479295050, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('86fd26aae2ecf09eba5caa698af50b78', '189.187.216.69', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480014825, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('871684e0f7783b4c245d7c1c0a99491d', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481746543, 'a:3:{s:11:"captchaWord";s:3:"915";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('87177efa5321521ce41a026107af51da', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612407, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('87758b00a946612b4aa9fe950159a9b7', '66.249.69.239', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480034382, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('877ee90d9999763b8978934ba8712b04', '189.166.35.200', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1479960044, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('87987376ddb2f9970c7fedf3822848c8', '66.249.65.57', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482209245, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('87b2d8d4802a90d59d82b466b925690a', '157.55.39.1', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480338979, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('87b541813cd3f5bfee574e75bd286c23', '66.249.65.171', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko;', 1479741863, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('87c94c3890c100c36cb1e5f348790c11', '66.102.7.152', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479947284, 'a:3:{s:11:"captchaWord";s:3:"705";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8820c8787bdd2cbfb7346da2973d83f6', '207.46.13.28', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481990852, NULL),
('885f30311f48d53d8f0cc25efc5ae1af', '64.246.165.180', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en; rv:1.', 1480157750, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('88689262c5253cfa80fbb7250dd901a2', '66.249.65.102', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481029273, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('889f122b638663ae9a67061dba0b28ce', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481748006, 'a:3:{s:11:"captchaWord";s:3:"243";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('88c12ad8bc964fdbf6fcc27065da183a', '187.193.8.233', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480536377, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('88f29e6254a183ae86844b98edb1d275', '157.55.39.91', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1479631957, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('895075fe007c2eaba700cc4fd831f31f', '66.249.90.128', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1481153280, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('896e3b59dd575e672ac4e9a01839004b', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651263, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('898d148ed5625b03a18912548ce34572', '66.249.65.56', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482115979, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('89a5ad2f4c1712c7b0d1dc35e369113c', '189.187.91.125', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1477679976, 'a:7:{s:6:"cart_p";a:1:{i:1;a:11:{s:7:"item_id";s:16:"Esencia Europea ";s:4:"line";i:1;s:4:"name";s:16:"Esencia Europea ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:7:"2735.00";}}s:4:"tots";s:1:"0";s:9:"data_save";a:2:{i:-1;a:11:{s:7:"item_id";s:2:"10";s:4:"line";i:-1;s:4:"name";s:16:"Esencia Europea ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:4:"2.00";s:8:"discount";i:0;s:5:"price";s:7:"2735.00";}i:-2;a:11:{s:7:"item_id";s:2:"11";s:4:"line";i:-2;s:4:"name";s:16:"Esencia Europea ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:4:"2.00";s:8:"discount";i:0;s:5:"price";s:6:"600.00";}}s:4:"kits";a:1:{i:1;a:11:{s:7:"item_id";s:16:"Esencia Europea ";s:4:"line";i:1;s:4:"name";s:16:"Esencia Europea ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:7:"2735.00";}}s:8:"customer";s:2:"-1";s:10:"payments_p";s:0:"";s:11:"captchaWord";s:3:"uZJ";}'),
('8a2c4c2911342d868b488e50ce7be42e', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651324, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8a61f9ca146b8118ce60700e63f76b79', '201.175.159.150', 'Mozilla/5.0 (Linux; Android 5.1; E5506 Build/29.1.', 1481414083, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8aa86a2714e9eb03a268a7cf0ab7e818', '189.187.108.161', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481073087, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8ae81e308c56b7cf5b79f1250f1faed1', '66.249.65.174', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479705695, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8b40451359b286ea8add575357482fa2', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482141233, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8bbea4bd2c64be07309b2557f615f4f1', '5.34.241.28', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479706391, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8bf73e3a73beabc5ee84fc9c98e5ad02', '54.197.93.141', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1481578203, 'a:3:{s:11:"captchaWord";s:3:"249";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8c052670fd35c7d7564b138c7d49306a', '189.166.52.5', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480609107, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8c40d4b95cc434ea3e6190a552970f09', '66.249.69.243', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481386060, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8c6faef31317158bcd375c9c7db6c5ec', '66.249.65.55', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482108950, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8c7fa63a36695d1414f1af6bd73da7cf', '66.249.65.174', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480468640, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8c8b4240203da9ec34d088a3353036d8', '66.249.65.177', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479671202, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8ca76aa0aa2dd9be4c1f8e9fea31d2c3', '189.187.108.161', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481072615, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8cad93feb2ffe6b143299bc8a17ac363', '66.249.65.174', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480097126, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8cbbd775f2535cd2e49b653042aeae1d', '187.144.126.231', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1479840416, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8d04a22e4943cb8daee9678cf0da7ef8', '184.88.28.14', 'Mail/3251 CFNetwork/807.1.3 Darwin/16.1.0 (x86_64)', 1482181189, 'a:3:{s:11:"captchaWord";s:3:"628";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8d948da189dbdaad194260edd5a64719', '187.144.112.193', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481863891, 'a:3:{s:11:"captchaWord";s:2:"24";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8e860a910397fdc24a7cb402e3b6e1f0', '157.55.39.142', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS', 1480090525, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8ee81ee6d85265dfab62291d1e35c2cd', '187.144.112.193', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481862785, 'a:3:{s:11:"captchaWord";s:3:"162";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8f1228b8e3ddb00deca478c4bf7f282c', '66.249.65.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481944995, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8f1243553130a20a8532de550c59c219', '189.187.203.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481677469, 'a:3:{s:11:"captchaWord";s:3:"123";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8f2b4fd44ed2f79206f49a6dc749f2cc', '185.2.151.146', 'GuzzleHttp/6.2.1 curl/7.35.0 PHP/5.6.23-1+deprecat', 1481914675, 'a:3:{s:11:"captchaWord";s:3:"666";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8f5d1a2098ac6dedd9ce12a700686a58', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481601753, 'a:3:{s:11:"captchaWord";s:3:"679";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8f7766db4cf4cd61677f407810770733', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481415231, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8f878e1b053c5044a3579430c6fd961e', '187.193.37.79', 'Mozilla/5.0 (iPad; CPU OS 9_3_1 like Mac OS X) App', 1480225628, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8fcfe8a8eb60e6decfb28d75b7d5c4af', '5.255.250.79', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://ya', 1479923476, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8fe1d1de4393dbfd6a9212e7ff939a92', '91.121.83.118', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebK', 1480693802, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('8ffed6dd3bb1fdc96fd64a45c93cac1e', '66.249.73.170', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1481495565, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9046c4fa58b4b2b5ef44d08ad0ed3efd', '157.55.39.213', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481808157, 'a:3:{s:11:"captchaWord";s:3:"664";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('905325cee5cdf0600fb948ae61b81a0e', '66.249.65.125', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482037583, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('91132b7e7b94da4e9f02beb29e6022ef', '174.113.167.192', 'Wget/1.9.1', 1481166896, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('912c61412222a2fcb93da182a9836778', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482068473, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('915ce32382bf1222550c2880cabd625d', '66.249.90.130', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1481573435, 'a:3:{s:11:"captchaWord";s:3:"702";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9175ef1febeb6701f28924a4de2a04d1', '66.249.85.183', 'Mozilla/5.0 (Linux; Android 5.1.1; C6906 Build/14.', 1482198557, 'a:3:{s:11:"captchaWord";s:3:"925";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('91834b18e6863619b880cdaa1a8235b4', '66.249.65.174', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480294906, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('920d931d5148f29fdf4f81b8d6d55448', '189.177.233.214', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1478021451, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('921c2cec7747b119e6641f2d52c14489', '66.249.85.184', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1482244687, 'a:3:{s:11:"captchaWord";s:3:"107";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9227947e2f667d645014a615b1c97ce6', '189.177.226.244', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1479255786, 'a:4:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"1";}'),
('9258af0bf4bd98bedd835863f7d18a0c', '54.145.99.235', 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv', 1479949688, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('926e5e5ad7edcb5136039c971490670f', '189.177.239.107', 'WhatsApp/2.16.225 A', 1478050917, 'a:3:{s:11:"captchaWord";s:3:"537";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('926f87ae5506c14ed75bcd160f08ffd7', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651257, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('92d58843053856d1cb6128598a5d194e', '66.102.7.154', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479862806, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9316d851e2ceb8f62aa5fbb9497c457d', '177.247.80.146', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0_2 like Mac', 1482167823, 'a:3:{s:11:"captchaWord";s:3:"882";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('93a1bd36dfcf5ed5ee8021878ee6d516', '189.187.159.133', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481322975, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('93b98fbc1006b989646da37c66e6004d', '187.144.136.145', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480356214, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9419091c1d3883f0e2e3718b5b517f34', '189.187.216.236', 'Mozilla/5.0 (iPad; CPU OS 9_3_2 like Mac OS X) App', 1477153492, 'a:4:{s:11:"captchaWord";s:3:"641";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";s:6:"cart_p";a:0:{}}'),
('941ac4905231d1dff200b966d321a499', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612399, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9425828179c925bb9ab7b5b425e8b2a6', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481242987, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('943dec586478ab8da65791daf2b2c8d8', '66.249.65.125', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482163313, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('949d12ca00c1e09def706c42bc6e82c4', '66.249.69.243', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481360616, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('94ba386646b5f32478330b13e6c0b821', '66.249.90.78', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1479587573, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('952b8e618da08d6edf928c9f524cf046', '189.187.203.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481673586, 'a:3:{s:11:"captchaWord";s:3:"650";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('95a32fcee8ce0cd6d2635df6fa917578', '66.249.65.56', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481719146, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('95b2a1fe0fe86c7bff7f376732a67bc1', '66.249.65.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481930355, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('95f0899c186c7f351d399d9a8543c402', '66.249.79.120', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481769999, 'a:3:{s:11:"captchaWord";s:2:"16";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('96130995190a101825082f75129d53a3', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482063603, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('96833f1f9fedf2592726faea6d604e0c', '66.249.65.102', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480852447, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9694b8bf5defd450ed184d6531bf38ab', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481222816, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('96b7a69d24d4df6ff28b0d97ddde3b90', '201.175.157.118', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0_2 like Mac', 1480361130, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('96cbe3469568d913c279b96dfc1792ac', '189.187.214.123', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480381043, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9774be6748b68d4435f2d64e497e0f9a', '199.59.148.209', 'Twitterbot/1.0', 1479405392, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('99bac43f711fd06600fc52952976db08', '72.14.199.154', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1481755054, 'a:3:{s:11:"captchaWord";s:3:"598";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('99de86f0b7da0ec1f105668e9a1ee486', '72.30.14.94', 'Mozilla/5.0 (compatible; Yahoo! Slurp; http://help', 1481914821, 'a:3:{s:11:"captchaWord";s:3:"477";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('99e8948691fed389f360169a303d80dc', '200.68.128.81', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1479409000, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('99f1e15b69c36896d6182ddb0c7f75a0', '157.55.39.109', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481809524, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9a077b938aada81aead565de69659907', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612361, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9a2a7f4cecfb6aeda39421e9fe5392c5', '200.68.128.39', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1479364282, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9a6405f59511bbb21b09e9efc7d61151', '189.166.52.5', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480445350, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9b16a36934a03f0201a6b22aa85624d5', '54.197.93.141', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1481564823, 'a:3:{s:11:"captchaWord";s:3:"292";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9b1e2b3531d350e77f76f7798fe7cef3', '66.249.65.152', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480119700, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9b4cf95cd4228980097bdf69bee99e23', '66.249.90.130', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1481489327, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9b6c36c81b3f0de56a2fb7c845f403fd', '187.164.130.145', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479662363, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9c2857163c025be104321aab94a6f0b5', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612373, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9c288db70ec88d937bf68892cda943a6', '189.187.115.79', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479231464, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9c8692e8c57a5a887b3ede200aed69ac', '189.177.239.107', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1477097977, 'a:3:{s:11:"captchaWord";s:3:"155";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('9ca182c2d16407ee6454a9729cd43a89', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479291053, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9d41a29bc3753887df108a8841d0463a', '207.46.13.127', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481414840, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9d4cf4f5085cd77c331b0864b47f5931', '66.102.6.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1480117400, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9d4eafb6f323e5133d262660257461e6', '66.249.90.116', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1480861767, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9d639176a2f80f0ced75d85b0856d240', '189.187.219.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479924140, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9d7589c9e35d5ad1f9652ecce092e46a', '189.177.239.107', 'WhatsApp/2.16.225 A', 1478204565, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9d7eefe51e7af486d926261325640032', '66.249.69.247', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481359934, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9da68f2c4199895c5ca4b8f2eacfe7dd', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612379, 'a:3:{s:11:"captchaWord";s:3:"TH6";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9dab86d8d4ee837f280db0ff5485292f', '66.249.89.90', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1481860414, 'a:3:{s:11:"captchaWord";s:3:"760";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9def19894e4cb18a87fb32332a1b2557', '66.249.65.125', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482068784, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9df6de4a9594a31fad7984db1c1cc944', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480108345, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9e97a2a16de11ef67744e521258f6734', '189.177.239.107', 'WhatsApp/2.16.225 A', 1478048942, 'a:3:{s:11:"captchaWord";s:3:"209";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9eb03ae5e686c8ff17f92698b8ab5e0d', '192.241.225.199', 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1;', 1480095356, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9eb2a0337e57b8d1f1b5414a76e9e318', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481045000, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9ee62db100282dda58478733630eaefc', '72.14.199.148', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1479884946, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9f0429ea3a2b25ee5fb32a29264d3041', '189.187.240.178', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479944273, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9f34a29a75649b160b4a14c7e4c9cb44', '66.249.65.177', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479372536, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9f58143beb7e6242bc929bede6a78c8f', '40.77.167.90', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS', 1480394613, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9f60d6032838444da9c3a2fb204a9803', '66.249.89.88', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1481860414, 'a:3:{s:11:"captchaWord";s:3:"760";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9f65b5f4c7cf17ddc85e66b9cb642d65', '211.123.206.77', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1479492719, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9f6800330f68c94a13ecddfd8a50680e', '189.187.203.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481664077, 'a:3:{s:11:"captchaWord";s:3:"124";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9f72edb60c4cee239aadf50ee2fdbf73', '66.102.6.150', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1477347807, 'a:3:{s:11:"captchaWord";s:3:"362";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('9f8ca269dd78372585b9ff699ca151ec', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481219651, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9fbb62648d285a4844bcf68c897451d0', '207.46.13.23', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS', 1479693982, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('9fe1bdf5b0a46f114dd3c326760b13c3', '66.249.65.200', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1481144938, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a01cbda2bec62cc2113a7bba6f2a41a0', '198.240.100.36', '=Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) A', 1480460173, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a033277845a2cac13b361fbdd00813fa', '66.249.65.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482008891, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a036abad74d83044a9d881f66fa6a145', '66.249.65.192', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481006950, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a0516b39933160d18a14349195867d52', '66.249.65.196', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481582558, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a05ef7ad193b48443011a94cb924e722', '54.226.94.113', 'Go-http-client/1.1', 1479445750, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a0f7c8aeaedc9b1fd3ecae112b5b05b3', '189.166.52.5', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480607661, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a118930575506fb31f35a947755df313', '187.144.134.51', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480881804, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a15b38057acac17f1fa7eae49c746930', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480378641, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a17cdd97728418ba9f33e440925aeea8', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651320, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a198af8668f338287ed2a80e5953806c', '98.139.190.55', 'Mozilla/5.0 (compatible; Yahoo! Slurp; http://help', 1480163695, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a1ce73e162896526e90c75783f5838e3', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612413, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a1ee660f546f8af338b2a53cf27c99ba', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482189648, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a20fbb3cc582e2d29b2efbf46730c765', '207.46.13.2', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1479710714, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a2320540a96315c73e7be7d88b317a9a', '189.187.197.58', 'Mozilla/5.0 (Linux; Android 5.0.2; Blade V6 Build/', 1476981368, 'a:4:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}}'),
('a25ba112a7889cac7dba7c9c12019170', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481737967, 'a:3:{s:11:"captchaWord";s:3:"875";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a26d968e3b4f0de5c08a29507126ae48', '66.249.90.78', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1480106447, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a29259a6cd9e0a441cc248755766805e', '187.193.8.233', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480462544, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a2a321e213b31e2318cb790fe4669c31', '66.249.65.171', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479629654, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a2ca869db1f43479937aecb49a3576d2', '186.202.120.146', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:48.0)', 1479692220, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a2cd59a3f3a8ec7e2395e11df16c08ed', '49.194.149.220', 'Scrapy/1.2.1 (+http://scrapy.org)', 1480587936, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a2e05014644d3ab79fd7d58fa87f9315', '177.66.133.87', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480177994, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a2e704e957e02e665ead80f9ef860917', '66.249.65.110', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481185036, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a2fab2ed571f51a86eee5f2a52f856bb', '66.249.65.70', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482026903, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a30fe5970be0793b917ebb4887ec0e14', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651265, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a3274498b7621c4c863290c314dd0d8b', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480272627, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a371e1621dfe499fb0e4252ce1036110', '187.144.120.30', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1479334127, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a3e125a6ad2c61066f3d8bb9a2f2da59', '199.189.163.252', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; r', 1481898117, 'a:3:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";s:11:"captchaWord";s:3:"fWl";}'),
('a431eb279331dc30ed562e6c0c9e433d', '66.102.6.150', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478272713, NULL),
('a46ccda808d5acd2d53001bf30a7db20', '189.166.52.5', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480607650, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a4a5d28250f5e2b50a48b40213c610f9', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481601753, 'a:3:{s:11:"captchaWord";s:3:"679";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a4d9980537ee7937948fc7142312a7c6', '189.177.239.107', 'WhatsApp/2.16.225 A', 1477153865, NULL),
('a52938b5d984443a307f3236b1426bcb', '189.187.196.244', 'Mozilla/5.0 (Linux; Android 6.0; MotoG3 Build/MPI2', 1480109758, 'a:6:{s:11:"captchaWord";s:3:"505";s:6:"cart_p";a:1:{i:1;a:11:{s:7:"item_id";s:13:"Super Canada ";s:4:"line";i:1;s:4:"name";s:13:"Super Canada ";s:11:"item_number";N;s:11:"description";s:1:"0";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:6:"999.00";}}s:4:"tots";s:1:"0";s:9:"data_save";a:1:{i:-1;a:11:{s:7:"item_id";s:2:"12";s:4:"line";i:-1;s:4:"name";s:13:"Super Canada ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:6:"999.00";s:8:"discount";i:0;s:5:"price";s:6:"999.00";}}s:4:"kits";a:1:{i:1;a:11:{s:7:"item_id";s:13:"Super Canada ";s:4:"line";i:1;s:4:"name";s:13:"Super Canada ";s:11:"item_number";N;s:11:"description";s:1:"0";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:6:"999.00";}}s:8:"customer";s:1:"5";}'),
('a570273f642a895c22580a39e4b5c98c', '66.249.65.174', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479748955, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a57464b10dc407cafbe210910dc0c228', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481748157, 'a:7:{s:6:"cart_p";a:1:{i:1;a:11:{s:7:"item_id";s:13:"Super Canada ";s:4:"line";i:1;s:4:"name";s:13:"Super Canada ";s:11:"item_number";N;s:11:"description";s:1:"0";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:6:"999.00";}}s:4:"tots";s:1:"0";s:11:"captchaWord";s:3:"773";s:9:"person_id";s:1:"2";s:8:"position";s:6:"inside";s:9:"data_save";a:1:{i:-1;a:11:{s:7:"item_id";s:2:"12";s:4:"line";i:-1;s:4:"name";s:13:"Super Canada ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:6:"999.00";s:8:"discount";i:0;s:5:"price";s:6:"999.00";}}s:4:"kits";a:1:{i:1;a:11:{s:7:"item_id";s:13:"Super Canada ";s:4:"line";i:1;s:4:"name";s:13:"Super Canada ";s:11:"item_number";N;s:11:"description";s:1:"0";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:6:"999.00";}}}'),
('a57ca33dcb3c5a291af0625a432d9e4c', '54.157.223.13', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1479759484, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a5a262817b32ca09ef86163d553b5aa5', '189.162.222.251', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1480636174, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a5bb4c62b8457328e3f24279eb975049', '187.144.112.193', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481863892, 'a:3:{s:11:"captchaWord";s:3:"438";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a5e6c8c2bcabc5c1beab7291fe926391', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480189601, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a60c4641fd984b82a909aceea0b4ea60', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481415049, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a636abca0078fc4efb3e015d509f03f5', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480953805, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a638a5c63121f4d3b116facec92cb259', '187.144.126.231', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1479840411, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a6511e8fb304873b1e3fc996de62b39c', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480379075, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a6a4463d480e8226f725e76f6493206e', '199.58.86.211', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1482173943, 'a:3:{s:11:"captchaWord";s:3:"482";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a6adca840553c18a483103dd7eb6fee6', '187.161.137.43', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1481578168, 'a:3:{s:11:"captchaWord";s:3:"794";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a6af3dc3ba6c3967f03e4a66421e6a17', '187.193.19.171', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1482170960, 'a:3:{s:11:"captchaWord";s:3:"756";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a6af5494e1153ab077543354664cba12', '66.249.65.110', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480991383, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a6b44e6d55bf40e488b7f79511003cb9', '66.249.69.247', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481360270, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a6b95b890db79845b13a2630b6867857', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651294, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a6c46efb3af95b1ca253eb9947830d85', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612333, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a6fd11f08b823505583a8913efbfc2f6', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480359498, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a76e9fc4403316df655f5a23a9490e3e', '66.249.65.177', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479376415, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a778091fb08323228ee41175dd7bf25f', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479290022, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a77cb1fe8609d180c84649314ad8e145', '66.249.65.177', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479856012, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a794cba20d52e84814b4766467b35bac', '187.144.82.150', 'Mozilla/5.0 (iPad; CPU OS 10_1_1 like Mac OS X) Ap', 1479246666, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a7ad997f1ffb8b2081734e6a6dfd11e9', '66.102.7.150', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479065663, 'a:3:{s:11:"captchaWord";s:3:"467";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a7b64c827d8519fd5a4375b2b88d12e1', '189.187.203.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481673984, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a7c3f1036f6ba9848918d82033ab1c5b', '198.240.81.180', '=Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) A', 1481725434, 'a:3:{s:11:"captchaWord";s:3:"800";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a7db255791f674c3c2a9830bc7d70f72', '189.152.48.33', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1479419243, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a7e0e4780039128b2ebf9a197236ada5', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479291019, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a7f69dd6c28d69437d5b13dc39410aed', '177.244.114.144', 'Mozilla/5.0 (iPad; CPU OS 9_2 like Mac OS X) Apple', 1481335966, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a8867c1c3a4a93225cf3d9b2bf8a774b', '66.249.65.156', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480230101, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}');
INSERT INTO `ospos_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('a93b358cce8764e39642ae7a0bb1dde0', '208.90.57.196', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:31.0) Gecko', 1480952160, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a968571c7f0baddbdf71fb6103dbdc37', '201.175.157.119', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1479245097, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a97438c7183645910bcb9bec33e422af', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481752232, 'a:3:{s:11:"captchaWord";s:3:"316";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a9d3509b484e56f4dfcc576d4696f55c', '54.196.242.84', 'Go-http-client/1.1', 1479843367, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('a9e6096f158bce6d30e275ba38ad7317', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651313, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('aa2cce4728db424a339a2edd61c170ce', '189.177.124.72', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1479238023, 'a:7:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:2:{i:1;a:11:{s:7:"item_id";s:1:"2";s:4:"line";i:1;s:4:"name";s:14:"Adulto Orlando";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:1:"5";s:8:"discount";i:0;s:5:"price";s:8:"10000.00";}i:2;a:11:{s:7:"item_id";s:19:"Orlando Maravilloso";s:4:"line";i:2;s:4:"name";s:19:"Orlando Maravilloso";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:8:"15000.00";}}s:4:"tots";s:1:"0";s:9:"data_save";a:1:{i:1;a:11:{s:7:"item_id";s:1:"2";s:4:"line";i:1;s:4:"name";s:14:"Adulto Orlando";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";d:9;s:8:"discount";i:0;s:5:"price";s:8:"10000.00";}}s:4:"kits";a:1:{i:2;a:11:{s:7:"item_id";s:19:"Orlando Maravilloso";s:4:"line";i:2;s:4:"name";s:19:"Orlando Maravilloso";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:8:"15000.00";}}s:8:"customer";s:2:"-1";s:10:"payments_p";s:8:"efectivo";}'),
('aa409ff75de04d22c5a9734a955a0fc0', '201.175.156.29', 'Mozilla/5.0 (Linux; Android 4.4.4; HUAWEI G7-L03 B', 1479239406, 'a:3:{s:11:"captchaWord";s:3:"872";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('aa481267ffead39f0bbf2e1088d61a67', '54.92.252.123', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1480398741, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('aadb803b94cd1c5b23b73def07fcac4c', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479255879, NULL),
('ab1963b23630e6b41ae915b8132dba16', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651274, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ab29bf088bfd6c35c42e429fc6cea55a', '52.71.155.178', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) Ap', 1480647354, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ab341790580cd09f54560e72f30fa82a', '77.88.47.1', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://ya', 1479895894, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ab5df0fcd4bbcf14fa604036894785c4', '187.144.112.193', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1482185025, 'a:3:{s:11:"captchaWord";s:3:"187";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ab5f2950ea6e58c7b263fb2109a19149', '181.48.93.106', 'Mozilla/5.0 (X11; CrOS i686 13.587.48) AppleWebKit', 1481034797, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ab9a72b13bb5332a79f46b5f02419768', '211.6.121.136', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1479550332, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('abe59fc839c7fcdc4c29018e06156f37', '189.177.226.244', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1481584727, 'a:4:{s:9:"person_id";s:1:"1";s:11:"captchaWord";s:3:"542";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('abff1178ca4005ff2d06d37b8772c9c5', '189.187.173.20', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481656664, 'a:3:{s:11:"captchaWord";s:3:"579";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('acc8f7878d2e70e752e69fe330811406', '189.177.233.214', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1476995983, 'a:3:{s:11:"captchaWord";s:3:"827";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('acce4880167eeeab922ac9d508d19be8', '66.249.69.239', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481360336, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ad2af842fed06ceb13a92040818981bf', '66.249.90.76', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1480628168, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('adc59e76f9d46334562c3e7e2b83de7b', '66.249.65.192', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481949546, 'a:3:{s:11:"captchaWord";s:3:"08g";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('adf81485373346f0c94e254992f62bf3', '54.146.140.179', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1480985857, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ae20b173bfbc1407a8c906a0fb7d098e', '187.210.117.4', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1481050464, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ae2b414d4e55ccfe3e097b16639e0220', '66.220.146.31', 'facebookexternalhit/1.1 (+http://www.facebook.com/', 1482169948, 'a:3:{s:11:"captchaWord";s:3:"746";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ae46f64907b598b7c170cea73c96027f', '189.177.245.196', 'WhatsApp/2.16.225 A', 1481849337, 'a:3:{s:11:"captchaWord";s:3:"622";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ae5ad9bc8ea30f88829e58ef90702c53', '45.32.229.228', 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:49.0) G', 1479545118, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('aed67bc4c6fa8787eb7888f191b1b4be', '167.114.172.225', 'Mozilla/5.0 (compatible; Dataprovider; https://www', 1482101536, 'a:3:{s:11:"captchaWord";s:3:"tGI";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('af0a240889485281f47589daf96d4419', '189.187.203.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481672272, 'a:3:{s:11:"captchaWord";s:3:"373";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('af2d5ccbc894f2d689d773938903a71b', '66.102.7.196', 'Mozilla/5.0 (Linux; Android 6.0.1; XT1563 Build/MP', 1481228316, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('af5a041ca4e3bd4bf9fb9d162ee22635', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239577, NULL),
('af616d66eb1aac7da675e541c33b93a7', '66.249.65.192', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481659773, 'a:3:{s:11:"captchaWord";s:3:"843";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('af705d28faeb369b6fae194303e8547b', '66.249.65.57', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481727291, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('af81f5a4e8b6c5016fcb3060ed157f01', '66.102.7.154', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479321668, NULL),
('afb7c74798264cb15b979a556b80c09f', '189.187.203.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481673884, 'a:3:{s:11:"captchaWord";s:3:"247";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('afc3a52ccb29f571ff6c0c13f29aa1d5', '207.46.13.64', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480945793, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('afdc0f5d72c684249af2124fcf724f90', '52.71.155.178', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) Ap', 1480644027, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('afe4be36d7983187ee4859d1aeaf587d', '184.88.28.14', 'Mail/3251 CFNetwork/807.1.3 Darwin/16.1.0 (x86_64)', 1479316853, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('affb3b1f165f82e2426d2472853fcd42', '46.248.189.244', 'Mozilla/5.0 (Windows; U; Windows NT 6.0; pl; rv:1.', 1482090168, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b054f6c685692f23d048a407b5083b12', '199.58.86.209', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481574984, 'a:3:{s:11:"captchaWord";s:2:"59";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b06c14b8dfed5ba155a4959a0138ca06', '66.249.65.70', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482105300, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b138807e9935754ece645c9d5bfd4b8a', '66.249.65.174', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480251701, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b1496a2e8d91b9e1140f93a25afc15db', '66.102.7.152', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1480083802, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b17bea2a72f3266f9ffcd4fb6beff51d', '189.187.214.123', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480380994, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b1d8063d3a6424d501f8cc6a6981a63d', '66.249.65.66', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482093124, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b1fabb0e4f13818778a3427a9db81995', '52.71.155.178', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) Ap', 1480649634, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b22a8f966aac8796f81dd8e8873a6c6d', '189.177.239.107', 'WhatsApp/2.16.225 A', 1476927574, 'a:2:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('b251a39348c35771d8c754d726f7cab6', '66.249.65.174', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479377668, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b267add526a9b88d64e883346cc78f3a', '187.210.117.5', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1481050464, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b26c6ca27bd81ed39e247602e18a4de8', '184.88.28.14', 'Mail/3251 CFNetwork/807.1.3 Darwin/16.1.0 (x86_64)', 1481052774, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b2738f999094961b7727e6dd930e1eb5', '187.193.85.246', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) Ap', 1480109711, 'a:6:{s:11:"captchaWord";s:3:"123";s:6:"cart_p";a:4:{i:1;a:11:{s:7:"item_id";s:4:"Peru";s:4:"line";i:1;s:4:"name";s:4:"Peru";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:6:"899.00";}i:2;a:11:{s:7:"item_id";s:6:"Cancun";s:4:"line";i:2;s:4:"name";s:6:"Cancun";s:11:"item_number";s:0:"";s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:0:"";}i:3;a:11:{s:7:"item_id";s:17:"Crucero al Caribe";s:4:"line";i:3;s:4:"name";s:17:"Crucero al Caribe";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:8:"13359.00";}i:4;a:11:{s:7:"item_id";s:14:"Crucero Alaska";s:4:"line";i:4;s:4:"name";s:14:"Crucero Alaska";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:6:"999.00";}}s:4:"tots";s:1:"0";s:9:"data_save";a:3:{i:-1;a:11:{s:7:"item_id";s:2:"13";s:4:"line";i:-1;s:4:"name";s:4:"Peru";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:6:"999.00";s:8:"discount";i:0;s:5:"price";s:6:"999.00";}i:-2;a:11:{s:7:"item_id";s:2:"30";s:4:"line";i:-2;s:4:"name";s:17:"Crucero al Caribe";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:8:"13359.00";s:8:"discount";i:0;s:5:"price";s:8:"12808.00";}i:-3;a:11:{s:7:"item_id";s:2:"29";s:4:"line";i:-3;s:4:"name";s:14:"Crucero Alaska";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:6:"999.00";s:8:"discount";i:0;s:5:"price";s:8:"19279.00";}}s:4:"kits";a:4:{i:1;a:11:{s:7:"item_id";s:4:"Peru";s:4:"line";i:1;s:4:"name";s:4:"Peru";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:6:"899.00";}i:2;a:11:{s:7:"item_id";s:6:"Cancun";s:4:"line";i:2;s:4:"name";s:6:"Cancun";s:11:"item_number";s:0:"";s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:0:"";}i:3;a:11:{s:7:"item_id";s:17:"Crucero al Caribe";s:4:"line";i:3;s:4:"name";s:17:"Crucero al Caribe";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:8:"13359.00";}i:4;a:11:{s:7:"item_id";s:14:"Crucero Alaska";s:4:"line";i:4;s:4:"name";s:14:"Crucero Alaska";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:6:"999.00";}}s:8:"customer";s:2:"-1";}'),
('b2fdd92c098c78a9a8cc99be626c03a6', '66.249.65.156', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479403898, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b30414f13e020b05f2adfebe70b0c629', '189.177.233.214', 'Mozilla/5.0 (Linux; Android 4.2.2; ALCATEL ONE TOU', 1477358129, 'a:8:{s:11:"captchaWord";s:2:"36";s:4:"cart";a:0:{}s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";s:9:"data_save";a:2:{i:-1;a:11:{s:7:"item_id";s:1:"4";s:4:"line";i:-1;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";d:2;s:8:"discount";i:0;s:5:"price";s:7:"1000.00";}i:-2;a:11:{s:7:"item_id";s:1:"5";s:4:"line";i:-2;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";d:4;s:8:"discount";i:0;s:5:"price";s:8:"12000.00";}}s:4:"kits";a:1:{i:1;a:11:{s:7:"item_id";s:9:"Tailandia";s:4:"line";i:1;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:8:"15300.00";}}s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b30dc9bf6ac04060290ab8225619aad3', '66.249.65.66', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482099540, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b32c96ca19ab60f15bb7dddfea553e7e', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239577, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b366497007030e341bd38f770477fa64', '54.167.126.88', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:29', 1479252685, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b3a0746ef712ffd9e73e801cf66a3933', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481314528, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b3b4aee677734495b24f78931136391f', '66.249.69.239', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481360565, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b3b85745434746965d41158a5a2191ce', '66.249.90.78', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1479864791, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b3bc5d211a078aa4d8810256bf5cde92', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239577, NULL),
('b3cf873773295f3fd2993f16a07338c1', '187.144.134.51', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480885729, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b437d0adeb99d0494b0be25dc2a9d9a9', '216.145.14.142', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en; rv:1.', 1480178964, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b450c2e6f4237d9846ce08bb0fb3f683', '189.212.125.235', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480719527, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b45a0036e06a467409f65e060f18e925', '189.187.108.161', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481073516, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b46a0ec850588a683711be01f1b34c2d', '189.177.245.196', 'WhatsApp/2.16.225 A', 1481679279, 'a:3:{s:11:"captchaWord";s:3:"791";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b4830c6aaf9c954066af431070fc5cf5', '117.214.15.16', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480305220, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b5948688efc0e7f60471ccf6b21db55c', '187.193.85.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480452317, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b5980b8693ea6cb228e21f085d949960', '66.102.6.148', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478188879, NULL),
('b619baaf118f2796d206a10cf2315ed1', '66.249.90.132', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1481036446, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b675c3313dbb3533407fc787c7b123b0', '66.249.80.18', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1476586904, 'a:2:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('b68882ea5917ce3848a66306bb073a26', '54.82.169.238', 'User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) Ap', 1481318941, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b6983c402c5eb858da251855899d4596', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481415374, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b6fc7bcd31630a141c5fb4f6692bc735', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651308, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b70c385cf2cdaef875dc3149c3726a0b', '66.249.65.200', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1481014445, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b7242ba94edee13613017862372d1755', '66.249.65.196', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480830907, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b79a222bc207ac2c0101b93433fea3d3', '189.166.35.200', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1479848327, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b7a38ded3b6fb5ccc1ec18c94882f220', '189.187.108.161', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481071654, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b7a7780df6b493e35d15f427a92fcae4', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482129308, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b7dd70c6155efa74fe6b1e93581cba56', '66.249.90.76', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1479418999, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b7f49d19148ffab3d6dce2248af81ac8', '200.76.91.99', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-G920I Build/', 1481672917, 'a:3:{s:11:"captchaWord";s:3:"801";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b80910f145fe26bda65bc3539e93f79c', '189.187.219.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479250319, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b81a5aceb6e289c246f2763821975442', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481414857, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b81fb0ca2efe55a191e9e5d19ea753ae', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480539458, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b85b7a89a31718f590c3b512be53ea98', '189.187.228.252', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1479484562, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b870db3c7ca64fffe0f183bf9f957e5a', '189.177.245.196', 'WhatsApp/2.16.225 A', 1481831675, NULL),
('b89ec88f22dfef1eeb3e349bd3685100', '189.177.239.107', 'WhatsApp/2.16.225 A', 1478204516, 'a:3:{s:11:"captchaWord";s:3:"551";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b9385a8cb70d2e58800e87dfa18b38b9', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651233, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b9619faae241c451e51c2c4ec9a843ca', '187.144.112.193', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1482003115, 'a:3:{s:11:"captchaWord";s:2:"79";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b98080d484dec66918b6a1107b4101a8', '167.114.233.118', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:48.0)', 1481623395, 'a:3:{s:11:"captchaWord";s:3:"965";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b98c0958ef3a752f852c7996f72ce51c', '141.8.132.74', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://ya', 1481904309, 'a:3:{s:11:"captchaWord";s:3:"216";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b9c40a52c2352ba0d0122aaa8cebbe42', '189.187.213.240', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) Ap', 1481679072, 'a:3:{s:11:"captchaWord";s:3:"340";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('b9c7ef580954075103c906f58de0d756', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482139164, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ba17b7e7a50fd9a96ed601293b22e155', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651276, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bac0239fbf9742825a1ecaa2322f53a8', '66.249.65.192', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480974020, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bb52923c0e2ca134fe3115467eec6e6d', '66.102.6.148', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478188879, 'a:3:{s:11:"captchaWord";s:3:"788";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bb7b75b546d536584830072085fb2e5b', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482141293, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bb8666867e6b584b58c7de2bed53708b', '189.187.108.161', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481071198, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bb86ef4a50913e341e6774a85a0cee46', '189.187.219.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479925335, 'a:4:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"2";}'),
('bb8a0e32935efb48c7d382efbb3bd06c', '66.249.85.138', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478105276, 'a:3:{s:11:"captchaWord";s:3:"417";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bb8d9e2f83693c05b43d59a294cccc5c', '54.197.206.38', 'CheckMarkNetwork/1.0 (+http://www.checkmarknetwork', 1481525822, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bbbac46c605772e29a9fd0b328f6b79b', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481752232, 'a:3:{s:11:"captchaWord";s:3:"403";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bbbafa60022525c78cef819b43f4f628', '66.249.65.66', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482097099, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bbda002f620a1b613b4b2914808a1106', '187.144.112.193', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1482210339, 'a:3:{s:11:"captchaWord";s:3:"282";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bca4515c6c63ac26fe132f255a0e354c', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479291108, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bcaf804e83881a0021150f4228adba62', '187.144.112.193', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1482210348, 'a:3:{s:11:"captchaWord";s:3:"997";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bcd4678ee5a39f51ea8e6fd6d56daa5e', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481745690, 'a:3:{s:11:"captchaWord";s:2:"63";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bcf5363c619f6104aeab9baae1cd0e7c', '187.144.112.193', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1482123893, 'a:3:{s:11:"captchaWord";s:3:"534";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bd044e5f2b2b12930c2f075df4682244', '66.249.73.166', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481733122, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bd4cf6ff3bfde2f81ccde435ed3b5f7b', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479597525, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bd63d0acb57b5a2d5027f1d3ac47f2bb', '199.58.86.211', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1482173934, 'a:3:{s:11:"captchaWord";s:3:"260";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bd9c3f429bfb98770adf6e3bfd164647', '5.9.145.132', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:14.0) G', 1481623537, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bdbdfd89cde9a128e4c5377108850b43', '52.53.190.164', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) Ap', 1480895431, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bde83d830fa0e5ba0474aad53b2ea9d5', '211.6.122.123', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1481440386, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bdef2eb64f7612ad83a8e9668dd08716', '189.177.226.244', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1479917202, 'a:5:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";}'),
('bdf9d679efaa92a53ed9c4cc99ba735a', '66.102.7.154', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478627713, 'a:3:{s:11:"captchaWord";s:3:"959";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('be1b8813b4b01867fa5a210111c9525b', '187.144.134.51', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480881805, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bec9b93053eb44d2057be2a78e7dd0cd', '66.249.65.55', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482200433, 'a:3:{s:11:"captchaWord";s:3:"854";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bed228f9e550315f5f1f8342344d43e2', '66.249.65.192', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1481483341, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bedaded80c0fe55d57d11e4a598eebba', '173.208.136.114', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1480080889, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('befa6977f4b474d0543158d90261d28d', '66.102.7.150', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478878532, NULL),
('bf1db2492b9503e47fc499a704bf0e0a', '187.193.8.233', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480441553, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bf4755601f1df53af606b64c8b7e766a', '66.249.65.196', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480819666, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bf58b5413cc9c03a7380324e93c7bd40', '66.249.89.82', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1481873937, 'a:3:{s:11:"captchaWord";s:3:"778";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bf805a743ec9fd31830b914d5b2767db', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612418, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bfa015f698e2c6ffbc26fff4c51db94f', '54.172.64.175', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko', 1481916634, 'a:3:{s:11:"captchaWord";s:3:"382";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('bfef4c495b46b251e5df30820c564ced', '157.55.39.213', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481893271, 'a:3:{s:11:"captchaWord";s:3:"188";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c10a8d70213dd1f4827150824500c381', '207.46.13.2', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1479817849, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c138a95017cb42f717375b971b3ed4d5', '66.249.80.22', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1476627161, 'a:2:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('c17e676784460b4159a822aa675df4b4', '187.144.120.30', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1479419765, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c1a0a319e8a7aed86868eca46458e966', '189.177.226.244', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1481590486, 'a:7:{s:11:"captchaWord";s:3:"RZd";s:9:"data_save";a:1:{i:-1;a:11:{s:7:"item_id";s:2:"12";s:4:"line";i:-1;s:4:"name";s:13:"Super Canada ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";d:3996;s:8:"discount";i:0;s:5:"price";s:6:"999.00";}}s:4:"kits";a:1:{i:1;a:11:{s:7:"item_id";s:13:"Super Canada ";s:4:"line";i:1;s:4:"name";s:13:"Super Canada ";s:11:"item_number";N;s:11:"description";s:1:"0";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:2;s:8:"discount";i:0;s:5:"price";s:6:"999.00";}}s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c1ad3db0d1adb021187c94586d2c8e81', '66.249.90.152', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1479245422, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c1bdce50da7b22d10c4da56dbc375e4d', '187.193.85.246', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) Ap', 1480109774, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c1c73ff7527d927425868f7a606ed4f0', '189.177.212.195', 'Mozilla/5.0 (Linux; Android 4.1.1; ilium Pad E10 B', 1479095032, 'a:3:{s:11:"captchaWord";s:2:"91";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c2011fe68b8cb54c3419fc3ef41ebc6f', '66.249.65.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481415160, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c23d78e768cfb00795c4aa9a7061e216', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481748005, 'a:3:{s:11:"captchaWord";s:3:"829";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c26e7c85cf9d824a7fd75cd82de7cfc8', '177.247.80.146', 'Mozilla/5.0 (iPad; CPU OS 10_0_2 like Mac OS X) Ap', 1481923871, 'a:3:{s:11:"captchaWord";s:3:"802";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c273681075d1a37c0e911ef250d39521', '211.0.145.108', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1479514328, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c2866a645ad70e61b108ed460ccadd93', '187.144.112.193', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1482238036, 'a:3:{s:11:"captchaWord";s:3:"739";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c29610d59614dca1351760fd9f9ce9c6', '66.249.65.125', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482012624, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c29c1a9002840083446f4cdd913f7733', '189.187.203.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481673892, 'a:3:{s:11:"captchaWord";s:2:"52";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c2c1186758b1c56f3863aac2bce922d5', '54.146.140.179', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1480971756, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c2eae6407a3ca2b3f08f4d9b340f0bbd', '207.46.13.64', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480945794, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c31656affae26b21a1f869679590a5c4', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480127839, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c31b8a1b8923de623a455b5d029d49dd', '157.55.39.1', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480436560, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c34837e74a33a340bf2a63e76f23bb3d', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480953804, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c35d417fa0db9fbfa3ccd43bf766a428', '66.249.65.156', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480140128, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c37ee856082e457b34ca243fa0dae736', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479780339, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c46d5298e0b9ae7f9a996de27ec5052a', '187.193.18.114', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1481316373, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c4d538e4851095db4651a9e674fa8d89', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480990704, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c4e4cafe029e4185d9676fed5134d971', '66.249.65.156', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480523890, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c50cdd3fb0a909e32270f5c4a2d62c40', '66.102.7.154', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479311442, 'a:3:{s:11:"captchaWord";s:3:"721";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c5191c4c0010adf46915a6f6b97ae754', '195.154.199.56', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:48.0)', 1479382507, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c5313aae9687937fb29a400b5abba15f', '98.139.190.56', 'Mozilla/5.0 (compatible; Yahoo! Slurp; http://help', 1479576263, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c534ea00308eed949bdcccbd645983dd', '187.216.136.81', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1480207008, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c552ad5e97ff823d9d52ac839e8bddc8', '189.187.219.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479927181, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c5a62390ad94f11d8b221941c67668b6', '66.220.146.20', 'facebookexternalhit/1.1 (+http://www.facebook.com/', 1480953545, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c5dd8061e3e9de093a46ea79e105036c', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481743716, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c5ddf70ba2e167156e49b08270068d1c', '66.249.85.138', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1477173236, 'a:3:{s:11:"captchaWord";s:3:"319";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('c619c00174da26c065e8e49375db9703', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651322, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c61dd01cfe683eb57d855828d73d56b5', '208.94.228.68', '=Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) A', 1481621177, 'a:3:{s:11:"captchaWord";s:3:"261";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c64caee48ddfd1311a23c0972d030815', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481975278, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c661900314f939f633446370f3721aa2', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651295, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c6b20329b6c3417b432b1b4a253c9950', '189.177.226.244', 'WhatsApp/2.16.225 A', 1481393033, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c713aa372e1853c4ccb64cec1f622acf', '66.249.65.174', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480282752, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c73d0ecc46730a53146cba7cc93a066c', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651299, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c77899dae7f23c1a9d7eef43f192a116', '200.68.128.81', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1479409007, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c790538e167dc3b391e5c5db90dc1376', '66.249.90.128', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1481573741, 'a:3:{s:11:"captchaWord";s:3:"105";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c7a7a994bc366c6b92d78872cc26dec2', '177.248.156.122', 'Mozilla/5.0 (Linux; Android 4.4.4; GT-I9060M Build', 1479476133, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c81ba8d800975757200b5796c9ee3f0a', '66.102.7.138', 'Mozilla/5.0 (Linux; Android 6.0; HUAWEI VNS-L23 Bu', 1479704124, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c82e120a34315fb881cd064b511283fd', '66.249.65.200', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480687183, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c8396914d71de84e1bd3d221f75eedea', '187.193.102.14', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481660073, 'a:3:{s:11:"captchaWord";s:1:"6";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c9132ddadf9eac9d210aaf61a30b9d52', '66.249.85.138', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1477109541, 'a:3:{s:11:"captchaWord";s:3:"962";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('c9260528533a6f0398c6bf69358148c0', '66.249.65.55', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482113229, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c92f6d5c2fb84e4603314ddd78e6844f', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480108181, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c93cf3586c22b2a6b58c5d11cee47130', '189.166.52.5', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480607647, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c97e74067cd1757d962101b2948deb78', '66.249.90.130', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1480810008, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c98c3c2d0e4ef99986d27cc500c3e41b', '187.144.112.193', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481863870, 'a:3:{s:11:"captchaWord";s:3:"350";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c999303e6ff4dd9804e9c2cfba3b580b', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480987661, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('c9ae32970009890a4576d94791edf4a4', '66.249.90.120', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1481723470, 'a:3:{s:11:"captchaWord";s:3:"516";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ca3a5e78b704124322bbde831988491f', '189.166.35.200', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1479848361, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ca8f04193da4c556e6e0e0d266b28791', '189.187.182.137', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko', 1480025912, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('caad86b171e83942c8a49c10a4a0c0b5', '187.144.136.145', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480356213, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cad5d25091ef890ddd7c67c25452477c', '200.76.83.206', 'Mozilla/5.0 (Linux; Android 5.0; LG-X165g Build/LR', 1481321144, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cada36d78a9a75306ff7831a1b0770f0', '52.70.48.63', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) App', 1479822785, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cb1198bc4dc22f37333a28ec78a8f4f8', '207.46.13.101', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481439383, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cb9b494b52229d186dc174cf525fecbb', '66.249.69.110', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479932482, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cbaf969636a3f76057219b46250f5fa0', '189.177.245.196', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1481660756, 'a:3:{s:11:"captchaWord";s:3:"271";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cbd4ae6e679641f3a5c8012fc3238b23', '189.166.52.5', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480609274, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cbd84bd9dd9d489b86adf1eb7425798e', '66.102.6.146', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478272713, NULL),
('cc097561e07272a009780b47e3804f33', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651272, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cc38ae92ecf744381eb5d08a546e3984', '189.177.239.107', 'WhatsApp/2.16.225 A', 1476929019, 'a:2:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('cc4bd599cdb0988b0fd5e7be8c8448ef', '66.249.90.132', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1481229127, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cc5450ce40b44e2d009afc993451e0dc', '187.144.112.193', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481862784, 'a:3:{s:11:"captchaWord";s:3:"248";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cc6e3ccbd1f4281da6c00b26a17d25ce', '66.249.65.106', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479906833, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cc829c161568d2c8a1ec54f114c20ee7', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480355189, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cca9e2997529bae3ed9fa1e7c39307ce', '66.249.65.148', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480525922, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ccd3a3443bc346d7f362a7651b7c9cfa', '66.102.8.156', 'Mozilla/5.0 (Linux; Android 6.0.1; SAMSUNG SM-G930', 1480209701, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cd0ba89c5cb5d409d375f0049025d533', '66.249.90.78', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1479355964, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cd10e39c91813086a63fa61000b00718', '66.249.65.174', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480238876, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cd2763fddbb6da2f379d1dc8fb3f928f', '66.249.79.110', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481756523, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cd2c61e07ef3e3fe9ded1c0064065518', '66.249.66.82', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480317176, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cd5f10ec679f34037c3b7479a61e8ace', '66.249.65.174', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479670627, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cd795f2d9ae309648649f875bb59f92c', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239578, NULL),
('cd8cfedc4f84ae89d4f2bb07923e31d8', '189.208.29.201', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1479399222, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cdde2e639d42c3f89adffdae38aaeb01', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480953458, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ce5bc2f3f74eea0da9007ec725305975', '66.102.7.178', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1480965737, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cea77610a6bc929aee392b888d1169c0', '189.166.52.5', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480609182, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cf09a4e3723a8246e6f614adf8d1716c', '52.71.155.178', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) Ap', 1480647354, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cf2d6f0111239b6ac27a759c3d3de3ff', '66.249.65.125', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482107131, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cf38f17ff63b6cea47116a7a65362732', '66.249.64.184', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481882156, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cf6bd97b08223d763dcd861e0a895fdd', '66.249.65.55', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482075803, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cf8a0e5f36a5d70eb94b1814869009b4', '54.166.69.236', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko', 1480971972, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cf8ad880af1705a3be94b9c37b0fc617', '66.249.90.76', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1479681310, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cfa8f7fa27c8b35c89fa1af1dbcf125f', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481415210, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cfb49bb48198b45baf0ed4f2b1b90a98', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651306, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cfe144294eac55584392e13fb0509ef0', '66.249.65.102', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480695904, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('cfeca6f5b8bc67e607ebb49c64c9e8b5', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479255877, NULL),
('d018c7ef173bc0370efcd4a4260d8944', '201.175.156.227', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1480536688, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d0608eed083a53961a04b9bc5710ee19', '66.249.79.110', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481770062, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d0c0dec08e5eac6bebc003d59703e77e', '207.46.13.28', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1482125180, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d0cccdb6fb42314acbd424c42f7e5ca8', '187.193.85.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480464636, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d0d2b48fa171555774a05e9f8592dd5b', '198.240.125.25', '=Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) A', 1481185830, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d0f741e435eb9d9b9fd6652fdea4d803', '189.187.203.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481672156, 'a:3:{s:11:"captchaWord";s:3:"447";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d1136447705a6c7976aa9c681b1df1c0', '189.177.226.244', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1481393070, 'a:5:{s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";s:11:"captchaWord";s:3:"271";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d17d7225e9b02ed8a03f9b67c91e72ec', '189.187.167.65', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1480955365, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d1967ae1c8461b3e71b04304b72aee12', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479376705, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d1d02bd1bc6d55d496e487bf96202f0b', '189.177.184.117', 'Mozilla/5.0 (Linux; Android 4.4.4; E2104 Build/24.', 1476927636, 'a:8:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";s:11:"captchaWord";s:3:"792";s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";s:6:"cart_p";a:1:{i:1;a:11:{s:7:"item_id";s:9:"Tailandia";s:4:"line";i:1;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:8:"15300.00";}}s:9:"data_save";a:2:{i:-1;a:11:{s:7:"item_id";s:1:"4";s:4:"line";i:-1;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:4:"1.00";s:8:"discount";i:0;s:5:"price";s:7:"1000.00";}i:-2;a:11:{s:7:"item_id";s:1:"5";s:4:"line";i:-2;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:4:"2.00";s:8:"discount";i:0;s:5:"price";s:8:"12000.00";}}s:4:"kits";a:1:{i:1;a:11:{s:7:"item_id";s:9:"Tailandia";s:4:"line";i:1;s:4:"name";s:9:"Tailandia";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:8:"15300.00";}}}'),
('d1ffc9fa3c424d713289242a9e75cb74', '207.46.13.162', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS', 1480924887, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d235a4c6d08c3fee98a183c96dac1483', '189.177.239.107', 'WhatsApp/2.16.225 A', 1476565553, 'a:3:{s:11:"captchaWord";s:3:"227";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('d23963708148a4583df9d738a1832698', '49.194.21.202', 'Scrapy/1.2.1 (+http://scrapy.org)', 1480161319, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d2463fedaa3944917fcac252347d71b3', '37.139.26.146', 'SafeDNSBot (https://www.safedns.com/searchbot)', 1482152300, 'a:3:{s:11:"captchaWord";s:3:"781";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d281edb2d53b701e5ffa1e106dbef072', '189.187.159.133', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481327246, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d29f2325cbec506f9f59dbf2715a4515', '66.249.90.56', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1482008064, 'a:3:{s:11:"captchaWord";s:3:"892";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}');
INSERT INTO `ospos_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('d2f4d6eb0973cb272bd8683544626804', '187.193.8.233', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480463407, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d3008a0cb48e7850c9defb7e43fa9c76', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612393, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d351faf68f8e2d34ed181d23406547cb', '66.102.7.152', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478539426, NULL),
('d409f6d13b7cc3144a5b1aaf95620489', '189.177.233.214', 'WhatsApp/2.16.225 A', 1477534576, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d43de17bb60095c7548280b2e827dd29', '66.249.65.125', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482043113, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d4439d640492a83ae1bb78dfa97c05a0', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612354, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d45f0927741c8a8ed2594929195604b4', '66.249.65.177', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480160576, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d48cd905466885895c28839af630b7c6', '189.187.115.79', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479232382, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d49e9ec61670ae9394266b6cfaba7c60', '207.46.13.28', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1482045270, 'a:3:{s:11:"captchaWord";s:3:"785";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d4f5f10ba724df2abc964a6c8d773e79', '187.193.19.171', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481829115, 'a:3:{s:11:"captchaWord";s:3:"904";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d52ce817d8c4b95289876e700278cc62', '66.249.90.128', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1481489327, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d535574705e0a79b287d2b749a881fb0', '187.144.134.51', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480881762, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d5387f5ec57010e2fa3165e6e3f69902', '66.249.65.177', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479559422, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d56f14caa07b706123a041eba9deff58', '187.144.112.193', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481863871, 'a:3:{s:11:"captchaWord";s:3:"760";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d5aaf885695e4bf6476d1cd3d6f4cf8b', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479377446, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d62126e93bfabb7df8ae3dcadbbe257d', '66.249.65.148', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479539993, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d62e51da49086f31fc0760cbf0fc7383', '66.249.65.106', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480966787, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d654a7a86f2b5b9fb2a6d69631295bf2', '66.249.79.106', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481735766, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d6955d0f6cf678af357620cf24ae5e95', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480377938, 'a:3:{s:11:"captchaWord";s:3:"349";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d6a5e7a8677e5e736eb7f932c2dd5772', '66.102.7.138', 'Mozilla/5.0 (Linux; Android 5.1.1; MOT-A6020l37 Bu', 1480383162, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d700b356194fb6633039e42404af6f6b', '66.249.65.200', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480847826, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d7254b54813a51d21f15efe4ec63d65b', '66.249.65.156', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479401338, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d727c9303b159fe2d9c01c4d229b8f42', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481518256, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d7675f8c525ff1298ca422b068beb732', '189.187.59.93', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (K', 1481220029, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d78fcd8fd8cdecd6f35da52adcc09517', '187.144.136.145', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480356193, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d795cc36d4fe905c76791793fa2cc5a4', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481752199, 'a:3:{s:11:"captchaWord";s:3:"265";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d799b04fecccfffeda823b22b4ab1b27', '66.249.65.156', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480137626, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d7b15f616de239299159245203daa802', '207.46.13.30', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480945789, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d7e71a8ed8609189ff66bdf9634574fe', '189.244.204.55', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1479579050, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d82157017392a8bc95785abaa4b62175', '52.71.155.178', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) Ap', 1480644027, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d85978b21dfc58758a88fdc0668110a0', '66.249.69.247', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481385885, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d86c27704f44f8ce21fc6d0ebe442a72', '66.249.65.177', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479512967, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d86ff65ddd1ba9c4b3f4f0ff92042fba', '187.144.120.30', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1479253525, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d88127230e2c0daad6a0ee504936cd45', '211.123.206.72', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1479543153, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d884aa79ac78f4a613804348aa5537ba', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480081601, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d885795437f8855dd7d81e141ae55d62', '210.232.15.25', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1480025615, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d8cf1c1b707a0b8e7ebc9751a73ecfb3', '66.249.65.156', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479291626, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d8d155a2ed22846d09be0941aab0e53d', '66.249.65.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482119940, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d8e0b4409dce4e17606b985c6edd7f73', '187.193.18.98', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:5', 1479839552, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d8ec079ea00c88304dcbc249c592931f', '54.212.212.180', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1481787826, 'a:3:{s:11:"captchaWord";s:3:"917";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d8f6c214767e657a34743bae8854c942', '46.4.123.172', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://ww', 1481651236, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d92211db5dae9305839df7a0f511eaf3', '200.94.133.242', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) Ap', 1481132400, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d92db1445baf088946461f00e9286f0c', '17.142.153.28', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) Ap', 1481914674, 'a:3:{s:11:"captchaWord";s:3:"249";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d94a4b703f279b45f2ca3916c93ec1ca', '189.187.250.234', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481824788, 'a:3:{s:11:"captchaWord";s:2:"25";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d9520e1c26b3807b6ebe47afe09a0363', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480208164, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d957b711bb2ac3ee1e2262c0bf5cbd00', '66.249.65.57', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482188933, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('d98b0bd6f00bf1ef13bd4f0a5a7dd95b', '187.144.120.30', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1479419765, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('da5dafc182012eced962001c9fcfcce5', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612388, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('da8285eafeb04df4c56819f9255d83bd', '66.249.64.174', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1481851543, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('da9001fd029ecb607a5ca9b9c6a5b649', '189.177.239.107', 'WhatsApp/2.16.225 A', 1477153723, 'a:3:{s:11:"captchaWord";s:3:"470";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('dac46a1a414ec441553a94beb3ffa377', '66.102.6.114', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1480201809, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('dadbc78a1af7a37074e3fd91bbba1d6d', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479333703, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('db2cfc8386fbddd2b0204c97e9d79094', '189.187.108.161', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481073554, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('db54d9b88f89dfc482ede500118b9207', '189.177.226.244', 'WhatsApp/2.16.225 A', 1480362983, NULL),
('db65f52582d22071f9d52cca89e93b6b', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239580, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('db9e7b85f617a973285112358c11766b', '189.187.108.161', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481072474, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('dba8fe33017cbfd8d279117a04c6c5e1', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481239219, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('dbf1776e9895207da0fcfb08c2ffee86', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239577, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('dbff33c356663be09a83428582d4e19b', '189.177.233.214', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1476394590, 'a:3:{s:11:"captchaWord";s:3:"419";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('dc2cb504686e1300493c6123b394c040', '66.249.65.174', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480222339, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('dc74fa3d61ef55058d9564c4531d156f', '189.177.239.107', 'WhatsApp/2.16.225 A', 1477160080, 'a:2:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('dc7fcbd9ba5f1192f09f785f0c505b9b', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480376853, 'a:5:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"2";s:8:"position";s:6:"inside";}'),
('dc805992e14cefa7d5b05b2e6f3f0a6d', '201.103.70.115', 'Mozilla/5.0 (iPad; CPU OS 9_3_1 like Mac OS X) App', 1480090464, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('dcc1b4b05acaa443d2e11f28bbda373b', '40.77.167.64', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481414831, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('dce098e0dfcf08fcc9e8f5b7b1188947', '66.249.69.243', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481386194, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('dcf7a1a894af711f1b0e3a077fa7cc84', '66.249.65.192', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480676494, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('dd16f613ef755d4190b07f9298643776', '187.193.3.186', 'Mozilla/5.0 (Linux; Android 4.4.2; SM-G355M Build/', 1480909627, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('dd311faa48262f6de1d118db9f14a4b4', '189.166.35.200', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1479781202, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('dd77fec90f1e56c435bed25ff8ac7259', '66.249.65.171', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479754442, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ddc3084c1a99070fd2748b16bfa409cf', '66.102.7.152', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478793528, NULL),
('ddcaf499b16417708a0cc0d9cc360816', '104.197.52.220', 'Java/1.7.0_75', 1479773330, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ddff4c1eeaf7b4a129207d6d12d79639', '189.187.219.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479947734, 'a:5:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"2";s:8:"position";s:6:"inside";}'),
('de12a17277edfed2ea0dd795052df656', '198.240.102.251', '=Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) A', 1480598318, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('de1bcee8cb163dbe4502865bae1447af', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480232464, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('de206f310821ace1396ae60f66047c75', '66.249.73.170', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481910902, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('de42bf06a82e7077242d10393b9f106e', '66.249.65.177', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479383166, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('de461aede1f288eded2f84a068de5984', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480358422, 'a:5:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"2";s:8:"position";s:6:"inside";}'),
('de6854cd49fe2542a8ada83ea390516f', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480953478, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('dec0ab1c2868c29c0e2425e6fc6206e2', '66.249.65.106', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480813581, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('df3903ad70c1f92525e7212ad8c37444', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239578, NULL),
('df56438f58b1472aa9323a2068cfa690', '208.184.112.74', 'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.1;', 1480657488, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('df66a5759cb134617e13bf2561e722c0', '66.249.92.139', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1480366169, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('df67be94b34aef6c5d958329a5a19c36', '66.249.65.177', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479865152, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('df6ae4ea2eb751f1368bbbd6daa60c63', '66.249.90.74', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1480277437, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('df8b62e00881c856cf5742935a070c52', '189.187.108.161', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481073651, 'a:6:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"2";s:8:"position";s:6:"inside";s:18:"customer_servicios";s:2:"-1";}'),
('df9425d77e4ad9a10ebe01b9abe3890e', '189.187.203.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481674540, 'a:5:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";s:11:"captchaWord";s:3:"590";s:9:"person_id";s:1:"2";s:8:"position";s:6:"inside";}'),
('dfb983b87dd9b6392c87a89320ec15c1', '66.249.65.196', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481959635, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('dfc123eb05edce3589a53a576c8020d0', '189.177.226.244', 'WhatsApp/2.16.225 A', 1481393020, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('dfc4f78cc5ac908b647fe597b12eeaf0', '189.187.216.69', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480032141, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('dfc92e576ae7ace52546dfaf408d3c4f', '66.102.7.150', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1480385238, 'a:3:{s:11:"captchaWord";s:3:"869";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e0534195c3891f143965e96708738fd4', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479290999, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e074a7885042a07bbdb0ea1243637a9b', '211.123.206.77', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1479521550, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e0a4d13ed555bd5aec2cf9466e62be82', '66.249.64.166', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481899042, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e0a64210cf0d9c1ef4ee78bd0c5e7f50', '189.187.203.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481673838, 'a:3:{s:11:"captchaWord";s:3:"746";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e18160aeb2f7d840221fc30fa4ebeec8', '189.177.239.107', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1477154901, 'a:5:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";s:11:"captchaWord";s:3:"153";s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";}'),
('e1b5c4232a57af69fed6ac426aa939ca', '66.249.65.56', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482013833, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e1bec56f5b4cb4aeff7e049a717ba796', '189.177.226.244', 'WhatsApp/2.16.225 A', 1481393040, NULL),
('e1cb1201c099a5028718bde510150eea', '66.249.75.80', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481365144, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e27186c47fb172dd33514265110e01a8', '66.102.7.182', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479226342, 'a:3:{s:11:"captchaWord";s:3:"563";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e2846cb586f90c04f0d05c08fc0cc6c5', '189.187.208.109', 'Mozilla/5.0 (iPad; CPU OS 9_3_5 like Mac OS X) App', 1480720171, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e2caba99025fa7355d080d868959b600', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239577, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e2da95662b9f61191bb92dd4d99503d5', '187.193.18.114', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1481316135, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e302fd9aa714f19b7a8f2cfeada75444', '208.94.228.150', '=Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) A', 1479234542, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e352c4dfc2a2512aa68a2d500af4a00d', '66.249.65.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482208821, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e364739370e0947201faf0ced1c3767f', '66.249.65.152', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480631417, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e383d30c4d849e849c0c467a95af4fec', '66.249.65.196', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481942880, 'a:3:{s:11:"captchaWord";s:3:"970";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e3da1cb4cd7c9b2437bc716fe8674418', '187.144.112.193', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481929500, 'a:3:{s:11:"captchaWord";s:1:"6";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e413139aa3778e1f19bf27c9fc1ef8b2', '66.249.65.177', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479376713, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e41cfef22f20f2862e1f29812d0367ed', '66.249.65.200', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1481972560, NULL),
('e42cd6e41ed0e4f0a001e99aedc27d64', '66.249.65.125', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482011097, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e44f3e8af6de5a9baef3ed028a91de43', '189.187.219.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1479924573, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e472390b6e0b5e1486bf6e41f37d54ea', '66.249.65.171', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479429477, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e47a0311c26f685b83251cd4816f43fa', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612344, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e4bd0ef6599f81010b3224b17f02e175', '66.249.85.134', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478019721, 'a:3:{s:11:"captchaWord";s:3:"227";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e52c1d3b936d99aa2e3211f15b902774', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480987695, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e55bbf99ce372b62983eaedd3ff9baaf', '66.102.6.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478727639, NULL),
('e57cdca9b981034cdc38cbabdba2a016', '157.55.39.37', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1479570220, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e5a42790a38078f7ee2777b755267e7c', '66.249.79.110', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481756287, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e5c5f7df87dfd030bc8a4f1a44e9f255', '66.102.6.148', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1477274013, 'a:3:{s:11:"captchaWord";s:3:"852";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('e5f2fb03d6b9542b7969f422faa6f94d', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481745878, 'a:3:{s:11:"captchaWord";s:3:"718";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e5f7efa218270b11ceb1ab25b172d675', '184.88.28.14', 'Mail/3251 CFNetwork/807.1.3 Darwin/16.1.0 (x86_64)', 1480709735, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e5ff29b2b4a39e1cbb60bc5340306f92', '157.55.39.1', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480428972, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e66c879ee29e1003492601bff6a2b070', '189.177.239.107', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1477103290, 'a:2:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('e6750a851b8c746f00dd033fedcb5493', '66.249.65.200', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1481136566, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e6833021a95d20416ed144fde1dfcd7a', '66.249.90.132', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1481921411, 'a:3:{s:11:"captchaWord";s:3:"643";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e6858f15c724739f2089c5e4883b4189', '150.70.173.55', 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1;', 1480659031, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e7360347b4863d277fade404e99b8273', '66.249.65.152', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1479822172, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e7c10c2485cdc961f4ce0e363860364c', '189.164.111.62', 'Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/201', 1479342598, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e811db4e68a6bf3699127790b2628931', '66.249.65.66', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482012313, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e826bf801b7639b4f294f574af50f024', '66.249.65.66', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482122956, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e848f3e208ded7dc3d28851b60257a04', '184.88.28.14', 'Mail/3251 CFNetwork/807.1.3 Darwin/16.1.0 (x86_64)', 1479922641, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e86d27d7999527cb3e99fd3f5996d90d', '66.102.6.202', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1480810520, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e892f8ab94ae9a396381407d8678f5d1', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612359, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e8e2277b9a5dcd8527b85ef7d213eff6', '66.249.65.66', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482141424, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e902e01d9ed37e5a24d654a90079318d', '66.249.90.76', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1480277375, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e93322083eb338cf69a12ba8e191f51f', '187.193.19.171', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481828604, 'a:8:{s:11:"captchaWord";s:3:"823";s:4:"kits";a:1:{i:1;a:11:{s:7:"item_id";s:6:"Cancun";s:4:"line";i:1;s:4:"name";s:6:"Cancun";s:11:"item_number";s:0:"";s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:0:"";}}s:6:"cart_p";a:1:{i:1;a:11:{s:7:"item_id";s:16:"Crucero a Europa";s:4:"line";i:1;s:4:"name";s:16:"Crucero a Europa";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:8:"17530.00";}}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"2";s:8:"position";s:6:"inside";s:9:"data_save";a:1:{i:-1;a:11:{s:7:"item_id";s:2:"31";s:4:"line";i:-1;s:4:"name";s:16:"Crucero a Europa";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";s:8:"17350.00";s:8:"discount";i:0;s:5:"price";s:8:"17350.00";}}s:8:"customer";s:1:"5";}'),
('e9a8a4e7c5b07655ecf7a5f49796f257', '187.144.112.193', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1482238011, 'a:3:{s:11:"captchaWord";s:3:"914";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e9bda64948664c11b2d316fbb18f47b0', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480184360, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('e9c2bc31455c7ecc35470018c91626e5', '189.177.226.244', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1481405210, 'a:2:{s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";}'),
('e9eb355196340087132658e224dee6cd', '189.177.239.107', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1477696994, 'a:3:{s:11:"captchaWord";s:3:"dHm";s:9:"data_save";a:1:{i:1;a:11:{s:7:"item_id";s:2:"10";s:4:"line";i:1;s:4:"name";s:22:"Adulto Esencia Europea";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";s:1:"0";s:13:"is_serialized";s:1:"0";s:8:"quantity";d:17;s:8:"discount";i:0;s:5:"price";s:7:"2735.00";}}s:4:"kits";a:2:{i:2;a:11:{s:7:"item_id";s:16:"Esencia Europea ";s:4:"line";i:2;s:4:"name";s:16:"Esencia Europea ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:7:"2735.00";}i:1;a:11:{s:7:"item_id";s:16:"Esencia Europea ";s:4:"line";i:1;s:4:"name";s:16:"Esencia Europea ";s:11:"item_number";N;s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";i:1;s:8:"discount";i:0;s:5:"price";s:7:"2735.00";}}}'),
('ea0adfce1275c5b9d2368b648effd1d1', '189.177.226.244', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1481420394, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ea2cfd8b520229d1f3f05ee8514716d8', '66.102.6.150', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1477095050, 'a:3:{s:11:"captchaWord";s:3:"737";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('ea6b9106a00a753b3585486ef97fd650', '66.102.7.150', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478539427, NULL),
('ea7a561d11c84506cbf2254b6b754b82', '189.177.233.214', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1476912464, 'a:6:{s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";s:11:"captchaWord";s:0:"";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";s:6:"cart_p";a:0:{}}'),
('ea9360410160c07387dbd9a0348cc3a0', '66.249.80.18', 'Mozilla/5.0 (Linux; Android 4.4.2; LG-D331 Build/K', 1476626902, 'a:2:{s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";}'),
('eab13f885fa7175628dd9fd370ebb875', '177.246.158.45', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1480658605, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('eacc2104babd6acccf5c9b89641a4399', '189.177.81.197', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1479424794, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('eae6ee444201f81546e7170becebefeb', '189.177.245.196', 'WhatsApp/2.16.225 A', 1481831675, NULL),
('eb0b326a64fd33a63443e8bff772835d', '189.187.108.161', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481071446, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('eb529c2c88d41dd7b1b7c0f9e6259826', '187.193.63.102', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_3_2 like Mac ', 1481847152, 'a:3:{s:11:"captchaWord";s:3:"735";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('eb6e04ff73826f594237e543dd36d051', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481242971, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('eb77b679259e8174898927ab03ae7597', '189.166.52.5', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480607662, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('eba9031cd0f8a0181af8053a2800a5c9', '66.102.7.150', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479321666, NULL),
('ebb19d20e7368124df483331e7915700', '66.249.65.70', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482120048, 'a:3:{s:11:"captchaWord";s:3:"wWB";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ebfa4191a4fecf114599c086343d6929', '66.249.85.134', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1477678986, 'a:3:{s:11:"captchaWord";s:3:"367";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ec4ee30c46375cd4cba03f7ab83a9ae9', '187.144.126.231', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1479840412, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ec714535102c721de826280f0fd98fad', '157.55.39.204', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1479251625, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ecb9e773dd0505cf19983acd42c825a6', '187.144.120.30', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1479253546, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ed02e98ee435b55724ddb94c0e297009', '40.77.167.62', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480518908, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ed404a9988317cc6b40ed6b1b85c6e8d', '40.77.167.38', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481334992, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ed44facff7bd33cf4ed04d13454a914f', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480110012, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ed4a209a31d506a52a248a4c3ffb812f', '54.203.197.73', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1481788115, 'a:3:{s:11:"captchaWord";s:3:"301";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ed9181a737805b294d0f8fc7c646eec2', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612386, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('edc19309ea930372e0cad07712d91acd', '157.55.39.55', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1481414836, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('edcb9a11d56e06bd04b6d6dd272a28e2', '66.102.7.180', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479226342, NULL),
('eddbd139e2ec1b8f419b9e06f72dbecf', '207.46.13.82', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480945807, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ee0735bc6eb17ad7923b998b2608d965', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480289164, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ee242fd4aca19c0c084e723997c2cc2e', '189.187.150.204', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1480057023, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ee953ff205b497826b5749a403fee8df', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481762502, 'a:3:{s:11:"captchaWord";s:3:"108";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ee9d8a3e341079593e043d7f18a8c56b', '187.193.85.246', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480109775, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('eeb7e28bc658c9be106d969e6e24a330', '66.102.6.146', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1477515702, 'a:3:{s:11:"captchaWord";s:3:"213";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('eee74f7530b4bb9cb994b9ff1676b927', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481415376, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ef4ba6023f87bd88372941a019e4c9f8', '66.102.6.114', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1480083802, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ef4cec117c3ba05fae9f4083e47e6fb0', '173.252.124.20', 'facebookexternalhit/1.1 (+http://www.facebook.com/', 1480301778, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ef6a2637db2121a03eaad4f032ba3032', '189.177.226.244', 'WhatsApp/2.16.225 A', 1481587262, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ef7f63838d407b62f2f546b04c55cb87', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481601723, 'a:3:{s:11:"captchaWord";s:3:"785";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('efc296b57411c6960cc998a46a9ab0ca', '141.8.143.233', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://ya', 1480743672, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('efc412763aaea3e14a5da51b09c9816d', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481219666, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('eff23da747df2fc9f234d457369ac88d', '211.6.122.123', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1479507137, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f01969370597cef7abf5ad2ece6d5db8', '173.252.124.15', 'facebookexternalhit/1.1 (+http://www.facebook.com/', 1479676065, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f01b408948b4a5d8d84df0abf6a7a233', '187.144.112.193', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481862785, 'a:3:{s:11:"captchaWord";s:3:"162";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f021ec6a359fdeaad31ba02b24354f22', '187.144.112.193', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1482123894, 'a:3:{s:11:"captchaWord";s:3:"448";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f0259c2ba8e0193c2e7ed8678c359203', '187.144.120.30', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1479419774, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f03bc12bf8e8281020d90854b7f5cc1b', '66.249.90.74', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1480106448, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f065cfeaa58859bd8809df9e7a7ef138', '189.187.108.161', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480969225, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f06c7ffde623959da29cd4703c0c987f', '189.187.53.115', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1481726510, 'a:3:{s:11:"captchaWord";s:3:"833";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f0c11dcf1ed1e5461d944f5961510d51', '187.193.37.79', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1480819820, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f101d90cc812ac92015c492f14ed7353', '200.140.55.223', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480109155, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f19dba9b8f67dc22f3077f45719743ad', '199.59.150.180', 'Twitterbot/1.0', 1481656760, 'a:3:{s:11:"captchaWord";s:3:"731";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f1d9e2fc953b6893d03478701ab5b8b0', '189.187.196.244', 'Mozilla/5.0 (iPad; CPU OS 9_3_2 like Mac OS X) App', 1480118924, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f1f10f4bbf6e5945dbf593641a4e1cc6', '66.249.65.200', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481956895, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f21c0b3ffbfdade63c5db4e15603863a', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481239218, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f227a16684a91144fee35c0c7e25e9d4', '66.249.85.136', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-G935F Build/', 1477423910, 'a:5:{s:11:"captchaWord";s:3:"863";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";}'),
('f2ceec7628418dcb993ea6b6951a90f2', '66.102.6.114', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479141985, 'a:3:{s:11:"captchaWord";s:3:"743";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f2ee37f579ced2213a051f6661940d5d', '187.144.112.193', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1482003133, 'a:3:{s:11:"captchaWord";s:3:"517";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f3043e295db64e6f77533c40c27818cb', '189.177.239.107', 'WhatsApp/2.16.225 A', 1477156537, 'a:2:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('f30618f8ada5d5662f4c988a2cd86207', '66.102.7.160', 'Mozilla/5.0 (Linux; Android 4.4.2; LG-D331 Build/K', 1481392400, 'a:5:{s:11:"captchaWord";s:3:"252";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";}'),
('f3367421c18f5e4d1b6d39185afd656f', '66.249.65.66', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482028133, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f3a99e309167330fa437c1591d8ee3a8', '157.55.39.1', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480436559, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f3b33229bff6202e33db66a773a8e235', '189.187.221.19', 'Mozilla/5.0 (iPad; CPU OS 10_1_1 like Mac OS X) Ap', 1479267862, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f3c2598994ad138b78c6479cde3f3899', '66.102.8.154', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; r', 1481995193, 'a:3:{s:11:"captchaWord";s:3:"693";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f3ca6949c249334c0939a0fdf79182b8', '66.249.65.156', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480488047, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f423b9129ada2af4a2cf6a3575ec143c', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480953454, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f42c96b8dac6e10aa5c85f67a4cf5a95', '37.130.227.133', 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1', 1480657533, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f44a59f51b3af329731938c30efe7f90', '184.88.28.14', 'Mail/3251 CFNetwork/807.1.3 Darwin/16.1.0 (x86_64)', 1482181189, 'a:3:{s:11:"captchaWord";s:3:"628";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f462c8693f2b0a38d23347a286c63c11', '198.140.5.38', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481218065, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f47c7341046f4c794097b1ba5ce66e74', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479290991, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f47f09bc12965704512f041d9e8acffa', '189.187.108.161', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481073265, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f4a91ada79eb1c5c24af8fac3e4771bf', '189.166.39.241', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1481044990, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f4cb8be962748b382a7fb9195fd0ceab', '66.220.146.26', 'facebookexternalhit/1.1 (+http://www.facebook.com/', 1482167771, 'a:3:{s:11:"captchaWord";s:3:"472";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f4df0b76e864c89f55f09ab9ab05a792', '181.48.93.106', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.30', 1481034828, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f4fa033a6c46b15e41e8d77c9c68dc77', '189.187.170.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1481748052, 'a:3:{s:11:"captchaWord";s:3:"245";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f504088c69ac485b872f9c56a7159a9e', '189.166.52.5', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480445335, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f53696fd88392098f5429a39239eb824', '207.46.13.82', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480945806, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f5502cbcb60c5b4d746c3c5c3f4da792', '66.249.65.171', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479588411, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f556e60e5fc53daed3832d4f3d8fd985', '189.166.52.5', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) Ap', 1480609181, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f5cb54e4b0244142c227a22de74876d2', '189.166.35.200', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1479960020, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f64f75a59f05b786809f00972d27d850', '207.46.13.23', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480611740, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f6cba51ff5f62efe52abc18c19a1bb89', '210.163.39.72', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1481861554, 'a:3:{s:11:"captchaWord";s:3:"670";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f74b70d8cd6e965bb1b7cf7e77fb7dcb', '66.102.6.118', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478727639, 'a:3:{s:11:"captchaWord";s:3:"467";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f7d0ab7a30c06bc41c32ebd220adb9b0', '189.177.233.214', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1477065750, 'a:2:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('f7f7fcd21ccadadcf8e228052fce9338', '66.249.65.192', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1480816970, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f81100a09937d41249cb615212fbd796', '209.95.183.184', '=Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) A', 1480099205, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f82f93b7bd28a62d0bb2937a9dd0c5b7', '167.114.172.225', 'Mozilla/5.0 (Linux; Android 4.05; Galaxy Nexus Bui', 1482101549, 'a:3:{s:11:"captchaWord";s:3:"772";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f83923ca48b9082899bbe904cc6dba96', '157.55.39.231', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1479942579, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f85f42547833016fd6d61c2c4c7a0d09', '189.187.196.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480349215, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f8690197feb1c35f891ede03f6cc0999', '54.196.112.250', 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv', 1479299780, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f89524be0e6096a54e76f4611bbe15b9', '66.249.65.152', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479291575, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f8ab83c1f3c69c0188e6576d73f69d54', '211.0.145.110', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100', 1481760783, 'a:3:{s:11:"captchaWord";s:3:"529";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f8c717a93b733f0a56c3a7c26619542e', '54.225.47.180', 'Mozilla/5.0 (compatible; linkdexbot/2.2; +http://w', 1481308024, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f8f27979ef2105326ffb5e8f23c406dd', '189.187.197.58', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1476313198, 'a:6:{s:11:"captchaWord";s:3:"288";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"2";s:8:"position";s:6:"inside";s:6:"cart_p";a:0:{}}'),
('f8f661fc302893e52c472e4863ced75e', '189.187.210.164', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1479413417, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f8fcffa0d5d0b58befc1c98a6ab5086d', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1481242989, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f900966fae1b1d0e4342b006007a903d', '189.177.184.117', 'WhatsApp/2.16.310 A', 1477148633, 'a:2:{s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('f9cbc3465294f79d9a10f18d7fcefa17', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482063913, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('f9e29a921d5c57ab54de2b7a2a2d274f', '189.177.226.244', 'WhatsApp/2.16.225 A', 1479239583, NULL),
('fa88f07a1aa8e5e1b42a211505c3434c', '66.102.7.154', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1478878532, 'a:3:{s:11:"captchaWord";s:3:"698";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fab28510d46443d68ecbcfc70bb3875f', '66.249.90.78', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1479681311, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fad804acdc7a8305f06cef336d3ed790', '187.144.112.193', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1482184988, 'a:3:{s:11:"captchaWord";s:3:"400";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fb4f62bd6a0a1530885eb4a89008c2e5', '189.177.233.214', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1476312207, 'a:5:{s:11:"captchaWord";s:3:"900";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";s:9:"person_id";s:1:"1";s:8:"position";s:6:"inside";}'),
('fb997207a11bbe1696842e5033cadc5f', '209.95.165.205', '=Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) A', 1481284089, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fba608eb872e50af99535197f5b3e0df', '66.249.73.166', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1481734625, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fbfab8e6e0b9bdff939f3cb8fe4ef12e', '189.177.226.244', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1480704442, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fc0e35df67448c0352613436798852ff', '200.94.102.2', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1479430686, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fc1ff56edee48f8945af4849cfd4b764', '187.193.8.233', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1480640912, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fc31d2adb323a482aac9000c0adef36c', '157.55.39.90', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480831356, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fc347e4c86d32107abe6d27aed96e9ef', '66.249.80.22', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1481732936, 'a:3:{s:11:"captchaWord";s:3:"193";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fc802c447d88dc88af9f61ad4d5aa08b', '66.249.65.148', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479290976, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fc8e9724f2d2af033921fbe885b99cc4', '66.249.65.125', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/', 1482022375, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fce9c5b3b7a2a3a006660ab332987fa3', '189.166.39.241', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480987662, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fcfcd8a929a6b935594c8564148d3fdc', '66.249.65.125', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1482066963, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fd24dcba77a98862d8548afcbbc010aa', '66.249.69.239', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480054264, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fd37a5d9c9adcbe2e49c5b43fdd3ce0c', '66.102.7.178', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1479226343, NULL),
('fd8801f0ca1d843a94504df9aa53a3d4', '187.237.231.37', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac', 1479499523, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}');
INSERT INTO `ospos_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('fd98d53e746d43679776c103f1a21203', '189.177.239.107', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/5', 1477438614, 'a:6:{s:11:"captchaWord";s:3:"579";s:4:"cart";a:0:{}s:9:"data_save";a:2:{i:1;a:1:{s:8:"quantity";i:0;}i:2;a:1:{s:8:"quantity";i:0;}}s:4:"kits";a:2:{i:3;a:11:{s:7:"item_id";s:1:"4";s:4:"line";i:3;s:4:"name";s:1:"4";s:11:"item_number";s:0:"";s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";s:0:"";s:8:"discount";i:0;s:5:"price";s:0:"";}i:1;a:11:{s:7:"item_id";s:1:"4";s:4:"line";i:1;s:4:"name";s:1:"4";s:11:"item_number";s:0:"";s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";N;s:8:"discount";i:0;s:5:"price";s:0:"";}}s:6:"cart_p";a:1:{i:1;a:11:{s:7:"item_id";s:1:"4";s:4:"line";i:1;s:4:"name";s:1:"4";s:11:"item_number";s:0:"";s:11:"description";s:0:"";s:12:"serialnumber";s:0:"";s:21:"allow_alt_description";i:0;s:13:"is_serialized";i:0;s:8:"quantity";N;s:8:"discount";i:0;s:5:"price";s:0:"";}}s:4:"tots";s:1:"0";}'),
('fdda2776c8d371e620fac39ed4519f06', '66.249.65.171', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479376183, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fe2e192166a9c5021596cb40ea8cdc1e', '207.46.13.28', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1482106794, NULL),
('fe547427ffe552dfb97f767ec8c886c2', '187.144.134.51', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480885730, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fe69c07a42819f1ba6d7a92e15dc182b', '66.249.90.74', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1480451549, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fe85195aeb4134847fcbad096c3182ef', '66.249.65.156', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1479734363, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fe86e1bdc986458d6cf142c6ca837492', '66.249.90.128', 'AdsBot-Google (+http://www.google.com/adsbot.html)', 1481229436, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fe89dde2c33807eed2248a088d556d85', '81.109.85.9', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj', 1481612342, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fe94acd9a347b1703a4b58793cd16357', '187.144.134.51', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac O', 1480883315, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fe9eb2d9832431ccd92214198a91ae5b', '207.46.13.193', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1480247407, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ff0067c1bebc0fc12ca533f26c848f5c', '189.177.233.214', 'WhatsApp/2.16.225 A', 1477416554, 'a:3:{s:11:"captchaWord";s:3:"579";s:4:"cart";a:0:{}s:4:"tots";s:1:"0";}'),
('ff10793549146e4304099828a228266c', '66.249.65.106', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480837646, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ff5049aecdec0f5f5198aba643f435d6', '66.249.65.196', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://ww', 1480696249, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ff667327a8ba0772a9743922c9b412c7', '206.41.161.145', '=Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) A', 1479663205, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ff97931b589b319989e6b568a29434ed', '189.187.150.200', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (K', 1481313778, 'a:3:{s:11:"captchaWord";s:0:"";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('ffe2348e4255811fba3313b8dcac6d23', '52.71.155.178', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) Ap', 1480647354, 'a:2:{s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}'),
('fffe5c267e683197f00f96153f5a923e', '66.249.90.57', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS', 1482092921, 'a:3:{s:11:"captchaWord";s:3:"762";s:6:"cart_p";a:0:{}s:4:"tots";s:1:"0";}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_suppliers`
--

CREATE TABLE IF NOT EXISTS `ospos_suppliers` (
  `person_id` int(10) NOT NULL,
  `company` varchar(255) NOT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `account_number` (`account_number`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_vehicles`
--

CREATE TABLE IF NOT EXISTS `ospos_vehicles` (
  `vehicle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `modelo` varchar(60) NOT NULL,
  `placas` varchar(60) NOT NULL,
  `kms` int(255) NOT NULL,
  `capacidad` int(255) NOT NULL,
  PRIMARY KEY (`vehicle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ospos_customers`
--
ALTER TABLE `ospos_customers`
  ADD CONSTRAINT `ospos_customers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`);

--
-- Filtros para la tabla `ospos_employees`
--
ALTER TABLE `ospos_employees`
  ADD CONSTRAINT `ospos_employees_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`);

--
-- Filtros para la tabla `ospos_inventory`
--
ALTER TABLE `ospos_inventory`
  ADD CONSTRAINT `ospos_inventory_ibfk_1` FOREIGN KEY (`trans_items`) REFERENCES `ospos_items` (`item_id`),
  ADD CONSTRAINT `ospos_inventory_ibfk_2` FOREIGN KEY (`trans_user`) REFERENCES `ospos_employees` (`person_id`);

--
-- Filtros para la tabla `ospos_items`
--
ALTER TABLE `ospos_items`
  ADD CONSTRAINT `ospos_items_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `ospos_suppliers` (`person_id`);

--
-- Filtros para la tabla `ospos_items_taxes`
--
ALTER TABLE `ospos_items_taxes`
  ADD CONSTRAINT `ospos_items_taxes_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `ospos_item_kit_items`
--
ALTER TABLE `ospos_item_kit_items`
  ADD CONSTRAINT `ospos_item_kit_items_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `ospos_item_kits` (`item_kit_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ospos_item_kit_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `ospos_permissions`
--
ALTER TABLE `ospos_permissions`
  ADD CONSTRAINT `ospos_permissions_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_employees` (`person_id`),
  ADD CONSTRAINT `ospos_permissions_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `ospos_modules` (`module_id`);

--
-- Filtros para la tabla `ospos_receivings`
--
ALTER TABLE `ospos_receivings`
  ADD CONSTRAINT `ospos_receivings_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `ospos_employees` (`person_id`),
  ADD CONSTRAINT `ospos_receivings_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `ospos_suppliers` (`person_id`);

--
-- Filtros para la tabla `ospos_receivings_items`
--
ALTER TABLE `ospos_receivings_items`
  ADD CONSTRAINT `ospos_receivings_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`),
  ADD CONSTRAINT `ospos_receivings_items_ibfk_2` FOREIGN KEY (`receiving_id`) REFERENCES `ospos_receivings` (`receiving_id`);

--
-- Filtros para la tabla `ospos_sales`
--
ALTER TABLE `ospos_sales`
  ADD CONSTRAINT `ospos_sales_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `ospos_employees` (`person_id`),
  ADD CONSTRAINT `ospos_sales_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `ospos_customers` (`person_id`);

--
-- Filtros para la tabla `ospos_sales_items`
--
ALTER TABLE `ospos_sales_items`
  ADD CONSTRAINT `ospos_sales_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`),
  ADD CONSTRAINT `ospos_sales_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`);

--
-- Filtros para la tabla `ospos_sales_items_taxes`
--
ALTER TABLE `ospos_sales_items_taxes`
  ADD CONSTRAINT `ospos_sales_items_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales_items` (`sale_id`),
  ADD CONSTRAINT `ospos_sales_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`);

--
-- Filtros para la tabla `ospos_sales_item_kits`
--
ALTER TABLE `ospos_sales_item_kits`
  ADD CONSTRAINT `ospos_sales_item_kits_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`),
  ADD CONSTRAINT `ospos_sales_item__kits_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `ospos_item_kits` (`item_kit_id`);

--
-- Filtros para la tabla `ospos_sales_payments`
--
ALTER TABLE `ospos_sales_payments`
  ADD CONSTRAINT `ospos_sales_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`);

--
-- Filtros para la tabla `ospos_sales_suspended`
--
ALTER TABLE `ospos_sales_suspended`
  ADD CONSTRAINT `ospos_sales_suspended_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `ospos_employees` (`person_id`),
  ADD CONSTRAINT `ospos_sales_suspended_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `ospos_customers` (`person_id`);

--
-- Filtros para la tabla `ospos_sales_suspended_items`
--
ALTER TABLE `ospos_sales_suspended_items`
  ADD CONSTRAINT `ospos_sales_suspended_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`),
  ADD CONSTRAINT `ospos_sales_suspended_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales_suspended` (`sale_id`);

--
-- Filtros para la tabla `ospos_sales_suspended_items_taxes`
--
ALTER TABLE `ospos_sales_suspended_items_taxes`
  ADD CONSTRAINT `ospos_sales_suspended_items_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales_suspended_items` (`sale_id`),
  ADD CONSTRAINT `ospos_sales_suspended_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`);

--
-- Filtros para la tabla `ospos_sales_suspended_payments`
--
ALTER TABLE `ospos_sales_suspended_payments`
  ADD CONSTRAINT `ospos_sales_suspended_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales_suspended` (`sale_id`);

--
-- Filtros para la tabla `ospos_suppliers`
--
ALTER TABLE `ospos_suppliers`
  ADD CONSTRAINT `ospos_suppliers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
