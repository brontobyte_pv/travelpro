Como instalarlo
-------------------------
/**Montado de base de datos MySQL**/
1. Crear una base de datos que se llame brontobyte en MySQL. 
2. Importar el estructura.sql ubicado en /database/ para crear las tablas necesarias
3. Colocar los archivos de la aplicacion en el servidor web
4. Modificar el archivo database.php ubicado en /config/ para conectarse a la base de datos
5. Vaya al la direccion http://localhost/ en su navegador o la url que le haya dado
6. Inicia sesión
7. Uusario y contraseña default:
   -nan
   -admin


Proceso de montado de CashControl a una Plantilla HTML.
	
	-Importante: Antes de realizar cada paso lea Todo el paso , no obie las notas. 

	Montado de archivos necesarios:
		1.-Tener una copia de los archivos de la aplicacion CashControl
			-y una copia de la Plantilla a montar (Esencialmente los views, hojas de estilo css y archivos JavaScript).
		
		2.-Desde la raiz de la carpeta de cash control nos dirijimos hacia \css
			-Aqui simplemente copiamos los archivos que contenga la carpeta css de nuestra plantilla (ignorando la carpeta "CashControl").
		
		3.-Desde la raiz de la carpeta de cash control nos dirijimos hacia \js
			-hacemos lo mismo que en el paso anterios solo que con los archivos de la carpeta js de nuestra plantilla (Ignorando la carpeta CashCoontrol).
		
		4.-Copiamos las imagenes que esten dentro de la carpta "images" de nuestra plantilla a la carpeta del mismo nombre en CashControl (Ignorando las carpetas dentro y archivos)
		
		5.-En el caso de que e la carpeta raiz de nuestra plantilla exista una carpeta llamada "fonst", Sera necesario copiarla a la raiz de CashControl
		
		6.-Desde la raiz de la carpeta de cash control nos dirijimos hacia \aplicacion\views
			-Copiamos los archivos con estencion .html que contenga nuestra plantilla.
				-Nota:los archivos antes de ser cambiados de extencion son mucho mas visibles que despues, recomiendo anotar cuales son para tener mas facilidad al momento de recurrir a dichos en pasos futuros.
			-Cambiamos dicha extencion(.hmtl) por .php , y el archivo llamado "index" lo renombramos por "front".
			
		
		-Nota:Por lo general las plantillas no contienen mas archivos , de lo contrario consulte con su administrador
			
		7.-Cambiamos el nombre de la carpeta contenedora de nuestra copia de CashControl por una que identifique a la plantilla montada en CashControl. Ejemplo:"CashControl_Boxing" ,"CashControl_Decolux", etc..

	Cambios o Adaptaciones al codigo de CashControl:
	
		Adaptacion de Views:
			1.-Desde la raiz de la carpeta de cash control nos dirijimos hacia \aplicacion\views\front
				-Dentro de cada view proveniente de nuestra plantilla original se tienen que actualizar las rutas por las cuales obtiene ciertos archivos necesarios para la carga de nuestro view(archivos css, js, imagenes, etc...) a las rutas actuales donde se encuentran dichos archivos, esto se hace pegando justo donde empieza la ruta de el archivo requerido cierto Script de php -" <?php echo base_url();?>  "- en cada propiedad href y src.
					Ejemplo: 
						-	<link href="<?php echo base_url();?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
						-	<script src="<?php echo base_url();?>js/jquery-1.11.1.js">
						-	<a href="<?php echo base_url();?>index.html">
						-	<img src="<?php echo base_url();?>images/ta2.jpg" class="img-responsive" />	
						
							-Nota:Para facilitar la busqueda de dichas pripiedades (href ,src), almenos en Notepad++ con el comando Ctrl+F en la pestaña Mark colocamos dichos elementos uno por uno y seleccionamos buscar todo. 
						
							-Nota:El Script de php y la ruta original estan dentro de las comillas "" pertenecientes a la propiedad href o src de la etiqueta de html correspondiente.
			
						-Nota:Si existen href o src con una ruta que no haga referencia a un archivo que se encuente en los archivos de la pagina no es necesario el script.
							Ejemplo:
								-	<a href="#home" class="active scroll">
								-	<a href="mailto:example@mail.com">
								-	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d158858..........................">
								-	<a href="http://w3layouts.com">
									
			2.-Guardamos.
			
		Adaptacion de Controladores Basico:
			1.-Desde la raiz de la carpeta de cash control nos dirijimos hacia \aplicacion\config
				-Nos dirijomos al archivo llamado "routes.php" a la linea 43 aprox , justo donde se declara - $route['default_controller'] = "login"; - cambiamos(en este caso) "login" por "front", quedaria - $route['default_controller'] = "front"; -.
				-Guardamos.
			
			2.-Desde la raiz de la carpeta de cash control nos dirijimos hacia \aplicacion\controlers
				-Nos dirijimos al archivo llamado "front.php"
				-Dentro creamos las funciones necesarias para cada uno de los views que pertenescan a nuestra plantilla original con el siguiente formato:
					
					function nombre_de_mi_view (){
						$this->load->view("nombre_de_mi_view");
					} 
					-Nota:Para la carga de el view front no se necesita hacer una funcion nueva ya que en la funcion index ya lo hace por defecto.
					-Nota:Cuando mando llamar a mi view solamente pongo su nombre sin su extencion.
				-Guardamos.
		
		
		Adaptacion de Cambio de Idioma:
			1.-Primero debemos identificar que texto queremos que cambie al momento de hacer el cambio de idioma, para ello
				-Dentro de nuestro view  al cual se le quiere aplicar el cambio de idioma (desde la raiz, \aplicacion\views), identificar todo texto que el usuario ve, 
				Ejemplo:
					
					-	<a href="#"><span></span> Lorem Ipsum has been </a>          //Lorem Ipsum has been , sería el texto a cambiar
					-	<h2>GET HELP CARING FOR YOUR LOVED ONE</h2                   //GET HELP CARING FOR YOUR LOVED ONE , sería el texto a cambiar
					-	<title>EldersCare a Charity Category Flat Bootstrap Responsive Website Template | Home :: w3layouts</title>			//EldersCare a Charity Cate....: w3layouts  ,  seria el texto a cambiar
					
				-Asignamos un nombre que identifique ese texto y el view de donde proviene. Ejemplo: "title_about" , "text4_contact" , "header1_front"
				
			2.-Desde la raiz de la carpeta de cash control nos dirijimos hacia \aplicacion\language
				-Entramos a la carpeta del idioma a modificar.
				-Creamos un archivo para cada view proveniente de la plantilla original con el nombre  -  "nombreDeMiView_lang.php" , Ejemplo:  about_lang.php.
				-Dentro del archivo. Primero colocamos el siguiente scritp que contendra todas nuestras variables de idioma -	<?php ?>	-
				-Dentro de este script debemos declarar nuestras variables de idioma de la siguiente manera;
					<?php
						$lang['about_title']='Valor asignado a la variable about_title';
						$lang['contact_text4']='Valor asignado a la variable contact_text4';
						$lang['front_header1']='Valor asignado a la variable front_header1';
						$lang['gale.................
					?>
					
					-Nota:Cada archivo de idioma debe contener las variables para el view por el cual fue creado dicho archivo, Ejem: en el archivo "about_lang.php" las variables que contendra seran para el view about.php y seran parecias a esto: about_title , abou_header1 , abou_text5, y asi para cada archivo.
					
				-Guardamos.
			
			3.-Se debe cargar el archivo de lenguas y la libreria de lenguas, para esto vamos a (desde la raiz) \aplicacion\config\autoload.php
				-Dentro de este archico nos dirijimos al areglo que contiene las librerias a cargar : $autoload['libraries'] = array('.... , y colocamos dentro ,  
				'Lang' respetando las demas existentes ,  $autoload['libraries'] = array('database','pagination','...','Lang');
				-Mas abajo , donde se cargan las lenguas colocamos de la msima manera los archivos de lengua que creamos , pero solo la parte antes de "_lang" , Ejem: $autoload['language'] = array('front','about','menu','contact','groups'); 
			
			4.-Teniendo declaradas las variables de idioma y las librerias y archivos de lengua cargados, sigue sutituir los  textos en el view por las variables de idioma.
				-donde queramos hacer el cambio de idioma , sustituimos el texto por	-	<?php echo $this->lang->line('mi_variable_de_idioma');?>	-
				Ejemplo: 
					-	<h3><?php echo $this->lang->line('front_title');?></h3>
					-	<a><?php echo $this->lang->line('header_tittle4'); ?></a>

			5.-Lo siguiente seria hacer el switch del idioma seleccionado 
				-Al principio de toda la clase, despues de donde se declara la clase, dentro de la funcion __construct, debajo de parent::__construct(); , coloque al siguiente linea de codigo
				
					$this->load->library('lang_lib');		//carga la libreria de idioma necesaria para el cambio de idioma
				
				-Dentro del view a a modificar se deben agregar los "botones" necesarios para los idiomas requeridos , algo como...
				
					-	<a href="<?php echo base_url();?>index.php/front<?php echo $link;?>/spanish">SPANISH</a>		//el echo que se le hace a la variable $link se explica mas adelante 
					-	<a href="<?php echo base_url();?>index.php/front<?php echo $link;?>/english">ENGLISH</a>
					
						-Nota: Estas etiquetas deben ser colocadas de preferencia al principio del documento, dentro del header o algo similar.
				
				-Dentro del controler de dicho view, en su respectiva funcion, se debe hacer el siguiente cambio respetando el codigo existente
				
					function funcionDeMiView($idioma){				//"funcion de mi view" es el nombre de su funcion
						$ci =& get_instance();
						$ci->lang->switch_to($idioma);
						$data['link']= '/nombreDeMiview';			//"nombreDeMiview" es la variable que indicara a que funcion de que view se le hara el cambio de idioma
						$this->lang_lib->set_idioma($idioma);
						if($datos_del_form['form_susces']==0){
							$this->load->view("miView",$data);
						}
					}
				
				, asi para cada funcion de mis views, exepto la funcion index.
				-Guardamos.
				
		Creacion de un Modulo para CashControl
			1.-En la base de datos predefinida para CashControl, En la tabla ospos_modules.
				-Se debe insertar los datos del nuevo modulo a crear
				-En el campo module_lang_key se insertaria el nombre de la variable de lengua que identifica al modulo, del modo "module_NombreDeMiModulo", el nombre del modulo debe estar en plurarl.
				-En el campo desc_lang_key es lo mismo que el paso anterior pero es de la manera module_NombreDeMiModulo_desc , el nombre del modulo debe estar en plurarl
				-En el campo sort se debe colocar un numero que sea mayor por 10 al numero mayor anterior . Si el numero mayor antes de insertar tu modulo es 90, el numero de tu nuevo modulo debera ser 100.
				-El el campo module_id simplemente coloca el nombre de tu modulo en plurarl
 				
 			2.-En la tabla permisions.
 				-Se deben dar permisos al nuevo modulo creado.
 				-Insertamos en la tabla, en el module_id el modulo creado, y en person_id la persona a quien se le daran permisos en ese modulo.

 			3.-En la base de datos predefinida para CashControl, En la tabla ospos_modules.
 				-Se debe crear una nueva tabla llamada ospos_NombreDeMiMOdulo con los campos que se necesiten.

 			4.-Desde la raiz nos dirijimos hacia \aplicacion\language
 				-En todas las carpetas de idioma que se necesiten, en el archivo module_lang-php se deben crear 2 variables de idioma,
 					$lang['module_NombreDeMiModulo']='Nombre de mi modulo';
					$lang['module_NombreDeMiModulo_desc']='Add, Update, Delete and Search Nombre de mi modilo';

					-Nota:el Valor de la variable Deve estar en el idioma correspondiente.
				-Guardamos.

			5.-Desde la raiz nos dirijimos hacia \aplicacion\views
				-Creamos una carpeta con el nombre del modulo y copiamos los archivos form.php y manage.php de un modulo ya existente, pueden ser copiados del modulo existente "services".

			6.-Desde la raiz nos dirijimos hacia \aplicacion\controllers
				-Creamos un control nuevo con el nombre de mi nuevo modulo, Este tambien puede ser copiado de uno ya existente, uno como services.php
				-Dentro del archivo creado, se deven hace runas modificaciones.
					a)Cambimos el nombre de la clase por el de el nombre de mo modulo
					b)Dentro de la funcion _construct , dentro de parent::_construct("nombredemimodulo"); cambiamos el nombre que tenga por el del modulo  
					c)En la funcion index , cambiar la linea donde se genera la variable manage_table , se deben cambiar el nombre de las funciones que generan los datos:

						-Esta seria laoriginal:
						$data['manage_table']=get_services_manage_table($this->Service->get_all($config['per_page'], $this->input->get('per_page')),$this);
						
						-Esta seria la modificada:
						$data['manage_table']=get_recursos_manage_table($this->Recurso->get_all($config['per_page'], $this->input->get('per_page')),$this);

							-Nota:En el Primer parametro de la funcion get_recursos_manage_table() se llama a una funcion de un modelo que se creara despues, el nombre del modulo deve estar en singular.

					d)En la funcion index, cambiar la direccion del view quecarga por el correspondiente del modulo.

					e)En la funcion view se deven camviar las ocurrencias de el nombre del modulo por el nombre del modulo nuevo .

					f)Las demas funciones como save son relativas a las necesidades de el modulo. y seran modificadas dependiendo de esas mismas.

			7.-Desde la raiz nos dirijimos hacia \aplicacion\models
				-Se debe crear un modelo de nuestro modulo, para ello simplemente copiamos uno ya existente como el de services, y lo nombramos como el modulo a crear pero en singular.
				-Se le hacen cambios a el nombre de la clase, a el nombre de la tabla donde se insertaran los datos y a las condiciones de agrupamiento y orden de la consulta.
				-Desde la raiz nos dirijimos hacia \aplicacion\config\config.php y en donde se hace la carga de los modelos se tiene que colocar el modelo que se a reado con la primera letra en mayusculas.

			8.-Desde la raiz nos dirijimos hacia \aplicacion\helpers\table_helper.php
				-En este archivo, hasta el final, , se deben crear las funciones de el helper para mi modulo creado, se puede copiar las funciones de un helper existente pero adaptandolo a las necesidades del nuestro, estan separadas por comentarios que indican donde empiezan cada grupo de funciones.
				-Se tienen que remplazar los nombres del modelo de las finciones originales por los de nuestro modelo, cuando aparescan en plural, en plural, y cuando aparescan en singular, en sungular.


				
