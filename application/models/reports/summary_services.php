<?php
require_once("report.php");
class Summary_services extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array($this->lang->line('report_service_fecha'), $this->lang->line('report_service_nom_cliente'), $this->lang->line('report_service_nom_asignado'), $this->lang->line('report_service_motivo'), $this->lang->line('report_service_accion'), $this->lang->line('report_service_reporte'), $this->lang->line('report_service_estado'), $this->lang->line('report_service_duracion'));
	}
	
	public function getData(array $inputs)
	{
		$this->db->from('services');
		$this->db->where('fecha BETWEEN "'. $inputs['start_date']. '" and "'. $inputs['end_date'].'"');
		if ($inputs['sale_type'] == 'sales')
		{
			//$this->db->where('payment_amount > 0');
		}
		elseif ($inputs['sale_type'] == 'returns')
		{
			//$this->db->where('payment_amount < 0');
		}
		$this->db->group_by("service_id");
		return $this->db->get()->result_array();
	}
	
	public function getSummaryData(array $inputs)
	{
		$this->db->select('sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit');
		$this->db->from('sales_items_temp');
		$this->db->join('items', 'sales_items_temp.item_id = items.item_id');
		$this->db->where('sale_date BETWEEN "'. $inputs['start_date']. '" and "'. $inputs['end_date'].'"');
		if ($inputs['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($inputs['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		return $this->db->get()->row_array();
	}
}
?>