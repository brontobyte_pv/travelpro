<?php
class Pedido extends Model
{
	
	
	function get_all($limit=10000, $offset=0)
	{
		$this->db->from('pedidos');
		$this->db->where('estado','pendiente');
		$this->db->order_by("pedido_time", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	function get_all_filtered($limit=10000, $offset=0)
	{
		$this->db->from('pedidos');
		$this->db->where('estado','Hecho');
		$this->db->order_by("pedido_time", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	
	function count_all()
	{
		$this->db->from('pedidos');
		return $this->db->count_all_results();
	}
	function get_info($pedido_id)
	{
		$this->db->from('pedidos');	
		$this->db->where('pedido_id',$pedido_id);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('gastos');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}
	
		
	function save ($items,$customer_id,$payments,$total,$pedido_id=false)
	{
		

		//Alain Multiple payments
		//Build payment types string
	
		$pedido_data = array(
			'pedido_time' => date('Y-m-d H:i:s'),
			'customer_id'=> $customer_id,
			'payment_type'=>$payments,
			'total'=>$total);

		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();

		$this->db->insert('pedidos',$pedido_data);
		$pedido_id = $this->db->insert_id();


			

		foreach($items as $line=>$item)
		{
			$cur_item_info = $this->Item->get_info($item['item_id']);

			$pedido_items_data = array
			(
				'pedido_id'=>$pedido_id,
				'item_id'=>$item['item_id'],
				'line'=>$item['line'],
				'description'=>'',
				'serialnumber'=>'',
				'quantity_purchased'=>$item['quantity'],
				'discount_percent'=>$item['discount'],
				'item_cost_price' => $cur_item_info->cost_price,
				'item_unit_price'=>$item['price'],
				
			);

			$this->db->insert('pedidos_items',$pedido_items_data);

			
			
			
		}
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			return -1;
		}
		
		return $pedido_id;
	}
	function concluir($pedido_id)
	{
			 
		$this->db->where('pedido_id', $pedido_id);
		return $this->db->update('pedidos', array('estado' =>'Hecho')); 
			 
			 
	}
	function cancel($pedido_id)
	{
			 
		$this->db->where('pedido_id', $pedido_id);
		$this->db->delete('pedidos');
			 
			 
	}
	function cancel_items($pedido_id)
	{
			 
		$this->db->where('pedido_id', $pedido_id);
		$this->db->delete('pedidos_items');
			 
			 
	}
	function get_items_info($pedido_id)
	{
		$this->db->from('pedidos_items');
		$this->db->where('pedido_id',$pedido_id);
		//return an array of item kit items for an item
		return $this->db->get()->result_array();
	}
	
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('pedidos');
		$this->db->join('people','pedidos.customer_id=people.person_id');
		$this->db->like('email', $search);
		$this->db->where('estado','pendiente');
		$this->db->order_by("pedido_time", "asc");
		$by_number = $this->db->get();
		foreach($by_number->result() as $row)
		{
			$suggestions[]=$row->email;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}
	function get_search_suggestions_f($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('pedidos');
		$this->db->join('people','pedidos.customer_id=people.person_id');
		$this->db->like('email', $search);
		$this->db->where('estado','Hecho');
		$this->db->order_by("pedido_time", "asc");
		$by_number = $this->db->get();
		foreach($by_number->result() as $row)
		{
			$suggestions[]=$row->email;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}
	/*
	Preform a search on pedidos
	*/
	function search($search)
	{
		$this->db->from('pedidos');
		$this->db->join('people','pedidos.customer_id=people.person_id');
		$this->db->where("email LIKE '%".$this->db->escape_like_str($search)."%' and estado='pendiente'");
		$this->db->order_by("pedido_time", "asc");
		return $this->db->get();
		
	}
	function search_f($search)
	{
		$this->db->from('pedidos');
		$this->db->join('people','pedidos.customer_id=people.person_id');
		$this->db->where("email LIKE '%".$this->db->escape_like_str($search)."%' and estado='Hecho'");
		$this->db->order_by("pedido_time", "asc");
		return $this->db->get();
		
	}

}

?>