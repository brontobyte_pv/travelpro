<?php
class Cobro extends Model
{
	
	function exists($cobro_id)
	{
		$this->db->from('cobros');
		$this->db->where('cobro_id',$cobro_id);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
	
	function get_all($limit=10000, $offset=0, $mes)
	{
		$this->db->from('cobros');
		if($mes>0)
		{
			$this->db->where('fecha BETWEEN "'. date('Y-'.$mes.'-00 00:00:00'). '" and "'. date('Y-'.++$mes.'-00 00:00:00)"'));
		}
		
		$this->db->order_by("fecha", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	
	
	function count_all()
	{
		$this->db->from('cobros');
		return $this->db->count_all_results();
	}
	function get_info($cobro_id)
	{
		$this->db->from('cobros');	
		$this->db->where('cobro_id',$cobro_id);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('cobros');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('cobros');
		$this->db->where('cobro_id', $cobro_id);
		$this->db->like('estado','Hecho');
		$this->db->order_by("fecha", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->fecha;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}
	function search($search)
	{
		$this->db->from('cobros');
		$this->db->where("fecha LIKE '%".$this->db->escape_like_str($search)."%' or 
		estado LIKE '%".$this->db->escape_like_str($search)."%'");
		$this->db->order_by("fecha", "asc");
		return $this->db->get();	
	}
	function save(&$cobros_data,$cobro_id=false)
	{
		if(!$cobro_id or !$this->exists($cobro_id))
		{
			if($this->db->insert('cobros',$cobros_data))
			{
				$cobros_data['cobro_id']=$this->db->insert_id();
				return true;
			}
		}
		$this->db->where('cobro_id', $cobro_id);
		return $this->db->update('cobros',$cobros_data);	
	}
	
	function cancelar($cobro_id)
	{
			 
		$this->db->where('cobro_id', $cobro_id);
		return $this->db->update('cobros', array('estado' => 1)); 
			 	 
	}
	function delete_list($cobros_ids)
	{
		
			
			$this->db->where_in('cobro_id',$cobros_ids);
			$query=$this->db->delete('cobros');
			return $query;
		
		
 	}
	function undone($mes)
	{
		$this->db->where('estado', 'Pendiente');
		if($mes>0)
		{
			$this->db->where('fecha BETWEEN "'. date('Y-'.$mes.'-00 00:00:00'). '" and "'. date('Y-'.++$mes.'-00 00:00:00)"'));
		}
		$query = $this->db->select_sum('cantidad', 'totales');
		$query = $this->db->get('cobros');
		$result = $query->result();

    return $result[0]->totales;
		
	}
	function done($mes)
	{
		$this->db->where('estado', 'Hecho');
		if($mes>0)
		{
			$this->db->where('fecha BETWEEN "'. date('Y-'.$mes.'-00 00:00:00'). '" and "'. date('Y-'.++$mes.'-00 00:00:00)"'));
		}
		$query = $this->db->select_sum('cantidad', 'totales');
		$query = $this->db->get('cobros');
		$result = $query->result();

    return $result[0]->totales;
	}
	
	
}

?>