<?php
class Recurso extends Model
{
	
	
	function get_all($limit=10000, $offset=0)
	{
		$this->db->from('recursos');
		$this->db->order_by("recurso_id", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	
	function exists($recurso_id)
	{
		$this->db->from('recursos');
		$this->db->where('recurso_id',$recurso_id);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
	
	function count_all()
	{
		$this->db->from('recursos');
		return $this->db->count_all_results();
	}
	
	function get_info($recurso_id)
	{
		$this->db->from('recursos');	
		$this->db->where('recurso_id',$recurso_id);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('recursos');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}
	
	function delete_list($recursos_to_delete)
	{
		$this->db->where_in('recurso_id',$recursos_to_delete);
		return $this->db->delete('recursos');		
 	}
	
	function save(&$recurso_data,$recurso_id=false)
	{
		if (!$recurso_id or !$this->exists($recurso_id))
		{		
			if($this->db->insert('recursos',$recurso_data))
			{
				$recurso_data['recurso_id']=$this->db->insert_id();
				return true;
			}
				
			
			
		}
		$this->db->where('recurso_id', $recurso_id);
		return $this->db->update('recursos',$recurso_data);
	}
	
		 
	function search($search)
	{
		$this->db->from('recursos');
		$this->db->where("nombre LIKE '%".$this->db->escape_like_str($search)."%'");
		$this->db->order_by("nombre", "asc");
		return $this->db->get();	
	}	 
		 
	function rest_recurso($cantidad,$recurso_id)
	{	
		$consulta=$this->get_info($recurso_id);
		$datos = array('recurso'=>$consulta->recurso - $cantidad);
			
		$consulta->recurso-=$cantidad;
		$this->db->where_in('recurso_id',$recurso_id);
		return $this->db->update('recursos',$datos);
	}
	 function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('recursos');
		$this->db->like('recurso', $search);
		$this->db->order_by("recurso_id", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->recurso;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}
		 
		 
		 
}

?>