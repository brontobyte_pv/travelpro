<?php
class Lang_lib
{
	var $CI;

  	function __construct()
	{
		$this->CI =& get_instance();
	}

	function get_idioma()
	{
		
		return $this->CI->session->userdata('idioma');
	}

	function set_idioma($idioma)
	{
		$this->CI->session->set_userdata('idioma',$idioma);
	}
	
	function get_position()
	{
		
		if(!$this->CI->session->userdata('position'))
			$this->set_position(array());

		return $this->CI->session->userdata('position');
		
		
		
		
	}
	
	function set_position($position)
	{
		
		$this->CI->session->set_userdata('position',$position);
		
	}
	
	function set_content_idioma($idioma)
	{
		$this->CI->session->set_userdata('content_idioma',$idioma);
	}
	function get_content_idioma()
	{
		
		return $this->CI->session->userdata('content_idioma');
	}
	function content_file_new($idioma,$id)
	{
		////////Generacion de archivo de lengua
		
		$this->CI->load->helper('file');
		$archivo_lang =  read_file('./application/language/'.$idioma.'/contenido_lang.php');
		$caracteres = array('<?php','?>');
$archivo_lang = str_replace($caracteres ,"", $archivo_lang);		
		$lineas=explode(";", $archivo_lang);
		$archivo_lang='';
		
$newdata ='$lang["contenido_'.$id.'_ciudades"]="";
$lang["contenido_'.$id.'_incluye"]="";
$lang["contenido_'.$id.'_lugares"]="";
$lang["contenido_'.$id.'_actividades"]="";
$lang["contenido_'.$id.'_recomendaciones"]="";
$lang["contenido_'.$id.'_observaciones"]="";
$lang["contenido_'.$id.'_salidaVuelo"]="";
$lang["contenido_'.$id.'_metaPalabrasUno"]="";
$lang["contenido_'.$id.'_metaPalabrasDos"]="";
';

			
$a=array();
		foreach($lineas as $key=>$linea)
		{
			
				
				
				//$piece_linea=explode("_", $linea[$key]);
				
				
				$a[]=$lineas[$key];
				
			
			
		}
		
		$str=implode(";",$a);
		
		$archivo_lang.=$str.$newdata;
			//$archivo_lang.=$newdata; 
$archivo_lang='<?php'.$archivo_lang.'?>';
		if ( ! write_file('./application/language/'.$idioma.'/contenido_lang.php', $archivo_lang))
		{
			// echo 'Unable to write the file';
		}
		else
		{
			// echo 'File written!';
		}
		
		
	
}
function content_file_save($idioma,$id)
{
		////////Generacion de archivo de lengua
		
		$this->CI->load->helper('file');
		$archivo_lang =  read_file('./application/language/'.$idioma.'/contenido_lang.php');	
		$lineas=explode(";", $archivo_lang);
		$archivo_lang='';
		
$newdata = '$lang["contenido_'.$id.'_ciudades"]="";
$lang["contenido_'.$id.'_incluye"]="";
$lang["contenido_'.$id.'_lugares"]="";
$lang["contenido_'.$id.'_actividades"]="";
$lang["contenido_'.$id.'_recomendaciones"]="";
$lang["contenido_'.$id.'_observaciones"]="";
$lang["contenido_'.$id.'_salidaVuelo"]="";
$lang["contenido_'.$id.'_metaPalabrasUno"]="";
$lang["contenido_'.$id.'_metaPalabrasDos"]="";
';
 $caracteres = array('?>','<?php');
$a=array();
		foreach($lineas as $key=>$linea)
		{
			
				$lin=explode("_", $lineas[$key]);
				
				//$piece_linea=explode("_", $linea[$key]);
				if($lineas[$key]=='?>' or $lineas[$key]=='<?php')
				{
					
					$lineas[$key]='';
				}
				elseif(isset($lin[1]))
				{//echo $lin[1];
				}
				$lineas[$key]= str_replace($caracteres ,"", $lineas[$key]);
				$a[]=$lineas[$key].';';
				
			
			
		}
		$str=implode("\n",$a);
		$archivo_lang.=$str.$newdata;
			//$archivo_lang.=$newdata; 
		$archivo_lang=$archivo_lang.'?>';
		if ( ! write_file('./application/language/'.$idioma.'/contenido_lang.php', $archivo_lang))
		{
			 //echo 'Unable to write the file';
		}
		else
		{
			// echo 'File written!';
		}
		
		
	}
	function get_content_file($idioma,$id)
	{
		
		$this->CI->load->helper('file');
		$archivo_lang =  read_file('./application/language/'.$idioma.'/contenido_lang.php');
		$caracteres = array('<?php','?>');
		$caracteres=str_replace($caracteres ,"", $caracteres);
		$lineas=explode(";", $archivo_lang);
		$a=array();
		;
		foreach($lineas as $key=>$linea)
		{
		
			$lin=explode("_", $lineas[$key]);
			//ejemp:$lang["contenido_1_ciudades"]="Quintana Roo"; array($lang["contenido, 1, ciudades"]="Quintana Roo";);
			$line=explode("=", $lineas[$key]);
		
		
		
		if(isset($lin[1]))
			{
				$indice=explode('"', $lineas[$key]);
				///ejemp:$lang["contenido_1_ciudades"]="Quintana Roo"; array($lang[, contenido_1_ciudades, ]="Quintana Roo";);
				$txt_linea=explode('"', $line[1]);
				
				//echo $lin[1];
					$txt_linea[1]=str_replace( '<br />', "\r", $txt_linea[1] ); 
				if($lin[1]==$id)
				{
					$a[$indice[1]]=''.$txt_linea[1].'';
					
				}
					
			}
		}
		return $a;
		
	}
	function save_edit_content($idioma,$id,$data_content)
	{
		$this->CI->load->helper('file');
		$archivo_lang =  read_file('./application/language/'.$idioma.'/contenido_lang.php');
		if ( ! write_file('./application/language/'.$idioma.'/contenido_lang_backup.php', $archivo_lang))
		{
			// echo 'Unable to write the file';
		}
		 $caracteres = array('?>','<?php');
		$archivo_lang= str_replace($caracteres ,"", $archivo_lang);
		$lineas=explode(";", $archivo_lang);
		$a=array();
		
		
		foreach($lineas as $key=>$linea)
		{
			
			$lin=explode("_", $lineas[$key]);
			//ejemp:$lang["contenido_1_ciudades"]="Quintana Roo"; array($lang["contenido, 1, ciudades"]="Quintana Roo";);
			$line=explode("=", $lineas[$key]);
		
			
			
					
				
			
			if(isset($lin[1]))
			{
				$indice=explode('"', $lineas[$key]);
				///ejemp:$lang["contenido_1_ciudades"]="Quintana Roo"; array($lang[, contenido_1_ciudades, ]="Quintana Roo";);
				 
				//echo $lin[1];
					
				if($lin[1]==$id)
				{
					$vals_lang=explode('_', $indice[1]);
					$value=trim($data_content[$vals_lang[2]]);
			$caracteres = array("'", '"', ";", ".'", '."', "<", ">", "''",'<?php','?>','/>','</','""');
			
			$value = str_replace($caracteres ,"", $value);
			
			$value=str_replace( '<br />', "\r", $value); 
			
$lineas[$key]='
$lang["contenido_'.$id.'_'.$vals_lang[2].'"]="'.nl2br($value).'"';
					//$a[$indice[1]]=''.$txt_linea[1].'';
					
				}
			
			}
			$a[]=$lineas[$key];
		
		
		}
		$archivo_lang_edit=implode(";",$a);
		$chars= array(';;');
$archivo_lang_edit='<?php'.$archivo_lang_edit.
'?>';
$archivo_lang_edit = str_replace($chars,";", $archivo_lang_edit);
		if ( ! write_file('./application/language/'.$idioma.'/contenido_lang.php', $archivo_lang_edit))
		{
			// echo 'Unable to write the file';
		}
		else
		{
			// echo 'File written!';
		}
	
	
	}
}
?>
