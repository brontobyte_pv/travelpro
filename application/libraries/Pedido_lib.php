<?php
class Pedido_lib
{
	var $CI;

  	function __construct()
	{
		$this->CI =& get_instance();
	}

	function get_cart()
	{
		if(!$this->CI->session->userdata('cart_p'))
			$this->set_cart(array());

		return $this->CI->session->userdata('cart_p');
	}
	function set_cart($cart_data)
	{
		$this->CI->session->set_userdata('cart_p',$cart_data);
	}
	function get_kits()
	{
		if(!$this->CI->session->userdata('kits'))
			$this->set_kits(array());

		return $this->CI->session->userdata('kits');
	}
	function set_kits($kits)
	{
		$this->CI->session->set_userdata('kits',$kits);
	}
	function get_data_save()
	{
		if(!$this->CI->session->userdata('data_save'))
			$this->set_data_save(array());

		return $this->CI->session->userdata('data_save');
	}
	function set_data_save($data_save)
	{
		$this->CI->session->set_userdata('data_save',$data_save);
	}
	
	function get_items_totales()
	{
			
			if(!$this->CI->session->userdata('tots'))
			$this->CI->session->set_userdata('tots',0);

		return $this->CI->session->userdata('tots');
	}
	function set_items_totales($cant)
	{
			$this->CI->session->set_userdata('tots',$cant);
	}
	
	function add_item($item_id,$quantity=1,$discount=0,$price=null,$description=null,$serialnumber=null)
	{
		//Verifica si el item existe
		if(!$this->CI->Item->exists($item_id))
		{
			//Intenta obtener el item_id con el item_number
			$item_id = $this->CI->Item->get_item_id($item_id);
			
			if(!$item_id)
				return false;
			
		}

	

        //Obtiene los items de la cockie que muestra los datos en el register
		$items = $this->get_cart();
		//Obtiene los items totales
		$data_save = $this->get_data_save();
        //Obtiene datos de la cockie que es exclusiva para kits
		$kits_in_cart=$this->get_kits();
       
        //If the item is already there, get it's key($updatekey).
        //We also need to get the next key that we are going to use in case we need to add the
        //item to the cart. Since items can be deleted, we can't use a count. we use the highest key + 1.
		$sihayitem=FALSE;
        $maxkey=0;                       //Highest key so far
        $itemalreadyinsale=FALSE;        //We did not find the item yet.
		$insertkey=0;                    //Variable para agregar una linea para un item nuevo.
		$updatekey=0;                    //Variable para actualizar(quantity)
		 //Recorremos los items de la cockie ($items)
		foreach ($items as $item)
		{
            //Cada ciclo se lo agregamos a maxkey comenzando con el valor 0 la primera vez.
            //Tambien guardamos las linea que coincide con la linea de item 
			
			if($maxkey <= $item['line'])
			{
				$maxkey = $item['line'];
				
			}
			 //Si el item ya esta actualizamos su linea($updatekey).
			if($item['item_id']==$item_id)
			{
				$itemalreadyinsale=TRUE;
				$updatekey=$item['line'];
				
			}
		}
		
		
		$insertkey=$maxkey+1;

		//Los campos de la cockie items son identificados por $insertkey y item_id es solo un campo mas.
		
		//Usamos este $item para cuando no hay algun item en la cockie de $data_save
		$item = array(($insertkey)=>
		array(
			'item_id'=>$item_id,
			'line'=>$insertkey,
			'name'=>$this->CI->Item->get_info($item_id)->name,
			'item_number'=>$this->CI->Item->get_info($item_id)->item_number,
			'description'=>$description!=null ? $description: $this->CI->Item->get_info($item_id)->description,
			'serialnumber'=>$serialnumber!=null ? $serialnumber: '',
			'allow_alt_description'=>$this->CI->Item->get_info($item_id)->allow_alt_description,
			'is_serialized'=>$this->CI->Item->get_info($item_id)->is_serialized,
			'quantity'=>$quantity,
                        'discount'=>$discount,
			'price'=>$price!=null ? $price: $this->CI->Item->get_info($item_id)->unit_price
			)
		);
		//Usamos este $item para cuando hay algun item en la cockie de $data_save
		$insertKey2=count($data_save)+1;
		$itemv2 = array($insertKey2=>
		array(
			'item_id'=>$item_id,
			'line'=>$insertKey2,
			'name'=>$this->CI->Item->get_info($item_id)->name,
			'item_number'=>$this->CI->Item->get_info($item_id)->item_number,
			'description'=>$description!=null ? $description: $this->CI->Item->get_info($item_id)->description,
			'serialnumber'=>$serialnumber!=null ? $serialnumber: '',
			'allow_alt_description'=>$this->CI->Item->get_info($item_id)->allow_alt_description,
			'is_serialized'=>$this->CI->Item->get_info($item_id)->is_serialized,
			'quantity'=>1,
            'discount'=>$discount,
			'price'=>$price!=null ? $price: $this->CI->Item->get_info($item_id)->unit_price
			)
		);
		//Si el item ya estaba en la cockie y no esta serializado, se agrega a quantity
		if($itemalreadyinsale==true && ($this->CI->Item->get_info($item_id)->is_serialized ==0) )
		{
			//echo'En carro';
			$items[$updatekey]['quantity']+=$quantity;
			//con kits
			if(count($kits_in_cart)>0)
			{
				//echo'Con kits';
				foreach($data_save as $single_item)
				{
					if($item_id==$single_item['item_id'])
					{
						$sihayitem=TRUE;
						$data_save[$single_item['line']]['quantity']+=$quantity;
						//echo 'add data';
						//echo $data_save[$single_item['line']]['quantity'];
					}
					
				}
				if($sihayitem!=TRUE)
				{
					
					$data_save+=$itemv2;
					//echo 'New Data';
					//echo $data_save[$insertKey2]['quantity'];
				}
				
			}
			else
			{	//echo'Sin kits';
				//echo 'add data';
				//echo $data_save[$updatekey]['quantity'];
				$data_save[$updatekey]['quantity']+=$quantity;
			}
		}
		else	
		{
			//echo'Nuevo en carro';
			$items+=$item;
			
			if(count($kits_in_cart)>0)
			{
				$sihayitem=FALSE;
				//echo'Con kits';
				foreach($data_save as $single_item)
				{
					
					if($item_id==$single_item['item_id'])
					{
						$sihayitem=TRUE;
						$data_save[$single_item['line']]['quantity']+=$quantity;
						//echo 'add data';
						//echo $data_save[$single_item['line']]['quantity'];
						
					}
					
				}
				if($sihayitem!=TRUE)
				{
					$data_save+=$item;
					//echo 'New data';
					//echo $data_save[$insertkey]['quantity'];
				}
				
			}
			
			
			
			if(count($kits_in_cart)<1)
			{
				//echo'Sin kits';
				$data_save+=$item;
				//echo'New data';
				//echo $data_save[count($data_save)]['quantity'];
			}
				
			
			
			
		}		
			
			
		
		$this->set_cart($items);
		$this->set_data_save($data_save);
		return true;

	}
	function add_item_kit($kit_name ,$quantity=1,$discount=0,$price=null,$description=null,$serialnumber=null)
	{
            
                    if(!$this->CI->Item_kit->exists($kit_name))
		{
			//try to get item id given an item_number
			$kit_data = $this->CI->Item_kit->get_info_by_name($kit_name);
			
			if(!$kit_data)
				return false;
			
		}
                
                
				
				$items_in_kit = $this->CI->Item_kit_items->get_info($this->CI->Item_kit->get_info_by_name($kit_name)->item_kit_id);
				//Aqui se insertan los items del kit en una cockie diferente
				
					
				$max=0;                
				$alreadyinsale=FALSE;        
				$insert=0;             
				$update=0;
				
                foreach($items_in_kit as $single_item)
				{
					
					
					
					$data_save = $this->get_data_save();
						foreach ($data_save as $linea)
						
						{
							if($max >= $linea['line'])
							{
								$max = $linea['line'];
								
							}
							if($linea['item_id']==$single_item['item_id'])
							{
								$alreadyinsale=TRUE;
								$update=$linea['line'];
								
							}
							
						}
						$insert=$max-1;
						$itemline= array(($insert)=>
						array(
							'item_id'=>$single_item['item_id'],
							'line'=>$insert,
							'name'=>$kit_name,
							'item_number'=>$this->CI->Item->get_info($single_item['item_id'])->item_number,
							'description'=>$description!=null ? $description: $this->CI->Item->get_info($single_item['item_id'])->description,
							'serialnumber'=>$serialnumber!=null ? $serialnumber: '',
							'allow_alt_description'=>$this->CI->Item->get_info($single_item['item_id'])->allow_alt_description,
							'is_serialized'=>$this->CI->Item->get_info($single_item['item_id'])->is_serialized,
							'quantity'=>$single_item['quantity'],
							'discount'=>$discount,
							'price'=>$this->CI->Item->get_info($single_item['item_id'])->unit_price
							)
						);
						if($alreadyinsale)
							{
								$data_save[$update]['quantity']+=$single_item['quantity'];
							}
						else
							{
								//add to existing array
								$data_save+=$itemline;
							}
						$this->set_data_save($data_save);
	
				
				
				}
				$kits_in_cart=$this->get_kits();
				
				$items_cart = $this->get_cart();
                
                $maxkey=0;                       //Limite
                $itemalreadyinsale=FALSE;        //No hay nada de item aun
				$insertkey=0;                    //esta llave crea una nueva entrada.
				$updatekey=0;                    //y esta actualiza la cantidad
                //Creamos un ciclo para checar todos los articulos agregados al carrito
                //si el articulo esta ahi actualizamos $updatekey
                //tambien necesitamos obtener la sigiente key en caso de querer agregar algo mas
                
                foreach ($items_cart as $item_cart)
		{
                    if($maxkey <= $item_cart['line'])
			{
				$maxkey = $item_cart['line'];
			}

			if($item_cart['name']==$kit_name)
			{
				$itemalreadyinsale=TRUE;
				$updatekey=$item_cart['line'];
			}
				
		}
                $insertkey=$maxkey+1;
          
                $line = array(($insertkey)=>
		array(
			'item_id'=>$kit_name,
			'line'=>$insertkey,
			'name'=>$kit_name,
			'item_number'=>$this->CI->Item_kit->get_info_by_name($kit_name)->kit_number,
			'description'=>$description!=null ? $description: $this->CI->Item_kit->get_info_by_name($kit_name)->description,
			'serialnumber'=>$serialnumber!=null ? $serialnumber: '',
			'allow_alt_description'=>0,
			'is_serialized'=>0,
			'quantity'=>$quantity,
            'discount'=>$discount,
			'price'=>$price!=null ? $price: $this->CI->Item_kit->get_info_by_name($kit_name)->kit_price
			)
		);
                
        if($itemalreadyinsale)
		{
			$items_cart[$updatekey]['quantity']+=$quantity;
			$kits_in_cart[$updatekey]['quantity']+=$quantity;
			
			
		}
		else
		{
			//add to existing array
			$kits_in_cart+=$line;
			$items_cart+=$line;
			
			
		}
		$this->set_kits($kits_in_cart);
		$this->set_cart($items_cart);
		
		return true;

               
		
		
		
	}
		function get_subtotal()
	{
		$subtotal = 0;
		foreach($this->get_cart() as $item)
		{
		    $subtotal+=($item['price']*$item['quantity']-$item['price']*$item['quantity']);
		}
		return to_currency_no_money($subtotal);
	}
	function get_total()
	{
		$total = 0;
		foreach($this->get_cart() as $item)
		{
            $total+=($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100);
		}

		

		return to_currency_no_money($total);
	}
	function empty_cart()
	{
		$this->CI->session->unset_userdata('cart_p');
		$this->CI->session->unset_userdata('tots');
		$this->CI->session->unset_userdata('pais');
	}
	function item_in_cart($item_id)
	{
		 $itemalreadyinsale=FALSE; 
		 $items = $this->get_cart();
		 foreach ($items as $item)
		{
			if($item['item_id']==$item_id)
			{
				$itemalreadyinsale=TRUE;
			
			}
			
		}
		return $itemalreadyinsale;
		 
	}
	function delete_item($line)
	{
		$items_totales=$this->get_items_totales()-1;
		
		$this->CI->session->set_userdata('tots',$items_totales);
		$items=$this->get_cart();
		unset($items[$line]);
		$this->set_cart($items);
	}
	function get_payments()
	{
		if(!$this->CI->session->userdata('payments_p'))
			$this->set_payments(array());

		return $this->CI->session->userdata('payments_p');
	}

	//Alain Multiple Payments
	function set_payments($payments_data)
	{
		$this->CI->session->set_userdata('payments_p',$payments_data);
	}
	
	function get_customer()
	{
		if(!$this->CI->session->userdata('customer'))
			$this->set_customer(-1);

		return $this->CI->session->userdata('customer');
	}

	function set_customer($customer_id)
	{
		$this->CI->session->set_userdata('customer',$customer_id);
	}
	function empty_payments()
	{
		$this->CI->session->unset_userdata('payments_p');
	}
		function remove_customer()
	{
		$this->CI->session->unset_userdata('customer');
	}
	function clear_all()
	{
		
		$this->empty_cart();
		
	
		$this->empty_payments();
		$this->remove_customer();
	}
}
?>
