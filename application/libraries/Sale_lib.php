<?php
class Sale_lib
{
	var $CI;

  	function __construct()
	{
		$this->CI =& get_instance();
	}

	function get_cart()
	{
		if(!$this->CI->session->userdata('cart'))
			$this->set_cart(array());

		return $this->CI->session->userdata('cart');
	}
	function get_kits()
	{
		if(!$this->CI->session->userdata('kits'))
			$this->set_kits(array());

		return $this->CI->session->userdata('kits');
	}
	
	function get_data_save()
	{
		if(!$this->CI->session->userdata('data_save'))
			$this->set_data_save(array());

		return $this->CI->session->userdata('data_save');
	}
	function set_cart($cart_data)
	{
		$this->CI->session->set_userdata('cart',$cart_data);
	}
	function set_kits($kits)
	{
		$this->CI->session->set_userdata('kits',$kits);
	}
	function set_data_save($data_save)
	{
		$this->CI->session->set_userdata('data_save',$data_save);
	}

	//Alain Multiple Payments
	function get_payments()
	{
		if(!$this->CI->session->userdata('payments'))
			$this->set_payments(array());

		return $this->CI->session->userdata('payments');
	}

	//Alain Multiple Payments
	function set_payments($payments_data)
	{
		$this->CI->session->set_userdata('payments',$payments_data);
	}
	
	function get_comment() 
	{
		return $this->CI->session->userdata('comment');
	}

	function set_comment($comment) 
	{
		$this->CI->session->set_userdata('comment', $comment);
	}
	function set_cardb($gcardbalance) 
	{
		$this->CI->session->set_userdata('gcardbalance', $gcardbalance);
	}
	function get_cardb() 
	{
		return $this->CI->session->userdata('gcardbalance');
	}

	function clear_comment() 	
	{
		$this->CI->session->unset_userdata('comment');
	}
	
	function get_email_receipt() 
	{
		return $this->CI->session->userdata('email_receipt');
	}

	function set_email_receipt($email_receipt) 
	{
		$this->CI->session->set_userdata('email_receipt', $email_receipt);
	}

	function clear_email_receipt() 	
	{
		$this->CI->session->unset_userdata('email_receipt');
	}

	function add_payment($payment_id,$payment_amount)
	{
		
		$payments=$this->get_payments();
		$payment = array($payment_id=>
		array(
			'payment_type'=>$payment_id,
			'payment_amount'=>$payment_amount
			)
			
		);

		//payment_method already exists, add to payment_amount
		if(isset($payments[$payment_id]))
		{
			$payments[$payment_id]['payment_amount']+=$payment_amount;
		}
		else
		{
			//add to existing array
			$payments+=$payment;
		}
		
		$this->set_payments($payments);
		return true;

	}

	//Alain Multiple Payments
	function edit_payment($payment_id,$payment_amount,$to)
	{
		$payments = $this->get_payments();
		if(isset($payments[$payment_id]))
		{
			$payments[$payment_id]['payment_type'] = $payment_id;
			$payments[$payment_id]['payment_amount'] = $payment_amount;
			$this->set_payments($payment_id);
		}

		return false;
	}

	//Alain Multiple Payments
	function delete_payment($payment_id)
	{
		$payments=$this->get_payments();
		unset($payments[$payment_id]);
		$this->set_payments($payments);
	}

	//Alain Multiple Payments
	function empty_payments()
	{
		$this->CI->session->unset_userdata('payments');
	}

	//Alain Multiple Payments
	function get_payments_total()
	{
		$subtotal = 0;
		foreach($this->get_payments() as $payments)
		{
		    $subtotal+=$payments['payment_amount'];
		}
		return to_currency_no_money($subtotal);
	}

	//Alain Multiple Payments
	function get_amount_due()
	{
		$amount_due=0;
		$payment_total = $this->get_payments_total();
		$sales_total=$this->get_total();
		$amount_due=to_currency_no_money($sales_total - $payment_total);
		if($amount_due<=0){
		$amount_due=to_currency_no_money($payment_total-$sales_total );
		
		}else{$amount_due=0;}
		return $amount_due;
	}

	function get_customer()
	{
		if(!$this->CI->session->userdata('customer'))
			$this->set_customer(-1);

		return $this->CI->session->userdata('customer');
	}

	function set_customer($customer_id)
	{
		$this->CI->session->set_userdata('customer',$customer_id);
	}

	function get_mode()
	{
		if(!$this->CI->session->userdata('sale_mode'))
			$this->set_mode('sale');

		return $this->CI->session->userdata('sale_mode');
	}

	function set_mode($mode)
	{
		$this->CI->session->set_userdata('sale_mode',$mode);
	}

	function add_item($item_id,$quantity=1,$discount=0,$price=null,$description=null,$serialnumber=null)
	{
		//Verifica si el item existe
		if(!$this->CI->Item->exists($item_id))
		{
			//Intenta obtener el item_id con el item_number
			$item_id = $this->CI->Item->get_item_id($item_id);
			
			if(!$item_id)
				return false;
			
		}

	

        //Obtiene los items de la cockie que muestra los datos en el register
		$items = $this->get_cart();
		//Obtiene los items totales
		$data_save = $this->get_data_save();
        //Obtiene datos de la cockie que es exclusiva para kits
		$kits_in_cart=$this->get_kits();
       
        //If the item is already there, get it's key($updatekey).
        //We also need to get the next key that we are going to use in case we need to add the
        //item to the cart. Since items can be deleted, we can't use a count. we use the highest key + 1.
		$sihayitem=FALSE;
        $maxkey=0;                       //Highest key so far
        $itemalreadyinsale=FALSE;        //We did not find the item yet.
		$insertkey=0;                    //Variable para agregar una linea para un item nuevo.
		$updatekey=0;                    //Variable para actualizar(quantity)
		 //Recorremos los items de la cockie ($items)
		foreach ($items as $item)
		{
            //Cada ciclo se lo agregamos a maxkey comenzando con el valor 0 la primera vez.
            //Tambien guardamos las linea que coincide con la linea de item 
			
			if($maxkey <= $item['line'])
			{
				$maxkey = $item['line'];
				
			}
			 //Si el item ya esta actualizamos su linea($updatekey).
			if($item['item_id']==$item_id)
			{
				$itemalreadyinsale=TRUE;
				$updatekey=$item['line'];
				
			}
		}
		
		
		$insertkey=$maxkey+1;

		//Los campos de la cockie items son identificados por $insertkey y item_id es solo un campo mas.
		
		//Usamos este $item para cuando no hay algun item en la cockie de $data_save
		$item = array(($insertkey)=>
		array(
			'item_id'=>$item_id,
			'line'=>$insertkey,
			'name'=>$this->CI->Item->get_info($item_id)->name,
			'item_number'=>$this->CI->Item->get_info($item_id)->item_number,
			'description'=>$description!=null ? $description: $this->CI->Item->get_info($item_id)->description,
			'serialnumber'=>$serialnumber!=null ? $serialnumber: '',
			'allow_alt_description'=>$this->CI->Item->get_info($item_id)->allow_alt_description,
			'is_serialized'=>$this->CI->Item->get_info($item_id)->is_serialized,
			'quantity'=>$quantity,
                        'discount'=>$discount,
			'price'=>$price!=null ? $price: $this->CI->Item->get_info($item_id)->unit_price
			)
		);
		//Usamos este $item para cuando hay algun item en la cockie de $data_save
		$insertKey2=count($data_save)+1;
		$itemv2 = array($insertKey2=>
		array(
			'item_id'=>$item_id,
			'line'=>$insertKey2,
			'name'=>$this->CI->Item->get_info($item_id)->name,
			'item_number'=>$this->CI->Item->get_info($item_id)->item_number,
			'description'=>$description!=null ? $description: $this->CI->Item->get_info($item_id)->description,
			'serialnumber'=>$serialnumber!=null ? $serialnumber: '',
			'allow_alt_description'=>$this->CI->Item->get_info($item_id)->allow_alt_description,
			'is_serialized'=>$this->CI->Item->get_info($item_id)->is_serialized,
			'quantity'=>1,
            'discount'=>$discount,
			'price'=>$price!=null ? $price: $this->CI->Item->get_info($item_id)->unit_price
			)
		);
		//Si el item ya estaba en la cockie y no esta serializado, se agrega a quantity
		if($itemalreadyinsale==true && ($this->CI->Item->get_info($item_id)->is_serialized ==0) )
		{
			echo'En carro';
			$items[$updatekey]['quantity']+=$quantity;
			//con kits
			if(count($kits_in_cart)>0)
			{
				echo'Con kits';
				foreach($data_save as $single_item)
				{
					if($item_id==$single_item['item_id'])
					{
						$sihayitem=TRUE;
						$data_save[$single_item['line']]['quantity']+=$quantity;
						echo 'add data';
						echo $data_save[$single_item['line']]['quantity'];
					}
					
				}
				if($sihayitem!=TRUE)
				{
					
					$data_save+=$itemv2;
					echo 'New Data';
					echo $data_save[$insertKey2]['quantity'];
				}
				
			}
			else
			{	echo'Sin kits';
				echo 'add data';
				echo $data_save[$updatekey]['quantity'];
				$data_save[$updatekey]['quantity']+=$quantity;
			}
		}
		else	
		{
			echo'Nuevo en carro';
			$items+=$item;
			
			if(count($kits_in_cart)>0)
			{
				$sihayitem=FALSE;
				echo'Con kits';
				foreach($data_save as $single_item)
				{
					
					if($item_id==$single_item['item_id'])
					{
						$sihayitem=TRUE;
						$data_save[$single_item['line']]['quantity']+=$quantity;
						echo 'add data';
						echo $data_save[$single_item['line']]['quantity'];
						
					}
					
				}
				if($sihayitem!=TRUE)
				{
					$data_save+=$item;
					echo 'New data';
					echo $data_save[$insertkey]['quantity'];
				}
				
			}
			
			
			
			if(count($kits_in_cart)<1)
			{
				echo'Sin kits';
				$data_save+=$item;
				echo'New data';
				echo $data_save[count($data_save)]['quantity'];
			}
				
			
			
			
		}		
			
			
		
		$this->set_cart($items);
		$this->set_data_save($data_save);
		return true;

	}
	
	function out_of_stock($item_id)
	{
		//make sure item exists
		if(!$this->CI->Item->exists($item_id))
		{
			//try to get item id given an item_number
			$item_id = $this->CI->Item->get_item_id($item_id);

			if(!$item_id)
				return false;
		}
		
		$item = $this->CI->Item->get_info($item_id);
		$quanity_added = $this->get_quantity_already_added($item_id);
		
		if ($item->quantity - $quanity_added < 0)
		{
			return true;
		}
		
		return false;
	}
	
	function get_quantity_already_added($item_id)
	{
		$items = $this->get_cart();
		$quanity_already_added = 0;
		foreach ($items as $item)
		{
			if($item['item_id']==$item_id)
			{
				$quanity_already_added+=$item['quantity'];
			}
		}
		
		return $quanity_already_added;
	}
	
function get_item_id($line_to_get)
{
		$items = $this->get_cart();

		foreach ($items as $line=>$item)
		{
			if($line==$line_to_get)
			{
				return $item['item_id'];
			}
		}
		
		return -1;
}

function edit_item($line,$description,$serialnumber,$quantity,$discount,$price,$name_item)
{
		//Obtengo datos de la cockie que es exclusiva para items
		$data_save = $this->get_data_save();
		//Obtengo datos de la cockie que es para mostrar datos en el register
		$items = $this->get_cart();
		//Obtengo datos de la cockie que es exclusiva para kits
		$kits_in_cart=$this->get_kits();
		//Obtengo el valor original de la cantidad de la linea a editar
		$qs=$items[$line]['quantity'];
		//Actualizo la cantidad en la cokie que muestra datos en el register
				$items[$line]['description'] = $description;
				$items[$line]['serialnumber'] = $serialnumber;
				$items[$line]['quantity'] = $quantity;
				$items[$line]['discount'] = $discount;
				$items[$line]['price'] = $price;
				$items[$line]['name'] = $name_item;
				$this->set_cart($items);
	
	if(count($kits_in_cart)>0)
	{	
			echo'Con kits';	
			
			//Si es un kit	
				
		if($items[$line]['item_id']<1)
		{
			echo 'Es un kit';
			//En la cockie de los kits encuentro el kit a editar y lo actualizo 			
			foreach ($kits_in_cart as $linea)
			{
				
				if($name_item==$linea['name'])
				{
					$kits_in_cart[$linea['line']]['quantity']=$quantity;
					$this->set_kits($kits_in_cart);
				}
			}	
			//Obtengo los items y cantidad de items que componen el kit		
			$items_in_kit = $this->CI->Item_kit_items->get_info($this->CI->Item_kit->get_info_by_name($name_item)->item_kit_id);
			$siexiste=FALSE;
			//Busco, actualizo y guardo la cantidad equivalente de los items que coincidan con el kit				
			foreach($data_save as $single_item)
			{
				foreach($items_in_kit as $articulo)
				{
					if($single_item['item_id']==$articulo['item_id'])
					{
						
						$siexiste=TRUE;
						$tots=$data_save[$single_item['line']]['quantity'];
						$itemt=$quantity*$articulo['quantity']+($tots-($qs*$articulo['quantity']));
						
						$data_save[$single_item['line']]['quantity']=$itemt;
						echo 'si existe un item';
						echo	$data_save[$single_item['line']]['quantity'];
									
					}
				}	
				
			}
			if($siexiste!=TRUE)
			{
				
							echo ' No existe el item';
							echo	$data_save[$line]['quantity'];

			}
			
		}
		else
		{
			echo 'Es un item';
			
			//Busco, actualizo y guardo la cantidad equivalente de los items que coincidan con el kit				
			foreach($data_save as $single_item)
			{
				
				if($single_item['item_id']==$items[$line]['item_id'])
				{		$tots=$data_save[$single_item['line']]['quantity'];
				
						$itemt=$quantity+($tots-$qs);
						$data_save[$single_item['line']]['quantity']=$itemt;
						
						echo 'se actualiza el item';
						echo	$data_save[$single_item['line']]['quantity'];
									
				}
					
				
			}
			
			
		
		}
		$this->set_data_save($data_save);
	}
	
		
		
	/*}	
			
				
						
	}//No es un kit	*/
	if(count($kits_in_cart)<1)
	{
		echo'Sin kits';		
		
		foreach($data_save as $single_item)
		{
				
			if($single_item['item_id']==$items[$line]['item_id'])
			{
						
						$data_save[$single_item['line']]['quantity']=$quantity;
						echo 'se actualiza el item';
						echo	$data_save[$single_item['line']]['quantity'];
									
			}
					
				
		}
		$this->set_data_save($data_save);
		
		
		
		
			
		
			
	}
	return false;
	
}
	


	function is_valid_receipt($receipt_sale_id)
	{
		//POS #
		$pieces = explode(' ',$receipt_sale_id);

		if(count($pieces)==2)
		{
			return $this->CI->Sale->exists($pieces[1]);
		}

		return false;
	}
	
	function is_valid_item_kit($item_kit_name)
	{
		

		if($kit=$this->CI->Item_kit->exists($item_kit_name))
		{
			return $kit;
		}
		else
		{
			return false;
		}

		
	}

	function return_entire_sale($receipt_sale_id)
	{
		//POS #
		$pieces = explode(' ',$receipt_sale_id);
		$sale_id = $pieces[1];

		$this->empty_cart();
		$this->remove_customer();

		foreach($this->CI->Sale->get_sale_items($sale_id)->result() as $row)
		{
			$this->add_item($row->item_id,-$row->quantity_purchased,$row->discount_percent,$row->item_unit_price,$row->description,$row->serialnumber);
		}
		$this->set_customer($this->CI->Sale->get_customer($sale_id)->person_id);

	}
	
	
	
	function add_item_kit($kit_name ,$quantity=1,$discount=0,$price=null,$description=null,$serialnumber=null)
	{
            
                    if(!$this->CI->Item_kit->exists($kit_name))
		{
			//try to get item id given an item_number
			$kit_data = $this->CI->Item_kit->get_info_by_name($kit_name);
			
			if(!$kit_data)
				return false;
			
		}
                
                
				
				$items_in_kit = $this->CI->Item_kit_items->get_info($this->CI->Item_kit->get_info_by_name($kit_name)->item_kit_id);
				//Aqui se insertan los items del kit en una cockie diferente
				
					
				$max=0;                
				$alreadyinsale=FALSE;        
				$insert=0;             
				$update=0;
				
                foreach($items_in_kit as $single_item)
				{
					
					
					
					$data_save = $this->get_data_save();
						foreach ($data_save as $linea)
						
						{
							if($max >= $linea['line'])
							{
								$max = $linea['line'];
								
							}
							if($linea['item_id']==$single_item['item_id'])
							{
								$alreadyinsale=TRUE;
								$update=$linea['line'];
								
							}
							
						}
						$insert=$max-1;
						$itemline= array(($insert)=>
						array(
							'item_id'=>$single_item['item_id'],
							'line'=>$insert,
							'name'=>$kit_name,
							'item_number'=>$this->CI->Item->get_info($single_item['item_id'])->item_number,
							'description'=>$description!=null ? $description: $this->CI->Item->get_info($single_item['item_id'])->description,
							'serialnumber'=>$serialnumber!=null ? $serialnumber: '',
							'allow_alt_description'=>$this->CI->Item->get_info($single_item['item_id'])->allow_alt_description,
							'is_serialized'=>$this->CI->Item->get_info($single_item['item_id'])->is_serialized,
							'quantity'=>$single_item['quantity'],
							'discount'=>$discount,
							'price'=>$this->CI->Item->get_info($single_item['item_id'])->unit_price
							)
						);
						if($alreadyinsale)
							{
								$data_save[$update]['quantity']+=$single_item['quantity'];
							}
						else
							{
								//add to existing array
								$data_save+=$itemline;
							}
						$this->set_data_save($data_save);
	
				
				
				}
				$kits_in_cart=$this->get_kits();
				
				$items_cart = $this->get_cart();
                
                $maxkey=0;                       //Limite
                $itemalreadyinsale=FALSE;        //No hay nada de item aun
				$insertkey=0;                    //esta llave crea una nueva entrada.
				$updatekey=0;                    //y esta actualiza la cantidad
                //Creamos un ciclo para checar todos los articulos agregados al carrito
                //si el articulo esta ahi actualizamos $updatekey
                //tambien necesitamos obtener la sigiente key en caso de querer agregar algo mas
                
                foreach ($items_cart as $item_cart)
		{
                    if($maxkey <= $item_cart['line'])
			{
				$maxkey = $item_cart['line'];
			}

			if($item_cart['name']==$kit_name)
			{
				$itemalreadyinsale=TRUE;
				$updatekey=$item_cart['line'];
			}
				
		}
                $insertkey=$maxkey+1;
          
                $line = array(($insertkey)=>
		array(
			'item_id'=>$kit_name,
			'line'=>$insertkey,
			'name'=>$kit_name,
			'item_number'=>$this->CI->Item_kit->get_info_by_name($kit_name)->kit_number,
			'description'=>$description!=null ? $description: $this->CI->Item_kit->get_info_by_name($kit_name)->description,
			'serialnumber'=>$serialnumber!=null ? $serialnumber: '',
			'allow_alt_description'=>0,
			'is_serialized'=>0,
			'quantity'=>$quantity,
            'discount'=>$discount,
			'price'=>$price!=null ? $price: $this->CI->Item_kit->get_info_by_name($kit_name)->kit_price
			)
		);
                
        if($itemalreadyinsale)
		{
			$items_cart[$updatekey]['quantity']+=$quantity;
			$kits_in_cart[$updatekey]['quantity']+=$quantity;
			
			
		}
		else
		{
			//add to existing array
			$kits_in_cart+=$line;
			$items_cart+=$line;
			
			
		}
		$this->set_kits($kits_in_cart);
		$this->set_cart($items_cart);
		
		return $data_save;

               
		
		
		
	}

	function copy_entire_sale($sale_id)
	{
		$this->empty_cart();
		$this->remove_customer();

		foreach($this->CI->Sale->get_sale_items($sale_id)->result() as $row)
		{
			$this->add_item($row->item_id,$row->quantity_purchased,$row->discount_percent,$row->item_unit_price,$row->description,$row->serialnumber);
		}
		foreach($this->CI->Sale->get_sale_payments($sale_id)->result() as $row)
		{
			$this->add_payment($row->payment_type,$row->payment_amount);
		}
		$this->set_customer($this->CI->Sale->get_customer($sale_id)->person_id);

	}
	
	function copy_entire_suspended_sale($sale_id)
	{
		$this->empty_cart();
		$this->remove_customer();

		foreach($this->CI->Sale_suspended->get_sale_items($sale_id)->result() as $row)
		{
			$this->add_item($row->item_id,$row->quantity_purchased,$row->discount_percent,$row->item_unit_price,$row->description,$row->serialnumber);
		}
		foreach($this->CI->Sale_suspended->get_sale_payments($sale_id)->result() as $row)
		{
			$this->add_payment($row->payment_type,$row->payment_amount);
		}
		$this->set_customer($this->CI->Sale_suspended->get_customer($sale_id)->person_id);
		$this->set_comment($this->CI->Sale_suspended->get_comment($sale_id));
	}

function delete_item($line)
{
		
		//Obtengo datos de la cockie que es exclusiva para items
		$data_save = $this->get_data_save();
		//Obtengo datos de la cockie que es para mostrar datos en el register
		$items = $this->get_cart();
		//Obtengo datos de la cockie que es exclusiva para kits
		$kits_in_cart=$this->get_kits();
		$qs=$items[$line]['quantity'];
		
		//Verifico sino hay un item en el register para ver sino coincide con el de la linea a borrar cuyo caso actualizar la cockie de items totales
		if(count($kits_in_cart)>0)
		{	//Obtengo los items y cantidad de items que componen el kit		
			echo 'Con kits';
			
			$siexiste=FALSE;
			if($items[$line]['item_id']<1)
			{
				$items_in_kit = $this->CI->Item_kit_items->get_info($this->CI->Item_kit->get_info_by_name($items[$line]['name'])->item_kit_id);
				
				foreach($data_save as $single_item)
				{
					foreach($items_in_kit as $articulo)
					{
						if($single_item['item_id']==$articulo['item_id'])
						{
						
							$siexiste=TRUE;
							$tots=$data_save[$single_item['line']]['quantity'];
							$itemt=$tots-($qs*$articulo['quantity']);
							$data_save[$single_item['line']]['quantity']=$itemt;
							echo 'se borro el kit ';
							if($itemt == 0)
							{
								echo 'Sin mas items';
								unset($data_save[$single_item['line']]);
							}
							else
							{
								echo	$data_save[$single_item['line']]['quantity'];
							}
						
						
									
						}
					}	
				
				}
				foreach ($kits_in_cart as $linea)
				{
				
					if($items[$line]['name']==$linea['name'])
					{
						echo 'Es un kit';
						unset($kits_in_cart[$line]);
					   $this->set_kits($kits_in_cart);
					}
				
				}
			}
			else
			{
				echo 'Pero no es un kit';
				foreach($data_save as $single_item)
				{
				
					if($single_item['item_id']==$items[$line]['item_id'])
					{
						
							$siexiste=TRUE;
							$tots=$data_save[$single_item['line']]['quantity'];
							$itemt=$tots-$qs;
							echo $itemt;
							$data_save[$single_item['line']]['quantity']=$itemt;
							echo 'se actualizo item';
							if($itemt == 0)
							{	echo 'Y se borro la linea';
								unset($data_save[$single_item['line']]);
							}
							else
							{echo	$data_save[$single_item['line']]['quantity'];}
						
						
								
						
					}	
				}
				
				
				
			}
			$this->set_data_save($data_save);	
		}
	if(count($kits_in_cart)<1)
	{
		echo'Sin kits';		
		
		foreach($data_save as $single_item)
		{
				
			if($single_item['item_id']==$items[$line]['item_id'])
			{	echo'Se borro item';	
				unset($data_save[$single_item['line']]);
				
			}
			//Busco, actualizo y guardo la cantidad equivalente de los items que coincidan con el kit				
			
		}	
		$this->set_data_save($data_save);
	}	
		
		unset($items[$line]);
		$this->set_cart($items);
		
		
}

	function empty_cart()
	{
		$this->CI->session->unset_userdata('cart');
		$this->CI->session->unset_userdata('data_save');
		$this->CI->session->unset_userdata('kits');
		$this->CI->session->unset_userdata('gcardbalance');
	}

	function remove_customer()
	{
		$this->CI->session->unset_userdata('customer');
	}

	function clear_mode()
	{
		$this->CI->session->unset_userdata('sale_mode');
	}

	function clear_all()
	{
		$this->clear_mode();
		$this->empty_cart();
		$this->clear_comment();
		$this->clear_email_receipt();
		$this->empty_payments();
		$this->remove_customer();
	}

	function get_taxes()
	{
		$customer_id = $this->get_customer();
		$customer = $this->CI->Customer->get_info($customer_id);

		//Do not charge sales tax if we have a customer that is not taxable
		if (!$customer->taxable and $customer_id!=-1)
		{
		   return array();
		}

		$taxes = array();
		foreach($this->get_cart() as $line=>$item)
		{
			$tax_info = $this->CI->Item_taxes->get_info($item['item_id']);

			foreach($tax_info as $tax)
			{
				$name = $tax['percent'].'% ' . $tax['name'];
				$tax_amount=($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100)*(($tax['percent'])/100);


				if (!isset($taxes[$name]))
				{
					$taxes[$name] = 0;
				}
				$taxes[$name] += $tax_amount;
			}
		}

		return $taxes;
	}

	function get_subtotal()
	{
		$subtotal = 0;
		foreach($this->get_cart() as $item)
		{
		    $subtotal+=($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100);
		}
		return to_currency_no_money($subtotal);
	}

	function get_total()
	{
		$total = 0;
		foreach($this->get_cart() as $item)
		{
            $total+=($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100);
		}

		foreach($this->get_taxes() as $tax)
		{
			$total+=$tax;
		}

		return to_currency_no_money($total);
	}
}
?>