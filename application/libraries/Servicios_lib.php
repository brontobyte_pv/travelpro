	<?php
class Servicios_lib
{
	var $CI;

  	function __construct()
	{
		$this->CI =& get_instance();
	}

	function get_data_save()
	{
		if(!$this->CI->session->userdata('data_service'))
			$this->set_data_save(array());

		return $this->CI->session->userdata('data_service');
	}

	function set_data_save($data_service)
	{
		$this->CI->session->set_userdata('data_service',$data_service);
	}


	function get_customer()
	{
		if(!$this->CI->session->userdata('customer_servicios'))
			$this->set_customer(-1);

		return $this->CI->session->userdata('customer_servicios');
	}

	function set_customer($customer_id)
	{
		$this->CI->session->set_userdata('customer_servicios',$customer_id);
	}

	function get_mode()
	{
		if(!$this->CI->session->userdata('sale_mode'))
			$this->set_mode('sale');

		return $this->CI->session->userdata('sale_mode');
	}

	function set_mode($mode)
	{
		$this->CI->session->set_userdata('sale_mode',$mode);
	}

	
	function remove_customer()
	{
		$this->CI->session->unset_userdata('customer_servicios');
	}

	function clear_mode()
	{
		$this->CI->session->unset_userdata('sale_mode');
	}

	function clear_all()
	{
		$this->clear_mode();
		$this->empty_cart();
		$this->clear_comment();
		$this->clear_email_receipt();
		$this->empty_payments();
		$this->remove_customer();
	}

	
}
?>