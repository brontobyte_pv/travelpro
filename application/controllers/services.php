<?php
require_once ("secure_area.php");
//require_once ("interfaces/iprescription_controller.php");
class Services extends Secure_area //implements iPrescription_controller
{
	function __construct()
	{
		parent::__construct('services');
		$this->load->library('servicios_lib');
		$data['controller_name']=strtolower(get_class());
		$this->load->helper('report',$data);
	}
	
	function index()
	{
		$config['base_url'] = site_url('?c=services&m=index');
		$config['total_rows'] = $this->Service->count_all();
		$config['per_page'] = '20'; 
		$this->pagination->initialize($config);
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_services_manage_table($this->Service->get_all($config['per_page'], $this->input->get('per_page')),$this);
		$customer_id=$this->servicios_lib->get_customer();
		if($customer_id!=-1)
		{
			$info=$this->Customer->get_info($customer_id);
			$data['customer']=$info->first_name." ".$info->last_name;
			
		}
		$this->load->view('services/manage',$data);
		
		
	}

	function view($service_id=-1)
	{
		$data['info']=$this->Service->get_info($service_id);
		
		
		foreach($this->Employee->get_all()->result_array() as $row)
		{
			$empleados[$row['first_name'].' '. $row['last_name']] =$row['first_name'] .' '. $row['last_name'];
		}
		$estado=array(
                  '1'  => 'Pendiente',
                  '0'    =>'Cancelada');
		
		
		
		$data['empleados']=$empleados;
		$data['estado']=$estado;
		$customer_id=$this->servicios_lib->get_customer();
		if($customer_id!=-1)
		{
			$data['report_date_range_simple'] = get_simple_date_ranges();
			$data['months'] = get_months();
			$data['days'] = get_days();
			$data['years'] = get_years();
			$data['hours'] = get_hours();
			$data['minutes'] = get_minutes();
			$data['selected_month']=date('n');
			$data['selected_day']=date('d');
			$data['selected_year']=date('Y');
			$data['selected_hour']=date('G');
			$data['selected_minute']=date('i');			
			$data['controller_name']=strtolower(get_class());
			$info=$this->Customer->get_info($customer_id);
			$data['customer']=$info->first_name." ".$info->last_name;
			$this->load->view("services/form",$data);
			
		}
		else
		{
			$this->load->view("services/form_error",$data);
		}
	}
	
	function get_form_width()
	{
		return 700;
	}
	function save($service_id=-1)
	{	
		
			
	
		$service_data = array(
		
		'nom_cliente'=>$this->input->post('customer'),
		'motivo'=>$this->input->post('motivo'),
		'fecha'=>$this->input->post('year').'-'.$this->input->post('month').'-'.$this->input->post('day').' '.$this->input->post('hour').':'.$this->input->post('minute').':00',
		'nom_asignado'=>$this->input->post('nom_asignado'),
		'accion'=>$this->input->post('accion'),
		'reporte'=>$this->input->post('reporte'),
		'estado'=>$this->input->post('estado'),
		'duracion'=>$this->input->post('duracion')
		
		);
		if($this->Service->save($service_data,$service_id))
		{
			//New item kit
			if($service_id==-1)
			{
				
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('services_save')));
				
			}
			else //previous item
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('services_save')));
			}
		}
			
		else//failure
			{
				echo json_encode(array('error'=>true,'message'=>$this->lang->line('services_save_error')));
			}
		
		
	}
	
	
	function delete()
	{
		
		
		$services_to_delete=$this->input->post('ids');
		
		if($this->Service->delete_list($services_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('services_cancel').' '.
			count($services_to_delete).' '.$this->lang->line('services_cancel')));
			
		}
		else
		{
			
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('services_no_cancel')));
			
			
		}
		
		
	}
	
	function get_row()
	{
		$service_id = $this->input->post('row_id');
		$data_row=get_services_data_row($this->Service->get_info($service_id),$this);
		echo $data_row;
	}
	
	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_services_manage_table_data_rows($this->Service->search($search),$this);
		echo $data_rows;
	}
	
	function suggest()
	{
		$suggestions = $this->Service->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}
	
	function customer_search()
	{
		$suggestions = $this->Customer->get_customer_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}
	
	function select_customer()
	{	
		$customer_id = $this->input->post("customer");
		$this->servicios_lib->set_customer($customer_id);
		
		$this->index();
		
	}
	
	function remove_customer()
	{
		$this->servicios_lib->remove_customer();
		
		$this->index();
	}
	
	
}