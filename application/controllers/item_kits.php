<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Item_kits extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('item_kits');
		$data['controller_name']=strtolower(get_class());
		$this->load->helper('report',$data);
		$this->load->library('lang_lib');
		
	}

	function index()
	{
		
		$this->lang_lib->set_position('inside');
		$idiomas=array(
				'english'  => 'Ingles',
                'spanish'  => 'Español'
				  );
			$selected_idioma=$this->lang_lib->get_content_idioma();	 
			if(empty($selected_idioma))
			{
				$selected_idioma='spanish';
				
			}
		$data['selected_idioma']=$selected_idioma;	
		$data['idiomas'] =$idiomas;
		$config['base_url'] = site_url('?c=item_kits&m=index');
		$config['total_rows'] = $this->Item_kit->count_all();
		$config['per_page'] = '20'; 
		$this->pagination->initialize($config);
		$meses=array(
				'0'  => 'Todos',
                  '01'  => 'Enero',
                  '02'  => 'Febrero',
				  '03'  => 'Marzo',
                  '04'  => 'Abril',
				  '05'  => 'Mayo',
                  '06'  => 'Junio',
				  '07'  => 'julio',
                  '08'  => 'Agosto',
				  '09'  => 'Septiembre',
                  '10'  => 'Octubre',
				  '11'  => 'Noviembre',
                  '12'  => 'Diciembre'
				  );
		$data['meses'] =$meses;
		$data['selected_month']=date('n');
		$data['controller_name']=strtolower(get_class());
		$data['manage_table']=get_item_kits_manage_table( $this->Item_kit->get_all($config['per_page'], $this->input->get('per_page')),$this);
		$this->load->view('item_kits/manage',$data);
		
	} 

	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_item_kits_manage_table_data_rows($this->Item_kit->search($search),$this);
		echo $data_rows;
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Item_kit->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function get_row()
	{
		$item_kit_id = $this->input->post('row_id');
		$data_row=get_item_kit_data_row($this->Item_kit->get_info($item_kit_id),$this);
		echo $data_row;
	}

	function view($item_kit_id=-1)
	{
		$item_kit_info=$this->Item_kit->get_info($item_kit_id);
		$meses=array(
		'0'  => 'Todos',
		'01'  => 'Enero',
		'02'  => 'Febrero',
		'03'  => 'Marzo',
		'04'  => 'Abril',
		'05'  => 'Mayo',
		'06'  => 'Junio',
		'07'  => 'julio',
		'08'  => 'Agosto',
		'09'  => 'Septiembre',
		'10'  => 'Octubre',
		'11'  => 'Noviembre',
		'12'  => 'Diciembre'
		);
		$data['meses'] =$meses;
		$data['months'] = get_months();
		$data['days'] = get_days();
		$data['years'] = get_years();
		$data['hours'] = get_hours();
		$data['minutes'] = get_minutes();
		if($item_kit_id==-1)
		{
			$data['selected_month']=date('n');
			$data['selected_day']=date('d');
			$data['selected_year']=date('Y');
			$data['selected_hour']=date('G');
			$data['selected_minute']=date('i');	
		}
		else
		{
		$timestamp=$item_kit_info->expiracion;
		$fecha=explode(" ", $timestamp);
		$fecha_dividida=explode("-", $fecha[0]);
		$data['selected_month']=$fecha_dividida[1];
		$data['selected_day']=$fecha_dividida[2];
		$data['selected_year']=$fecha_dividida[0];
		$data['selected_hour']=date('G');
		$data['selected_minute']=date('i');	

		}
		$data['item_kit_info']=$this->Item_kit->get_info($item_kit_id);
		$this->load->view("item_kits/form",$data);
		
	}
	function form_items_kit($item_kit_id=-1)
	{
		
		$data['item_kit_info']=$this->Item_kit->get_info($item_kit_id);
		$this->load->view("item_kits/form_items_kit",$data);
		
	}
	
	function save_demo($item_kit_id)
	{
		$item_kit_items=$this->input->post("item_kit_item")!=false ? $this->input->post("item_kit_item"):array();
		$this->Item_kit_items->save($item_kit_items, $item_kit_id);
		$this->output->enable_profiler(TRUE);
	}
	
	/////
	function save_items_kit($item_kit_id)
	{
		
			
			$this->load->library('upload');
			$item_kit_items=$this->input->post("item_kit_item")!=false ? $this->input->post("item_kit_item"):array();
			$cat_folder='./images/paquetes/front-img-paquetes/'.$item_kit_id.'/';
			//echo $cat_folder;
		$this->upload->initialize(array(
				"file_name"     => array("main.jpg"),
				"upload_path"   => 	$cat_folder,
				"allowed_types"     =>  "gif|jpg|png",
						"max_size"          =>  '10000',
						"max_width"         =>  '1366',
						"max_height"        =>  '500'
					));
				$this->upload->do_multi_upload("uploadfile");
				$data['upload_data'] = $this->upload->get_multi_upload_data();
			
			
		if($this->Item_kit_items->save($item_kit_items, $item_kit_id))
		{
			
			
				
				echo json_encode(array('success'=>true,'','item_kit_id'=>$item_kit_items['item_kit_id']));
				$item_kit_id = $item_kit_items['item_kit_id'];
				
			
			
		
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'','item_kit_id'=>-1));
		}
			
			
	}
	function save($item_kit_id=-1)
	{
		$CI=& get_instance();
		$CI->load->helper('file');
		$this->load->library('upload');
		$item_kit_data = array(
		'expiracion'=>$this->input->post('year').'-'.$this->input->post('month').'-'.$this->input->post('day').' 00:00:00',
		'name'=>$this->input->post('kit_name'),
		'description'=>$this->input->post('description'),
        'kit_number'=>$this->input->post('kit_number')=='' ? null:$this->input->post('kit_number'),
		'category'=>$this->input->post('category'),
		'kit_price'=>$this->input->post('kit_price'),
		'publicar'=>$this->input->post('publicar')
		);
		
		
			
		
		if($this->Item_kit->save($item_kit_data,$item_kit_id))
		{
			$cat_folder='./images/paquetes/categoria-img-paquetes/'.strtoupper($item_kit_data['category']).'/';
			
			$cat_folder=url_title($cat_folder);
			if (!file_exists($cat_folder))
			{
					mkdir($cat_folder,0777,true);
					chmod($cat_folder,0777);
			}
			//New item kit
			if($item_kit_id==-1)
			{
				$idioma='spanish';
				$this->lang_lib->content_file_new($idioma,$item_kit_data['item_kit_id']);
				json_encode(array('success'=>true,'message'=>$this->lang->line('item_kits_successful_adding').' '.
				$item_kit_data['name'],'item_kit_id'=>$item_kit_data['item_kit_id']));
				$item_kit_id=$item_kit_data['item_kit_id'];
				$paq_folder='./images/paquetes/front-img-paquetes/'.$item_kit_data['item_kit_id'].'/'; 
				
				if (!file_exists($paq_folder))
				{
					mkdir($paq_folder,0777,true);
					chmod($paq_folder,0777);
				}
				
			}
			else //previous item
			{
				json_encode(array('success'=>true,'message'=>$this->lang->line('item_kits_successful_updating').' '.
				$item_kit_data['name'],'item_kit_id'=>$item_kit_id));
				$paq_folder='./images/paquetes/front-img-paquetes/'.$item_kit_id.'/'; 
				if (!file_exists($paq_folder))
				{
					mkdir($paq_folder,0777,true);
					chmod($paq_folder,0777);
				}
			}
			
		
		}
		else//failure
		{
			json_encode(array('error'=>true,'message'=>$this->lang->line('item_kits_error_adding_updating').' '.
			$item_kit_data['name'],'item_kit_id'=>-1));
		}
		$this->upload->initialize(array(
				"file_name"     => array("paquete_thumbail.jpg"),
				"upload_path"   => 	$paq_folder,
				"allowed_types"     =>  "gif|jpg|png",
						"max_size"          =>  '10000',
						"max_width"         =>  '800',
						"max_height"        =>  '800'
					));
				$this->upload->do_multi_upload("uploadfile");
				$data['upload_data'] = $this->upload->get_multi_upload_data();
				$config['base_url'] = site_url('?c=item_kits&m=index');
		$config['total_rows'] = $this->Item_kit->count_all();
		$config['per_page'] = '20'; 
		$this->pagination->initialize($config);
		$data['controller_name']=strtolower(get_class());
		$data['manage_table']=get_item_kits_manage_table( $this->Item_kit->get_all($config['per_page'], $this->input->get('per_page')),$this);
		$this->load->view('item_kits/manage',$data);
	//$this->output->enable_profiler(TRUE);
	}
	
	function delete()
	{
		$item_kits_to_delete=$this->input->post('ids');

		if($this->Item_kit->delete_list($item_kits_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('item_kits_successful_deleted').' '.
			count($item_kits_to_delete).' '.$this->lang->line('item_kits_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('item_kits_cannot_be_deleted')));
		}
	}
	
	function generate_barcodes($item_kit_ids)
	{
		$result = array();

		$item_kit_ids = explode(',', $item_kit_ids);
		foreach ($item_kit_ids as $item_kid_id)
		{
			$item_kit_info = $this->Item_kit->get_info($item_kid_id);

			$result[] = array('name' =>$item_kit_info->name, 'id'=> 'KIT '.$item_kid_id);
		}

		$data['items'] = $result;
		$this->load->view("barcode_sheet", $data);
	}
	
	function suggest_category()
	{
		$suggestions = $this->Item_kit->get_category_suggestions($this->input->post('q'));
		echo implode("\n",$suggestions);
	}
	



/***********************
	function generate_barcodes($item_ids)
	{
		$result = array();

		$item_ids = explode(',', $item_ids);
		foreach ($item_ids as $item_id)
		{
			$item_info = $this->Item->get_info($item_id);

			$result[] = array('name' =>$item_info->name, 'id'=> $item_id);
		}

		$data['items'] = $result;
		$this->load->view("barcode_sheet", $data);
	}*************/
// De aqui en delante funciones que tienen que ver con el contenido del front
	function idioma_contenido()
	{
		$CI =& get_instance();
		
		$lang_to_change=$this->input->post('idiomas');
		$data['selected_idioma']=$lang_to_change;
		
		$this->lang_lib->set_content_idioma($lang_to_change);
	
		
		
		$this->lang_lib->set_position('inside');
		$idiomas=array(
				'english'  => 'Ingles',
                'spanish'  => 'Español'
				  );
		$data['idiomas'] =$idiomas;
		
		$config['base_url'] = site_url('?c=item_kits&m=index');
		$config['total_rows'] = $this->Item_kit->count_all();
		$config['per_page'] = '20'; 
		$this->pagination->initialize($config);
		$this->lang_lib->set_content_idioma($lang_to_change);
		
		$data['controller_name']=strtolower(get_class());
		$data['manage_table']=get_item_kits_manage_table( $this->Item_kit->get_all($config['per_page'], $this->input->get('per_page')),$this);
		$this->load->view('item_kits/manage',$data);
//$this->output->enable_profiler(TRUE);
		
		
	}
	//Esta funcion no se necesita en el original cct aun
	
	function form_content($item_kit_id=-1)
	{
		
			
		$CI =& get_instance();
		$idioma='spanish';
		$data['lang']=$this->lang_lib->get_content_file($idioma,$item_kit_id);
		
		
		$data['controller_name']=strtolower(get_class());
		
		
		$data['item_kit_info']=$this->Item_kit->get_info($item_kit_id);
		$this->load->view("item_kits/form_content",$data);
	}
	
	function edit_content($paquete_id)
	{
		$this->load->library('upload');
		$data_content = array(
		'ciudades'=>$this->input->post('contenido_'.$paquete_id.'_ciudades'),
		'incluye'=>$this->input->post('contenido_'.$paquete_id.'_incluye'),
		'lugares'=>$this->input->post('contenido_'.$paquete_id.'_lugares'),
		'actividades'=>$this->input->post('contenido_'.$paquete_id.'_actividades'),
		'recomendaciones'=>$this->input->post('contenido_'.$paquete_id.'_recomendaciones'),
		'observaciones'=>$this->input->post('contenido_'.$paquete_id.'_observaciones'),
		'salidaVuelo'=>$this->input->post('contenido_'.$paquete_id.'_salidaVuelo'),
		'metaPalabrasUno'=>$this->input->post('contenido_'.$paquete_id.'_metaPalabrasUno'),
		'metaPalabrasDos'=>$this->input->post('contenido_'.$paquete_id.'_metaPalabrasDos')
		);
		$this->lang_lib->save_edit_content('spanish',$paquete_id,$data_content);
		$paq_folder='./images/paquetes/front-img-paquetes/'.$paquete_id.'/';
		$this->upload->initialize(array(
				"file_name"     => array("banner_paquete.jpg"),
				"upload_path"   => 	$paq_folder,
				"allowed_types"     =>  "gif|jpg|png",
						"max_size"          =>  '10000',
			 			"max_width"         =>  '2000',
						"max_height"        =>  '732'
					));
				$this->upload->do_multi_upload("uploadfile");
				$data['upload_data'] = $this->upload->get_multi_upload_data();
		$config['base_url'] = site_url('?c=item_kits&m=index');
		$config['total_rows'] = $this->Item_kit->count_all();
		$config['per_page'] = '20'; 
		$this->pagination->initialize($config);
		$data['controller_name']=strtolower(get_class());
		$data['manage_table']=get_item_kits_manage_table( $this->Item_kit->get_all($config['per_page'], $this->input->get('per_page')),$this);
		$this->load->view('item_kits/manage',$data);
	}
	function view_load_cat_banner($categoria)
	{
		$data['categoria']=$categoria;
		$this->load->view("item_kits/form_cat",$data);
	}
	function upload_cat_images($categoria)
	{
		
		$this->load->library('upload');
		$cat_folder='./images/paquetes/categoria-img-paquetes/'.strtoupper($categoria).'/';
		$this->upload->initialize(array(
				"file_name"     => array("banner_categoria.jpg"),
				"upload_path"   => 	$cat_folder,
				"allowed_types"     =>  "gif|jpg|png",
						"max_size"          =>  '10000',
						"max_width"         =>  '1366',
						"max_height"        =>  '500'
					));
				$this->upload->do_multi_upload("uploadfile");
				$data['upload_data'] = $this->upload->get_multi_upload_data();
		$config['base_url'] = site_url('?c=item_kits&m=index');
		$config['total_rows'] = $this->Item_kit->count_all();
		$config['per_page'] = '20'; 
		$this->pagination->initialize($config);
		$data['controller_name']=strtolower(get_class());
		$data['manage_table']=get_item_kits_manage_table( $this->Item_kit->get_all($config['per_page'], $this->input->get('per_page')),$this);
		$this->load->view('item_kits/manage',$data);
	}
	
}

?>