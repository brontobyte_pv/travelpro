<?php
require_once ("secure_area.php");
//require_once ("interfaces/iprescription_controller.php");
class Recursos extends Secure_area //implements iPrescription_controller
{
	function __construct()
	{
		parent::__construct('recursos');
		
	}
	
	function index()
	{
		$config['base_url'] = site_url('?c=Recursos&m=index');
		$config['total_rows'] = $this->Recurso->count_all();
		$config['per_page'] = '20'; 
		$this->pagination->initialize($config);
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_recursos_manage_table($this->Recurso->get_all($config['per_page'], $this->input->get('per_page')),$this);
		$this->load->view('recursos/manage',$data);
		//$this->output->enable_profiler(TRUE);
		
	}
	function view($recurso_id=-1)
	{
		
		$data['info']=$this->Recurso->get_info($recurso_id);
		 if(empty($data))
		 {
			$data= array('nombre','recurso');
		 }
		$this->load->view("recursos/form",$data);
	}
	
	function get_form_width()
	{
		return 350;
	}
	function save($recurso_id=-1)
	{	
		
		$recurso_data = array(
		'nombre'=>$this->input->post('nombre_cuenta'),
		'recurso'=>$this->input->post('cantidad')
		);
		
		if($this->Recurso->save($recurso_data,$recurso_id))
		{
			
			if($recurso_id==-1)
			{
				
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('recursos_successful_adding')));
				
			}
			else //previous item
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('recursos_successful_adding')));
			}
		}
		else//failure
		{
			echo json_encode(array('error'=>true,'message'=>$this->lang->line('recursos_error_adding')));
		}
		
	}
	
	function suggest()
	{
	$suggestions = $this->Recurso->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}
	
	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_recursos_manage_table_data_rows($this->Recurso->search($search),$this);
		echo $data_rows;
	}
	
	function get_row()
	{
		$recurso_id = $this->input->post('row_id');
		$data_row=get_recursos_data_row($this->Recurso->get_info($recurso_id),$this);
		echo $data_row;
	}
	function delete()
	{
		$recursos_to_delete=$this->input->post('ids');

		if($this->Recurso->delete_list($recursos_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('item_kits_successful_deleted').' '.
			count($recursos_to_delete).' '.$this->lang->line('item_kits_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('item_kits_cannot_be_deleted')));
		}
	}
	
	
	
}