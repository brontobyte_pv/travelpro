<?php
require_once ("secure_area.php");
//require_once ("interfaces/iprescription_controller.php");
class Gastos extends Secure_area //implements iPrescription_controller
{
	function __construct()
	{
		parent::__construct('gastos');
		$data['controller_name']=strtolower(get_class());
		$this->load->helper('report',$data);
		
	}
	
	function index()
	{
		
		$config['base_url'] = site_url('?c=gastos&m=index');
		$config['total_rows'] = $this->Gasto->count_all();
		$config['per_page'] = '20'; 
		$total_undone = $this->Gasto->undone(date('n'));
		$total_done = $this->Gasto->done(date('n'));
		$data['total_undone'] =$this->Gasto->undone(date('n'));
		$data['total_done'] =$this->Gasto->done(date('n'));
		if(empty($total_undone))
		{
			$data['total_undone'] =0;
		}
		
		if(empty($total_done))
		{
			$data['total_done'] =0;
		}
		$meses=array(
				  '0'  => 'Todos',
                  '01'  => 'Enero',
                  '02'  => 'Febrero',
				  '03'  => 'Marzo',
                  '04'  => 'Abril',
				  '05'  => 'Mayo',
                  '06'  => 'Junio',
				  '07'  => 'julio',
                  '08'  => 'Agosto',
				  '09'  => 'Septiembre',
                  '10'  => 'Octubre',
				  '11'  => 'Noviembre',
                  '12'  => 'Diciembre'
				  );
		$data['meses'] =$meses;
		$data['selected_month']=date('n');
		$this->pagination->initialize($config);
		//Muestra cuentas
		/*foreach($this->Recurso->get_all()->result_array() as $row)
		{
		$recursos[$row['recurso_id']] =$row['nombre'];
		}
		foreach($this->Recurso->get_all()->result_array() as $row)
		{
		$cuentas[$row['recurso_id']] ='  '.$row['nombre'].'  '.$row['recurso'];
		}*/
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_gastos_manage_table($this->Gasto->get_all($config['per_page'], $this->input->get('per_page'),date('n')),$this);
		 if(empty($cuentas))
				  {
					  $cuentas['-1'] ='No existen';
				  }
		$data['cuentas']=$cuentas;
		$this->load->view('gastos/manage',$data);
        //$this->output->enable_profiler(TRUE);
		
	}
	function filtro()
	{
		$filtro=array(
		'mes'=>$this->input->post('meses'));
		$config['base_url'] = site_url('?c=gastos&m=index');
		$config['total_rows'] = $this->Gasto->count_all();
		$config['per_page'] = '20'; 
		$total_undone = $this->Gasto->undone($filtro['mes']);
		$total_done = $this->Gasto->done($filtro['mes']);
		$data['total_undone'] =$this->Gasto->undone($filtro['mes']);
		$data['total_done'] =$this->Gasto->done($filtro['mes']);
		if(empty($total_undone))
		{
			$data['total_undone'] =0;
		}
		
		if(empty($total_done))
		{
			$data['total_done'] =0;
		}
		$meses=array(
				  '0'  => 'Todos',
                 '01'  => 'Enero',
                  '02'  => 'Febrero',
				  '03'  => 'Marzo',
                  '04'  => 'Abril',
				  '05'  => 'Mayo',
                  '06'  => 'Junio',
				  '07'  => 'julio',
                  '08'  => 'Agosto',
				  '09'  => 'Septiembre',
                  '10'  => 'Octubre',
				  '11'  => 'Noviembre',
                  '12'  => 'Diciembre'
				  );
		$data['meses'] =$meses;
		$data['selected_month']=$filtro['mes'];
		$this->pagination->initialize($config);
		//Muestra cuentas
		/*foreach($this->Recurso->get_all()->result_array() as $row)
		{
		$recursos[$row['recurso_id']] =$row['nombre'];
		}
		foreach($this->Recurso->get_all()->result_array() as $row)
		{
		$cuentas[$row['recurso_id']] ='  '.$row['nombre'].'  '.$row['recurso'];
		}*/
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_gastos_manage_table($this->Gasto->get_all($config['per_page'], $this->input->get('per_page'),$filtro['mes']),$this);
		 if(empty($cuentas))
				  {
					  $cuentas['-1'] ='No existen';
				  }
		$data['cuentas']=$cuentas;
		$this->load->view('gastos/manage',$data);
        $this->output->enable_profiler(TRUE);
		
	}
	function view($gasto_id=-1)
	{
		$data['info']=$this->Gasto->get_info($gasto_id);
		
		/*foreach($this->Customer->get_all()->re|sult_array() as $row)
		{
			$clientes[$row['first_name'].' '. $row['last_name']] =$row['first_name'] .' '. $row['last_name'];
		}*/
		foreach($this->Recurso->get_all()->result_array() as $row)
		{
		$recursos[$row['recurso_id']] =$row['nombre'];
		}
		$estado=array(
                  'Pendiente'  => 'Pendiente',
                  'Hecho'    =>'Hecho');
		$modo=array(
                  'Efectivo'  => 'Efectivo',
                  'Cheque'    =>'Cheque',
				  'Transferencia' =>'Transferencia',
				  'Tarjeta'    =>'Tarjeta');
				  if(empty($recursos))
				  {
					  $recursos['-1'] ='No existen';
					  
				  }
		$data['report_date_range_simple'] = get_simple_date_ranges();
		$data['months'] = get_months();
		$data['days'] = get_days();
		$data['years'] = get_years();
		$data['hours'] = get_hours();
		$data['minutes'] = get_minutes();
		$data['selected_month']=date('n');
		$data['selected_day']=date('d');
		$data['selected_year']=date('Y');
		$data['selected_hour']=date('G');
		$data['selected_minute']=date('i');	
		$data['recursos']=$recursos;
		$data['modo']=$modo;
		$data['estado']=$estado;
		$this->load->view("gastos/form",$data);
	}
	
	function get_form_width()
	{
		return 700;
	}
	function save($gasto_id=-1)
	{	
		$gastos_data = array(
		'fecha'=>$this->input->post('year').'-'.$this->input->post('month').'-'.$this->input->post('day').' '.$this->input->post('hour').':'.$this->input->post('minute').':00',
		'concepto'=>$this->input->post('concepto'),
		'recurso_id'=>$this->input->post('recurso'),
		'estado'=>$this->input->post('estado'),
		'cantidad'=>$this->input->post('cantidad'),
		'tipo'=>$this->input->post('modo')
		);
		if($this->Gasto->save($gastos_data,$gasto_id))
		{
			if($gastos_data['estado']=='Hecho')
			{
				$this->Recurso->rest_recurso($gastos_data['cantidad'],$gastos_data['recurso_id']);
			}	
			if($gasto_id==-1)
			{
				
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('gastos_successful_adding')));
				
			}
			else //previous item
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('gastos_successful_adding')));
			}
		}
			
		else//failure
		{
				echo json_encode(array('error'=>true,'message'=>$this->lang->line('gastos_error_adding')));
		}
			
		
		
	}
	
	
	function cancel()
	{
		
        //$this->output->enable_profiler(TRUE);
		$gasto_id=$this->input->post('ids');
		$cur_app_info = $this->Gasto->get_info($appointment_id);
		if($this->Appointment->cancelar($appointment_id))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('items_successful_updating').' '.
			$cur_app_info->gasto_id,'gasto_id'=>$gasto_id));
			
		}else
		{
			
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('items_error_adding_updating').' '.
			$cur_app_info->gasto_id,'gasto_id'=>-1));
			
			
		}
		
		
	}
	function get_row()
	{
		$appointment_id = $this->input->post('row_id');
		$data_row=get_gastos_data_row($this->Gasto->get_info($gasto_id),$this);
		echo $data_row;
	}
	
	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_gastos_manage_table_data_rows($this->Gasto->search($search),$this);
		echo $data_rows;
	}
	
	function suggest()
	{
	$suggestions = $this->Gasto->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}
	
	
}