<?php
require_once ("secure_area.php");
//require_once ("interfaces/iprescription_controller.php");
class Cobros extends Secure_area //implements iPrescription_controller
{
	function __construct()
	{
		parent::__construct('cobros');
		$this->load->library('cobros_lib');
		$data['controller_name']=strtolower(get_class());
		$this->load->helper('report',$data);
	}
	
	function index()
	{
		$config['base_url'] = site_url('?c=cobros&m=index');
		$config['total_rows'] = $this->Cobro->count_all();
		$total_undone = $this->Cobro->undone(date('n'));
		$total_done = $this->Cobro->done(date('n'));
		$data['total_undone'] =$this->Cobro->undone(date('n'));
		$data['total_done'] =$this->Cobro->done(date('n'));
		if(empty($total_undone))
		{
			$data['total_undone'] =0;
		}
		
		if(empty($total_done))
		{
			$data['total_done'] =0;
		}
		$meses=array(
				'0'  => 'Todos',
                  '01'  => 'Enero',
                  '02'  => 'Febrero',
				  '03'  => 'Marzo',
                  '04'  => 'Abril',
				  '05'  => 'Mayo',
                  '06'  => 'Junio',
				  '07'  => 'julio',
                  '08'  => 'Agosto',
				  '09'  => 'Septiembre',
                  '10'  => 'Octubre',
				  '11'  => 'Noviembre',
                  '12'  => 'Diciembre'
				  );
		$data['meses'] =$meses;
		$data['selected_month']=date('n');
		$config['per_page'] = '20'; 
		$this->pagination->initialize($config);
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_cobros_manage_table($this->Cobro->get_all($config['per_page'], $this->input->get('per_page'),date('n')),$this);
		$customer_id=$this->cobros_lib->get_customer();
		if($customer_id!=-1)
		{
			$info=$this->Customer->get_info($customer_id);
			$data['customer']=$info->first_name." ".$info->last_name;
			
		}
		$this->load->view('cobros/manage',$data);
        //$this->output->enable_profiler(TRUE);
		
	}
	function filtro()
	{
		$filtro=array(
		'mes'=>$this->input->post('meses'));
		$config['base_url'] = site_url('?c=cobros&m=index');
		$config['total_rows'] = $this->Cobro->count_all();
		$total_undone = $this->Cobro->undone($filtro['mes']);
		$total_done = $this->Cobro->done($filtro['mes']);
		$data['total_undone'] =$this->Cobro->undone($filtro['mes']);
		$data['total_done'] =$this->Cobro->done($filtro['mes']);
		if(empty($total_undone))
		{
			$data['total_undone'] =0;
		}
		
		if(empty($total_done))
		{
			$data['total_done'] =0;
		}
		$meses=array(
				'0'  => 'Todos',
                  '01'  => 'Enero',
                  '02'  => 'Febrero',
				  '03'  => 'Marzo',
                  '04'  => 'Abril',
				  '05'  => 'Mayo',
                  '06'  => 'Junio',
				  '07'  => 'julio',
                  '08'  => 'Agosto',
				  '09'  => 'Septiembre',
                  '10'  => 'Octubre',
				  '11'  => 'Noviembre',
                  '12'  => 'Diciembre'
				  );
		$data['meses'] =$meses;
		$data['selected_month']=$filtro['mes'];
		$config['per_page'] = '20'; 
		$this->pagination->initialize($config);
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_cobros_manage_table($this->Cobro->get_all($config['per_page'], $this->input->get('per_page'),$filtro['mes']),$this);
		$customer_id=$this->cobros_lib->get_customer();
		if($customer_id!=-1)
		{
			$info=$this->Customer->get_info($customer_id);
			$data['customer']=$info->first_name." ".$info->last_name;
			
		}
		$this->load->view('cobros/manage',$data);
	}
	function view($cobro_id=-1)
	{
		$data['info']=$this->Cobro->get_info($cobro_id);
		foreach($this->Recurso->get_all()->result_array() as $row)
		{
		$recursos[$row['recurso_id']] =$row['nombre'];
		}
		foreach($this->Cobro->get_info($cobro_id) as $row)
		{
			$nom=$row; 
		}
		
		$estado=array(
                  'Pendiente'  => 'Pendiente',
                  'Hecho'    =>'Hecho');
		
		$modo=array(
                  'Efectivo'  => 'Efectivo',
                  'Cheque'    =>'Cheque',
				  'Transferencia' =>'Transferencia',
				  'Tarjeta'    =>'Tarjeta');
		
		$data['recursos']=$recursos;
		$data['modo']=$modo;
		$data['estado']=$estado;
		
		$customer_id=$this->cobros_lib->get_customer();
		$this->load->model('reports/Summary_payments');
		$model = $this->Summary_payments;
		$pagos=$model->getSummaryDatabyCustomer($customer_id);
		$data['pagos'] = $pagos;
		if($customer_id!=-1 || !empty($nom))
		{
			
			$info=$this->Customer->get_info($customer_id);
			$query=$this->Sale->cuentas_pendientes($customer_id);
			
			if(count($query)!=0)
			{
				foreach($this->Sale->cuentas_pendientes($customer_id)->result_array() as $row)
			{
				$cuentas_abiertas[$row['sale_id']] ="Folio:".$row['sale_id']." Cantidad Abonada:".$row['payment_amount'];
			}
				
			
			}
			else
			{
				$cuentas_abiertas=array(
                  '-1'  => 'No hay');
			}
			
			$data['report_date_range_simple'] = get_simple_date_ranges();
			$data['months'] = get_months();
			$data['days'] = get_days();
			$data['years'] = get_years();
			$data['hours'] = get_hours();
			$data['minutes'] = get_minutes();
			$data['selected_month']=date('n');
			$data['selected_day']=date('d');
			$data['selected_year']=date('Y');
			$data['selected_hour']=date('G');
			$data['selected_minute']=date('i');	
			$data['cuentas_abiertas']=$cuentas_abiertas;
			$data['customer']=$info->first_name." ".$info->last_name;
			$this->load->view("cobros/form",$data);
			
			
		}
		else
		{
			$this->load->view("cobros/form_error",$data);
		}
		//$this->output->enable_profiler(TRUE);
	}
	
	function get_form_width()
	{
		return 700;
	}
	function save($cobro_id=-1)
	{	
		$cobros_data = array(
		'fecha'=>$this->input->post('year').'-'.$this->input->post('month').'-'.$this->input->post('day').' '.$this->input->post('hour').':'.$this->input->post('minute').':00',
		'nom_cliente'=>$this->input->post('customer'),
		'concepto'=>$this->input->post('concepto'),
		'estado'=>$this->input->post('estado'),
		'cantidad'=>$this->input->post('cantidad'),
		'tipo'=>$this->input->post('modo'),
		'recurso_id'=>$this->input->post('recurso'),
		'sale_id'=>$this->input->post('cuentas_abierta')
		
		);
		if($this->Cobro->save($cobros_data,$cobro_id))
		{
			if($cobros_data['estado']=='Hecho')
			{
				$this->Recurso->sum_recurso($cobros_data['cantidad'],$cobros_data['recurso_id']);
				$this->Sale->update_payment_amount($cobros_data['cantidad'],$cobros_data['sale_id']);
				
			}
			if($cobro_id==-1)
			{
				
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('cobros_successful_adding').$cobros_data['fecha']));
				
			}
			else //previous item
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('cobros_successful_adding').$cobros_data['fecha']));
			}
		
		}
		else//failure
		{
			echo json_encode(array('error'=>true,'message'=>$this->lang->line('cobros_error_adding')));
		}
		$this->remove_customer();
		
	}
	
	function suggest()
	{
		$suggestions = $this->Cobro->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}
	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_cobros_manage_table_data_rows($this->Cobro->search($search),$this);
		echo $data_rows;
	}
	function cancel()
	{
		
       
		$cobro_id=$this->input->post('ids');
		$cur_app_info = $this->Cobro->get_info($appointment_id);
		if($this->Appointment->cancelar($appointment_id))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('items_successful_updating').' '.
			$cur_app_info->cobro_id,'cobro_id'=>$cobro_id));
			
		}else
		{
			
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('items_error_adding_updating').' '.
			$cur_app_info->cobro_id,'cobro_id'=>-1));
			
			
		}
		
		
	}
	function get_row()
	{
		$cobro_id = $this->input->post('row_id');
		$data_row=get_cobros_data_row($this->Cobro->get_info($cobro_id),$this);
		echo $data_row;
	}
	
	function customer_search()
	{
		$suggestions = $this->Customer->get_customer_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}
	
	function select_customer()
	{	
		$customer_id = $this->input->post("customer");
		$this->cobros_lib->set_customer($customer_id);
		
		$this->index();
		
	}
	
	function remove_customer()
	{
		$this->cobros_lib->remove_customer();
		$id=$this->cobros_lib->get_customer();
		echo $id;
		
		
	}
	function delete()
	{
		$cobro_to_delete=$this->input->post('ids');
		
		if($this->Cobro->delete_list($cobro_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('customers_successful_deleted').' '.
			count($customers_to_delete).' '.$this->lang->line('customers_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('customers_cannot_be_deleted')));
		}
	}
	
}