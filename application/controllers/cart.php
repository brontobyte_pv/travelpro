<?php

class Cart extends Controller
{
	function __construct()
	{
		parent::__construct();	
		$this->load->library('pedido_lib');
	}
	
	function index()
	{
		//Parametros para crear captcha
		$vals = array(
		
		'img_path' => './images/',
        'img_url' => base_url() .'images/',
		'img_width' => ' 200',
		'expiration' => 3600);
		//Se genera la captcha le pasamos los parametros de creacion con el array $vals y asignamos el array de resultado a la variable $captcha
		$captcha = create_captcha($vals);
		//Asignamos el valor 'image' extraido del array $captcha y se lo asignamos al array de salida $datos con el nombre de img
		$datos['error_captcha']='';
		$datos['img']=$captcha['image'];
		$datos['subtotal']=$this->pedido_lib->get_subtotal();
		$datos['total']=$this->pedido_lib->get_total();
		$datos['items_totales']=$this->pedido_lib->get_items_totales();
		$config['per_page'] = '20';
		$datos['categories']= $this->Item_kit->get_categories();
		$datos['cart']=$this->pedido_lib->get_cart();
		$datos['cant']=$this->pedido_lib->get_items_totales();
		$datos['total']=$this->pedido_lib->get_total();
		// $this->output->enable_profiler(TRUE);

		$this->load->view("reservaciones", $datos);
		
		
	}
	function add($id)
	{
		/*if($this->Customer->is_logged_in())
		{*/
		$datos=array();
		$cant_adulto=$this->input->post('input_cant_adulto');
		$cant_menor=$this->input->post('input_cant_menor');
		
		
		
		$item_kit_id = $id;
		
		$item_kit_items_info=$this->Item_kit_items->get_info($item_kit_id);
			foreach($item_kit_items_info as $item)
			{
				
				$item_info = $this->Item->get_info($item['item_id']);
				$piezas=explode(" ", strtoupper($item_info->name));
				if($piezas[0]=='ADULTO')
				{
					if($cant_adulto>0)
					{	
						$cantidad=$cant_adulto;
						$this->pedido_lib->add_item($item['item_id'],$cantidad);
					}	
				}
				else
				{
					if($cant_menor>0)
					{
						$cantidad=$cant_menor;
						$this->pedido_lib->add_item($item['item_id'],$cantidad);
					}
				}	
				
				
			}
		
		$item_kit_info=$this->Item_kit->get_info($item_kit_id);
		
		$exist=$this->pedido_lib->add_item_kit($item_kit_info->name);
		$datos['cart']=$this->pedido_lib->get_cart();
		
		$datos['total']=$this->pedido_lib->get_total();
		if(!$exist)
		{
			$datos['error']=$this->lang->line('sales_unable_to_add_item');
		}
		
		
		redirect('/cart/', 'refresh');
		
		/*}else
		{*/
			
			
	//redirect(url_title($this->lang->line('buying_title'),'refresh','_'));
			
			
		/*}*/
	}
	function add_express()
	{
		/*if($this->Customer->is_logged_in())
		{*/
		$datos=array();
		$datos['pais']=$this->input->post('pais');
		$cantidad=$this->input->post('cantidad');
		$this->pedido_lib->set_country($datos['pais']);
		
		
		$item_id = $this->input->post('id');
		$quantite=null;
		
		$exist=$this->pedido_lib->add_item($item_id,$cantidad);
		$datos['cart']=$this->pedido_lib->get_cart();
		
		$datos['total']=$this->pedido_lib->get_total();
		if(!$exist)
		{
			$datos['error']=$this->lang->line('sales_unable_to_add_item');
		}
		
		
		redirect('/cart/', 'refresh');
		
		/*}else
		{*/
			
			
	//redirect(url_title($this->lang->line('buying_title'),'refresh','_'));
			
			
		/*}*/
	}
	function _reload($datos=array())
	{
		
		
		
		$datos['cart']=$this->pedido_lib->get_cart();
		$datos['total']=$this->pedido_lib->get_total();
		
	
	
			
			$datos['customer']=$info->first_name." ".$info->last_name;
			$datos['customer_email']=$info->email;
	
		
		
	}
	function cancelar_pedido()
    {
    	$this->pedido_lib->empty_cart();

    	redirect(url_title($this->lang->line('buying_title'),'refresh','_'));

    }
	
	function borrar_item($line)
    {
		
    	
		$this->pedido_lib->delete_item($line);

    	redirect('/cart/', 'refresh');

    }
	function exist_client()
	{
		$datos['categories']= $this->Item_kit->get_categories();
		$correo=$this->input->post('chk_email');
		$exist=$this->Customer->exists_by_mail($correo);
		$datos['correo']=$correo;
		$datos['payments']=array(
                  'efectivo'  => $exist.'Efectivo',
                  'card'    =>'Tarjeta de Credito',
				  'giftcard'    =>'Tarjeta de Regalo',
				  );
				  echo $exist;
		if($exist==1)
		{	
			
				//Parametros para crear captcha
				$vals = array(
			
				'img_path' => './images/',
				'img_url' => base_url() .'images/',
				'img_width' => ' 200',
				'img_height' => '60', 
				'expiration' => 3600);
				//Se genera la captcha le pasamos los parametros de creacion con el array $vals y asignamos el array de resultado a la variable $captcha
				$captcha = create_captcha($vals);
				//Asignamos el valor 'image' extraido del array $captcha y se lo asignamos al array de salida $datos con el nombre de img
				$datos['img']=$captcha['image'];
				//Ponemos el valor del string de la captcha y lo guardamos en una cockie de session para poder compararlo despues del submit
				
				$this->session->set_userdata('captchaWord', $captcha['word']);
				$datos['cant']=$this->pedido_lib->get_items_totales();
				$datos['subtotal']=$this->pedido_lib->get_subtotal();
				$datos['total']=$this->pedido_lib->get_total();
				$datos['pedido']=$this->pedido_lib->get_cart();
				
				
				$datos['error_captcha']='';
			$customer_info=$this->Customer->get_info_by_email($correo);
			$datos['customer_info']=$customer_info;
			$this->pedido_lib->set_customer($customer_info->person_id);
			$this->load->view('confirmar_pedido_cliente',$datos);
			
			
			
			
			
				/*$this->session->unset_userdata('captchaWord');
				$action=1;
				$person_data['cant']=$this->pedido_lib->get_items_totales();
				$person_data['pais']='USA';
				$this->mail_contact_msj($person_data,$action);
			
				
			
				*/
				
			
		
		}
		else
		{	
				//Parametros para crear captcha
				
				$vals = array(
			
				'img_path' => './images/',
				'img_url' => base_url() .'images/',
				'img_width' => ' 200',
				'img_height' => '60', 
				'expiration' => 3600);
				//Se genera la captcha le pasamos los parametros de creacion con el array $vals y asignamos el array de resultado a la variable $captcha
				$captcha = create_captcha($vals);
				//Asignamos el valor 'image' extraido del array $captcha y se lo asignamos al array de salida $datos con el nombre de img
				$datos['img']=$captcha['image'];
				//Ponemos el valor del string de la captcha y lo guardamos en una cockie de session para poder compararlo despues del submit
				
				$this->session->set_userdata('captchaWord', $captcha['word']);
				$datos['cant']=$this->pedido_lib->get_items_totales();
				$datos['subtotal']=$this->pedido_lib->get_subtotal();
				$datos['total']=$this->pedido_lib->get_total();
				$datos['pedido']=$this->pedido_lib->get_cart();
				$datos['error_captcha']='';
				$this->pedido_lib->set_customer(-1);
				$this->load->view('confirmar_pedido',$datos);
			}	
	$this->output->enable_profiler(TRUE);
				
	}
	function enviar_pedido()
	{
		
		
			$this->session->unset_userdata('captchaWord');
			$action=1;
			$person_data['cant']=$this->pedido_lib->get_items_totales();
			$person_data['pais']='USA';
			$this->mail_contact_msj($person_data,$action);
			$person_data['txt_p']='USA';
			$this->pedido_lib->empty_cart();
			
    	$this->load->view("msj_pedido_enviado",$person_data);
		
			

	
	
			//$this->output->enable_profiler(TRUE);
		
		
		
		
	}
	function complete()
	{
		$datos['categories']= $this->Item_kit->get_categories();
			$datos['correo']=$this->input->post('correo');
	   $customer_id=$this->pedido_lib->get_customer();
		$datos['payments']=array(
                  'efectivo'  => 'Efectivo',
                  'card'    =>'Tarjeta de Credito',
				  'giftcard'    =>'Tarjeta de Regalo',
				  );
				  
				  $capt=$this->input->post('captcha');
				  $this->pedido_lib->set_payments($this->input->post('payments'));
				 
		$person_data = array(
					'first_name'=>$this->input->post('nombre'),
					'last_name'=>$this->input->post('apellidos'),
					'email'=>$this->input->post('correo'),
					'phone_number'=>$this->input->post('telefono'),
					'address_1'=>$this->input->post('direccion'),
					'address_2'=>$this->input->post('address_2'),
					'city'=>$this->input->post('ciudad'),
					'state'=>$this->input->post('state'),
					'zip'=>$this->input->post('cpostal'),
					'comments'=>$this->input->post('mensaje')
					
					);
			$datos['error_captcha']='';
			if($customer_id==-1)
			{
			$this->form_validation->set_rules('nombre',$this->lang->line('form_name') , 'required');
			$this->form_validation->set_rules('apellidos',$this->lang->line('form_name') , 'required');
			$this->form_validation->set_rules('direccion', $this->lang->line('form_dire') , 'required');
			$this->form_validation->set_rules('telefono', $this->lang->line('form_dire') , 'required');
			$this->form_validation->set_rules('ciudad', $this->lang->line('form_city') , 'required');
			$this->form_validation->set_rules('correo', $this->lang->line('form_correo') , 'required|matches[correo2]');
			$this->form_validation->set_rules('correo2', $this->lang->line('form_confirm') , 'required');
			$val_form=$this->form_validation->run();
			}
			else
			{
				$val_form=TRUE;
			}
			$word = $this->session->userdata('captchaWord');
			
		
			if ($val_form==TRUE && strcmp(strtoupper($capt),strtoupper($word)!= 0))
			{
				if($customer_id==-1)
				{
					$customer_data=array(
					'account_number'=>$this->input->post('account_number')=='' ? null:$this->input->post('account_number'),
					'taxable'=>$this->input->post('taxable')=='' ? 0:1,
					'company'=>$this->input->post('company')
					);
					//$this->session->unset_userdata('captchaWord');
					$this->Customer->save($person_data,$customer_data,$customer_id);
					
					$customer_id=$customer_data['person_id'];
					
				}
				$datos['cart']=$this->pedido_lib->get_cart();
				$datos['subtotal']=$this->pedido_lib->get_subtotal();
				$datos['total']=$this->pedido_lib->get_total();
				$datos['transaction_time']= date('m/d/Y h:i:s a');
				$comment = '';

				$datos['payments']=$this->pedido_lib->get_payments();
				$pedido_id=$this->Pedido->save($datos['cart'], $customer_id,$datos['payments'],$datos['total']);
				$this->msj_admin($pedido_id);
				$this->msj_custom($pedido_id);
				$this->pedido_lib->clear_all();
				// $this->output->enable_profiler(TRUE);
				$datos['cant']=0;
				$this->load->view("msj_pedido_enviado",$datos);
				
				/*
				$person_data['cant']=$this->pedido_lib->get_items_totales();
				$person_data['pais']='USA';
				$this->mail_contact_msj($person_data,$action);
				$person_data['txt_p']='USA';
				$this->pedido_lib->empty_cart();
				$this->load->view("msj_pedido_enviado",$person_data);*/
			}
			else
			{
				
			
				//Parametros para crear captcha
				$vals = array(
			
				'img_path' => './images/',
				'img_url' => base_url() .'images/',
				'img_width' => ' 200',
				'img_height' => '60', 
				'expiration' => 3600);
				//Se genera la captcha le pasamos los parametros de creacion con el array $vals y asignamos el array de resultado a la variable $captcha
				$captcha = create_captcha($vals);
				//Asignamos el valor 'image' extraido del array $captcha y se lo asignamos al array de salida $datos con el nombre de img
				$datos['img']=$captcha['image'];
				//Ponemos el valor del string de la captcha y lo guardamos en una cockie de session para poder compararlo despues del submit
				
				$this->session->set_userdata('captchaWord', $captcha['word']);
				$datos['cant']=$this->pedido_lib->get_items_totales();
				$datos['subtotal']=$this->pedido_lib->get_subtotal();
				$datos['total']=$this->pedido_lib->get_total();
				$datos['pedido']=$this->pedido_lib->get_cart();
				$datos['error_captcha']='<span id="error_captcha">Error Captcha</span>';
				
				
				if($customer_id==-1)
				{
					$this->load->view('confirmar_pedido',$datos);
				}
				else
				{
					$customer_info=$this->Customer->get_info($customer_id);
			$datos['customer_info']=$customer_info;
					$this->load->view('confirmar_pedido_cliente',$datos);
			
				}
				$this->output->enable_profiler(TRUE);
			}
		
	}
	function msj_admin($pedido_id)
	{
		
		$info=$this->Pedido->get_info($pedido_id);
		$info_custom=$this->Customer->get_info($info->customer_id);
		
		$msj='Una persona de nombre:
		'.$info_custom->first_name.''.$info_custom->last_name.'
		Hizo un pedido de Ayahuasca
		Puedes revisar los detalles en el siguiente enlace:
		http://travelpro.mx/index.php/pedidos
		User:klara
		Pass:Vallart@123';
		$this->load->library('email');
		$config['mailtype'] = 'text';
		$this->email->initialize($config);
		$this->email->from($info_custom->email,'Reservacion Online');
		$this->email->to('nan.php.mx@gmail.com','clara@travelpro.mx','info@travelpro.mx'); 
		$this->email->subject('Estimado'.$info_custom->first_name.''.$info_custom->last_name);
		$this->email->message($msj);	

		$this->email->send();
		//joseluis@ayahuasca.com.pe
		//$this->output->enable_profiler(TRUE);
	}
	function msj_custom($pedido_id)
	{
		$info=$this->Pedido->get_info($pedido_id);
		$info_custom=$this->Customer->get_info($info->customer_id);
		$msj='Estimado:'.$info_custom->first_name.' '.$info_custom->last_name.'
		Su reservacion Se Género Correctamente,
		Cuando lo haya pagado favor de enviar una copia del comprobante a:
		info@travelpro.mx
		';
		
		$this->load->library('email');
		$config['mailtype'] = 'text';
		$this->email->initialize($config);
		$this->email->from('info@travelpro.mx','Reservaciones TravelPro');
		$this->email->to($info_custom->email); 
		$this->email->subject('Estimado'.$info_custom->first_name.''.$info_custom->last_name);
		$this->email->message($msj);	

		$this->email->send();
	}
}
?>