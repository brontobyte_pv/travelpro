<?php
require_once ("secure_area.php");
//require_once ("interfaces/iprescription_controller.php");
class Pedidos extends Secure_area //implements iPrescription_controller
{
	function __construct()
	{
		parent::__construct('pedidos');
		$this->load->library('sale_lib');
		
	}
	
	function index()
	{
		
		$this->lang_lib->set_position('inside');
		$config['base_url'] = site_url('?c=services&m=index');
		$config['total_rows'] = $this->Pedido->count_all();
		$config['per_page'] = '20'; 
		$data['checked'] = FALSE; 
		$this->pagination->initialize($config);
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_pedidos_manage_table($this->Pedido->get_all($config['per_page'], $this->input->get('per_page')),$this);
		$this->load->view('pedidos/manage',$data);
        //$this->output->enable_profiler(TRUE);
		
	}
	function view($pedido_id=-1)
	{
		$data['info']=$this->Pedido->get_info($pedido_id);
		
		foreach($this->Customer->get_all()->result_array() as $row)
		{
			$clientes[$row['first_name'].' '. $row['last_name']] =$row['first_name'] .' '. $row['last_name'];
		}
		foreach($this->Employee->get_all()->result_array() as $row)
		{
			$empleados[$row['first_name'].' '. $row['last_name']] =$row['first_name'] .' '. $row['last_name'];
		}
		$estado=array(
                  '1'  => 'Pendiente',
                  '0'    =>'Cancelada');
		 $data['clientes']=$clientes;
		  $data['empleados']=$empleados;
		 $data['estado']=$estado;
		$this->load->view("pedidos/form",$data);
	}
	function customer_details($pedido_id=-1)
	{
		$info=$this->Pedido->get_info($pedido_id);
		$info_custom=$this->Customer->get_info($info->customer_id);
		$data['cliente']=$info_custom->first_name.' '.$info_custom->last_name;
		$data['dir']=$info_custom->address_1;
		$data['city']=$info_custom->city;
		$data['zip']=$info_custom->zip;
		$data['country']=$info_custom->country;
		$data['email']=$info_custom->email;
		$data['tel']=$info_custom->phone_number;
		$data['payment']=$info->payment_type;
		
		$this->load->view("pedidos/detail_customer",$data);
	}
	function items_details($pedido_id=-1)
	{
		$info=$this->Pedido->get_info($pedido_id);
		$items=$this->Pedido->get_items_info($pedido_id);
		
		$data['pedido_id']=$pedido_id;
		$data['total']=$info->total;
		
		$this->load->view("pedidos/items_details",$data);
	}
	function send_msj($pedido_id=-1)
	{
		$info=$this->Pedido->get_info($pedido_id);
		$info_custom=$this->Customer->get_info($info->customer_id);
		$data['cliente']=$info_custom->first_name.''.$info_custom->last_name;
		$data['pedido_id']=$pedido_id;
		
		
		$this->load->view("pedidos/send_msj",$data);
	}
	function get_form_width()
	{
		return 350;
	}
	function msj($pedido_id)
	{
		
		$msj=$this->input->post('msj');
		$info=$this->Pedido->get_info($pedido_id);
		$info_custom=$this->Customer->get_info($info->customer_id);
		
		
		$this->load->library('email');
		$config['mailtype'] = 'text';
		$this->email->initialize($config);
		$this->email->from('joseluis@ayahuasca.com.pe','Ayahuasca Chamán');
		$this->email->to($info_custom->email); 
		$this->email->subject('Estimado'.$info_custom->first_name.''.$info_custom->last_name);
		$this->email->message($msj);	

		$this->email->send();
		redirect('/pedidos/', 'refresh');
	}
	function concluir_pedido($pedido_id=-1)
	{
		
		$this->sale_lib->set_pedido_sale($pedido_id);
		$info=$this->Pedido->get_info($pedido_id);
		$info_custom=$this->Customer->get_info($info->customer_id);
		$customer_id_a=$this->sale_lib->get_customer();
		if($info->customer_id!=$customer_id_a)
		{
			$this->sale_lib->empty_cart();
			
		}
		
		$info=$this->Pedido->get_info($pedido_id);
		$items=$this->Pedido->get_items_info($pedido_id);
		$this->sale_lib->set_customer($info->customer_id);
		foreach($items as $item)
		{
			$this->sale_lib->add_item($item['item_id'],$item['quantity_purchased'],$discount=0,$item['item_unit_price'],$description='',$serialnumber='');
			
		}
		
		redirect('/sales/', 'refresh');
	}
	
	function cancel($pedido_id)
	{
		
        $this->Pedido->cancel($pedido_id);
		$this->Pedido->cancel_items($pedido_id);
		redirect('/pedidos/', 'refresh');
		
		
	}
	function get_row()
	{
		$pedido_id = $this->input->post('row_id');
		$data_row=get_pedidos_data_row($this->Pedido->get_info($pedido_id),$this);
		echo $data_row;
		
	}
	function get_row_f()
	{
		$pedido_id = $this->input->post('row_id');
		$data_row=get_pedidosD_data_row($this->Pedido->get_info($pedido_id),$this);
		echo $data_row;
		
	}
	function suggest()
	{
		$suggestions = $this->Pedido->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}
	function suggest_f()
	{
		$suggestions = $this->Pedido->get_search_suggestions_f($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}
	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_pedidos_manage_table_data_rows($this->Pedido->search($search),$this);
		echo $data_rows;
		
	}
	function search_f()
	{
		$search=$this->input->post('search');
		$data_rows=get_pedidosD_manage_table_data_rows($this->Pedido->search_f($search),$this);
		echo $data_rows;
		
	}
	function hechos()
	{
		$value=$this->input->post('hechos');
		if($value==1)
		{
			$config['base_url'] = site_url('?c=services&m=index');
			$config['total_rows'] = $this->Pedido->count_all();
			$config['per_page'] = '20'; 
			$data['checked'] = TRUE; 
			$this->pagination->initialize($config);
			$data['controller_name']=strtolower(get_class());
			$data['form_width']=$this->get_form_width();
			$data['manage_table']=get_pedidosD_manage_table($this->Pedido->get_all_filtered($config['per_page'], $this->input->get('per_page')),$this);
			$this->load->view('pedidos/manage_f',$data);
		}
		else
		{
			redirect('/pedidos/', 'refresh');
		}
		
	}
	
	
}