<?php
class Front extends Controller
{
	function __construct()
	{
		parent::__construct();	
		$this->load->library('pedido_lib');
		$this->load->library('lang_lib');
	}
	
	function index()
	{
		$data['categories']= $this->Item_kit->get_categories();
		$person_data = array(
		'mensaje'=>$this->input->post('email'));
		$data['error_captcha']='';
	
		
		//////////Captcha
		$this->form_validation->set_rules('mensaje', "Name", 'required');
		$word = $this->session->userdata('captchaWord');
		$palabra=rand(0, 1000);
		if ($this->form_validation->run() == TRUE && strcmp(strtoupper($person_data['captcha']),strtoupper($word)) == 0)
    {
      /** Validation was successful; show the Success view **/
      
      
      /* Clear the session variable */
      $this->session->unset_userdata('captchaWord');
      
     $this->mail_contact_msj($person_data);
      /* Get the user's name from the form */
      $name = set_value('name');
	  $correo = set_value('correo');
		$data['error_captcha']='';
     $data = array('name' => $name,'correo' => $correo);
      $this->load->view('contact_msj', $data);}
	  else{ $vals = array(
		'word' => $palabra,
		'img_path' => './images/',
        'img_url' => base_url() .'images/',
		'img_width' => '100',
		'expiration' => 3600);
		$captcha = create_captcha($vals);
		$data['img']=$captcha['image'];
		if ((!empty($person_data['captcha'])&&!empty($person_data['name']))||(empty($person_data['captcha'])&& !empty($person_data['name'])))
		{
		$data['error_captcha']='<span id="error_captcha">Error captcha</span>';
		}else{$data['error_captcha']='';}
		$this->session->set_userdata('captchaWord', $captcha['word']);
		
		//Aqui cominezan el cambio de idioma
		$idioma=$this->lang_lib->get_idioma();
		if(empty($idioma)){$idioma='english';}	
		$data['link']= '';
		$data['clase']='active';
		$data['cart']=$this->pedido_lib->get_cart();
		$data['items_totales']=$this->pedido_lib->get_items_totales();
		$data['active1']="active";
		$this->load->view("front",$data);
	}
	}
	
	function paquetes_temp()
	{
		$data['cart']=$this->pedido_lib->get_cart();
		$data['items_totales']=$this->pedido_lib->get_items_totales();
		$data['active2']="active";
		$this->load->view("paquetes",$data);
	}
	function cruceros()
	{
		$data['cruceros']=get_cruceros_manage_table($this->Item_kit->get_cruceros(),$this);
		$data['categorie']= 'Cruceros';
		$data['categories']= $this->Item_kit->get_categories();
		$data['cart']=$this->pedido_lib->get_cart();
		$data['items_totales']=$this->pedido_lib->get_items_totales();
		$data['active4']="active";
		$this->load->view("cruceros",$data);
	}
	
	function privacidad()
	{
		$data['categories']= $this->Item_kit->get_categories();
		$data['cart']=$this->pedido_lib->get_cart();
		$data['items_totales']=$this->pedido_lib->get_items_totales();
		$data['active5']="active";
		$this->load->view("privacidad",$data);
	}
	
	function corporativos()
	{
		$data['categories']= $this->Item_kit->get_categories();
		$data['cart']=$this->pedido_lib->get_cart();
		$data['items_totales']=$this->pedido_lib->get_items_totales();
		$data['active7']='active';
		$this->load->view("corporativos",$data);
	}
	
	function reservaciones()
	{
		$data['categories']= $this->Item_kit->get_categories();
		$data['cart']=$this->pedido_lib->get_cart();
		$data['items_totales']=$this->pedido_lib->get_items_totales();
		$data['active3']='active';
		$this->load->view("reservaciones",$data);
	}
	
	function contacto()
	{
		$data['categories']= $this->Item_kit->get_categories();
		$datos_del_form = array(            //array que obtiene los valores del view 
		'nombre'=>$this->input->post('nombre'),
		'correo'=>$this->input->post('correo'),
		'tel'=>$this->input->post('tel'),
		'ciudad'=>$this->input->post('ciudad'),
		'mensaje'=>$this->input->post('mensaje'),
		'captcha'=>$this->input->post('captcha'));
		
		$this->form_validation->set_rules('nombre','Nombre','required');//pone la regla de validacion para los data 
		$this->form_validation->set_rules('correo','Correo','required');
		$this->form_validation->set_rules('tel','Teléfono','required');
		$this->form_validation->set_rules('ciudad','Ciudad','required');
		$this->form_validation->set_rules('mensaje','Mensaje','required');
		$word = $this->session->userdata('captchaWord');
		$data['error_captcha']='';
		
	if ($this->form_validation->run() == TRUE && strcmp(strtoupper($datos_del_form['captcha']),strtoupper($word)) == 0)
    {
		  /** Validation was successful; show the Success view **/
		  /* Clear the session variable */
		  $this->session->unset_userdata('captchaWord');
		  $action=2;
		  $this->mail_contact_msj($datos_del_form,$action);
		  /* Get the user's name from the form */
		  $nombre = set_value('nombre');
		  $correo = set_value('correo');
		  $data['error_captcha']='';
		  $data = array('nombre' => $nombre,'correo' => $correo);
		  $this->load->view('contact_msj', $data);
	}
	else
	{
			 
			 //Se crea un string random para la captcha y se asigna el valor a $palabra
			//Parametros para crear captcha
			$vals = array(
				
				'img_path' => './images/',
				'img_url' => base_url().'images/',
				'img_width' => ' 50',
				'img_height' => '31', 
				'expiration' => 3600);
			//Se genera la captcha le pasamos los parametros de creacion con el array $vals y asignamos el array de resultado a la variable $captcha
			$captcha = create_captcha($vals);
			//Asignamos el valor 'image' extraido del array $captcha y se lo asignamos al array de salida $data con el nombre de img
			$data['img']=$captcha['image'];
			//Ponemos el valor del string de la captcha y lo guardamos en una cockie de session para poder compararlo despues del submit
			$this->session->set_userdata('captchaWord', $captcha['word']);
		
			if ((!empty($datos_del_form['captcha'])&&!empty($datos_del_form['nombre']))||(empty($datos_del_form['captcha'])&& !empty($datos_del_form['nombre'])))
			{
				$data['error_captcha']='<span id="error_captcha">Error captcha</span>';
			}
			else
			{
				$data['error_captcha']='<span id="error_captcha">Código incorrecto, favor de escribirlo de nuevo</span>';
			}
			
		

		$data['cart']=$this->pedido_lib->get_cart();
		$data['items_totales']=$this->pedido_lib->get_items_totales();
		$this->load->view("contacto",$data);
		}
	}
	
	function tips()
	{
		$data['categories']= $this->Item_kit->get_categories();
		$data['cart']=$this->pedido_lib->get_cart();
		$data['items_totales']=$this->pedido_lib->get_items_totales();
		$data['active8']='active';
		$this->load->view("tips",$data);
		
	}
	
	function detalles_paquete($item_kit_id)
	{
		$paquete_detail=$this->Item_kit->get_info($item_kit_id);
		$productos=$this->Item_kit_items->get_info($item_kit_id);
			$data['cant_adulto']='';
			$data['cant_menor']='';
		$data['precio_adulto'] ='';
			 $data['precio_menor'] ='';
			 $data['duracion'] =0;
			 $data['precio_menor_t'] ='';
			 $data['precio_adulto_t'] ='';
			  $data['precio_paquete'] =$paquete_detail->kit_price;
			 $quantity=array();
			 $vigencia=explode("-", $paquete_detail->expiracion);
		$dia=explode(" ", $vigencia[2]);
		$data['dia']=$dia[0];
		$data['year']=$vigencia[0];
		switch ($vigencia[1]) 
		{
			case 1:
				$data['mes']='Enero';
				break;
			case 2:
				$data['mes']='Febrero';
				break;
			case 3:
				$data['mes']='Marzo';
				break;
			case 4:
				$data['mes']='Abril';
				break;
			case 5:
				$data['mes']='Mayo';
				break;
			case 6:
				$data['mes']='Junio';
				break;
			case 7:
				$data['mes']='Julio';
				break;
			case 8:
				$data['mes']='Agosto';
				break;
			case 9:
				$data['mes']='Septiembre';
				break;
			case 10:
				$data['mes']='Octubre';
				break;
			case 11:
				$data['mes']='Noviembre';
				break;
			case 12:
				$data['mes']='Diciembre';
				break;
			
		}
		if(isset($productos))
		{	
			foreach ($productos as $producto)
			{ 
				$item_info = $this->Item->get_info($producto['item_id']);
				 
				 $tipo=explode(" ", $item_info->name);
				 if($tipo[0]=='Adulto')
				 {
					  $data['cant_adulto']=$producto['quantity'];
					$data['precio_adulto'] =to_currency($item_info->unit_price);
					 $quantity=explode(".", $item_info->reorder_level);
					$data['duracion'] =$quantity[0];
					$data['precio_adulto_t'] ='Adulto(s)';
				 }
				 elseif($tipo[0]=='Menor')
				 {
					 $data['cant_menor']=$producto['quantity'];
					 $data['precio_menor'] =to_currency($item_info->unit_price);
					 $data['precio_menor_t'] ='Menor(es)';
				 }
			

			}
		}
		$productos=$this->Item_kit_items->get_info($item_kit_id);
		
		
		
		
		
			
		
		
		$data['item_detail']= $paquete_detail;
		$data['categories']= $this->Item_kit->get_categories();
		$data['cart']=$this->pedido_lib->get_cart();
		$data['items_totales']=$this->pedido_lib->get_items_totales();
		$this->load->view("detalles_paquete",$data);
		
	}
	
	
	
	
	function mail_contact_msj($to_send, $action) {
		/*if($action==1){
			$asunto=$to_send['asunto'];
			$cuerpo='Una persona de nombre: '.$to_send['nombre'].' <br>Quiere hacer una reservacion el dia<br>"'.$to_send['date'].'"<br>A las :'.$to_send['time'].'  para: '.$to_send['nu_personas'].' personas <br> Comentarios: '.$to_send['mensaje'].'';		
	/*	echo 'Una persona de nombre: <b>'.$to_send['nombre'].'</b><br>Quiere hacer una reservacion el dia<br>"'.$to_send['date'].'"<br>A las :'.$to_send['time'].'  para: '.$to_send['nu_personas'].' personas <br> Comentarios: '.$to_send['mensaje'].'';
		}*/
	//	if($action==2){
			$asunto='Mensaje de contacto';
			$cuerpo='Nombre: '.$to_send['nombre'].'<br>
			Correo:'.$to_send['correo'].'<br>
			Teléfono:'.$to_send['tel'].'<br>
			Ciudad:'.$to_send['ciudad'].'<br>
			Mensaje:<br>'.$to_send['mensaje'].' ';
		/*	echo 'Una persona de nombre: '.$to_send['name'].'<br>Nos envia el siguiente mensaje:<br>"'.$to_send['mensaje'].'" ';*/
		/*}*/
		
		$this->load->library('email');
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->from($to_send['correo'],'Mensaje de la WEB');
		$this->email->to('nan.php.mx@gmail.com, info@travelpro.mx, maru@travelpro.mx, clara@travelpro.mx'); 
		$this->email->subject($asunto);
		$this->email->message($cuerpo);			
		$this->email->send();
	}
	function paquetes($category)
	{
		
		
		$data['paquetes']=get_itemsCAT_manage_table($this->Item_kit->count_all_cat($category),$this);
		$data['categorie']= $category;
		$data['categories']= $this->Item_kit->get_categories();
		$data['cart']=$this->pedido_lib->get_cart();
		$data['items_totales']=$this->pedido_lib->get_items_totales();
		$data['active2']="active";
		$this->load->view("paquetes",$data);
		//$this->output->enable_profiler(TRUE);
		
	}
	
	
	
	
	
	
	
	
}
?>