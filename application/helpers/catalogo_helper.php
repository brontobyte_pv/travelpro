<?php
	

/*******El catalogo muestra solo las categorias***********/
	function get_itemsCAT_manage_table($paquetes,$controller)
	{
		$CI =& get_instance();
		$table='<section id="cover-paq" class="cover_standar">';
		

		
		
		
		$table.=get_itemsCAT_manage_table_data_rows($paquetes,$controller);
		$table.='<a id="btn-next" href="">Ver más paquetes<span> ></span></a>
					</section>';
		return $table;
	}
	
	function get_itemsCAT_manage_table_data_rows($paquetes,$controller)
	{
		$CI =& get_instance();
		$table_data_rows='';
		
		foreach($paquetes->result() as $paquete)
		{
			$table_data_rows.=get_itemCAT_data_row($paquete,$controller);
		}
		
		if($paquetes->num_rows()==0)
		{
			$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('items_no_items_to_display')."</div></tr></tr>";
		}
		
		return $table_data_rows;
	
	}
	function get_itemCAT_data_row($paquete,$controller)
	{
		$CI =& get_instance();
		$vigencia=explode("-", $paquete->expiracion);
		$dia=explode(" ", $vigencia[2]);
		
		switch ($vigencia[1]) 
		{
			case 1:
				$vigencia[1]='Ene';
				break;
			case 2:
				$vigencia[1]='Feb';
				break;
			case 3:
				$vigencia[1]='Mar';
				break;
			case 4:
				$vigencia[1]='Abr';
				break;
			case 5:
				$vigencia[1]='May';
				break;
			case 6:
				$vigencia[1]='Jun';
				break;
			case 7:
				$vigencia[1]='Jul';
				break;
			case 8:
				$vigencia[1]='Ago';
				break;
			case 9:
				$vigencia[1]='Sep';
				break;
			case 10:
				$vigencia[1]='Oct';
				break;
			case 11:
				$vigencia[1]='Nov';
				break;
			case 12:
				$vigencia[1]='Dic';
				break;
			
		}
		$expira=''.$dia[0].'.'.$vigencia[1].'.'.$vigencia[0].'';
		$hoy= date("d.M.Y"); ;
		$controller_name=strtolower(get_class($CI));
		$width = 200;

		$table_data_row='<article class="articles_paquetes_categoria">
						<a class="a-img-paq-section" href="#"><img src="'.base_url().'images/paquetes/front-img-paquetes/'.$paquete->item_kit_id.'/paquete_thumbail.jpg"></a>';
									 
								
		if($expira==$hoy)
		{
		$table_data_row.='<div class="box-info-paq-cat">
									<span id="txt-expira" class="txt-vigencia-cat"><b>Hoy último día</b></span>
									<h3 class="name-paq-section">'.$paquete->name.'</h3>
									<span class="price-paq-section">'.to_currency($paquete->kit_price).'USD</span>
									</div>
									<a class="btn-paq-section"href="'.base_url().'index.php/front/detalles_paquete/'.$paquete->item_kit_id.'">Ver detalles</a>';
		}
		else
		{
			$table_data_row.='<div class="box-info-paq-cat">
									<span class="txt-vigencia-cat">Vigencia '.$dia[0].' de '.$vigencia[1].' de '.$vigencia[0].'</span>
									<h3 class="name-paq-section">'.$paquete->name.'</h3>
									<span class="price-paq-section">'.to_currency($paquete->kit_price).'USD</span>
									</div>
									<a class="btn-paq-section"href="'.base_url().'index.php/front/detalles_paquete/'.$paquete->item_kit_id.'">Ver detalles</a>';
		}
									
		$table_data_row.='</article>';
		return $table_data_row;
	}

	function get_items_options($productos,$controller)
{
	$CI =& get_instance();
	$table='<select class="select-head" name="id" type="text" id="id"  placeholder="">';
	

	
	
	
	$table.=get_items_options_data_rows($productos,$controller);
	$table.='</select>';
	return $table;
}
function get_items_options_data_rows($productos,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($productos->result() as $producto)
	{
		$table_data_rows.=get_items_options_data_row($producto,$controller);
	}
	
	if($productos->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('items_no_items_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}
function get_items_options_data_row($producto,$controller)
{
	
	$CI =& get_instance();
	
	$controller_name=strtolower(get_class($CI));
	$width = 200;

	
	
	$table_data_row='<option value="'.$producto->item_id.'">'.$producto->name.'</option>';
	
				
		
			return $table_data_row;
}

?>