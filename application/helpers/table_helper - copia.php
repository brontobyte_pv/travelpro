<?php
/*
Gets the html table to manage customers.
*/
function get_customers_manage_table($customers,$controller)
{
	$CI =& get_instance();
	$table="";
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('customers_company'),
	$CI->lang->line('common_last_name'),
	$CI->lang->line('common_first_name'),
	$CI->lang->line('common_email'),
	$CI->lang->line('common_phone_number'),
	'&nbsp');
	
 
	foreach($headers as $header)
	{
		$table.="<div class='casillas-float'>$header</div>";
	}
	$table.='<section class="tablesorter" id="sortable_table">';
	$table.=get_customers_manage_table_data_rows($customers,$controller);
	$table.='<section>';
	return $table;
}

 
/*
Gets the html data rows for the customers.
*/
function get_customers_manage_table_data_rows($customers,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($customers->result() as $customer)
	{
		$table_data_rows.=get_customer_data_row($customer,$controller);
	}
	
	if($customers->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' style='padding:7px;'>".$CI->lang->line('common_no_persons_to_display')."</div>";
	}
	
	return $table_data_rows;
}

function get_customer_data_row($customer,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-all'><input type='checkbox' id='person_$customer->person_id' value='".$customer->person_id."'/></div>";
	$table_data_row.='<div class="casillas-all">'.character_limiter($customer->company,13).'</div>';
	$table_data_row.='<div class="casillas-all">'.character_limiter($customer->last_name,13).'</div>';
	$table_data_row.='<div class="casillas-all">'.character_limiter($customer->first_name,13).'</div>';
	$table_data_row.='<div id="opc-mail"class="casillas-all">'.mailto($customer->email,character_limiter($customer->email,22)).'</div>';
	$table_data_row.='<div class="casillas-all">'.character_limiter($customer->phone_number,13).'</div>';		
	$table_data_row.='<div  id="opc-edit" class="casillas-all">'.anchor($controller_name."/view/$customer->person_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';		
	$table_data_row.='</div>';
	
	return $table_data_row;
}

/*****************Employees***********************************/

function get_employees_manage_table($people,$controller)
{
	$CI =& get_instance();
	$table="";
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('common_last_name'),
	$CI->lang->line('common_first_name'),
	$CI->lang->line('common_email'),
	$CI->lang->line('common_phone_number'),
	'&nbsp');
	
 
	foreach($headers as $header)
	{
		$table.="<div class='casillas-float'>$header</div>";
	}
	$table.='<section class="tablesorter" id="sortable_table">';
	$table.=get_employees_manage_table_data_rows($people,$controller);
	$table.='<section>';
	return $table;
}

 
/*
Gets the html data rows for the people.
*/
function get_employees_manage_table_data_rows($people,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($people->result() as $person)
	{
		$table_data_rows.=get_employee_data_row($person,$controller);
	}
	
	if($people->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' style='padding:7px;'>".$CI->lang->line('common_no_persons_to_display')."</div>";
	}
	
	return $table_data_rows;
}

function get_employee_data_row($person,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-all'><input type='checkbox' id='person_$person->person_id' value='".$person->person_id."'/></div>";
	$table_data_row.='<div class="casillas-all">'.character_limiter($person->last_name,13).'</div>';
	$table_data_row.='<div class="casillas-all">'.character_limiter($person->first_name,13).'</div>';
	$table_data_row.='<div id="opc-mail"class="casillas-all">'.mailto($person->email,character_limiter($person->email,22)).'</div>';
	$table_data_row.='<div class="casillas-all">'.character_limiter($person->phone_number,13).'</div>';		
	$table_data_row.='<div  id="opc-edit" class="casillas-all">'.anchor($controller_name."/view/$person->person_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';		
	$table_data_row.='</div>';
	
	return $table_data_row;
}








/******************************************************
/*
Gets the html table to manage suppliers.
*/
function get_suppliers_manage_table($suppliers,$controller)
{
	$CI =& get_instance();
	
	$table="";
	$headers = array('<input type="checkbox" id="select_all" />',
	$CI->lang->line('suppliers_company_name'),
	$CI->lang->line('common_last_name'),
	$CI->lang->line('common_first_name'),
	$CI->lang->line('common_email'),
	$CI->lang->line('common_phone_number'),
	'&nbsp');
	
	
	foreach($headers as $header)
	{
	$table.="<div class='casillas-floate'>$header</div>";
	}
	$table.='<section class="tablesorter" id="sortable_table">';
	$table.=get_suppliers_manage_table_data_rows($suppliers,$controller);
	$table.='<section>';
	return $table;
}

/*
Gets the html data rows for the supplier.
*/
function get_suppliers_manage_table_data_rows($suppliers,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($suppliers->result() as $supplier)
	{
		$table_data_rows.=get_supplier_data_row($supplier,$controller);
	}
	
	if($suppliers->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' style='padding:7px;'>".$CI->lang->line('common_no_persons_to_display')."</div>";
	}
	
	return $table_data_rows;
}

function get_supplier_data_row($supplier,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-all-b'><input type='checkbox' id='person_$supplier->person_id' value='".$supplier->person_id."'/></div>";
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($supplier->company,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($supplier->last_name,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($supplier->first_name,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.mailto($supplier->email,character_limiter($supplier->email,22)).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($supplier->phone_number,13).'</div>';		
	$table_data_row.='<div id="opc-edit2" class="casillas-all-b">'.anchor($controller_name."/view/$supplier->person_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';		
	$table_data_row.='</div>';
	
	return $table_data_row;
}

/*
Gets the html table to manage items.
*/
function get_items_manage_table($items,$controller)
{
	$CI =& get_instance();
	$table="";
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('items_model'),
	$CI->lang->line('items_name'),
	$CI->lang->line('items_brand'),
	$CI->lang->line('items_category'),
	$CI->lang->line('items_cost_price'),
	$CI->lang->line('items_unit_price'),
	
	$CI->lang->line('items_quantity'),
	'&nbsp;',
	$CI->lang->line('items_inventory')
	);
	
	
	foreach($headers as $header)
	{
		$table.="<div class='casillas-floate'>$header</div>";
	}
	$table.='<section class="tablesorter" id="sortable_table">';
	
	
	
	$table.=get_items_manage_table_data_rows($items,$controller);
	
	return $table;
}

/*
Gets the html data rows for the items.
*/
function get_items_manage_table_data_rows($items,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($items->result() as $item)
	{
		$table_data_rows.=get_item_data_row($item,$controller);
	}
	
	if($items->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' style='padding:7px;'>".$CI->lang->line('items_no_items_to_display')."</div>";
	}
	
	return $table_data_rows;
}

function get_item_data_row($item,$controller)
{
	$CI =& get_instance();
	$item_tax_info=$CI->Item_taxes->get_info($item->item_id);
	$tax_percents = '';
	foreach($item_tax_info as $tax_info)
	{
		$tax_percents.=$tax_info['percent']. '%, ';
	}
	$tax_percents=substr($tax_percents, 0, -2);
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-all-b'><input type='checkbox' id='item_$item->item_id' value='".$item->item_id."'/></div>";
	$table_data_row.='<div class="casillas-all-b">'.$item->modelo.'</div>';
	$table_data_row.='<div class="casillas-all-b">'.$item->name.'</div>';
	$table_data_row.='<div class="casillas-all-b">'.$item->brand.'</div>';
	$table_data_row.='<div class="casillas-all-b">'.$item->category.'</div>';
	$table_data_row.='<div class="casillas-all-b">'.to_currency($item->cost_price).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.to_currency($item->unit_price).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.$item->quantity.'</div>';
	$table_data_row.='<div  id="opc-edit3" class="casillas-all-b">'.anchor($controller_name."/view/$item->item_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';		
	
	//Ramel Inventory Tracking
	$table_data_row.='<div class="casillas-all-b">'.anchor($controller_name."/inventory/$item->item_id/width:$width", $CI->lang->line('common_inv'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_count')))./*'</div>';//inventory count	
	$table_data_row.='<div class="casillas-all-b">'*/'&nbsp;&nbsp;&nbsp;&nbsp;'.anchor($controller_name."/count_details/$item->item_id/width:$width", $CI->lang->line('common_det'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_details_count'))).'</div>';//inventory details	
	
	$table_data_row.='</div>';
	return $table_data_row;
}

/*
Gets the html table to manage giftcards.
*/
function get_giftcards_manage_table( $giftcards, $controller )
{
	$CI =& get_instance();
	
	$table="";
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('giftcards_giftcard_number'),
	$CI->lang->line('giftcards_card_value'),
	'&nbsp', 
	);
	
	
	foreach($headers as $header)
	{
		$table.="<div class='casillas-float'>$header</div>";
	}
	$table.='<section class="tablesorter" id="sortable_table">';
	$table.=get_giftcards_manage_table_data_rows( $giftcards, $controller );
	$table.='<section>';
	return $table;
}

/*
Gets the html data rows for the giftcard.
*/
function get_giftcards_manage_table_data_rows( $giftcards, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($giftcards->result() as $giftcard)
	{
		$table_data_rows.=get_giftcard_data_row( $giftcard, $controller );
	}
	
	if($giftcards->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' style='padding:7px;'>".$CI->lang->line('giftcards_no_giftcards_to_display')."</div>";
	}
	
	return $table_data_rows;
}

function get_giftcard_data_row($giftcard,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-all'><input type='checkbox' id='giftcard_$giftcard->giftcard_id' value='".$giftcard->giftcard_id."'/></div>";
	$table_data_row.='<div class="casillas-all">'.$giftcard->giftcard_number.'</div>';
	$table_data_row.='<div class="casillas-all">'.to_currency($giftcard->value).'</div>';
	$table_data_row.='<div id="opc-edit4" class="casillas-all">'.anchor($controller_name."/view/$giftcard->giftcard_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';		
	
	$table_data_row.='</div>';
	return $table_data_row;
}

/*
Gets the html table to manage item kits.
*/
function get_item_kits_manage_table( $item_kits, $controller )
{
	$CI =& get_instance();
	$table="";
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('item_kits_name'),
	$CI->lang->line('item_kits_description'),
	'Modificar', 
	'Agregar Items'
	);
	
	
	foreach($headers as $header)
	{
		$table.="<div class='casillas-float'>$header</div>";
	}
	$table.='<section class="tablesorter" id="sortable_table">';
	$table.=get_item_kits_manage_table_data_rows( $item_kits, $controller );
	$table.='<section>';
	return $table;
}

/*
Gets the html data rows for the item kits.
*/
function get_item_kits_manage_table_data_rows( $item_kits, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($item_kits->result() as $item_kit)
	{
		$table_data_rows.=get_item_kit_data_row( $item_kit, $controller );
	}
	
	if($item_kits->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' style='padding:7px;'>".$CI->lang->line('item_kits_no_item_kits_to_display')."</div>";
	}
	
	return $table_data_rows;
}

function get_item_kit_data_row($item_kit,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-all'><input type='checkbox' id='item_kit_$item_kit->item_kit_id' value='".$item_kit->item_kit_id."'/></div>";
	$table_data_row.='<div class="casillas-all">'.$item_kit->name.'</div>';
	$table_data_row.='<div class="casillas-all">'.character_limiter($item_kit->description, 25).'</div>';
	$table_data_row.='<div id="opc-edit5" class="casillas-all">'.anchor($controller_name."/view/$item_kit->item_kit_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';	
	$table_data_row.='<div id="opc-edit5" class="casillas-all">'.anchor($controller_name."/form_items_kit/$item_kit->item_kit_id/width:$width", '+ Items',array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	
	$table_data_row.='</div>';
	return $table_data_row;
}


/******************Aqui comienza el helper para los servicios****************/

function get_services_manage_table($services,$controller)
{
	$CI =& get_instance();
	$table="";
	$headers = array('<input type="checkbox" id="select_all" />',
	$CI->lang->line('services_cliente'),
	$CI->lang->line('services_date'),
	$CI->lang->line('services_asignado'),
	$CI->lang->line('services_motivo'),
	$CI->lang->line('services_estado'),
	$CI->lang->line('services_duracion'),
	'Cobrar',
	$CI->lang->line('common_edit')
	);
	
	
	foreach($headers as $header)
	{
		$table.="<div class='casillas-floate'>$header</div>";
	}
	$table.='<section class="tablesorter" id="sortable_table">';
	
	
	
	$table.=get_services_manage_table_data_rows($services,$controller);
	
	return $table;
}

function get_services_manage_table_data_rows($services,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($services->result() as $service)
	{
		$table_data_rows.=get_services_data_row($service,$controller);
	}
	
	if($services->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' style='padding:7px;'>".$CI->lang->line('items_no_items_to_display')."</div>";
	}
	
	return $table_data_rows;
}

function get_services_data_row($service,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-all-b'><input type='checkbox' id='$service->service_id' value='".$service->service_id."'/></div>";
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($service->nom_cliente,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($service->fecha,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($service->nom_asignado,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($service->motivo,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($service->estado,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($service->duracion,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.anchor($controller_name."/view/$service->service_id/width:$width","Cobrar" ,array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	$table_data_row.='<div id="opc-edit2" class="casillas-all-b">'.anchor($controller_name."/view/$service->service_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	$table_data_row.='</div>';
	
	return $table_data_row;
}


/******************Aqui comienza el helper para los cobros****************/

function get_cobros_manage_table($cobros,$controller)
{
	$CI =& get_instance();
	$table="";
	$headers = array('<input type="checkbox" id="select_all" />',
	$CI->lang->line('services_date'),
	$CI->lang->line('services_cliente'),
	'Motivo',
	'Por Cobrar',
	'Estado',
	'Tipo de pago',
	'Notificación',
	'Modificar'
	
	);
	
	
	foreach($headers as $header)
	{
		$table.="<div class='casillas-floate'>$header</div>";
	}
	$table.='<section class="tablesorter" id="sortable_table">';
	
	
	
	$table.=get_cobros_manage_table_data_rows($cobros,$controller);
	
	return $table;
}

function get_cobros_manage_table_data_rows($cobros,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($cobros->result() as $cobro)
	{
		$table_data_rows.=get_cobros_data_row($cobro,$controller);
	}
	
	if($cobros->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' style='padding:7px;'>".$CI->lang->line('items_no_items_to_display')."</div>";
	}
	
	return $table_data_rows;
}

function get_cobros_data_row($cobro,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-all-b'><input type='checkbox' id='$cobro->cobro_id' value='".$cobro->cobro_id."'/></div>";
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($cobro->fecha,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($cobro->nom_cliente,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($cobro->motivo,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($cobro->cantidad,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($cobro->estado,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($cobro->tipo,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.anchor($controller_name."/view/$cobro->cobro_id/width:$width","Enviar" ,array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	$table_data_row.='<div id="opc-edit2" class="casillas-all-b">'.anchor($controller_name."/view/$cobro->cobro_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';		
	$table_data_row.='</div>';
	
	
	return $table_data_row;
}

/******************Aqui comienza el helper para los gastos****************/
function get_gastos_manage_table($gastos,$controller)
{
	$CI =& get_instance();
	$table="";
	$headers = array('<input type="checkbox" id="select_all" />',
	$CI->lang->line('services_date'),
	'Motivo',
	'Concepto',
	'Por Cobrar',
	'Estado',
	'Tipo de pago',
	'Modificar',

	);
	
	
	foreach($headers as $header)
	{
		$table.="<div class='casillas-floate'>$header</div>";
	}
	$table.='<section class="tablesorter" id="sortable_table">';
	
	
	
	$table.=get_gastos_manage_table_data_rows($gastos,$controller);
	
	return $table;
}

function get_gastos_manage_table_data_rows($gastos,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($gastos->result() as $gasto)
	{
		$table_data_rows.=get_gastos_data_row($gasto,$controller);
	}
	
	if($gastos->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' style='padding:7px;'>".$CI->lang->line('items_no_items_to_display')."</div>";
	}
	
	return $table_data_rows;
}

function get_gastos_data_row($gasto,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-all-b'><input type='checkbox' id='$gasto->gasto_id' value='".$gasto->gasto_id."'/></div>";
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($gasto->fecha,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($gasto->motivo,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($gasto->concepto,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($gasto->cantidad,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($gasto->estado,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($gasto->tipo,13).'</div>';

	
	
	$table_data_row.='<div id="opc-edit2" class="casillas-all-b">'.anchor($controller_name."/view/$gasto->gasto_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';		
	$table_data_row.='</div>';
	
	return $table_data_row;
}
/******************Aqui comienza el helper para los Pedidos****************/
function get_pedidos_manage_table($pedidos,$controller)
{
	$CI =& get_instance();
	$table="";
	$headers = array(
	'<input type="checkbox" id="select_all" />',
	'Fecha',
	'Editar'
	);
	
	
	foreach($headers as $header)
	{
		$table.="<div class='casillas-floate'>$header</div>";
	}
	$table.='<section class="tablesorter" id="sortable_table">';
	
	
	
	$table.=get_pedidos_manage_table_data_rows($pedidos,$controller);
	
	return $table;
}

function get_pedidos_manage_table_data_rows($pedidos,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($pedidos->result() as $pedido)
	{
		$table_data_rows.=get_pedidos_data_row($pedido,$controller);
	}
	
	if($pedidos->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' style='padding:7px;'>".$CI->lang->line('items_no_items_to_display')."</div>";
	}
	
	return $table_data_rows;
}

function get_pedidos_data_row($pedido,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-all-b'><input type='checkbox' id='$pedido->pedido_id' value='".$pedido->pedido_id."'/></div>";
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($pedido->fecha,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($pedido->motivo,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($pedido->concepto,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($pedido->cantidad,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($pedido->estado,13).'</div>';
	$table_data_row.='<div class="casillas-all-b">'.character_limiter($pedido->tipo,13).'</div>';

	
	
	$table_data_row.='<div id="opc-edit2" class="casillas-all-b">'.anchor($controller_name."/view/$pedido->pedido_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';		
	$table_data_row.='</div>';
	
	return $table_data_row;
}



?>