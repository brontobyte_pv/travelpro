<?php
function get_customers_manage_table($customers,$controller)
{
	$CI =& get_instance();
	$table='<div class="cover-header-tablas">';
	
	$headers = array (
	' <input type="checkbox" id="select_all" />', 
	$CI->lang->line('customers_company'),
	$CI->lang->line('common_last_name'),
	$CI->lang->line('common_first_name'),
	$CI->lang->line('common_email'),
	$CI->lang->line('common_phone_number'),
	'</div>&nbsp'
	);
	
	$table.='';
 
	foreach($headers as $header)
	{
		$table.="<div class='titulos-tablas'>$header</div>";
	}
	$table.='<section class="tablesorter" id="cover_resultados_tabla">';
	$table.=get_customers_manage_table_data_rows($customers,$controller);
	$table.='<section>';
	return $table;
}

 
/*
Gets the html data rows for the customers.
*/
function get_customers_manage_table_data_rows($customers,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($customers->result() as $customer)
	{
		$table_data_rows.=get_customer_data_row($customer,$controller);
	}
	
	if($customers->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' >".$CI->lang->line('common_no_persons_to_display')."</div>";
	}
	
	return $table_data_rows;
}

function get_customer_data_row($customer,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	

	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-resultados-tablas'><div class='soloVisibleResponsivamente'>Seleccionar</div><input type='checkbox' id='person_$customer->person_id' value='".$customer->person_id."'/></div>";
	$table_data_row.='<div class="casillas-resultados-tablas"><div class="soloVisibleResponsivamente">Empresa</div>'.character_limiter($customer->company,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas"><div class="soloVisibleResponsivamente">Apellido</div>'.character_limiter($customer->last_name,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas"><div class="soloVisibleResponsivamente">Nombre</div>'.character_limiter($customer->first_name,13).'</div>';
	$table_data_row.='<div id="opc-mail"class="casillas-resultados-tablas"><div class="soloVisibleResponsivamente">Correo</div>'.mailto($customer->email,character_limiter($customer->email,22)).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas"><div class="soloVisibleResponsivamente">Teléfono</div>'.character_limiter($customer->phone_number,13).'</div>';		
	$table_data_row.='<div  id="opc-edit" class="casillas-resultados-tablas"><div class="soloVisibleResponsivamente">Editar</div>'.anchor($controller_name."/view/$customer->person_id/", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';		
	$table_data_row.='</div>';
	
	return $table_data_row;
}

/*****************Employees***********************************/

function get_employees_manage_table($people,$controller)
{
	$CI =& get_instance();
	$table='<div class="cover-header-tablas">';
	
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('common_last_name'),
	$CI->lang->line('common_first_name'),
	$CI->lang->line('common_email'),
	$CI->lang->line('common_phone_number'),
	'</div>&nbsp'
	);
	
	$table.='';
	
 
	foreach($headers as $header)
	{
		$table.="<div class='titulos-tablas-c'>$header</div>";
	}
	$table.='<section class="tablesorter" id="cover_resultados_tabla">';
	$table.=get_employees_manage_table_data_rows($people,$controller);
	$table.='<section>';
	return $table;
}

 
/*
Gets the html data rows for the people.
*/
function get_employees_manage_table_data_rows($people,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($people->result() as $person)
	{
		$table_data_rows.=get_employee_data_row($person,$controller);
	}
	
	if($people->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' >".$CI->lang->line('common_no_persons_to_display')."</div>";
	}
	
	return $table_data_rows;
}

function get_employee_data_row($person,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	

	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-resultados-tablas-c'><div class='soloVisibleResponsivamente'>Seleccionar</div><input type='checkbox' id='person_$person->person_id' value='".$person->person_id."'/></div>";
	$table_data_row.='<div class="casillas-resultados-tablas-c"><div class="soloVisibleResponsivamente">Apellido</div>'.character_limiter($person->last_name,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-c"><div class="soloVisibleResponsivamente">Nombre</div>'.character_limiter($person->first_name,13).'</div>';
	$table_data_row.='<div id="opc-mail"class="casillas-resultados-tablas-c"><div class="soloVisibleResponsivamente">Correo</div>'.mailto($person->email,character_limiter($person->email,22)).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-c"><div class="soloVisibleResponsivamente">Teléfono</div>'.character_limiter($person->phone_number,13).'</div>';		
	$table_data_row.='<div  id="opc-edit" class="casillas-resultados-tablas-c"><div class="soloVisibleResponsivamente">Editar</div>'.anchor($controller_name."/view/$person->person_id/", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';		
	$table_data_row.='</div>';
	
	return $table_data_row;
}








/******************************************************
/*
Gets the html table to manage suppliers.
*/
function get_suppliers_manage_table($suppliers,$controller)
{
	$CI =& get_instance();
	
	$table='<div class="cover-header-tablas">';
	
	$headers = array('<input type="checkbox" id="select_all" />',
	$CI->lang->line('suppliers_company_name'),
	$CI->lang->line('common_last_name'),
	$CI->lang->line('common_first_name'),
	$CI->lang->line('common_email'),
	$CI->lang->line('common_phone_number'),
	'</div>&nbsp'
	);
	
	$table.='';
	
	
	
	foreach($headers as $header)
	{
	$table.="<div class='titulos-tablas'>$header</div>";
	}
	$table.='<section class="tablesorter" id="cover_resultados_tabla">';
	$table.=get_suppliers_manage_table_data_rows($suppliers,$controller);
	$table.='<section>';
	return $table;
}

/*
Gets the html data rows for the supplier.
*/
function get_suppliers_manage_table_data_rows($suppliers,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($suppliers->result() as $supplier)
	{
		$table_data_rows.=get_supplier_data_row($supplier,$controller);
	}
	
	if($suppliers->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' >".$CI->lang->line('common_no_persons_to_display')."</div>";
	}
	
	return $table_data_rows;
}

function get_supplier_data_row($supplier,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	

	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-resultados-tablas'><div class='soloVisibleResponsivamente'>Seleccionar</div><input type='checkbox' id='person_$supplier->person_id' value='".$supplier->person_id."'/></div>";
	$table_data_row.='<div class="casillas-resultados-tablas"><div class="soloVisibleResponsivamente">Nombre de la Compañia</div>'.character_limiter($supplier->company,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas"><div class="soloVisibleResponsivamente">Apellido</div>'.character_limiter($supplier->last_name,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas "><div class="soloVisibleResponsivamente">Nombre</div>'.character_limiter($supplier->first_name,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas"><div class="soloVisibleResponsivamente">Correo</div>'.mailto($supplier->email,character_limiter($supplier->email,22)).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas"><div class="soloVisibleResponsivamente">Teléfono</div>'.character_limiter($supplier->phone_number,13).'</div>';		
	$table_data_row.='<div id="opc-edit2" class="casillas-resultados-tablas"><div class="soloVisibleResponsivamente">Editar</div>'.anchor($controller_name."/view/$supplier->person_id/", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';		
	$table_data_row.='</div>';
	
	return $table_data_row;
}

/*
Gets the html table to manage items.
*/
function get_items_manage_table($items,$controller)
{
	$CI =& get_instance();
	$table='<div class="cover-header-tablas">';
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('items_reorder_level'),
	$CI->lang->line('items_name'),
	$CI->lang->line('items_category'),
	$CI->lang->line('items_cost_price'),
	$CI->lang->line('items_unit_price'),
	$CI->lang->line('items_config'),
	$CI->lang->line('items_inventory').'</div>&nbsp'
	);
	
	$table.='';
	
	foreach($headers as $header)
	{
		$table.="<div class='titulos-tablas-b'>$header</div>";
	}
	$table.='<section class="tablesorter" id="cover_resultados_tabla">';
	$table.=get_items_manage_table_data_rows($items,$controller);
	$table.='<section>';
	return $table;
}

/*
Gets the html data rows for the items.
*/
function get_items_manage_table_data_rows($items,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($items->result() as $item)
	{
		$table_data_rows.=get_item_data_row($item,$controller);
	}
	
	if($items->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' >".$CI->lang->line('items_no_items_to_display')."</div>";
	}
	
	return $table_data_rows;
}

function get_item_data_row($item,$controller)
{
	$CI =& get_instance();
	$item_tax_info=$CI->Item_taxes->get_info($item->item_id);
	$tax_percents = '';
	foreach($item_tax_info as $tax_info)
	{
		$tax_percents.=$tax_info['percent']. '%, ';
	}
	$tax_percents=substr($tax_percents, 0, -2);
	$controller_name=strtolower(get_class($CI));
	

	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-resultados-tablas-b'><div class='soloVisibleResponsivamente'>Seleccionar</div><input type='checkbox' id='item_$item->item_id' value='".$item->item_id."'/></div>";
	$table_data_row.='<div class="casillas-resultados-tablas-b"><div class="soloVisibleResponsivamente">Modelo</div>'.$item->reorder_level.'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-b "><div class="soloVisibleResponsivamente">Nombre</div>'.$item->name.'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-b"><div class="soloVisibleResponsivamente">Categoria</div>'.$item->category.'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-b"><div class="soloVisibleResponsivamente">Costo</div>'.to_currency($item->cost_price).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-b"><div class="soloVisibleResponsivamente">Precio Unitario</div>'.to_currency($item->unit_price).'</div>';
	$table_data_row.='<div  id="opc-edit3" class="casillas-resultados-tablas-b"><div class="soloVisibleResponsivamente">'.$CI->lang->line('items_select').'</div>'.anchor($controller_name."/view/$item->item_id/", $CI->lang->line('items_select'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';		
	
	//Ramel Inventory Tracking
	$table_data_row.='<div class="casillas-resultados-tablas-b"><div class="soloVisibleResponsivamente">Detalles</div>'.anchor($controller_name."/inventory/$item->item_id/", $CI->lang->line('common_inv'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_count')))./*'</div>';//inventory count	
	$table_data_row.='<div class="casillas-resultados-tablas-b"><div class="soloVisibleResponsivamente">X</div>'*/'&nbsp;&nbsp;&nbsp;&nbsp;'.anchor($controller_name."/count_details/$item->item_id/", $CI->lang->line('common_det'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_details_count'))).'</div>';//inventory details	
	
	$table_data_row.='</div>';
	return $table_data_row;
}

/*
Gets the html table to manage giftcards.
*/
function get_giftcards_manage_table( $giftcards, $controller )
{
	$CI =& get_instance();
	
	$table='<div class="cover-header-tablas">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('giftcards_giftcard_number'),
	$CI->lang->line('giftcards_card_value').'</div>'
	);
	
	$table.='';
	
	foreach($headers as $header)
	{
		$table.="<div class='titulos-tablas-f'>$header</div>";
	}
	$table.='<section class="tablesorter" id="cover_resultados_tabla">';
	$table.=get_giftcards_manage_table_data_rows( $giftcards, $controller );
	$table.='<section>';
	return $table;
}

/*
Gets the html data rows for the giftcard.
*/
function get_giftcards_manage_table_data_rows( $giftcards, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($giftcards->result() as $giftcard)
	{
		$table_data_rows.=get_giftcard_data_row( $giftcard, $controller );
	}
	
	if($giftcards->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' >".$CI->lang->line('giftcards_no_giftcards_to_display')."</div>";
	}
	
	return $table_data_rows;
}

function get_giftcard_data_row($giftcard,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	

	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-resultados-tablas-f'><div class='soloVisibleResponsivamente'>X</div><input type='checkbox' id='giftcard_$giftcard->giftcard_id' value='".$giftcard->giftcard_id."'/></div>";
	$table_data_row.='<div class="casillas-resultados-tablas-f"><div class="soloVisibleResponsivamente">Número de Tarjeta</div>'.$giftcard->giftcard_number.'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-f"><div class="soloVisibleResponsivamente">Valor</div>'.to_currency($giftcard->value).'</div>';
	$table_data_row.='<div id="opc-edit4" class="casillas-resultados-tablas-f"><div class="soloVisibleResponsivamente">Editar</div>'.anchor($controller_name."/view/$giftcard->giftcard_id/", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';		
	
	$table_data_row.='</div>';
	return $table_data_row;
}

/*
Gets the html table to manage item kits.
*/
function get_item_kits_manage_table( $item_kits, $controller )
{
	$CI =& get_instance();
	$table='<div class="cover-header-tablas">';
	
	$headers = array('<input type="checkbox" id="select_all" />',
	'Categoría',
	$CI->lang->line('item_kits_name'),
	'Contenido',
	'Info',
	'Productos</div>'
	);
	
	$table.='';
	
	foreach($headers as $header)
	{
		$table.="<div class='titulos-tablas-b'>$header</div>";
	}
	$table.='<section class="tablesorter" id="cover_resultados_tabla">';
	$table.=get_item_kits_manage_table_data_rows( $item_kits, $controller );
	$table.='<section>';
	return $table;
}

/*
Gets the html data rows for the item kits.
*/
function get_item_kits_manage_table_data_rows($item_kits, $controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($item_kits->result() as $item_kit)
	{
		$table_data_rows.=get_item_kit_data_row( $item_kit, $controller );
	}
	
	if($item_kits->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' >".$CI->lang->line('item_kits_no_item_kits_to_display')."</div>";
	}
	
	return $table_data_rows;
}

function get_item_kit_data_row($item_kit,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	

	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-resultados-tablas-b'><div class='soloVisibleResponsivamente'>Seleccionar</div><input type='checkbox' id='item_kit_$item_kit->item_kit_id' value='".$item_kit->item_kit_id."'/></div>";
	$table_data_row.='<div id="opc-edit5" class="casillas-resultados-tablas-b"><div class="soloVisibleResponsivamente">'.$item_kit->category.'</div>'.anchor($controller_name."/view_load_cat_banner/$item_kit->category/", $item_kit->category,array('class'=>'thickbox','title'=>$CI->lang->line('item_kits_cats'))).'</div>';	
	$table_data_row.='<div class="casillas-resultados-tablas-b"><div class="soloVisibleResponsivamente">Nombre</div>'.$item_kit->name.'</div>';
	$table_data_row.='<div id="opc-edit5" class="casillas-resultados-tablas-b"><div class="soloVisibleResponsivamente">EditCont</div>'.anchor($controller_name."/view/$item_kit->item_kit_id/", 'EditCont',array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';	
$table_data_row.='<div id="opc-edit5" class="casillas-resultados-tablas-b"><div class="soloVisibleResponsivamente">EditPaq</div>'.anchor($controller_name."/form_content/$item_kit->item_kit_id/", 'EditPaq',array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_info'))).'</div>';	
	
	$table_data_row.='<div id="opc-edit5" class="casillas-resultados-tablas-b"><div class="soloVisibleResponsivamente">+ Productos</div>'.anchor($controller_name."/form_items_kit/$item_kit->item_kit_id/", '+ Productos',array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_productos'))).'</div>';
	
	$table_data_row.='</div>';
	return $table_data_row;
}


/******************Aqui comienza el helper para los servicios****************/

function get_services_manage_table($services,$controller)
{
	$CI =& get_instance();
	
	$table='<div class="cover-header-tablas">';
	$headers = array('<input type="checkbox" id="select_all" />',
	$CI->lang->line('services_cliente'),
	$CI->lang->line('services_date'),
	$CI->lang->line('services_asignado'),
	$CI->lang->line('services_motivo'),
	$CI->lang->line('services_estado'),
	$CI->lang->line('services_duracion'),
	'Cobrar',
	$CI->lang->line('common_edit').'</div>'
	);
	
	$table.='';
	
	
	
	foreach($headers as $header)
	{
		$table.="<div class='titulos-tablas-e'>$header</div>";
	}

	
	$table.='<section class="tablesorter" id="cover_resultados_tabla">';
	$table.=get_services_manage_table_data_rows($services,$controller);
	$table.='</section>';
	return $table;
}

function get_services_manage_table_data_rows($services,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($services->result() as $service)
	{
		$table_data_rows.=get_services_data_row($service,$controller);
	}
	
	if($services->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' >".$CI->lang->line('services_empty')."</div>";
	}
	
	return $table_data_rows;
}

function get_services_data_row($service,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	

	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-resultados-tablas-e'><div class='soloVisibleResponsivamente'>Seleccionar</div><input type='checkbox' id='$service->service_id' value='".$service->service_id."'/></div>";
	$table_data_row.='<div class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Cliente</div>'.character_limiter($service->nom_cliente,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Fecha</div>'.character_limiter($service->fecha,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Asignado</div>'.character_limiter($service->nom_asignado,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Motivo</div>'.character_limiter($service->motivo,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Estado</div>'.character_limiter($service->estado,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Duración</div>'.character_limiter($service->duracion,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Cobrar</div>'.anchor($controller_name."/$service->service_id/","Cobrar" ,array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	$table_data_row.='<div id="opc-edit2" class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Editar</div>'.anchor($controller_name."/view/$service->service_id/", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	$table_data_row.='</div>';
	
	return $table_data_row;
}


/******************Aqui comienza el helper para los cobros****************/

function get_cobros_manage_table($cobros,$controller)
{
	$CI =& get_instance();
	$table='<div class="cover-header-tablas">';
	
	$headers = array('<input type="checkbox" id="select_all" />',
	$CI->lang->line('services_date'),
	'Cantidad',
	//$CI->lang->line('services_cliente'),
	'Concepto',
	'Nombre de Cliente',
	'Estado',
	'Notificar',
	'Modificar</div>'
	);
	
	$table.='';
	
	
	foreach($headers as $header)
	{
		$table.="<div class='titulos-tablas-e'>$header</div>";
	}
	
	
	
	$table.='<section class="tablesorter" id="cover_resultados_tabla">';
	$table.=get_cobros_manage_table_data_rows($cobros,$controller);
	$table.='<section>';
	return $table;
}

function get_cobros_manage_table_data_rows($cobros,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($cobros->result() as $cobro)
	{
		$table_data_rows.=get_cobros_data_row($cobro,$controller);
	}
	
	if($cobros->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' >".$CI->lang->line('items_no_items_to_display')."</div>";
	}
	
	return $table_data_rows;
}

function get_cobros_data_row($cobro,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	

	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-resultados-tablas-e'><div class='soloVisibleResponsivamente'>Seleccionar</div><input type='checkbox' id='$cobro->cobro_id' value='".$cobro->cobro_id."'/></div>";
	$table_data_row.='<div class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Fecha</div>'.character_limiter($cobro->fecha,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Cantidad</div>'.character_limiter(to_currency($cobro->cantidad),13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Concepto</div>'.character_limiter($cobro->concepto,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Nombre del cliente</div>'.character_limiter($cobro->nom_cliente,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Estado</div>'.character_limiter($cobro->estado,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Notificar</div>'.anchor($controller_name."/view/$cobro->cobro_id/","Enviar" ,array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	$table_data_row.='<div id="opc-edit2" class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Editar</div>'.anchor($controller_name."/view/$cobro->cobro_id/", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';		
	$table_data_row.='</div>';
	
	
	return $table_data_row;
}

/******************Aqui comienza el helper para los gastos****************/
function get_gastos_manage_table($gastos,$controller)
{
	$CI =& get_instance();
	$table='<div class="cover-header-tablas">';
	$headers = array('<input type="checkbox" id="select_all" />',
	$CI->lang->line('services_date'),
	'Cantidad',
	'Concepto',
	'Estado',
	'Modo de pago',
	'Modificar </div>'
	);
	
	$table.='';
	
	
	
	foreach($headers as $header)
	{
		$table.="<div class='titulos-tablas'>$header</div>";
	}
	
	
	
	$table.='<section class="tablesorter" id="cover_resultados_tabla">';
	$table.=get_gastos_manage_table_data_rows($gastos,$controller);
	$table.='<section>';
	return $table;
}

function get_gastos_manage_table_data_rows($gastos,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($gastos->result() as $gasto)
	{
		$table_data_rows.=get_gastos_data_row($gasto,$controller);
	}
	
	if($gastos->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' >".$CI->lang->line('items_no_items_to_display')."</div>";
	}
	
	return $table_data_rows;
}

function get_gastos_data_row($gasto,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	

	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-resultados-tablas'><div class='soloVisibleResponsivamente'>Seleccionar</div><input type='checkbox' id='$gasto->gasto_id' value='".$gasto->gasto_id."'/></div>";
	$table_data_row.='<div class="casillas-resultados-tablas"><div class="soloVisibleResponsivamente">Fecha</div>'.character_limiter($gasto->fecha,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas"><div class="soloVisibleResponsivamente">Cantidad</div>'.character_limiter(to_currency($gasto->cantidad),13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas"><div class="soloVisibleResponsivamente">Concepto</div>'.character_limiter($gasto->concepto,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas"><div class="soloVisibleResponsivamente">Estado</div>'.character_limiter($gasto->estado,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas"><div class="soloVisibleResponsivamente">Modo de Pago</div>'.character_limiter($gasto->tipo,13).'</div>';

	
	
	$table_data_row.='<div id="opc-edit2" class="casillas-resultados-tablas"><div class="soloVisibleResponsivamente">Editar</div>'.anchor($controller_name."/view/$gasto->gasto_id/", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';		
	$table_data_row.='</div>';
	
	return $table_data_row;
}
/******************Aqui comienza el helper para los Pedidos****************/
function get_pedidos_manage_table($pedidos,$controller)
{
	$CI =& get_instance();
	$table='<div class="cover-header-tablas">';
	$headers = array(
	'Fecha','Cliente','Productos','Mensaje','Concluir','Cancelar','Total</div>'
	);
	
	$table.='';
	
	foreach($headers as $header)
	{
		$table.="<div class='titulos-tablas-e'>$header</div>";
	}
	
	
	
	$table.='<section class="tablesorter" id="cover_resultados_tabla">';
	$table.=get_pedidos_manage_table_data_rows($pedidos,$controller);
	$table.='</section>';
	return $table;
}

function get_pedidos_manage_table_data_rows($pedidos,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($pedidos->result() as $pedido)
	{
		$table_data_rows.=get_pedidos_data_row($pedido,$controller);
	}
	
	if($pedidos->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' >".$CI->lang->line('common_no_exist')."</div>";
	}
	
	return $table_data_rows;
}

function get_pedidos_data_row($pedido,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<div class="base-casillas">';
	
	
	$table_data_row.='<div class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Fecha</div>'.character_limiter($pedido->pedido_time,13).'</div>';
	
	$table_data_row.='<div id="opc-edit2" class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Clientes	</div>'.anchor($controller_name."/customer_details/$pedido->pedido_id/width:$width", $CI->lang->line('common_det'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	
	$table_data_row.='<div id="opc-edit2" class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Productos</div>'.anchor($controller_name."/items_details/$pedido->pedido_id/width:$width", $CI->lang->line('common_ver'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	
	$table_data_row.='<div id="opc-edit2" class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Mensaje</div>'.anchor($controller_name."/send_msj/$pedido->pedido_id/width:$width", $CI->lang->line('common_submit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	
	$table_data_row.='<div id="opc-edit2" class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Concluir</div>'.anchor($controller_name."/concluir_pedido/$pedido->pedido_id/width:$width", $CI->lang->line('common_complet'),array('class'=>'complet','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	
	$table_data_row.='<div id="opc-edit2" class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Cancelar</div><a class="cancel" value="'.$pedido->pedido_id.'">'.$CI->lang->line('common_cancel').'</a></div>';		

	$table_data_row.='<div class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Total</div>'.character_limiter(to_currency($pedido->total),13).'</div>';
	return $table_data_row;
}
/********Pedidos******Hechos*******************/
/******************Aqui comienza el helper para los Pedidos****************/
function get_pedidosD_manage_table($pedidos,$controller)
{
	$CI =& get_instance();
	$table='<div class="cover-header-tablas">';
	$headers = array(
	
	'Fecha','Cliente','Productos','Total</div>'
	);
	
	$table.='';
	
	foreach($headers as $header)
	{
		$table.="<div class='titulos-tablas-e'>$header</div>";
	}
	
	
	
	$table.='<section class="tablesorter" id="cover_resultados_tabla">';
	$table.=get_pedidosD_manage_table_data_rows($pedidos,$controller);
	$table.='</section>';
	return $table;
}

function get_pedidosD_manage_table_data_rows($pedidos,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($pedidos->result() as $pedido)
	{
		$table_data_rows.=get_pedidosD_data_row($pedido,$controller);
	}
	
	if($pedidos->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' >".$CI->lang->line('common_no_exist')."</div>";
	}
	
	return $table_data_rows;
}

function get_pedidosD_data_row($pedido,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<div class="base-casillas">';
	
	
	$table_data_row.='<div class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Fecha</div>'.character_limiter($pedido->pedido_time,13).'</div>';
	
	$table_data_row.='<div id="opc-edit2" class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Cliente</div>'.anchor($controller_name."/customer_details/$pedido->pedido_id/width:$width", $CI->lang->line('common_det'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	
	$table_data_row.='<div id="opc-edit2" class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Productos</div>'.anchor($controller_name."/items_details/$pedido->pedido_id/width:$width", $CI->lang->line('common_ver'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';
	
	
	

	
	
	

	$table_data_row.='<div class="casillas-resultados-tablas-e"><div class="soloVisibleResponsivamente">Cantidad</div>'.character_limiter(to_currency($pedido->total),13).'</div>';
	return $table_data_row;
}

/****************** Aqui comienza el helper para las Entradas ****************/
function get_entradas_manage_table($entradas,$controller)
{
	$CI =& get_instance();
	$table='<div class="cover-header-tablas">';
	$headers = array(
	
	'Fecha',
	'Provedor',
	'Total',
	'Tipo de Pago',
	'Modo</div>'
	);
	
	$table.='';
	
	
	foreach($headers as $header)
	{
		$table.="<div class='titulos-tablas-d'>$header</div>";
	}
	
	
	
	$table.='<section class="tablesorter" id="cover_resultados_tabla">';
	$table.=get_entradas_manage_table_data_rows($entradas,$controller);
	$table.='<section>';
	return $table;
}

function get_entradas_manage_table_data_rows($entradas,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($entradas->result() as $entrada)
	{
		$table_data_rows.=get_entradas_data_row($entrada,$controller);
	}
	
	if($entradas->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' >".$CI->lang->line('items_no_items_to_display')."</div>";
	}
	
	return $table_data_rows;
	$this->output->enable_profiler(TRUE);
}

function get_entradas_data_row($entrada,$controller)
{	
	
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));

	$table_data_row='<div class="base-casillas">';
	
	$table_data_row.='<div class="casillas-resultados-tablas-d"><div class="soloVisibleResponsivamente">Fecha</div>'.character_limiter($entrada->receiving_time,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-d"><div class="soloVisibleResponsivamente">Proveedor</div>'.character_limiter($entrada->company,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-d"><div class="soloVisibleResponsivamente">Total</div>'.character_limiter($entrada->total,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-d"><div class="soloVisibleResponsivamente">Tipo de pago</div>'.character_limiter($entrada->payment_type,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-d"><div class="soloVisibleResponsivamente">Modo</div>'.character_limiter($entrada->modo,13).'</div>';
		
	$table_data_row.='</div>';
	
	return $table_data_row;
}
/****************** Aqui comienza el helper para los recursos ****************/

function get_recursos_manage_table($recursos,$controller)
{
	$CI =& get_instance();
	$table='<div class="cover-header-tablas">';
	$headers = array(
	'<input type="checkbox" id="select_all" />',
	'Cuenta',
	'Cantidad',
	'Ajustar</div>'
	);
	
	$table.='';
	
	foreach($headers as $header)
	{
		$table.="<div class='titulos-tablas-f'>$header</div>";
	}
	
	
	
	$table.='<section class="tablesorter" id="cover_resultados_tabla">';
	$table.=get_recursos_manage_table_data_rows($recursos,$controller);
	$table.='<section>';
	return $table;
}

function get_recursos_manage_table_data_rows($recursos,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($recursos->result() as $recurso)
	{
		$table_data_rows.=get_recursos_data_row($recurso,$controller);
	}
	
	if($recursos->num_rows()==0)
	{
		$table_data_rows.="<div class='warning_message' >".$CI->lang->line('items_no_items_to_display')."</div>";
	}
	
	return $table_data_rows;
	
}

function get_recursos_data_row($recurso,$controller)
{	
	
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	
	
	$table_data_row='<div class="base-casillas">';
	$table_data_row.="<div class='casillas-resultados-tablas-f'><div class='soloVisibleResponsivamente'>Seleccionar</div><input type='checkbox' id='$recurso->recurso_id' value='".$recurso->recurso_id."'/></div>";
	$table_data_row.='<div class="casillas-resultados-tablas-f"><div class="soloVisibleResponsivamente">Cuenta</div>'.character_limiter($recurso->nombre,13).'</div>';
	$table_data_row.='<div class="casillas-resultados-tablas-f"><div class="soloVisibleResponsivamente">Cantidad</div>'.character_limiter(to_currency($recurso->recurso),13).'</div>';
	$table_data_row.='<div id="opc-edit2" class="casillas-resultados-tablas-f"><div class="soloVisibleResponsivamente">Ajustar</div>'.anchor($controller_name."/view/$recurso->recurso_id/", 'Ajustar',array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</div>';		
	$table_data_row.='</div>';
		

	
	return $table_data_row;


}




















?>