<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open('items/save_inventory/'.$item_info->item_id,array('id'=>'item_form'));
?>
<fieldset id="item_basic_info" class="fieldset_form_popup">
<legend class="name-forms-popup"><?php echo $this->lang->line("items_basic_information"); ?></legend>


<div class="field_row clearfix">
<?php echo form_label($this->lang->line('items_name').': <b>'.$item_info->name.'</b>', 'name',array('class'=>'wide')); ?>

<div class='form_field'>
	

  </div>
</div>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('items_category').': <b>'.$item_info->category.'</b>', 'category',array('class'=>'wide')); ?>
	<div class='form_field'>
	

  </div>
</div>
<div class="field_row clearfix">
<?php
$utilidad =$item_info->unit_price-$item_info->cost_price;

$porcentaje_ganancia=($utilidad/$item_info->unit_price)*100;
 echo form_label($this->lang->line('items_cost_utilidad').'<b>'.to_currency($utilidad).'</b>', 'unit_price',array('class'=>'wide')); ?>
	<div class='form_field'>
	

  </div>
</div>
<div class="field_row clearfix">
<?php echo form_label('Utilidad  del <b>'.$porcentaje_ganancia.' %</b>', 'quantity',array('class'=>'wide')); ?>

<div class='form_field'>
</div></div>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('items_current_quantity').': <b>'.$item_info->quantity.'</b>', 'quantity',array('class'=>'wide')); ?>
 <div class='form_field'>
	



 </div>
</div>
<div class="field_row clearfix">	
<?php 
echo form_label($this->lang->line('items_aprox_utili').': <b>'.$item_info->quantity*$utilidad.'</b>', 'name',array('class'=>'wide')); ?>
<div class='form_field'>
	

  </div>
</div>
<div class="field_row clearfix"><div class='form_field'></div></div>
<div class="field_row clearfix"><div class='form_field'></div></div>
</fieldset>
<?php 
echo form_close();
?>
<!--  
<table border="0" align="center">
<tr bgcolor="#FF0033" align="center" style="font-weight:bold"><td colspan="4">Seguimiento de datos de inventario</td></tr>
<tr align="center" style="font-weight:bold"><td width="15%">Fecha</td><td width="25%">Empleado</td><td width="15%">Entrada / Salida Cantidad</td><td width="45%">Observaciones</td></tr>
<?php
//foreach($this->Inventory->get_inventory_data_for_item($item_info->item_id)->result_array() as $row)
{
?>
<tr bgcolor="#CCCCCC" align="center">
<td><?php//echo $row['trans_date'];?></td>
<td><?php
	//$person_id = $row['trans_user'];
	//$employee = $this->Employee->get_info($person_id);
	//echo $employee->first_name." ".$employee->last_name;
	?>
</td>
<td align="right"><?php// echo $row['trans_inventory'];?></td>
<td><?php// echo $row['trans_comment'];?></td>
</tr>

<?php
}
?>
</table>-->