<!DOCTYPE html>
<html lang="es">

<head>
<meta charset="UTF-8">
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="noindex, follow" />
<meta name="description" content="Venta de hardware y Reparación de equipo computacional, Configuraci&oacute;n de Redes, C&aacute;maras de Seguridas... Reparaci&oacute;n de Computadoras en Puerto Vallarta">
<meta name="keywords" content="Venta de hardware y Reparaci&oacute;n Computadoras en Puerto Vallarta, Configuración de Redes y C&aacute;maras de Seguridas, Refacciones de Computadoras, cargadores para laptop accesorios para celulares , venta de computadoras, Instalaci&oacute;n de programas, Intalaci&oacute;n de Aplicaciones">
<meta name="author" content="Brontobyte Computaci&oacute;n">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>Brontobyte Computación, Reparacion de computadoras en Puerto vallarta</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style-header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style-contenido.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/component.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/dispositivos.css" />
		

<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,200,500,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300' rel='stylesheet' type='text/css'>

<script src="http://code.jquery.com/jquery.js"></script>
<script src="<?php echo base_url();?>js/modernizr.custom.js"></script>
<link rel="shortcut icon" href="<?php echo base_url();?>images/ico/ico-logo.ico">

</head>

 
<body>
	<div class="wrapper" id="wrap-port" itemscope itemtype="http://schema.org/LocalBusiness" >
		<?php $this->load->view("header"); ?>
		<div id="sb-site" class="box-contenido">

<div class="head_movil">
	<div id="ico-menu-movil">
		<span class="sb-toggle-left">
			<img src="<?php echo base_url();?>images/ico/main-movil.png"/>
		</span>
			
	</div>
<a href="#"> 

	<img class="logo-movil" src="<?php echo base_url();?>images/img-header/Logo-head-bontobytes.png" alt="logo-computacion"/>
	
</a>	

	
</div>
		<section class="contenido-secciones" id="seccion-porta" itemprop="location" itemscope itemtype="http://schema.org/Place">
			<div id="box-subtitulo-secciones">
				<h3 class="h3-subtitulo-secciones">
				
				<a class="head-a-ubi" href="<?php echo base_url();?>">Pagina de inicio / </a>
				
				<span class="head-ubica2"  itemprop="keywords" >
				Portafolio
				</span>
				
				
				
				</h3>
			</div>
			
			
			<div id="box-nombre-portafolio">
					<span class="subtitulos-seccion subtitulos-seccion-2">Conoce nuestros Proyectos más de cerca.</span> 
					<p class="p-intro2">
					Aquí podras encontrar algunos de los servicios que hemos realizado : Diseño de sitios web, Publicidad , Logotipos , Apps , etc. Conoce los  proyectos para nuestros clientes  en la ciudad de Puerto Vallarta , Bahia de Banderas, Cd. Mexico y fuera del Pais como Perú y Chile.
					</p>
			</div>
			<div class="box-cover-portafolio">
				<div class="box-portafolio">
					<div class="grid cs-style-5">
					
							<figure>
								<img class="img-portafolio" src="<?php echo base_url();?>images/img-portafolio/cliente-himexico.jpg"/>
								<figcaption>
									<h5>Hostelling International México, Afiliación y Reserva de Hostales en México</h5>
									<a href="http://himexico.com/"  target="_blank">Ver website</a>
								</figcaption>
							</figure>
						
					</div>
					<span class="txt-articulos-index"  itemprop="name">DISEÑO WEB</span>
					<hr class="hr-art"/>
				 </div>
				 <div class="box-portafolio">
					<div class="grid cs-style-5">
					
							<figure>
								<img class="img-portafolio" src="<?php echo base_url();?>images/img-portafolio/cliente-ayahuasca.jpg"/>
								<figcaption>
									<h5>Ayahuasca Peru , Blog & Tienda Online, cura para el Alma y Cuerpo.</h5>
									<a href="http://ayahuasca.com.pe/" target="_blank" >Ver website</a>
								</figcaption>
							</figure>
						
					</div>
					<span class="txt-articulos-index"  itemprop="name">DISEÑO WEB</span> <hr class="hr-art"/>
				 </div>
				 
				 <div class="box-portafolio">
					<div class="grid cs-style-5">
					
							<figure>
								<img class="img-portafolio" src="<?php echo base_url();?>images/img-portafolio/cliente-mispies.jpg"/>
								<figcaption>
									<h5>Mis Pies , Tienda Online de Productos Quirurgicos.</h5>
									<a href="http://mispiespv.com/" target="_blank" >Ver website</a>
								</figcaption>
							</figure>
						
					</div>
					<span class="txt-articulos-index"  itemprop="name">DISEÑO WEB</span> <hr class="hr-art"/>
				 </div>
				 
				  <div class="box-portafolio">
					<div class="grid cs-style-5">
					
							<figure>
								<img class="img-portafolio" src="<?php echo base_url();?>images/img-portafolio/cliente-pvd.jpg"/>
								<figcaption>
									<h5>PVD, Reserva de Tours y Traslados en Puerto Vallarta </h5>
									<a href="http://puertovallartadiscovery.com/" target="_blank">Ver website</a>
								</figcaption>
							</figure>
						
					</div>
					<span class="txt-articulos-index"  itemprop="name">DISEÑO WEB</span> <hr class="hr-art"/>
				 </div>
				 
				  <div class="box-portafolio">
					<div class="grid cs-style-5">
					
							<figure>
								<img class="img-portafolio" src="<?php echo base_url();?>images/img-portafolio/cliente-sufimita.jpg"/>
								<figcaption>
									<h5>Sufi Mita, Restaurante Mediterraneo en Punta de Mita.</h5>
									<a href="http://www.sufipuntamita.com/" target="_blank">Ver website</a>
								</figcaption>
							</figure>
						
					</div>
					<span class="txt-articulos-index"  itemprop="name">DISEÑO WEB</span> <hr class="hr-art"/>
				 </div>
				 
				  <div class="box-portafolio">
					<div class="grid cs-style-5">
					
							<figure>
								<img class="img-portafolio" src="<?php echo base_url();?>images/img-portafolio/cliente-mexican.jpg"/>
								<figcaption>
									<h5>Mexican Logde, Reserva de Cabañas en Pucón, Chile.</h5>
									<a href="http://mexicanlodge.com/" target="_blank">Ver website</a>
								</figcaption>
							</figure>
						
					</div>
					<span class="txt-articulos-index"  itemprop="name">DISEÑO WEB</span> <hr class="hr-art"/>
				 </div>
				 
				 
				 
				
				</div>
			</div>
		</section>
			</div>
			</div>

		<?php include('footer.php'); ?>


</body>


