<!DOCTYPE html>
<html lang="es">
<head> 
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Author" content="Adrián Ramirez | brontobytemx.com" />
<meta name="Subject" content="Diseño web Mexico" />
<meta name="GOOGLEBOT" content="INDEX, FOLLOW, ALL" />
<meta name="robots" content="index, follow" />
<meta name="GOOGLEBOT" content="NOARCHIVE" />
<meta name="Generator" content="html" />
<meta name="Language" content="Spanish" />
<meta name="Revisit" content="1 day" />
<meta name="Distribution" content="Global" />
<meta name="Robots" content="All" />
	<meta property="og:url"           content="" />
    <meta property="og:type"          content="" />
    <meta property="og:title"         content="" />
    <meta property="og:description"   content="" />
    <meta property="og:image"         content="" />

<title>TravelPro | Home</title>
<meta name="Description" content="TravelPro es una empresa dedicada a brindarle servicios profesionales en viajes a todo el mundo en los mejores hoteles, destinos paradisiacos y ciudades de ensueño.
Nuestro trabajo es darte la mejor experiencia en tu proximo viaje y que cumplas el sueño rebasando las expectativas que tenias en el, somos tus expertos en viajes!.
 "/>
<meta name="Keywords" content="viajes, vuelos, tours, expediciones, paquetes turisticos, servicio turisticos" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/style-header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/style-all.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/dispositivos.css"/>

<!--web-font-->
<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,600,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Cabin:400,600,500' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oleo+Script" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Suez+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans|Open+Sans+Condensed:300" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Covered+By+Your+Grace|Satisfy" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Caveat|Satisfy|Yellowtail" rel="stylesheet">







<!--//web-font-->

<script src="http://code.jquery.com/jquery.js"></script>


<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


<link rel="shortcut icon" href="<?php echo base_url();?>images/ico/favicon.ico">

    <style>

    /* CUSTOMIZE THE CAROUSEL
    -------------------------------------------------- */

    /* Carousel base class */
    .carousel {
      margin-bottom: 60px;
    }

    .carousel .container {
      position: relative;
      z-index: 9;
    }

    .carousel-control {
      height: 80px;
      margin-top: 0;
      font-size: 120px;
      text-shadow: 0 1px 1px rgba(0,0,0,.4);
      background-color: transparent;
      border: 0;
      z-index: 10;
    }

    .carousel .item {
      height: 500px;
    }
    .carousel img {
      position: absolute;
      top: 0;
      left: 0;
      min-width: 100%;
      height: 500px;
    }

    .carousel-caption {
      background-color: transparent;
      position: static;
      max-width: 550px;
      padding: 0 20px;
      margin-top: 200px;
    }
    
    .carousel-caption .lead {
      margin: 0;
      line-height: 1.25;
      color: #fff;
      text-shadow: 0 1px 1px rgba(0,0,0,.4);
    }
    .carousel-caption .btn {
      margin-top: 10px;
    }



    </style>

</head>

<body>
	<div class="wrapper" id="w-home">

	<!--navigation-->
	<?php $this->load->view("header");?>
	<!--navigation-->
	
		<div id="sb-site" class="box-contenido">
		
	<!--MAIN MOVIL-->
	<?php $this->load->view("menu-movil");?>
	<!--MAIN MOVIL-->
			
		

		<div id="banner">

				<div id="myCarousel" class="carousel slide">

				  <div class="carousel-inner">

					<div class="item active">
					 
					  <img src="<?php echo base_url();?>images/banner/travel3.jpg" alt="Paquetes"/>
					 
					  <div class="container">
						  <div class="carousel-caption">
							<span  class="txt-banner">
						ORLANDO  USD$779
							</span >
							<a class="btn_banner" href="http://travelpro.mx/index.php/front/detalles_paquete/2">M&aacute;s Info</a>
						  </div>
					  </div>
					</div>
			
					
					<div class="item ">
					 
					  <img src="<?php echo base_url();?>images/banner/travel2.jpg" alt="">
					 
					  <div class="container">
						  <div class="carousel-caption">
							<span  class="txt-banner">
							Peru USD$899
							</span >
							<a class="btn_banner" href="http://travelpro.mx/index.php/front/detalles_paquete/7">M&aacute;s Info</a>
						  </div>
					  </div>
					</div>
						<div class="item ">
					 
					  <img src="<?php echo base_url();?>images/banner/travel.jpg" alt="">
					 
					  <div class="container">
						  <div class="carousel-caption">
							<span  class="txt-banner">
							Europa USD$999
							</span >
							<a class="btn_banner" href="http://travelpro.mx/index.php/front/detalles_paquete/6">M&aacute;s Info</a>
						  </div>
					  </div>
					</div>

			
				  </div>
				
				  <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
				  <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
				</div>
				</div>
				
				<!-- *********Motor de reservas (start) *******-->
					<div id="title-reserv-front" class="cover_titles_half_rigth">
					<span class="title-big title-big-right">RESERVAR</span> <span class="fx-border fx-border-right"></span>
					</div>		
<div id="ptw-container">
<script type="text/javascript" src="http://widgets.priceres.com.mx/travel-pro/jsonpBooker/startWidget?container=ptw-container&UseConfigs=false"></script></div>

<!-- *********Fin de motor (end) *******-->


				
				
				<section id="cover-promo" class="cover_standar">
					<div class="cover_titles_half">
					<span class="title-big">CRUCEROS</span> <span class="fx-border"></span>
					</div>
					
					<div class="box-all-prom">
					<article class="articles_promo">
				
					<a href="http://travelpro.mx/index.php/front/detalles_paquete/25">
							<img src="<?php echo base_url();?>images/ico/travel-crucero.png">
							<h3 class="name-travel-prom">Crucero al Caribe</h3>
						
							<span class="price-travel-pro">Desde $13359 MXN</span>
					</a>
					</article>
					<article class="articles_promo">
					<a href="http://travelpro.mx/index.php/front/detalles_paquete/24">
							<img src="<?php echo base_url();?>images/ico/travel-crucero.png">
							<h3 class="name-travel-prom">Crucero a Alaska</h3>
							
							<span class="price-travel-pro">Desde $999 USD</span>
					</a>
					</article>
					<article class="articles_promo">
					<a href="http://travelpro.mx/index.php/front/detalles_paquete/26">
							<img src="<?php echo base_url();?>images/ico/travel-crucero.png">
							<h3 class="name-travel-prom">Crucero a Europa</h3>
							<span class="price-travel-pro">Desde $17530 MXN</span>
					</a>
					</article>
					<article class="articles_promo">
					<a href="http://travelpro.mx/index.php/front/detalles_paquete/27">
							<img src="<?php echo base_url();?>images/ico/travel-crucero.png">
							<h3 class="name-travel-prom">Crucero a Canada</h3>
							<span class="price-travel-pro">Desde $999 USD</span>
					</a> 
					</article>
					<article class="articles_promo">
					<a href="http://travelpro.mx/index.php/front/detalles_paquete/32">
							<img src="<?php echo base_url();?>images/ico/travel-crucero.png">
							<h3 class="name-travel-prom">Harmony Of the seas</h3>
							<span class="price-travel-pro"></span>
					</a>
					</article>
					<article class="articles_promo">
					<a href="http://travelpro.mx/index.php/front/detalles_paquete/39">
							<img src="<?php echo base_url();?>images/ico/travel-crucero.png">
							<h3 class="name-travel-prom">Crucero a las Bahamas</h3>
							<span class="price-travel-pro">Dese $7939 MXN</span>
					</a>
					</article>
				
					</div>
				
					
				
					
				</section>
				<section id="cover-paq" class="cover_standar">
					<div class="cover_titles_half_rigth">
					<span class="title-big title-big-right">PAQUETES</span> <span class="fx-border fx-border-right"></span>
					</div>
					<h2 id="frase_front">
					¡Estos son algunos de los destinos favoritos de nuestros viajeros con promociones permanentes a todo el mundo!
					</h2>
					<div class="box-par">
						<article class="articles_paquetes_f">
							 <a class="a-img-vuelos" href="#"><img src="<?php echo base_url();?>images/paquetes/front-img-paquetes/cancun-travel.jpg"></a>
							 <span class="fx-vuelos-color"></span>
							
								<div class="line-name">
								<span class="name-paq-prom">Cancun</span>
								</div>
								<span class="price-paq-pro"></span>
								<a class="btn-detalles-viajes" href="http://travelpro.mx/index.php/front/detalles_paquete/16">MAS DETALLES</a>
						</article>
						
						<article class="articles_paquetes_f articles_paquetes_f2">
								<a class="a-img-vuelos" href="#"><img src="<?php echo base_url();?>images/paquetes/front-img-paquetes/peru-travel.jpg"></a>
								<span class="fx-vuelos-color"></span>
							
								<div class="line-name">
								<span class="name-paq-prom">Peru</span>
							
								</div>
								<span class="price-paq-pro"></span>
								<a class="btn-detalles-viajes" href="http://travelpro.mx/index.php/front/detalles_paquete/7">MAS DETALLES</a>
						</article>
					
					</div>
					
					<div class="box-par box-par2">
						<article class="articles_paquetes_f articles_paquetes_f2">
								<a class="a-img-vuelos" href="#"><img src="<?php echo base_url();?>images/paquetes/front-img-paquetes/chiapas-travel.jpg"></a>
								<span class="fx-vuelos-color"></span>
							
								<div class="line-name">
								<span class="name-paq-prom">Europa</span>
								</div>
								<span class="price-paq-pro"></span>
								<a class="btn-detalles-viajes" href="http://travelpro.mx/index.php/front/detalles_paquete/6">MAS DETALLES</a>
						</article>
						
					
						<article class="articles_paquetes_f ">
								<a class="a-img-vuelos" href="#"><img src="<?php echo base_url();?>images/paquetes/front-img-paquetes/canada-travel.jpg"></a>
								<span class="fx-vuelos-color"></span>
							
								<div class="line-name">
								<span class="name-paq-prom">Canada</span>
								</div>
								<span class="price-paq-pro"></span>
								<a class="btn-detalles-viajes" href="http://travelpro.mx/index.php/front/detalles_paquete/4">MAS DETALLES</a>
						</article>
						
					</div>
					
					
					
					
			
				</section>
				
				<div id="box-suscribir" >
				<h3 class="title-news">NEWSLETTER</h3>
				<span class="fx-tri-suscri"></span>
				<p class="p-news">Suscribete y recibe exclusivas promociones en nuestro boletin turistico .</p>
				
				
				<form id="form-news">
				<label>Correo electronico</label>
					<input class="" name="newsletter" type="text" id="newsletter" value=""  />
					<input type="submit" name="button"  id="button-news" value="ENVIAR" />
					
				</form>
				
				
				</div>
				
				<section id="cover-testimonios" class="cover_standar">
					<div class="cover_titles_half">
					<span id="title-testimo-front" class="title-big">Testimonios</span> <span class="fx-border"></span>
					</div>
					
					<div >
						<div id="box-p-testim" >
						<p>
						<span class="comillas-tes">"</span>
						Los expertos de Travel Pro nos diseñaron un paquete con el cual pudimos conocer Europa y sus principales ciudades así como museos, lugares históricos y atracciones a un costo muy accesible que se ajusto a nuestro presupuesto.	
						<span class="comillas-tes">"</span>
					
						</p>
						<p>
						<span class="comillas-tes">"</span>
						Nuestro viaje fue maravilloso , gracias al profesionalismo y conocimiento de Travel Pro. Gracias por hacer nuestro sueño realidad.	
						<span class="comillas-tes">"</span>
					
						</p>
						<p>
						<span class="comillas-tes">"</span>
						Gracias Travel Pro por hacer un viaje de aprendizaje , sin duda son experiencias que jamas olvidare.	
						<span class="comillas-tes">"</span>
					
						</p>
					
						
						
						</p>
						</div>
						
						
					</div>
					
					<span  class="btn_testimon" id="go"	 >Go</span>
					<span  class="btn_testimon" id="stop" >Stop</span>
					<span  class="btn_testimon" id="back">Back</span>
				</section>
		
		</div>
	</div>
	<?php $this->load->view("footer"); ?>
	
<!-- bootstrop-->
  <script src="<?php echo base_url();?>js/fjs/bootstrap.js"></script>
     <script>
      !function ($) {
        $(function(){
          // carousel demo
          $('#myCarousel').carousel()
        })
      }(window.jQuery)
    </script>
 <!-- bootstrop-->
 <script>
// Start animation
$( "#go" ).click(function() {
  $( "#box-p-testim p" ).animate({top: "-=50px" }, 3000 );
});
 
// Stop animation when button is clicked
$( "#stop" ).click(function() {
  $("#box-p-testim p" ).stop();
});
 
// Start animation in the opposite direction
$( "#back" ).click(function() {
  $("#box-p-testim p" ).animate({ top: "+=50px" }, 3000 );
});
</script>


</body>

</html>

