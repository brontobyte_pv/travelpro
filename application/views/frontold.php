
<!DOCTYPE html>
<html>
<head>
<title>Travelpro | Inicio ::</title>
<link href="<?php echo base_url();?>css/fcss/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="<?php echo base_url();?>css/fcss/style.css" type="text/css" rel="stylesheet" media="all">
<link href="<?php echo base_url();?>css/fcss/redesing.css" type="text/css" rel="stylesheet" media="all">
<!--web-font-->
<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,600,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Cabin:400,600,500' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=BenchNine" rel="stylesheet">

<!--//web-font-->
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Excursion Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //Custom Theme files -->
<!-- js -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>  
<!-- //js -->	
<!-- start-smoth-scrolling-->
<script type="text/javascript" src="<?php echo base_url();?>js/fjs/move-top.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/fjs/easing.js"></script>	
<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
</script>
<!--//end-smoth-scrolling-->
</head>
<body>
	<!--navigation-->
	<?php $this->load->view("header");?>
	<!--navigation-->
<div class="wrapper">
	<div class="banner">
		<!-- banner-text Slider starts Here -->
				<script src="<?php echo base_url();?>js/fjs/responsiveslides.min.js"></script>
				 <script>
					// You can also use "$(window).load(function() {"
						$(function () {
						// Slideshow 4
							$("#slider3").responsiveSlides({
							auto: true,
							pager:false,
							nav:true,
							speed: 500,
							namespace: "callbacks",
							before: function () {
							$('.events').append("<li>before event fired.</li>");
							},
							after: function () {
								$('.events').append("<li>after event fired.</li>");
							}
						});	
					});
				</script>
			<!--//End-slider-script -->
			<div id="banner-title">
					<div  id="top" class="callbacks_container">
						<ul class="rslides" id="slider3">
							<li>
							<img src="<?php echo base_url();?>images/front/banner.jpg">
								<h5>ewewewe wonderful trip</h5>
								<h1>Molestias excepturi sintprovident</h1>
							</li>
							<li>
							<img src="<?php echo base_url();?>images/front/banner2.jpg">
								<h5>Vident sintocca</h5>
								<h1>Cupiditate non provident molepturi </h1>
							</li>
							<li>
							<img src="<?php echo base_url();?>images/front/banner3.jpg">
								<h5>Excepturi sintstias</h5>
								<h1>Non provident sint occaestiasexce</h1>
							</li>
							<li>
							<img src="<?php echo base_url();?>images/front/banner4.jpg">
								<h5>Excepturi sintstias</h5>
								<h1>Non provident sint occaestiasexce</h1>
							</li>
							<li>
							<img src="/web1/images/banner5.jpg">
								<h5>Excepturi sintstias</h5>
								<h1>Non provident sint occaestiasexce</h1>
							</li>
						</ul>	
					</div>
				
            </div>	
	</div>	
	
	
	
	
	
<div id="cover_articles_front">
    <div class="cover-titles">
		
			<h3>Paquetes de Temporada</h3>
			<p>Estos son algunos de los destinos favoritos de nuestros viajeros con promociones permanentes a todo el mundo!</p>
		
	</div>	
	<div class="box-articles-front">
	
			<div class="row">
				<div class="col-md-4 bb-grids">
					<a href="detalles.html" class="thumbnail">
					  <img src="<?php echo base_url();?>images/front/img1.jpg" alt="">
					</a>
				</div>
				<div class="col-md-4 bb-grids">
					<a href="detalles.html" class="thumbnail">
					  <img src="<?php echo base_url();?>images/front/img2.jpg" alt="">
					</a>
				</div>
				<div class="col-md-4 bb-grids">
					<a href="detalles.html" class="thumbnail">
					  <img src="<?php echo base_url();?>images/front/img3.jpg" alt="">
					</a>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="row">
				<div class="col-md-4 bb-grids">
					<a href="detalles.html" class="thumbnail">
					  <img src="<?php echo base_url();?>images/front/img4.jpg" alt="">
					</a>
				</div>
				<div class="col-md-4 bb-grids">
					<a href="singlepage.html" class="thumbnail">
					  <img src="<?php echo base_url();?>images/front/img5.jpg" alt="">
					</a>
				</div>
				<div class="col-md-4 bb-grids">
					<a href="singlepage.html" class="thumbnail">
					  <img src="<?php echo base_url();?>images/front/img6.jpg" alt="">
					</a>
				</div>
				
				<div class="clearfix"> </div>
			</div>
			<div class="row">
				<div class="col-md-4 bb-grids">
					<a href="singlepage.html" class="thumbnail">
					  <img src="<?php echo base_url();?>images/front/img7.jpg" alt="">
					</a>
				</div>
				<div class="col-md-4 bb-grids">
					<a href="singlepage.html" class="thumbnail">
					  <img src="<?php echo base_url();?>images/front/img8.jpg" alt="">
					</a>
				</div>
				<div class="col-md-4 bb-grids">
					<a href="singlepage.html" class="thumbnail">
					  <img src="<?php echo base_url();?>images/front/img9.jpg" alt="">
					</a>
				</div>
				
				<div class="clearfix"> </div>
			</div>
		</div>	

	
	
	
	</div>
	    <div class="cover-titles cover-titles2">
		
			<h3>Lo más impotante es tu satisfacción </h3>
			<p>Estamos comprometidos con tu satisfacción, pára Travel Pro Usted es lo mas importante es por eso que le garantizamos la mejor oferta a destinos nacionales y extranjeros de la region.</p>
		</div>
	
</div>
	<!--footer-->
	
	<?php $this->load->view("footer");?>
	
	<!--footer-->
	
	
		<script type="text/javascript">
			$(document).ready(function() {
				
				$('.banner-title').css('display','none');
				
				/*
				var defaults = {
					containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
				};
				*/
				
				$().UItoTop({ easingType: 'easeOutQuart' });
				
			});
		</script>
		<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<!--//smooth-scrolling-of-move-up-->
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	
    <script src="<?php echo base_url();?>js/fjs/bootstrap.js"></script>
	
</body>
</html>