<!DOCTYPE html>
<html lang="es">
<head> 
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Author" content="Adrián Ramirez | brontobytemx.com" />
<meta name="Subject" content="Diseño web Mexico" />
<meta name="GOOGLEBOT" content="INDEX, FOLLOW, ALL" />
<meta name="robots" content="index, follow" />
<meta name="GOOGLEBOT" content="NOARCHIVE" />
<meta name="Generator" content="html" />
<meta name="Language" content="Spanish" />
<meta name="Revisit" content="1 day" />
<meta name="Distribution" content="Global" />
<meta name="Robots" content="All" />
	<meta property="og:url"           content="" />
    <meta property="og:type"          content="" />
    <meta property="og:title"         content="" />
    <meta property="og:description"   content="" />
    <meta property="og:image"         content="" />

<title>TravelPro|Home</title>
<meta name="Description" content=""/>
<meta name="Keywords" content="" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/style-header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/style-all.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/dispositivos.css"/>


<!--web-font-->
<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,600,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Cabin:400,600,500' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=BenchNine" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Crete+Round" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oleo+Script" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Suez+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">





<!--//web-font-->

<script src="http://code.jquery.com/jquery.js"></script>
 
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="shortcut icon" href="<?php echo base_url();?>images/ico/favicon.ico">

 

</head>
<body id="body-reserv">
<div class="wrapper w-sections" id="w-reserv">

	<!--navigation-->
	<?php $this->load->view("header");?>
	<!--navigation-->
	
		<div id="sb-site" class="box-contenido">
				
	<!--MAIN MOVIL-->
	<?php $this->load->view("menu-movil");?>
	<!--MAIN MOVIL-->
			
		
				<div class="banner-section">
					<img class="img-paquete-detail" src="<?php echo base_url();?>images/reserva-img-contact/banner-reservar.jpg"/>
					<div class="cover-subtitles">
					<?php 
		if(empty($cart))
		{?>
		<h2 class="subtitles-bg"><?php echo $this->lang->line('sales_no_items_in_cart');?></h2>
		<div class="cover-subtitles">
					<div id="nav-sub"><span>VER </span><a href="<?php echo base_url();?>">PAQUETES</a> </div>
					</div>
				</div>
		<?php	
		}
		else
		{
			echo '<h2 class="subtitles-bg">RESERVA AHORA!</h2>';
		?>
		
					<div id="nav-sub"><span>VER </span><a href="<?php echo base_url();?>">PAQUETES</a> </div>
					</div>
				</div>
				<div class="cover_standar">
				<h3 id="txt-res-paquete">Paso 1: Revisa que sea el paquete que elegiste</h3>
		<div id="table_info_paquetes">
		
			<div class="cover_filas_paq_titles">
			<div class="box-conten-paq" ><h5 class="resalt-table"><?php echo $this->lang->line('sales_quantity');?></h5></div>
				<div class="box-conten-paq" ><h5 class="resalt-table"><?php echo $this->lang->line('item_kits_paquete');?></h5></div>
				
				<div class="box-conten-paq" ><h5 class="resalt-table"><?php echo $this->lang->line('sales_sub_total');?></h5></div>
				
				
			</div>
			<div class="cover_filas_paq">
		<?php 
		$i=1;
		
		foreach(array_reverse($cart, true) as $line=>$item)
		{
			
			
			
			$cur_item_info = $this->Item->get_info($item['item_id']);
			
			
			//$subtotal[$i]= $cur_item_info->unit_price *$item['quantity'];
		 
		 ?>
		 <div class="box-conten-paq" >
		<span class="resalt-table">
		<?php echo $item['quantity'];?></span></div>
				 <div class="box-conten-paq" >
		<span class="resalt-table"><?php echo $item['name']; ?></span></div>
				
				<div class="box-conten-paq" >
		<span class="resalt-table"><?php 
				$subtotal=$item['price']*$item['quantity'];
				echo to_currency($subtotal); ?></span></div>
		
		
	
		
	
		
		<?php $i++; }?>
		
		
				
			</div>
			
			
			
			<h3 id="txt-res-paquete">
			Total: 
			<output class="total-car"><?php 
			if(isset($total))
			{
			echo to_currency($total); 
			}
			?></output>
			
			
			<?php 
		echo form_open("cart/exist_client/",array('id'=>'reservation_form'));
	
		?>
			</h3><h3 class="email_hidden">
¿Cuál es tu correo?<?php
		echo form_input(array('name'=>'chk_email','id'=>'chk_email','size'=>'30','value'=>''));?>
		</h3>
			
<a id="btn-mod-paq-now" href="<?php echo base_url();?>index.php/cart/cancelar_pedido"><?php echo $this->lang->line('sales_order_cancel'); ?></a>
			<a id="btn_rese_cont" class="btns-pedido" ><?php echo $this->lang->line('sales_order_cont'); ?></a>
			<a id="btn_rese" class="btns-pedido popup-with-form"  href="#popup-pago-reg"><?php echo $this->lang->line('sales_order_next'); ?></a>
		
		</div>
		
		</div>
		
<?php		
		}

		?>
						
				</form>	
		<section  class="cover_standar" id="cover-reserv">
			<article>
		
		

	
			
		</article>
		<article>
		
		
		<h3 class="subtitles"><?php echo $this->lang->line('buying_note'); ?></h3>
		
			<p class="txt-p2">
				<?php echo $this->lang->line('buying_p'); ?>
			</p>
		</article>
			<div id="ptw-container">
			<script type="text/javascript" src="http://widgets.priceres.com.mx/travel-pro/jsonpBooker/startWidget?container=ptw-container&UseConfigs=false"></script>
			</div>
			
			
		
				
	
		
		</section>
		</div>	
	

</div>
	<?php $this->load->view("footer");?>
	
	<!--footer-->
	<!-- bootstrop-->
  <script src="<?php echo base_url();?>js/fjs/bootstrap.js"></script>
     <script>
	$('#btn_rese_cont').click(function()
    {
		
		$(".email_hidden").css("display","block");
		$("#btn_rese_cont").css("display","none");
		$("#btn_rese").css("display","block");
		$("input:text:visible:first").focus();
		$('#chk_email').select();
		

	});
	$("#btn_rese").click(function()
	{
		
	   $('#reservation_form').submit();
	   
	   
    });
      !function ($) {
        $(function(){
          // carousel demo
          $('#myCarousel').carousel()
        })
      }(window.jQuery)
    </script>
 <!-- bootstrop-->
	
</body>
</html>