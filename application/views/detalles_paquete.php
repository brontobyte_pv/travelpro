<!DOCTYPE html>
<html lang="es">
<head> 
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Author" content="Adrián Ramirez | brontobytemx.com" />
<meta name="Subject" content="Diseño web Mexico" />
<meta name="GOOGLEBOT" content="INDEX, FOLLOW, ALL" />
<meta name="robots" content="index, follow" />
<meta name="GOOGLEBOT" content="NOARCHIVE" />
<meta name="Generator" content="html" />
<meta name="Language" content="Spanish"/>
<meta name="Revisit" content="1 day" />
<meta name="Distribution" content="Global" />
<meta name="Robots" content="All" />
	<meta property="og:url"           content="" />
    <meta property="og:type"          content="" />
    <meta property="og:title"         content="" />
    <meta property="og:description"   content="" />
    <meta property="og:image"         content="" />

<title>TravelPro | <?php echo $item_detail->name ?></title>
<meta name="Description" content="<?php echo $this->lang->line("contenido_".$item_detail->item_kit_id."_metaPalabrasUno") ?>"/>
<meta name="Keywords" content="<?php echo $this->lang->line("contenido_".$item_detail->item_kit_id."_metaPalabrasDos") ?>" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/style-header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/style-all.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/dispositivos.css"/>


<!--web-font-->
<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,600,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Cabin:400,600,500' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=BenchNine" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Crete+Round" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oleo+Script" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Suez+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Open+Sans|Open+Sans+Condensed:300,600" rel="stylesheet">






<!--//web-font-->

<script src="http://code.jquery.com/jquery.js"></script>
 
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="<?php echo base_url();?>images/ico/favicon.ico">

 

</head>
<body>
	<div class="wrapper w-sections" id="w-paque">

	<!--navigation-->
	<?php $this->load->view("header");?>
	<!--navigation-->
	
		<div id="sb-site" class="box-contenido">
				<!--MAIN MOVIL-->
			<?php $this->load->view("menu-movil");?>
			<!--MAIN MOVIL-->
				<div class="banner-section">
					<img class="img-paquete-detail" src="<?php echo base_url();?>images/paquetes/front-img-paquetes/<?php echo $item_detail->item_kit_id;?>/banner_paquete.jpg"/>
					<div class="cover-subtitles">
						<h2 class="subtitles-bg"><?php echo strtoupper($item_detail->name);?></h2>
						<div id="nav-sub"><a href="<?php echo base_url();?>">PAQUETES /</a> <a href="<?php echo base_url();?>index.php/front/paquetes/<?php echo $item_detail->category;?>"><?php echo strtoupper($item_detail->category);?> /</a> <span><?php echo strtoupper($item_detail->name);?></span></div>
					</div>
				</div>
				<div class="cover_standar">
				
				<section id="section-paquete" >
					
					 
				
					<article class="box-detail-paquete">
						<img class="img-paquete-detail" src="<?php echo base_url();?>images/paquetes/front-img-paquetes/<?php echo $item_detail->item_kit_id;?>/main.jpg">
						<!-- <div id="cover-volamos-con">
							<span id="span-volamos">Volamos con:</span>
							<img id="logo-linea-viaje" src="<?php echo base_url();?>images/paquetes/xtras/british-airways-logo.png">
						</div>-->
						<div class="cover-iconos-paquete">
						<div  id="box-ico-ubi-paq"  class="box-iconos-paq">
							<img src="<?php echo base_url();?>images/ico/ico-ubica-travel.png">
							<span><?php echo $this->lang->line("contenido_".$item_detail->item_kit_id."_ciudades") ?></span>
						</div>
						<div   id="box-ico-time-paq"  class="box-iconos-paq">
							<img src="<?php echo base_url();?>images/ico/ico-time-travel-bco.png">
							<span>Duración:<?php echo $duracion;
		?> días y <?php echo $duracion-1;
		?> noches.</span>
						</div>
						<div   id="box-ico-price-paq"  class="box-iconos-paq">
							<img src="<?php echo base_url();?>images/ico/ico-price-travel.png">
							<span>Costo:<?php echo to_currency($item_detail->kit_price);?> USD</span>
						</div>
						
					
						</div>
					</article>
					<span id="txt-vigencia"><b>Vigencia:</b> <?php echo $dia;
					?> de <?php echo $mes;
					?> del <?php echo $year;
					?></span>
				
					<div class="box-detail-paquete" id="box-detail-paquete-complete" >
					<h4 class="subtitles-paquetes-in">DESCRIPCION:</h4>
					<span id="word_itinerario">*SUJETO A CAMBIO</span>
					<span class="sub-in-paq">INCLUYE</span>
					<p>
						<?php echo $this->lang->line("contenido_".$item_detail->item_kit_id."_incluye") ?>
						 
					</p>
					<span class="sub-in-paq">LUGARES </span>
					<p>
					<?php echo $this->lang->line("contenido_".$item_detail->item_kit_id."_lugares") ?>
					</p>
					<span class="sub-in-paq">ACTIVIDADES</span>
					<p>
					<?php echo $this->lang->line("contenido_".$item_detail->item_kit_id."_actividades") ?>
					</p>
					<span class="sub-in-paq">RECOMENDACIONES</span>
					<p>
					<?php echo $this->lang->line("contenido_".$item_detail->item_kit_id."_recomendaciones") ?>
					</p>
					<span class="sub-in-paq">OBSERVACIONES</span>
					<p>
					<?php echo $this->lang->line("contenido_".$item_detail->item_kit_id."_observaciones") ?>
					</p>
					<span class="sub-in-paq">SALIDA DEL VUELO</span>
					<p>
					<?php echo $this->lang->line("contenido_".$item_detail->item_kit_id."_salidaVuelo") ?>
					</p>
					</div>		
						
<div id="table_info_paquetes">
<h3 id="txt-res-paquete">PRECIO PAQUETE <?php echo to_currency($precio_paquete);
		?></h3>
<div class="cover_filas_paq_titles">
	<div class="box-conten-paq" >
		<h5 class="resalt-table" id="pax_inc">PERSONAS INCLUIDAS</h5>
		<h5 class="resalt-table" id="pax_add">PERSONAS ADICIONALES</h5>
	</div>

	<div class="box-conten-paq" >
		<h5>CANTIDAD</h5>
	</div>
	<div class="box-conten-paq" >
		<h5 id="precio_xta_t">PRECIO POR PERSONA EXTRA</h5>
	</div>
	
</div>
<div class="cover_filas_paq">
	<div class="box-conten-paq" >
		<span class="resalt-table">
		<?php 
		echo form_open("cart/add/".$item_detail->item_kit_id,array('id'=>'reservation_form'));
		echo $precio_adulto_t;
		?>
		
		
		</span>
	</div>
	<div class="box-conten-paq" >
		<span id="cant_adulto"><?php
		 
		echo $cant_adulto;
		?>
		</span><span id="cant_adulto_ext"><?php
		echo form_input(array('name'=>'input_cant_adulto','id'=>'input_cant_adulto','size'=>'4','value'=>'0'));?>
		</span>
	</div>
	<div class="box-conten-paq" >
		<span class="precios"><?php echo $precio_adulto;
		?></span>
	</div>
	
</div>



<div class="cover_filas_paq">
	<div class="box-conten-paq" >
		<span class="resalt-table" >
		<?php echo $precio_menor_t;
		?>
		
		</span>
	</div>
	<div class="box-conten-paq" >
		<span id="cant_menor"><?php echo $cant_menor;
		?></span>
		<span id="cant_menor_ext"><?php 
		echo form_input(array('name'=>'input_cant_menor','id'=>'input_cant_menor','size'=>'4','value'=>'0'));?></span>
	</div>
	<div class="box-conten-paq" >
		<span class="precios"><?php echo $precio_menor;
		?>
	
	</span>
	</div>

	
</div>

<a id="btn-reservar-paq-now">RESERVAR</a>
<a id="btn-mod-paq-now"  class="sinxtas" >AGREGAR PERSONAS</a>
<a id="btn-mod-paq-now_xtas" class="xtas">SIN PERSONAS EXTRAS</a>

</form>

</div>						
		
		
				</section>
				<div  id="box-aside-paquetes">
					<aside id="aside-paquetes">
						<h3>CATEGORIAS</h3>
						<ul id="nav-aside">
							
							<?php
									if(isset($categories))
									{
									foreach($categories as $categorie)
									{	
									
									echo '<li><a href="'.base_url().'index.php/front/paquetes/'.$categorie.'">'.strtoupper($categorie).'</a></li>';
									
								} }?>
							
							
						</ul>
					</aside>
				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view("footer");?>
	
	<!--footer-->
	<!-- bootstrop-->
  <script src="<?php echo base_url();?>js/fjs/bootstrap.js"></script>
     <script>
	$(document).ready(function()
{	
	$("#cant_adulto_ext").css("display","none");
	$("#cant_menor_ext").css("display","none");
	$("#pax_add").css("display","none");
	$("#precio_xta_t").css("display","none");
	$(".precios").css("display","none");
	$(".xtas").css("display","none");
	
	$('#btn-mod-paq-now_xtas').click(function()
    {
		//Cambia la propiedad css del elemento html con el id encerrado en los primeros parentesis
			$("#cant_adulto_ext").css("display","none");
			$("#cant_adulto").css("display","block");
			$("#cant_menor_ext").css("display","none");
			$("#cant_menor").css("display","block");
			$("#pax_add").css("display","none");
			$("#pax_inc").css("display","block");
			$("#precio_xta_t").css("display","none");
			$(".precios").css("display","none");
			$(".xtas").css("display","none");
			$(".sinxtas").css("display","block");
			
		
		
	});
	
	$('#btn-mod-paq-now').click(function()
    {
		var disp =$("#cant_adulto_ext").css("display");
		//Comparamos si disp esta oculto lo hacemos visible
		
		if (disp == 'none')
		{
			//Cambia la propiedad css del elemento html con el id encerrado en los primeros parentesis
			$("#cant_adulto_ext").css("display","block");
			$("#cant_adulto").css("display","none");
			$("#cant_menor_ext").css("display","block");
			$("#cant_menor").css("display","none");
			$("#pax_add").css("display","block");
			$("#pax_inc").css("display","none");
			$("#precio_xta_t").css("display","block");
			$(".precios").css("display","block");
			$(".xtas").css("display","block");
			$(".sinxtas").css("display","none");
			$("input:text:visible:first").focus();
			$('#input_cant_adulto').select();
			
			
		}//De lo contrario lo ocultamos
		else
		{
			//Cambia la propiedad css del elemento html con el id encerrado en los primeros parentesis
			$("#cant_adulto_ext").css("display","none");
			$("#cant_adulto").css("display","block");
			$("#cant_menor_ext").css("display","none");
			$("#cant_menor").css("display","block");
			$("#pax_add").css("display","none");
			$("#pax_inc").css("display","block");
			$("#precio_xta_t").css("display","none");
			$(".precios").css("display","none");
			$(".xtas").css("display","none");
			$(".sinxtas").css("display","block");
					
		}
		
	});
$("#btn-reservar-paq-now").click(function()
	{
		
	   $('#reservation_form').submit();
	   
	   
    });
	
	});
	 
	 
      !function ($) {
        $(function(){
          // carousel demo
          $('#myCarousel').carousel()
        })
      }(window.jQuery)
    </script>
 <!-- bootstrop-->
	
</body>
</html>