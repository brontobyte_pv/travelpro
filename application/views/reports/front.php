<!DOCTYPE html>
<html lang="es">
<head> 
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Author" content="Adrián Ramirez | brontobytemx.com" />
<meta name="Subject" content="Diseño web Mexico" />
<meta name="GOOGLEBOT" content="INDEX, FOLLOW, ALL" />
<meta name="robots" content="index, follow" />
<meta name="GOOGLEBOT" content="NOARCHIVE" />
<meta name="Generator" content="html" />
<meta name="Language" content="Spanish" />
<meta name="Revisit" content="1 day" />
<meta name="Distribution" content="Global" />
<meta name="Robots" content="All" />
	<meta property="og:url"           content="" />
    <meta property="og:type"          content="" />
    <meta property="og:title"         content="" />
    <meta property="og:description"   content="" />
    <meta property="og:image"         content="" />

<title>TravelPro | Home</title>
<meta name="Description" content="TravelPro es una empresa dedicada a brindarle servicios profesionales en viajes a todo el mundo en los mejores hoteles, destinos paradisiacos y ciudades de ensueño.
Nuestro trabajo es darte la mejor experiencia en tu proximo viaje y que cumplas el sueño rebasando las expectativas que tenias en el, somos tus expertos en viajes!.
 "/>
<meta name="Keywords" content="viajes, vuelos, tours, expediciones, paquetes turisticos, servicio turisticos" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/style-header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/style-all.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/bootstrap.css">


<!--web-font-->
<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,600,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Cabin:400,600,500' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=BenchNine" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Crete+Round" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oleo+Script" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Suez+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans|Open+Sans+Condensed:300" rel="stylesheet">






<!--//web-font-->

<script src="http://code.jquery.com/jquery.js"></script>
 
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/dispositivos.css"/>

<link rel="shortcut icon" href="<?php echo base_url();?>images/ico/favicon.ico">

    <style>

    /* CUSTOMIZE THE CAROUSEL
    -------------------------------------------------- */

    /* Carousel base class */
    .carousel {
      margin-bottom: 60px;
    }

    .carousel .container {
      position: relative;
      z-index: 9;
    }

    .carousel-control {
      height: 80px;
      margin-top: 0;
      font-size: 120px;
      text-shadow: 0 1px 1px rgba(0,0,0,.4);
      background-color: transparent;
      border: 0;
      z-index: 10;
    }

    .carousel .item {
      height: 500px;
    }
    .carousel img {
      position: absolute;
      top: 0;
      left: 0;
      min-width: 100%;
      height: 500px;
    }

    .carousel-caption {
      background-color: transparent;
      position: static;
      max-width: 550px;
      padding: 0 20px;
      margin-top: 200px;
    }
    
    .carousel-caption .lead {
      margin: 0;
      line-height: 1.25;
      color: #fff;
      text-shadow: 0 1px 1px rgba(0,0,0,.4);
    }
    .carousel-caption .btn {
      margin-top: 10px;
    }



    </style>

</head>

<body>
	<div class="wrapper" id="w-home">

	<!--navigation-->
	<?php $this->load->view("header");?>
	<!--navigation-->
	
		<div id="sb-site" class="box-contenido">
		
		
		<div id="box-header-movil">

		
		</div>
		
<!-- *********Motor de reservas (start) *******-->
<div id="ptw-container">
<script type="text/javascript" src="http://widgets.priceres.com.mx/travel-pro/jsonpBooker/startWidget?container=ptw-container&UseConfigs=false"></script></div>

<!-- *********Fin de motor (end) *******-->

		<div id="banner">

				<div id="myCarousel" class="carousel slide">

				  <div class="carousel-inner">

					<div class="item active">
					 
					  <img src="<?php echo base_url();?>images/banner/travel3.jpg" alt="Paquetes"/>
					 
					  <div class="container">
						  <div class="carousel-caption">
							<span  class="txt-banner">Aqui un anuncio
							</span >
							<a class="btn_banner" href="#">RESERVA</a>
						  </div>
					  </div>
					</div>
			
					
					<div class="item ">
					 
					  <img src="<?php echo base_url();?>images/banner/travel2.jpg" alt="">
					 
					  <div class="container">
						  <div class="carousel-caption">
							<span  class="txt-banner">
							</span >
							<a class="btn_banner" href="RESERVA"></a>
						  </div>
					  </div>
					</div>
						<div class="item ">
					 
					  <img src="<?php echo base_url();?>images/banner/travel.jpg" alt="">
					 
					  <div class="container">
						  <div class="carousel-caption">
							<span  class="txt-banner">
							</span >
							<a class="btn_banner" href="">RESERVA</a>
						  </div>
					  </div>
					</div>

			
				  </div>
				
				  <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
				  <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
				</div>
				</div>
				<section id="cover-promo" class="cover_standar">
					<div class="cover_titles_half">
					<span class="title-big">PROMOCIONES</span> <span class="fx-border"></span>
					</div>
					
					<div class="box-all-prom">
					<article class="articles_promo">
					<a href="#">
							<img src="<?php echo base_url();?>images/ico/avion-avion.png">
							<h3 class="name-travel-prom">Vuelo a Paris</h3>
							<span class="price-travel-pro">Desde $456 DLS</span>
					</a>
					</article>
					<article class="articles_promo">
					<a href="#">
							<img src="<?php echo base_url();?>images/ico/avion-avion.png">
							<h3 class="name-travel-prom">Vuelo a Paris</h3>
							<span class="price-travel-pro">Desde $456 DLS</span>
					</a>
					</article>
					<article class="articles_promo">
					<a href="#">
							<img src="<?php echo base_url();?>images/ico/avion-avion.png">
							<h3 class="name-travel-prom">Vuelo a Paris</h3>
							<span class="price-travel-pro">Desde $456 DLS</span>
					</a>
					</article>
					<article class="articles_promo">
					<a href="#">
							<img src="<?php echo base_url();?>images/ico/avion-avion.png">
							<h3 class="name-travel-prom">Vuelo a Paris</h3>
							<span class="price-travel-pro">Desde $456 DLS</span>
					</a>
					</article>
					<article class="articles_promo">
					<a href="#">
							<img src="<?php echo base_url();?>images/ico/avion-avion.png">
							<h3 class="name-travel-prom">Vuelo a Paris</h3>
							<span class="price-travel-pro">Desde $456 DLS</span>
					</a>
					</article>
					<article class="articles_promo">
					<a href="#">
							<img src="<?php echo base_url();?>images/ico/avion-avion.png">
							<h3 class="name-travel-prom">Vuelo a Paris</h3>
							<span class="price-travel-pro">Desde $456 DLS</span>
					</a>
					</article>
					</div>
				
					
				
					
				</section>
				<section id="cover-paq" class="cover_standar">
					<div class="cover_titles_half_rigth">
					<span class="title-big title-big-right">PAQUETES</span> <span class="fx-border fx-border-right"></span>
					</div>
					<h2 id="frase_front">
					Estos son algunos de los destinos favoritos de nuestros viajeros con promociones permanentes a todo el mundo!
					</h2>
					<div class="box-par">
						<article class="articles_paquetes_f">
							 <a class="a-img-vuelos" href="#"><img src="<?php echo base_url();?>images/paquetes/front-img-paquetes/cancun-travel.jpg"></a>
							 <span class="fx-vuelos-color"></span>
							
								<div class="line-name">
								<span class="name-paq-prom">Mega Italia Promocional</span>
								</div>
								<span class="price-paq-pro">$000 por persona</span>
								<a class="btn-detalles-viajes" href="">MAS DETALLES</a>
						</article>
						
						<article class="articles_paquetes_f articles_paquetes_f2">
								<a class="a-img-vuelos" href="#"><img src="<?php echo base_url();?>images/paquetes/front-img-paquetes/peru-travel.jpg"></a>
								<span class="fx-vuelos-color"></span>
							
								<div class="line-name">
								<span class="name-paq-prom">Tierra Santa, Petra y Egipto</span>
								</div>
								<span class="price-paq-pro">$000 USD</span>
								<a class="btn-detalles-viajes" href="">MAS DETALLES</a>
						</article>
					
					</div>
					
					<div class="box-par box-par2">
						<article class="articles_paquetes_f articles_paquetes_f2">
								<a class="a-img-vuelos" href="#"><img src="<?php echo base_url();?>images/paquetes/front-img-paquetes/chiapas-travel.jpg"></a>
								<span class="fx-vuelos-color"></span>
							
								<div class="line-name">
								<span class="name-paq-prom">Tierra Santa, Petra y Egipto</span>
								</div>
								<span class="price-paq-pro">$000 MXN + IVA</span>
								<a class="btn-detalles-viajes" href="">MAS DETALLES</a>
						</article>
						
					
						<article class="articles_paquetes_f ">
								<a class="a-img-vuelos" href="#"><img src="<?php echo base_url();?>images/paquetes/front-img-paquetes/canada-travel.jpg"></a>
								<span class="fx-vuelos-color"></span>
							
								<div class="line-name">
								<span class="name-paq-prom">Tierra Santa, Petra y Egipto</span>
								</div>
								<span class="price-paq-pro">$000 DLS</span>
								<a class="btn-detalles-viajes" href="">MAS DETALLES</a>
						</article>
						
					</div>
					
					
					
			
				</section>
				
				<div id="box-suscribir" >
				<h3 class="title-news">NEWSLETTER</h3>
				<span class="fx-tri-suscri"></span>
				<p class="p-news">Suscribete y recibe exclusivas promociones en nuestro boletin turistico .</p>
				
				<form id="form-news">
				<label>Correo electronico</label>
					<input class="" name="newsletter" type="text" id="newsletter" value=""  />
					<input type="submit" name="button"  id="button-news" value="ENVIAR" />
					
				</form>
				
				
				</div>
				
				<section id="cover-testimonios" class="cover_standar">
					<div class="cover_titles_half">
					<span class="title-big">Testimonios</span> <span class="fx-border"></span>
					
					</div>
					
					<div id="box-p-testim">
					<p>
					<span class="comillas-tes">"</span>
					Los expertos de Travel Pro nos diseñaron un paquete con el cual pudimos conocer Europa y sus principales ciudades así como museos, lugares históricos y atracciones a un costo muy accesible que se ajusto a nuestro presupuesto.
					
					<span class="comillas-tes">"</span>
					
					
					</p>
					</div>
					
				
					
				</section>
		
		</div>
	</div>
	<?php $this->load->view("footer"); ?>
	
<!-- bootstrop-->
  <script src="<?php echo base_url();?>js/fjs/bootstrap.js"></script>
     <script>
      !function ($) {
        $(function(){
          // carousel demo
          $('#myCarousel').carousel()
        })
      }(window.jQuery)
    </script>
 <!-- bootstrop-->
 
 



</body>

</html>

