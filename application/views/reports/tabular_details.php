<?php 
//OJB: Check if for excel export process
if($export_excel == 1){
	ob_start();
	$this->load->view("partial/header_excel");
}else{
	$this->load->view("partial/header");
} 
?>
<div id="page_title" style="margin-bottom:8px;"><?php echo $title ?></div>
<div id="page_subtitle"><?php echo $subtitle ?></div>
<div id="table_holder_report">
	<div  id="sortable_table_report">
		<div class="box_head_reports_table">
			<div class="file_report_head">
				<div class="casilla_head_report">+</div>
				<?php foreach ($headers['summary'] as $header) { ?>
				<div class="casilla_head_report"><?php echo $header; ?></div>
				<?php } ?>
			</div>
		</div>
		<div class="box_result_table_reports">
			<?php foreach ($summary_data as $key=>$row) { ?>
			<div class="file_report_result">
				<div class="casilla_result_report"><a href="#" class="expand">+</a></div>
				<?php foreach ($row as $cell) { ?>
				<div class="casilla_result_report"><?php echo $cell; ?></div>
				<?php } ?>
			</div>
		</div>
		
			
				<div class="box_head_reports_table">
						<div class="file_report_head">
							<?php foreach ($headers['details'] as $header) { ?>
							<div class="casilla_head_report"><?php echo $header; ?></div>
							<?php } ?>
						</div>
					</div>
				
					<div class="box_result_table_reports">
						<?php foreach ($details_data[$key] as $row2) { ?>
						
							<div class="file_report_result">
								<?php foreach ($row2 as $cell) { ?>
								<div class="casilla_result_report"><?php echo $cell; ?></div>
								<?php } ?>
							</div>
						<?php } ?>
					</div>
			
			<?php } ?>
		
		
	</div>
</div>





<div id="report_summary">
<?php foreach($overall_summary_data as $name=>$value) { ?>
	<div class="summary_row"><?php echo $this->lang->line('reports_'.$name). ': '.to_currency($value); ?></div>
<?php }?>
</div>
<?php 
if($export_excel == 1){
	$this->load->view("partial/footer_excel");
	$content = ob_end_flush();
	
	$filename = trim($filename);
	$filename = str_replace(array(' ', '/', '\\'), '', $title);
	$filename .= "_Export.xls";
	header('Content-type: application/ms-excel');
	header('Content-Disposition: attachment; filename='.$filename);
	echo $content;
	die();
	
}else{
	 
?>
<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	$(".tablesorter a.expand").click(function(event)
	{
		$(event.target).parent().parent().next().find('.innertable').toggle();
		
		if ($(event.target).text() == '+')
		{
			$(event.target).text('-');
		}
		else
		{
			$(event.target).text('+');
		}
		return false;
	});
	
});
</script>
<?php 
} // end if not is excel export 
?>