<footer id="footer-new">

	<div id="cover-art-foo" class="cover_standar">
	



	<article class="articles-foot">
	

	<h5 class="titles-foot"> AGENCIA</h5>
		<div id="logo_foot">
			<a href="#"><img src="<?php echo base_url();?>images/banner/logo-travel-bco2.png" alt=""></a>
		</div>
		<p class="p-foot">
		Somos una empresa dedicada a brindarte servicios profesionales en viajes a todo el mundo en los mejores hoteles, destinos paradisiacos y ciudades de ensueño.
 <br/> <br/>
Nuestro trabajo es darte la mejor experiencia en tu proximo viaje y que cumplas el sueño rebasando las expectativas que tenias en el, somos tus expertos en viajes!.
		</p>
	
	
	</article>
	<article class="articles-foot af2">
	

		<h5 class="titles-foot">CONTACTO</h5>
			<div id="cover-red-foot">
				<a href="https://www.facebook.com/TravelPro.mx" target="_blank"><img id="r-fa" src="<?php echo base_url();?>images/ico/face-ico.png" alt=""> </a>
				<a href="https://twitter.com/TRAVELPROIRAPUA?lang=es" target="_blank" ><img id="r-tw" src="<?php echo base_url();?>images/ico/tw-ico.png" alt=""></a>
				<a href="https://www.instagram.com/travelpro.mx/" target="_blank" ><img id="r-in" src="<?php echo base_url();?>images/ico/insta-ico.png" alt=""> </a>
				<a href="https://es.pinterest.com/t_pro/pins/" target="_blank" ><img id="r-pin" src="<?php echo base_url();?>images/ico/red-pin.png" alt=""> </a>
			</div>
			
			<div class="cover-ubi-foot">
				<img class="ico-ubica-foot" src="<?php echo base_url();?>images/ico/	ico-ubicacion-travel.png" alt="">
				
				<span class="ubi-txt">Plaza Delta Avenida Héroes de Nacozari 1655 Int. 14-A</span>
			</div>
			<div class="cover-ubi-foot">
				<img class="ico-ubica-foot" src="<?php echo base_url();?>images/ico/ico-phone-travel.png" alt="">
				<span class="ubi-txt">(+52) 462 114 2525</span>
			</div>
			<div class="cover-ubi-foot">
				<img class="ico-ubica-foot" src="<?php echo base_url();?>images/ico/ico-wat-travel.png" alt="">
				<span class="ubi-txt">(462) 133 49 30</span>
			</div>
			<div class="cover-ubi-foot">			
				<img class="ico-ubica-foot" src="<?php echo base_url();?>images/ico/ico-mail-travel.png" alt="">
				<span class="ubi-txt">contacto@travelpro.mx</span>
		
			
			
			</div>
			
		
	</article>
	<article  id="article-foot-map" class="articles-foot">
	

		<h5 class="titles-foot"> MAPA</h5>
		
		<div id="cover-mapa">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3732.7233589285383!2d-101.37845158550209!3d20.68083088618794!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842c7fbab8b14bef%3A0xd1831b842779744c!2sPlaza+Delta!5e0!3m2!1ses!2sco!4v1474917135785"  frameborder="0" style="border:0" allowfullscreen></iframe>
		
		</div>
		
			<div class="cover-ubi-foot">			
				
				<span class="iata-txt">MIEMBRO IATA DESDE 1989</span>
				<img  class="ico-iata-foot"src="<?php echo base_url();?>images/ico/ico-iata-foot.png" alt="">
		
			
			
			</div>
	</article>
	
	</div>
	
	<div id="cover-copyrigth">
	<span>Travelpro © 2016 All Rights Reserveded Brontobyte Diseño Web</span> <a id="a-politicas" href="<?php echo base_url();?>index.php/front/privacidad"> Politicas de Privacidad</a>
	</div>
	

<span class="ir-arriba icon-arrow-up2">^</span>

<script>
$(document).ready(function(){
 
	$('.ir-arriba').click(function(){
		$('body, html').animate({
			scrollTop: '0px'
		}, 300);
	});
 
	$(window).scroll(function(){
		if( $(this).scrollTop() > 0 ){
			$('.ir-arriba').slideDown(300);
		} else {
			$('.ir-arriba').slideUp(300);
		}
	});
 
});
</script>
</footer>