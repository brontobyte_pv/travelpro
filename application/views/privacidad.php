<!DOCTYPE html>
<html lang="es">
<head> 
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Author" content="Adrián Ramirez | brontobytemx.com" />
<meta name="Subject" content="Diseño web Mexico" />
<meta name="GOOGLEBOT" content="INDEX, FOLLOW, ALL" />
<meta name="robots" content="index, follow" />
<meta name="GOOGLEBOT" content="NOARCHIVE" />
<meta name="Generator" content="html" />
<meta name="Language" content="Spanish" />
<meta name="Revisit" content="1 day" />
<meta name="Distribution" content="Global" />
<meta name="Robots" content="All" />
	<meta property="og:url"           content="" />
    <meta property="og:type"          content="" />
    <meta property="og:title"         content="" />
    <meta property="og:description"   content="" />
    <meta property="og:image"         content="" />

<title>TravelPro | Home</title>
<meta name="Description" content=""/>
<meta name="Keywords" content="" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/style-header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/style-all.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/dispositivos.css"/>



<!--web-font-->
<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,600,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Cabin:400,600,500' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=BenchNine" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Crete+Round" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oleo+Script" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Suez+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">




<!--//web-font-->

<script src="http://code.jquery.com/jquery.js"></script>
 
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="<?php echo base_url();?>images/ico/favicon.ico">

 

</head>
<body>
<div class="wrapper w-sections" id="w-priva">

	<!--navigation-->
	<?php $this->load->view("header");?>
	<!--navigation-->
	
		<div id="sb-site" class="box-contenido">
			<!--MAIN MOVIL-->
			<?php $this->load->view("menu-movil");?>
			<!--MAIN MOVIL-->
				<section  class="cover_standar">
					<div class="cover_titles_half">
					<span class="title-big title-big-color">Privacidad</span> <span class="fx-border"></span>
					</div>
					
					<p class="privacidad-txt">
					Travel Pro valora a sus clientes y está comprometida a salvaguardar su confidencialidad. En el desempeño de dicho compromiso, Travel Pro ha desarrollado esta "Declaración de Confidencialidad", que describe las políticas y prácticas de Travel Pro en lo que se refiere a la recolección y divulgación de información personal en su página web.
					 <br/> <br/>
					Responsable de la protección de sus Datos Personales: Travel Pro México con domicilio en Avenida Héroes de Nacozari 1655 Int. 14-A Irapuato, Gto. son responsables, respectivamente, del tratamiento de sus datos personales. Forma y medios de contacto: Puede contactarnos, redactando un documento en idioma español, dirigido al Area de atención a clientes y hacerlo llegar por correo electrónico a la siguiente dirección electrónica: contacto@travelpro.mx; vía correo postal a la siguiente dirección: Avenida Héroes de Nacozari 1655 Int. 14-A Irapuato, Gto. Cp: 36556 o al teléfono: 462114 25 25
					 <br/> <br/> 
					Fines de los Datos Personales recabados: Sus datos personales serán utilizados para fines de seguimiento, actualización y confirmación en cuanto a productos y servicios contratados; con fines promocionales y de contratación; con fines financieros y crediticios; dar cumplimiento a obligaciones contraídas con nuestros clientes; evaluar la calidad del servicio; realizar estudios sobre hábitos de consumo y preferencias. Datos recabados y medios de obtención de Datos Personales: Los datos personales que recabamos de usted, con los fines descritos en el presente aviso de privacidad, son recabados de manera personal, cuando usted nos los proporciona directamente; por vía de nuestro Portal en Internet (www.travelpro.mx) cuando ingresa sus datos o utiliza nuestros servicios en línea, cuando los proporciona de manera telefónica en nuestro centro de atención y cuando obtenemos información a través de otras fuentes que están permitidas por la Ley. Los datos personales que recabamos de forma directa cuando usted mismo nos los proporciona por diversos medios, como cuando los proporciona para participar en promociones, ofertas o cuando contrata con nosotros algún producto o servicio. Los datos que recabamos de manera directa y vía internet, son los siguientes: Nombre y apellidos, Género (masculino o femenino), fecha de nacimiento, teléfono, correo electrónico y dirección física.
					  <br/> <br/>
					Toda vez que la Ley permite otras fuentes de allegarnos de información como lo son directorios telefónicos, de servicios y laborales, los datos que por dichos medios podemos obtener son nombre y apellidos, teléfono, correo electrónico y dirección física. En cuanto a los datos financieros como lo son su número de tarjeta de crédito, fecha de expiración de la misma, código de seguridad y dirección de correspondencia de la misma de conformidad con los artículos 8°, 10° y 37° de la Ley no son considerados datos que requieran de su consentimiento expreso para ser utilizados. Limitaciones al uso de datos: Usted puede cancelar su suscripción para recibir promociones, ofertas y servicios de manera telefónica, por correo electrónico y vía correo postal haciendo llegar una carta escrita en idioma español donde indique su solicitud y los datos necesarios como lo son Nombre completo (nombre o nombres y apellido o apellidos) copia simple de su identificación oficial o en medios electrónicos versión digitalizada de la misma (escaneo), número de socio o de cuenta (en caso de tener uno), teléfono y dirección física o electrónica a la dirección para fines de notificaciones relacionadas al caso a la siguiente dirección: en Avenida Héroes de Nacozari 1655 Int. 14-A Irapuato, Gto.; o al correo electrónico contacto@travelpro.mx;
					  <br/> <br/>
					Ejercicio de los Derechos ARCO: Usted tiene derecho de acceder a los datos personales que poseemos, a los detalles de tratamiento de los mismos, a la rectificación en el caso de ser estos inexactos o incompletos, a cancelarlos cuando sea su consideración que no son necesarios para alguna de las finalidades contenidas en el presente Aviso de Privacidad, que son utilizados para finalidades no consentidas y finalmente a oponerse al tratamiento de dichos datos para fines específicos y que deberá de manera clara expresar.
					  <br/> <br/>
					Mecanismos para el ejercicio de los Derechos ARCO: Los Derechos antes descritos se ejercen a través de la presentación de la solicitud respectiva que por escrito y en idioma español debe presentar de manera gratuita en en Avenida Héroes de Nacozari 1655 Int. 14-A Irapuato, Gto. o para mayor información comunicarse al teléfono: 10840470, con atención al Área de atención a clientes; o bien hacerla llegar por correo postal, previo pago del porte correspondiente, a la antes mencionada dirección y con atención a la misma persona. Para mayor información favor de comunicarse a Área de atención a clientes al teléfono: 462114 25 25.
					
					
					</p>
	
				
					
				</section>
		
		</div>
</div>
	<?php $this->load->view("footer");?>
	
	<!--footer-->
	<!-- bootstrop-->
  <script src="<?php echo base_url();?>js/fjs/bootstrap.js"></script>
     <script>
      !function ($) {
        $(function(){
          // carousel demo
          $('#myCarousel').carousel()
        })
      }(window.jQuery)
    </script>
 <!-- bootstrop-->
	
</body>
</html>