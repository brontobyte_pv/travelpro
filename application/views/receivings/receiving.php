<?php $this->load->view("partial/header"); ?>


<div id="page_title2" style="margin-bottom:8px;"><?php echo $this->lang->line('recvs_register'); ?></div>

<?php
if(isset($error))
{
	echo "<div class='error_message'>".$error."</div>";
}
?>



<div id="register_wrapper">
<div id="mode_form_div" class="oculta">
	<?php echo form_open("receivings/change_mode",array('id'=>'mode_form')); ?>
		<span id='mode_form_span'><?php echo $this->lang->line('recvs_mode') ?></span>
	<?php echo form_dropdown('mode',$modes,$mode,'onchange="$(\'#mode_form\').submit();"'); ?>
	</form>
	<?php echo form_open("receivings/clear_receiving",array('id'=>'cancel_sale_form')); ?>
	<div id="cover_btn_new_ctas_cre">
			    <div id='btn_new_ctas_cre'  class='btn_cancel'>
					<span>Cancel </span>
				</div></div>
				<div id="cover_btn_new_ctas_cre">
				<?php echo anchor("items/view/-1/width:360",
		"<div id='btn_new_art_entradas' class='btn_ok'><span>".$this->lang->line('sales_new_item')."</span></div>",
		array('class'=>'thickbox none','title'=>$this->lang->line('sales_new_item')));
		?>
				</div>
				
        </form>
	</div>
	<?php echo form_open("receivings/add",array('id'=>'add_item_form')); ?>
	<label id="item_label" for="item">

	<?php
	if($mode=='receive')
	{
		echo $this->lang->line('sales_find_or_scan_item');
	}
	else
	{
		echo $this->lang->line('recvs_find_or_scan_item_or_receipt');
	}
	?>
	<div id="mas"></div>
	</label>
<?php echo form_input(array('name'=>'item','id'=>'item','size'=>'40'));?>


</form>

<!-- Receiving Items List -->

<section id="cover_register">




<?php
if(count($cart)==0)
{
?>
<div class='warning_message' id='warning_message_reg'><?php echo $this->lang->line('sales_no_items_in_cart'); ?></div>
<?php
}
else
{?> 
	<div id="filas-head">

<div class="head-filas-c"><?php echo $this->lang->line('recvs_quantity'); ?></div>
<div class="head-filas" id="item-name"><?php echo $this->lang->line('recvs_item_name'); ?></div>
<div class="head-filas"><?php echo $this->lang->line('recvs_cost'); ?></div>
<div class="head-filas"><?php echo $this->lang->line('recvs_total'); ?></div>
<div class="head-filas-o"><?php echo $this->lang->line('common_delete'); ?></div>
</div>
<?php 
	foreach(array_reverse($cart, true) as $line=>$item)
	{ ?>
		<div id="filas-contenido">
	<?php
		$cur_item_info = $this->Item->get_info($item['item_id']);
		echo form_open("receivings/edit_item/$line");
	?>
	<div id="filas-head-r"><?php echo $this->lang->line('items_quantity'); ?></div>
		<div class="filas-items-reg-c">
		<?php
        	echo form_input(array('name'=>'quantity','value'=>$item['quantity'],'size'=>'2'));
		?>
		</div>
	
	
		
	
		
		
		<div id="filas-head-r"><?php echo $this->lang->line('sales_item_name'); ?></div>
		<div  id="name-reg_r" class="filas-items-reg"> <span><b><?php echo $item['name']; ?></b><br />[  <?php echo $cur_item_info->quantity; ?> existencias ]</span></div>

		<?php
			//echo $item['description'];
      		//echo form_hidden('description',$item['description']);
		?>
		


		<?php if ($items_module_allowed)
		{
		?>
		
		
		<div id="filas-head-r"><?php echo $this->lang->line('sales_price'); ?></div>
		<div class="filas-items-reg"><?php echo form_input(array('name'=>'price','value'=>$item['price']));?></div>
		<?php
		}
		else
		{
		?>
		
			<div class="filas-items-reg"><?php echo to_currency($item['price']); ?></div>
			<?php echo form_hidden('price',$item['price']); ?>
		<?php
		}
		?>
	
		<div class="filas-items-reg"><?php echo to_currency($item['price']*$item['quantity']-$item['price']*$item['quantity']/100); ?></div>
		<div class="soloVisibleResponsivamente2">X</div>
		
		<div class="filas-items-reg-o"><?php echo anchor("receivings/delete_item/$line",'['.$this->lang->line('common_delete').']');?></div>
		
		
		
		<?php echo form_submit(array('id'=>'edit_item'),'');?>
		</form>
	<?php
	}
}
?>
</div>
</section>
</div>

<!-- Overall Receiving -->

<div id="overall_sale">
		<div class="cover_overall_process">
		<?php
		if(isset($supplier))
		{
			echo $this->lang->line("recvs_supplier").': <b>'.$supplier. '</b><br />';
			echo anchor("receivings/delete_supplier",'['.$this->lang->line('common_delete').' '.$this->lang->line('suppliers_supplier').']');
		}
		else
		{
			echo form_open("receivings/select_supplier",array('id'=>'select_supplier_form')); ?>
			<label class="overall_label" for="supplier"><?php echo $this->lang->line('recvs_select_supplier'); ?></label>
			<?php echo form_input(array('name'=>'supplier','id'=>'input_empieza','size'=>'30','value'=>$this->lang->line('recvs_start_typing_supplier_name')));?>
			</form>

		</div>
		
		<div class="cover_overall_process" id="box-or-btn">
		<h3 id="or"><?php echo $this->lang->line('common_or'); ?></h3>
		<?php echo anchor("suppliers/view/-1/width:350",
		"<div class='btn_ok' id='btn_new_proveedor_entradas'><span>".$this->lang->line('recvs_new_supplier')."</span></div>",
		array('class'=>'thickbox none','title'=>$this->lang->line('recvs_new_supplier')));
		?>
		</div>
		<?php
	}
	?>

	<div class="cover_overall_process" id='sale_details'>
		<div class="boxs-cover-sub-and-total"  id="boxs-cover-total" >
			<div class="price-total"><?php echo $this->lang->line('sales_total'); ?>:</div>
			<div class="price-total"  id="total_total" ><?php echo to_currency($total); ?></div>
		</div>
	</div>
	<?php
	if(count($cart) > 0)
	{
	?>
	<div id="finish_sale">
		<?php echo form_open("receivings/complete",array('id'=>'finish_sale_form')); ?>
		
		<label id="comment_label" for="comment"><?php echo $this->lang->line('common_comments'); ?>:</label>
		<?php echo form_textarea(array('name'=>'comment','value'=>'','rows'=>'4','cols'=>'23'));?>
	
		
		<div id="box-pay">
		<span>
		<?php
			echo $this->lang->line('sales_payment').':   ';?>
		</span>
		<span>
		<?php
		    echo form_dropdown('payment_type',$payment_options);?>
        
        </span>

        <span>
        
        <?php
			echo $this->lang->line('sales_amount_tendered').':   ';?>
		</span>
		<span>
		<?php
		    echo form_input(array('name'=>'amount_tendered','value'=>'','size'=>'10'));
		?>
      
        </span>

        </div>
		<?php echo "<div class='btn_ok' id='finish_sale_btn'><span>".$this->lang->line('recvs_complete_receiving')."</span></div>";
		?>
		
		
		</form>

	    
		</div>

	</div>
	
	<?php
	}
	?>

</div>
</div>

<div id="box-footer">
<?php $this->load->view("partial/footer"); ?>

</div>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	$(".oculta").css("display","none");
	$("#comment").css("display","none");
	$('#mas').click(function()
    {
    	var disp =$(".oculta").css("display");
		//Comparamos si disp esta oculto lo hacemos visible
		if (disp == 'none')
		{
			//Cambia la propiedad css del elemento html con el id encerrado en los primeros parentesis
			$(".oculta").css("display","block");
			$("#mas").css("background-image","url('images/menubar/menos.png')");
			
			
		}//De lo contrario lo ocultamos
		else
		{
			//Cambia la propiedad css del elemento html con el id encerrado en los primeros parentesis
			$(".oculta").css("display","none");
			$("#mas").css("background-image","url('images/menubar/mas.png')");
		}
    });
	
    $("#item").autocomplete('<?php echo site_url("receivings/item_search"); ?>',
    {
    	minChars:0,
    	max:100,
       	delay:10,
       	selectFirst: false,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#item").result(function(event, data, formatted)
    {
		$("#add_item_form").submit();
    });

	$('#item').focus();

	$('#item').blur(function()
    {
    	$(this).attr('value',"<?php echo $this->lang->line('sales_start_typing_item_name'); ?>");
    });

	$('#item,#supplier').click(function()
    {
    	$(this).attr('value','');
    });

    $("#supplier").autocomplete('<?php echo site_url("receivings/supplier_search"); ?>',
    {
    	minChars:0,
    	delay:10,
    	max:100,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#supplier").result(function(event, data, formatted)
    {
		$("#select_supplier_form").submit();
    });

    $('#supplier').blur(function()
    {
    	$(this).attr('value',"<?php echo $this->lang->line('recvs_start_typing_supplier_name'); ?>");
    });

    $("#finish_sale_btn").click(function()
    {
    	if (confirm('<?php echo $this->lang->line("recvs_confirm_finish_receiving"); ?>'))
    	{
    		$('#finish_sale_form').submit();
    	}
    });

    $("#cancel_sale_btn").click(function()
    {
    	if (confirm('<?php echo $this->lang->line("recvs_confirm_cancel_receiving"); ?>'))
    	{
    		$('#cancel_sale_form').submit();
    	}
    });


});

function post_item_form_submit(response)
{
	if(response.success)
	{
		$("#item").attr("value",response.item_id);
		$("#add_item_form").submit();
	}
}

function post_person_form_submit(response)
{
	if(response.success)
	{
		$("#supplier").attr("value",response.person_id);
		$("#select_supplier_form").submit();
	}
}

</script>