<!DOCTYPE html>
<html lang="es">

<head>
<meta charset="UTF-8">
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="index, follow">
<meta property="og:url"           content="http://brontobytemx.com/index.php/catalogo/detalles/<?php echo $id;?>" />
    <meta property="og:type"          content="<?php echo $categoria; ?>" />
    <meta property="og:title"         content="<?php echo $item_nom;?>" />
    <meta property="og:description"   content="<?php echo $descripcion;?> Marca:<?php echo $marca;?> Modelo:<?php echo $modelo;?> Precio:$<?php echo $precio;?> " />
    <meta property="og:image"         content="<?php echo base_url();?>images/items/<?php echo $id;?>/1.png" />
</head>
<meta name="author" content="Brontobyte Computaci&oacute;n">

<title><?php echo $item_nom;?></title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style-header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style-contenido.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style-lista-categoria.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style-detalle-item.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/dispositivos.css" />
 <link rel="shortcut icon" href="images/ico/ico-logo.ico">
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="<?php echo base_url();?>images/ico/ico-logo.ico">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</head>

<body>
<div class="wrapper" id="wrap-lista" itemscope itemtype="http://schema.org/LocalBusiness" >
	<?php $this->load->view("header"); ?>
	
	
	<div id="sb-site" class="box-contenido">

<div class="head_movil">
	<div id="ico-menu-movil">
		<span class="sb-toggle-left">
			<img src="<?php echo base_url();?>images/ico/main-movil.png"/>
		</span>
			
	</div>
<a href="#"> 

	<img class="logo-movil" src="<?php echo base_url();?>images/img-header/Logo-head-bontobytes.png" alt="logo-computacion"/>
	
</a>	

	
</div>
	
		<div id="box-subtitulo-secciones">
			<h3 class="h3-subtitulo-secciones">
				
				<a class="head-a-ubi" href="<?php echo base_url();?>">Pagina de inicio / </a>
				
				<a class="head-ubica"  itemprop="keywords"  href="<?php echo base_url();?>index.php/catalogo">Tienda / </a>
			
				
				<a   class="head-ubica" itemprop="keywords" href="<?php echo base_url();?>index.php/catalogo/items_data/<?php echo $categoria; ?>"><?php echo $categoria; ?> / 
				</a>

				<span class="head-ubica2" itemprop="keywords"  itemprop="keywords" ><?php echo $item_nom;?></span>
				
				</h3>
	</div>
		<?php
			$contador=0;
	foreach(array_reverse($cart, true) as $line=>$item)
	{ 
	$contador++;
	
	
	
	}
?>
		
			

	
	<section itemscope itemtype="http://schema.org/Store" class="contenido-secciones" id="section-detalle-item" >
		
		
		<div id="box-img-item">
		    <img src="<?php echo base_url();?>images/items/<?php echo $id;?>/1.png"/>
			
		</div>
		
		 
		<div class="box-descrip">
				<div id="box-titulo-item">
				<h2 class="titulo-item"><?php echo $item_nom;?></h2>
				</div>
				
				<div class="box-subtitulos-detalle">
						<div class="box-izq">
							<span id="precio-detalle">Precio:</span>
							<span id="precio-num">$<?php echo $precio;?></span>
							
						</div>
						<div class="box-izq">
						
							<a class="comprar" href="<?php echo base_url();?>index.php/cart/add/<?php echo $id;?>">Comprar</a>
					
						</div>
				</div>
				<div class="box-subtitulos-detalle">
					
					<span id="color">Marca:<span class="txt-modco"><?php echo $marca;?></span></span>
					
					
					<span id="modelo">Modelo:<span class="txt-modco"><?php echo $modelo;?></span></span>
			
					
				</div>
				
				
				
				
				
				<div class="base-decripcion">	
				<h3>Descripción</h3>
				<p>
				<?php echo $descripcion;?>
				</p>	
			
				</div>
				<div class="base-decripcion">	
					<h3>Informaci&oacute;n adicional</h3>
				<ul id="lista-caracte">
				
				
				<li>Disponible</li>
				<li><a id="ficha" href="<?php echo base_url();?>fichas/<?php echo $id;?>/ficha_tecnica.pdf">Ficha Técnica</a></li>
				<li>Para comprar</li>
				<li>Políticas de compra</li>
				<li>Envios</li>
				<li>&iquest;Alguna duda especial?</li>
				</ul>	
			
			</div>
			<div class="base-decripcion">
			<br>
			<div class="fb-share-button" data-href="http://brontobytemx.com/index.php/catalogo/detalles/<?php echo $id;?>" data-layout="button"></div><br><br></div>
				<a href="<?php echo base_url();?>index.php/catalogo/items_data/<?php echo $categoria; ?>" id="ver-mas">Ver más de esta categoría</a>
				</div>

	
	</section>
	</div>
	</div>
<?php include('footer.php'); ?>


</body>