<?php $this->load->view("partial/header"); ?>
<script type="text/javascript">
$(document).ready(function() 
{ 
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo $this->lang->line("common_confirm_search")?>');
    enable_delete('<?php echo $this->lang->line($controller_name."_confirm_delete")?>','<?php echo $this->lang->line($controller_name."_none_selected")?>');
    enable_bulk_edit('<?php echo $this->lang->line($controller_name."_none_selected")?>');
}); 


function post_recurso_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',false);
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.item_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.item_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				hightlight_row(response.item_id);
				set_feedback(response.message,'success_message',false);
			});
		}
	}
}
</script>

<div id="title_bar">
	<div id="title" ><?php echo $this->lang->line('common_list_of').' '.$this->lang->line('module_'.$controller_name); ?></div>
	<div id="cover_buttons_top_manage">
		<?php echo anchor("$controller_name/view/-1/width:$form_width",
		"<div class='buttons_top_manage'><img src='".base_url()."images/ico/ico-recursos.png'/><span>".$this->lang->line($controller_name.'_new')."</span></div>",
		array('class'=>'thickbox none','title'=>$this->lang->line($controller_name.'_new')));
		?>
	</div>
</div>

<div id="table_action_header">
	<ul>
		
		<li  id="box-buscar" >
		<img src="images/ico/icon-lupa.png"/>
		<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
		<input type="text" name ='search' id='search'/>
		</form>
		</li>
		
		<li id="btn-borrar-buscar" ><span><?php echo anchor("$controller_name/delete",$this->lang->line("common_delete"),array('id'=>'delete')); ?></span></li>
		<li  id="btn-mail-buscar" ><span><a href="#" id="email"><?php echo $this->lang->line("common_email");?></a></span></li>
		
	</ul>
</div>

<div id="table_holder">
<?php echo $manage_table; ?>
</div>
<div class="box-pagination">
<?php echo $this->pagination->create_links();?>
</div>
<div id="feedback_bar"></div>
</div>
</div>

<?php $this->load->view("partial/footer"); ?>



