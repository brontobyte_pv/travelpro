
<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open('recursos/save/'.$info->recurso_id,array('id'=>'form_recursos'));
?>
<fieldset id="item_basic_info">
<legend><?php echo $this->lang->line(""); ?></legend>

<div class="field_row clearfix">
 <?php echo form_label($this->lang->line('recursos_nombre'), 'nombre', array('class'=>'required')); ?>
  <div class='form_field'>
	<?php echo form_input(array(
		'name'=>'nombre_cuenta',
		'id'=>'nombre_cuenta',
		'value'=>$info->nombre)
	);?>
	</div>
</div>
<div class="field_row clearfix">
 <?php echo form_label($this->lang->line('recursos_cantidad'), 'cantidad', array('class'=>'required')); ?>
  <div class='form_field'>
	<?php echo form_input(array(
		'name'=>'cantidad',
		'id'=>'cantidad',
		'value'=>$info->recurso)
	);?>
	</div>
</div>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_right')
);
?>

</fieldset>
<?php
echo form_close();
?>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{
	$('#form_recursos').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_recurso_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			nombre_cuenta:"required"
			
		},
		messages:
		{
			nombre_cuenta:"<?php echo $this->lang->line('items_name_required'); ?>"
			
		}
	});
});


</script>
