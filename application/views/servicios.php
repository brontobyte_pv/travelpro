<!DOCTYPE html>
<html lang="es">

<head>
<meta charset="UTF-8">
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="noindex, follow" />
<meta name="description" content="Venta de hardware y Reparación de equipo computacional, Configuraci&oacute;n de Redes, C&aacute;maras de Seguridas, Reparaci&oacute;n de Computadoras en Puerto Vallarta">
<meta name="keywords" content="Venta de hardware y Reparaci&oacute;n Computadoras en Puerto Vallarta, Configuración de Redes y C&aacute;maras de Seguridas, Refacciones de Computadoras, cargadores para laptop accesorios para celulares , venta de computadoras, Instalaci&oacute;n de programas, Intalaci&oacute;n de Aplicaciones">
<meta name="author" content="Brontobyte Computaci&oacute;n">

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>Brontobyte Computación, Reparacion de computadoras en Puerto vallarta</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style-header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style-contenido.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/dispositivos.css" />

<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,200,500,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300' rel='stylesheet' type='text/css'>

<script src="http://code.jquery.com/jquery.js"></script>

<link rel="shortcut icon" href="<?php echo base_url();?>images/ico/ico-logo.ico">

</head>


<body>

<div id="wrap-serv" class="wrapper"  itemscope itemtype="http://schema.org/LocalBusiness" >
	<?php $this->load->view("header"); ?>
<div id="sb-site" class="box-contenido">

<div class="head_movil">
	<div id="ico-menu-movil">
		<span class="sb-toggle-left">
			<img src="<?php echo base_url();?>images/ico/main-movil.png"/>
		</span>
			
	</div>
<a href="#"> 

	<img class="logo-movil" src="<?php echo base_url();?>images/img-header/Logo-head-bontobytes.png" alt="logo-computacion"/>
	
</a>	

	
</div>
			
		<section itemscope itemtype="http://schema.org/Services"class="contenido-secciones" id="seccion-serv">
						
			<div id="box-subtitulo-secciones">
				<h3 class="h3-subtitulo-secciones">
				
				<a class="head-a-ubi" href="<?php echo base_url();?>">Pagina de inicio / </a>
				
				<span class="head-ubica2"  itemprop="keywords" >
				Servicios
				</span>
				
				
				
				</h3>
			</div>
				<div class="box-nombre-seccion">
					<span class="subtitulos-seccion">Servicios</span> 
					<p class="p-intro">
					Brontobyte Software & Hardware te ofrece servicios desde Soporte Técnico para Pymes , hasta desarrollo y administración de sitios web, en la ciudad de Puerto Vallarta y Bahia de Banderas. Comunicate con nosotros y te cotizamos sin compromiso.
					
					</p>
				</div>
						
						<div class="cover-services">
									<article class="box-servicios">
										<div class="icono-services-services" id="ico-ser-one">
											<img  class="ico-servicios" itemprop="image"  src="<?php echo base_url();?>images/ico/servicios-pc2.png"/>
										</div>
										<h2 class="h2-articulos-servicios" itemprop="serviceType">
											Soporte T&eacute;cnico a <span id="df-color-1">PYMES</span>
										</h2>
											
											
												<ul itemprop="description"  itemscope itemtype="http://schema.org/Services" class="box-texto-servicios" id="box-txt-reparacion">
											
													<li>Polizas de servicio mensual seg&uacute;n la necesida de su negocio</li>
													<li>Asesor&iacute;a en proyectos de implementaci&oacute;n de Tecnolog&iacute;a Informatica</li>
											
												</ul>
													
									</article>

									<article class="box-servicios">
											<div class="icono-services-services" id="ico-ser-two">
											  <img  class="ico-servicios"  itemprop="image"src="<?php echo base_url();?>images/ico/servicios-cam2.png"/>
											</div>
											<h2 class="h2-articulos-servicios" itemprop="serviceType"> 
												Seguridad & <span id="df-color-2">Redes</span>
											</h2>
									
												<ul itemprop="description" itemscope itemtype="http://schema.org/Services"  class="box-texto-servicios" id="box-txt-seguri">
												
													<li>Configuraci&oacute;n e instalación de redes. </li>
													<li>Configuraci&oacute;n de conmutadores.</li>
													<li>Instalaci&oacute;n y configuraci&oacute;n de C&aacute;maras de Vigilancia</li>
												
												</ul>
											
									
									</article>


									<article class="box-servicios">
									<div class="icono-services-services" id="ico-ser-three">
									<img  class="ico-servicios" itemprop="image" src="<?php echo base_url();?>images/ico/servicios-actu2.png"/>
									</div> 
										<h2 class="h2-articulos-servicios" itemprop="serviceType">
										Instalaci&oacute;n o Actualizaci&oacute;n <span id="df-color-3"> Apps</span></h2>
											
											<ul itemprop="description"   itemscope itemtype="http://schema.org/Services"  class="box-texto-servicios" id="box-txt-instala">
										
													<li>Instalaci&oacute;n de nuevas aplicaciones/programas (versiones recientes)</li>
													<li>Configuraci&oacute;n de aplicaciones/programas</li>
												</ul>
												

									</article>

									<article  class="box-servicios">
										<div class="icono-services-services" id="ico-ser-four" >
										  <a href="<?php echo base_url();?>index.php/front/paquetes">
										  <img  class="ico-servicios"  itemprop="image"src="<?php echo base_url();?>images/ico/servicios-mov2.png"/>
										  </a>
										</div>
											
											<h2 class="h2-articulos-servicios" itemprop="serviceType"><a href="<?php echo base_url();?>index.php/front/paquetes" >
											Desarrollo de Apps y  <span id="df-color-4">Servicios Web</span></a>
											</h2>
												<ul  itemprop="description"   itemscope itemtype="http://schema.org/Services" class="box-texto-servicios" id="box-txt-web">
														
														<li>Desarrollo de Sitios web.</li>
														<li>Posicionamiento en buscadores.</li>
														<li>Diseño web para dispositivos moviles</li>
														<li>Administraci&oacute;n de sitios.</li>
														<li><a href="" id="txt-conoce-cash">CONOCE NUESTRA APP PARA LLEVAR EL CONTROL DE VENTAS DE TU TIENDA</a></li>
													</ul>
												
									
									</article>


								</div>

							

							</section>
		</div>
	</div>
<?php include('footer.php'); ?>


</body>