
<div id="cover-header">

<div id="top-top">
	<div id="logo">
	<a href="#"><img src="<?php echo base_url();?>images/front/logo1.jpg" alt=""></a>
	</div>
	
	<div id="cover-phones">
		
		<div class="phones">
			<img src="<?php echo base_url();?>images/ico/phonetravel.png"/>
		<span>(462) 114 25 25</span>
		</div>
		<div class="phones">
		
		<img src="<?php echo base_url();?>images/ico/wats.png"/>
		<span>(462) 133 49 30</span>
		
		</div>
	</div>
</div>


<div class="top-nav">
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-left">
						<li><a href="<?php echo base_url();?>" class="<?php if(isset($active1))echo $active1;?>">INICIO</a></li>
						
						<li><a href="<?php echo base_url();?>index.php/front/paquetes_temp" class="<?php if(isset($active2))echo $active2;?>">PAQUETES DE TEMPORADA</a></li>
						
						<li><a href="<?php echo base_url();?>index.php/front/reservaciones" class="<?php if(isset($active3))echo$active3;?>" >RESERVACIONES</a></li>
						
						
						<li><a href="<?php echo base_url();?>index.php/front/promociones" class="<?php if(isset($active4))echo$active4;?>">PROMOCIONES</a></li>
						
						<li><a href="<?php echo base_url();?>index.php/front/privacidad"class="<?php if(isset($active5))echo$active5;?>"> POLITICAS DE PRIVACIDAD</a></li>
						
						<li><a href="<?php echo base_url();?>index.php/front/contact"class="<?php if(isset($active6))echo$active6;?>"> CONTACTO</a></li>
						
						<li><a href="<?php echo base_url();?>index.php/front/corporativos"class="<?php if(isset($active7))echo$active7;?>"> 
                        CORPORATIVOS</a></li>
						
						<li><a href="<?php echo base_url();?>index.php/front/tips"class="<?php if(isset($active8))echo$active8;?>"> TIPS</a></li>
					</ul>	
					<div class="social-icons">
						<ul>
							<li><a href="#"></a></li>
							<li><a href="#" class="fb"></a></li>
							<li><a href="#" class="gg"></a></li>
						</ul>	
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>	
		</nav>		
	</div>	
	
		<!--header-->
	<div class="cover-look">
		<div class="container">
		
			<form class="navbar-form navbar-right" role="search">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Search">
				</div>
				<button type="submit" class="btn btn-default" aria-label="Left Align">
					<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
				</button>
			</form>	
		</div>	
	</div>
</div>
	<!--//header-->