<div id="required_fields_message"><?php echo $this->lang->line('common_det_cus'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open('gastos/save/',array('id'=>'form_gastos'));
?>
<fieldset id="pedidos_basic_info">

<div class="field_row clearfix"><?php echo form_label('Nombre: '.$cliente, 'fecha', array('class'=>'items_category')); ?></div>
<div class="field_row clearfix"><?php echo form_label('Dirección: '.$dir, 'fecha', array('class'=>'items_category')); ?></div>
<div class="field_row clearfix"><?php echo form_label('Ciudad: '.$city, 'fecha', array('class'=>'items_category')); ?></div>
<div class="field_row clearfix"><?php echo form_label('CP: '.$zip, 'fecha', array('class'=>'items_category')); ?></div>
<div class="field_row clearfix"><?php echo form_label('País: '.$country, 'fecha', array('class'=>'items_category')); ?></div>
<div class="field_row clearfix"><?php echo form_label('Email: '.$email, 'fecha', array('class'=>'items_category')); ?></div>
<div class="field_row clearfix"><?php echo form_label('Teléfono: '.$tel, 'fecha', array('class'=>'items_category')); ?></div>
<div class="field_row clearfix"><?php echo form_label('Forma de pago: '.$payment, 'fecha', array('class'=>'items_category')); ?></div>
</fieldset>
<?php
echo form_close();
?>

