<?php $this->load->view("partial/header"); ?>
<script type="text/javascript">
$(document).ready(function()
{
	$("input:checkbox").change(
            function()
            {
                if( $(this).is(":checked") ||$(this).is(":unchecked") )
                {
                    $("#hechos_form").submit();
                }
				
            }
        )
    init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest_f")?>','<?php echo $this->lang->line("common_confirm_search")?>');
    enable_delete('<?php echo $this->lang->line($controller_name."_confirm_delete")?>','<?php echo $this->lang->line($controller_name."_none_selected")?>');

});

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter section').length >1)
	{
		$("#sortable_table").tablesorter(
		{
			sortList: [[1,0]],
			headers:
			{
				0: { sorter: false},
				3: { sorter: false}
			}
		});
	}
}

function post_pedido_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.giftcard_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.giftcard_id,'<?php echo site_url("$controller_name/get_row_f")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				hightlight_row(response.giftcard_id);
				set_feedback(response.message,'success_message',false);
			});
		}
	}
}
</script>

<div id="title_bar">
	<div id="title" ><?php echo $this->lang->line('common_list_of').' '.$this->lang->line('module_'.$controller_name); ?></div>
	
</div>

<div id="table_action_header">
	<ul>
		
		<li  id="box-buscar">
		
		<?php echo form_open("$controller_name/search_f",array('id'=>'search_form')); ?>
		<div id="datetimepicker2" class="input-append date">
	<?php echo form_input(array(
		'name'=>'search',
		'id'=>'search',
		'data-format'=>'yyyy/MM/dd',
		'value'=>''));?>
	<span class="add-on" id="icono-calendar-pedidos">
      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	  <img src="images/ico/ico-appo2.png"/>
      </i>
	   
    </span>
	</div>
		</form>
		</li>
	
		
		<li><span>Ver pedidos hechos</span><?php
		echo form_open_multipart('pedidos/hechos/',array('id'=>'hechos_form'));
		$data = array(
		'name'        => 'hechos',
		'id'          => 'hechos',
		'value'       => '1',
		'checked'     => $checked,
		'style'       => 'margin:10px',
		);

		echo form_checkbox($data);
		echo form_close();

?></li>
		
	</ul>
</div>

<div id="table_holder">
<?php echo $manage_table; ?>
</div>
<div class="box-pagination">
<?php echo $this->pagination->create_links();?>
</div>
<div id="feedback_bar"></div>
</div>
</div>


<?php $this->load->view("partial/footer"); ?>
<script type="text/javascript" language="javascript">
$(document).ready(function()
{
/*
$(".complet").click(function()
	{
		var pedido_id=$(this).attr('value');
		if (confirm('<?php echo $this->lang->line("common_warning"); ?>'))
    	{
			$(location).attr('href', '<?php echo site_url("pedidos/concluir_pedido"); ?>/'+pedido_id);
    		
    	}
	});*/
$(".cancel").click(function()
	{
		var pedido_id=$(this).attr('value');
		if (confirm('<?php echo $this->lang->line("common_warning_cancel"); ?>'))
    	{
			$(location).attr('href', '<?php echo site_url("pedidos/cancel"); ?>/'+pedido_id);
    		
    	}
	});
});
</script>