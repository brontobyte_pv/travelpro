<div id="required_fields_message"><?php echo $this->lang->line('common_send_msj'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open('pedidos/msj/'.$pedido_id,array('id'=>'form_gastos'));
?>
<fieldset id="pedidos_basic_info">

<div class="field_row clearfix"><?php echo form_label('Mensaje', 'fecha', array('class'=>'items_category')); ?>
<div class='form_field'>
	<?php echo form_textarea(array(
		'name'=>'msj',
		'id'=>'msj',
		'value'=>'Estimado '.$cliente.': ',
		'rows'=>'5',
		'cols'=>'17')
	);?>
	</div>
</div>


	 
	<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button')
);
?>
        </div>
	


</fieldset>
<?php
echo form_close();
?>

<script type='text/javascript'>

//validation and submit handling
 var $j =jQuery.noConflict();
$j(document).ready(function()
{
	
	$j("#nom_pac").autocomplete("<?php echo site_url('appointments/suggest_pac');?>",{max:100,minChars:0,delay:10});
    $j("#nom_pac").result(function(event, data, formatted){});
	$j("#nom_pac").search();


	$j('#form_citas').validate({
		submitHandler:function(form)
		{
			/*
			make sure the hidden field #item_number gets set
			to the visible scan_item_number value
			*/
			
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_item_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			nom_pac:"required",
			
   		},
		messages:
		{
			nom_pac:"<?php echo $this->lang->line('prescription_name_required'); ?>"
			

		}
	});
});
</script>
<script type="text/javascript">

  $(function() {
    $('#datetimepicker1').datetimepicker({
      language: 'pt-BR',
	  pick12HourFormat: true,
	  pickSeconds: false,
    });
  });
  
</script>