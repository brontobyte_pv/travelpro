<div id="required_fields_message"><?php echo $this->lang->line('common_det_pedido'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open('gastos/save/',array('id'=>'form_gastos'));
?>
<fieldset id="pedidos_basic_info">

<table id="item_kit_items">
	<tr>
		<th><?php echo $this->lang->line('item_kits_quantity');?></th>
		<th><?php echo $this->lang->line('item_kits_item');?></th>
		<th><?php echo $this->lang->line('item_kits_unit_price');?></th>
		
	</tr>
	
	<?php 

	foreach ($this->Pedido->get_items_info($pedido_id) as $item) { ?>
		<tr>
		
			<?php
			
			$item_info = $this->Item->get_info($item['item_id']);
			?>
			<td><?php echo $item['quantity_purchased']; ?></td>
			<td><?php echo $item_info->name; ?></td>
			<td><?php echo to_currency($item_info->unit_price); ?></td>
		</tr>
		
	<?php } ?>
	
</table>

<div class="field_row clearfix"><?php echo form_label('Total: '.to_currency($total), 'fecha', array('class'=>'items_category')); ?></div>
</fieldset>
<?php
echo form_close();
?>


