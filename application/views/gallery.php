
<!DOCTYPE html>
<html>
<head>
<title>Travelpro | Gallery</title>
<link href="<?php echo base_url();?>css/fcss/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="<?php echo base_url();?>css/fcss/style.css" type="text/css" rel="stylesheet" media="all">
<!--web-font-->
<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
<!--//web-font-->
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Excursion Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //Custom Theme files -->
<!-- js -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>  
<!-- //js -->	
<!-- start-smoth-scrolling-->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>	
<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
</script>
<!--//end-smoth-scrolling-->
</head>
<body>
	<!--navigation-->
	<div class="top-nav">
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-left">
						<li><a href="index.html">Inicio</a></li>
						<li><a href="Paquetes de temporadas.html" class="active">Paquetes de temporadas</a></li>
						<li><a href="gallery.html" class="active">Gallery</a></li>
						<li><a href="testimonial.html">Testimonials</a></li>
						<li><a href="blog.html">Promociones</a></li>
						<li><a href="contact.html">Contacto</a></li>
					</ul>	
					<div class="social-icons">
						<ul>
							<li><a href="#"></a></li>
							<li><a href="#" class="fb"></a></li>
							<li><a href="#" class="gg"></a></li>
						</ul>	
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>	
		</nav>		
	</div>	
	<!--navigation-->
	<!--header-->
	<div class="header">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="index.html"><img src="images/logo1.jpg" alt=""></a>
			</div>	
			<form class="navbar-form navbar-right" role="search">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Search">
				</div>
				<button type="submit" class="btn btn-default" aria-label="Left Align">
					<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
				</button>
			</form>	
		</div>	
	</div>
	<!--//header-->
	<div class="gallery">
		<div class="container">
			<ol class="breadcrumb">
				<li><a href="index.html">Home</a></li>
				<li>Gallery</li>
			</ol>
			<div class="tesimonial"><h3>Gallery</h3></div>
			<div class="gallry-info">
				<div class="col-md-3 gallery-grids">
					<a href="images/img33.jpg" class="b-link-stripe b-animate-go  swipebox"  title="">
						<img src="images/img33.jpg" class="img-responsive glry-img" alt="">
						<div class="b-wrapper">
							<span class="b-animate b-from-left    b-delay03 ">
								<img class="img-responsive zoom-img img-circle" src="images/e.png" alt=""/>
							</span>					
						</div>
					</a>
				</div>
				<div class="col-md-3 gallery-grids">
					<a href="images/img31.jpg" class="b-link-stripe b-animate-go  swipebox"  title="">
						<img src="images/img34.jpg" class="img-responsive glry-img" alt="">
						<div class="b-wrapper">
							<span class="b-animate b-from-left    b-delay03 ">
								<img class="img-responsive zoom" src="images/e.png" alt=""/>
							</span>					
						</div>
					</a>
				</div>
				<div class="col-md-3 gallery-grids">
					<a href="images/img33.jpg" class="b-link-stripe b-animate-go  swipebox"  title="">
						<img src="images/img35.jpg" class="img-responsive glry-img " alt="">
						<div class="b-wrapper">
							<span class="b-animate b-from-left    b-delay03 ">
								<img class="img-responsive zoom" src="images/e.png" alt=""/>
							</span>					
						</div>
					</a>
				</div>
				<div class="col-md-3 gallery-grids">
					<a href="images/img32.jpg" class="b-link-stripe b-animate-go  swipebox"  title="">
						<img src="images/img36.jpg" class="img-responsive glry-img" alt="">
						<div class="b-wrapper">
							<span class="b-animate b-from-left    b-delay03 ">
								<img class="img-responsive zoom" src="images/e.png" alt=""/>
							</span>					
						</div>
					</a>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="gallry-info">
				<div class="col-md-3 gallery-grids">
					<a href="images/img37.jpg" class="b-link-stripe b-animate-go  swipebox"  title="">
						<img src="images/img37.jpg" class="img-responsive glry-img" alt="">
						<div class="b-wrapper">
							<span class="b-animate b-from-left    b-delay03 ">
								<img class="img-responsive zoom-img img-circle" src="images/e.png" alt=""/>
							</span>					
						</div>
					</a>
				</div>
				<div class="col-md-3 gallery-grids">
					<a href="images/img38.jpg" class="b-link-stripe b-animate-go  swipebox"  title="">
						<img src="images/img38.jpg" class="img-responsive glry-img" alt="">
						<div class="b-wrapper">
							<span class="b-animate b-from-left    b-delay03 ">
								<img class="img-responsive zoom" src="images/e.png" alt=""/>
							</span>					
						</div>
					</a>
				</div>
				<div class="col-md-3 gallery-grids">
					<a href="images/img39.jpg" class="b-link-stripe b-animate-go  swipebox"  title="">
						<img src="images/img39.jpg" class="img-responsive glry-img " alt="">
						<div class="b-wrapper">
							<span class="b-animate b-from-left    b-delay03 ">
								<img class="img-responsive zoom" src="images/e.png" alt=""/>
							</span>					
						</div>
					</a>
				</div>
				<div class="col-md-3 gallery-grids">
					<a href="images/img40.jpg" class="b-link-stripe b-animate-go  swipebox"  title="">
						<img src="images/img40.jpg" class="img-responsive glry-img" alt="">
						<div class="b-wrapper">
							<span class="b-animate b-from-left    b-delay03 ">
								<img class="img-responsive zoom" src="images/e.png" alt=""/>
							</span>					
						</div>
					</a>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="gallry-info">
				<div class="col-md-3 gallery-grids">
					<a href="images/img41.jpg" class="b-link-stripe b-animate-go  swipebox"  title="">
						<img src="images/img41.jpg" class="img-responsive glry-img" alt="">
						<div class="b-wrapper">
							<span class="b-animate b-from-left    b-delay03 ">
								<img class="img-responsive zoom-img img-circle" src="images/e.png" alt=""/>
							</span>					
						</div>
					</a>
				</div>
				<div class="col-md-3 gallery-grids">
					<a href="images/img42.jpg" class="b-link-stripe b-animate-go  swipebox"  title="">
						<img src="images/img42.jpg" class="img-responsive glry-img" alt="">
						<div class="b-wrapper">
							<span class="b-animate b-from-left    b-delay03 ">
								<img class="img-responsive zoom" src="images/e.png" alt=""/>
							</span>					
						</div>
					</a>
				</div>
				<div class="col-md-3 gallery-grids">
					<a href="images/img28.jpg" class="b-link-stripe b-animate-go  swipebox"  title="">
						<img src="images/img28.jpg" class="img-responsive glry-img " alt="">
						<div class="b-wrapper">
							<span class="b-animate b-from-left    b-delay03 ">
								<img class="img-responsive zoom" src="images/e.png" alt=""/>
							</span>					
						</div>
					</a>
				</div>
				<div class="col-md-3 gallery-grids">
					<a href="images/img24.jpg" class="b-link-stripe b-animate-go  swipebox"  title="">
						<img src="images/img24.jpg" class="img-responsive glry-img" alt="">
						<div class="b-wrapper">
							<span class="b-animate b-from-left    b-delay03 ">
								<img class="img-responsive zoom" src="images/e.png" alt=""/>
							</span>					
						</div>
					</a>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>	
	</div>	
	<link rel="stylesheet" href="css/swipebox.css">
		<script src="js/jquery.swipebox.min.js"></script> 
			<script type="text/javascript">
				jQuery(function($) {
					$(".swipebox").swipebox();
				});
			</script>
	<!--footer-->
	
	<?php $this->load->view("footer");?>
	
	<!--footer-->
	
		<script type="text/javascript">
			$(document).ready(function() {
				/*
				var defaults = {
					containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
				};
				*/
				
				$().UItoTop({ easingType: 'easeOutQuart' });
				
			});
		</script>
		<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<!--//smooth-scrolling-of-move-up-->
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.js"></script>
</body>
</html>