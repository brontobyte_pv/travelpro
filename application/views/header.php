<div class="sb-slidebar sb-left">

<header>
	<div id="logo_head">
		<a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>images/banner/logo-travel-azul.png" alt=""></a>
	</div>
	
	
	
	<nav id="menu_header">
				<ul>
						<li class="list-header" id="nav-home"><a  class="ancla-menu" href="<?php echo base_url();?>">Home</a><span class="line-menu"></span></li>
						
						<li class="list-header" id="nav-paq-movil"><a  class="ancla-menu" href="<?php echo base_url();?>index.php/front/paquetes_temp">Paquetes</a>
						<span class="line-menu"></span><span class="line-menu"></span></li>
						
						
						<li class="list-header"  id="nav-paq">
							<details id="details-paque">
								
				
								
								
								<summary id="summ-ancla">
								<span  class="ancla-menu" href="">
								Paquetes
								</span>
								
								</summary>
							
							
								<ul id="subnav-header">
								<?php
									if(isset($categories))
									{
									foreach($categories as $categorie)
									{	
									
									echo '<li><a href="'.base_url().'index.php/front/paquetes/'.$categorie.'">'.strtoupper($categorie).'</a></li>';
									
								} }?>
									
								</ul>
									
								
					
							</details>
						
							<span class="line-menu"></span>
						
						
						</li>
						
						
							
						
					
						
						
						
					
						
						<li class="list-header"  id="nav-res"><a  class="ancla-menu" href="<?php echo base_url();?>index.php/front/reservaciones">Reservaciones</a><span class="line-menu"></span></li>
						
						
						<li  class="list-header" id="nav-prom"><a  class="ancla-menu" href="<?php echo base_url();?>index.php/front/cruceros">Cruceros</a><span class="line-menu"></span></li>
						
					
						
						<li class="list-header"  id="nav-con"><a  class="ancla-menu" href="<?php echo base_url();?>index.php/front/contacto"> Contacto</a><span class="line-menu"></span></li>
						
						<li  class="list-header" id="nav-corp"><a  class="ancla-menu" href="<?php echo base_url();?>index.php/front/corporativos"> 
                        Corporativos</a><span class="line-menu"></span></li>
						
						<li class="list-header" id="nav-tip"><a  class="ancla-menu" href="#"> Bodas</a></li>
				</ul>
	</nav>
</header>

</div>

		<!-- Slidebars -->
		<script src="<?php echo base_url();?>js/fjs/slidebars.js"></script>
		<script>
			(function($) {
				$(document).ready(function() {
					$.slidebars();
				});
			}) (jQuery);
		</script>