<!DOCTYPE html>
<html lang="es">
<head> 
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Author" content="Adrián Ramirez | brontobytemx.com" />
<meta name="Subject" content="Diseño web Mexico" />
<meta name="GOOGLEBOT" content="INDEX, FOLLOW, ALL" />
<meta name="robots" content="index, follow" />
<meta name="GOOGLEBOT" content="NOARCHIVE" />
<meta name="Generator" content="html" />
<meta name="Language" content="Spanish" />
<meta name="Revisit" content="1 day" />
<meta name="Distribution" content="Global" />
<meta name="Robots" content="All" />
	<meta property="og:url"           content="" />
    <meta property="og:type"          content="" />
    <meta property="og:title"         content="" />
    <meta property="og:description"   content="" />
    <meta property="og:image"         content="" />

<title>TravelPro | Paquetes <?php echo strtoupper($categorie);?></title>
<meta name="Description" content=""/>
<meta name="Keywords" content="" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/style-header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/style-all.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/dispositivos.css"/>



<!--web-font-->
<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,600,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Cabin:400,600,500' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=BenchNine" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Crete+Round" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oleo+Script" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Suez+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans|Open+Sans+Condensed:300" rel="stylesheet">






<!--//web-font-->

<script src="http://code.jquery.com/jquery.js"></script>
 
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="shortcut icon" href="<?php echo base_url();?>images/ico/favicon.ico">

 

</head>
<body>
	<div id="w-catego-paque" class="wrapper w-sections" >

	<!--navigation-->
	<?php $this->load->view("header");?>
	<!--navigation-->
	 
		<div id="sb-site" class="box-contenido box-contenido2">
				
	<!--MAIN MOVIL-->
	<?php $this->load->view("menu-movil");?>
	<!--MAIN MOVIL-->
			
		
		
				<div class="banner-section">
					<img class="img-paquete-detail" src="<?php echo base_url();?>images/paquetes/categoria-img-paquetes/<?php echo strtoupper($categorie);?>/banner_categoria.jpg"/>
					<div class="cover-subtitles">
						<h2 class="subtitles-bg"><?php echo strtoupper($categorie);?></h2>
						<div id="nav-sub"><a href="<?php echo base_url();?>">PAQUETES/</a> <span><?php echo strtoupper($categorie);?></span></div>
					</div>
				</div>
				
				
				
				<?php echo $paquetes;?>
				
					
					
					
			
		
		
		
		
		
		
		</div>
	</div>
	<?php $this->load->view("footer");?>
	
	<!--footer-->
	<!-- bootstrop-->
  <script src="<?php echo base_url();?>js/fjs/bootstrap.js"></script>
     <script>
      !function ($) {
        $(function(){
          // carousel demo
          $('#myCarousel').carousel()
        })
      }(window.jQuery)
    </script>
 <!-- bootstrop-->
	
</body>
</html>