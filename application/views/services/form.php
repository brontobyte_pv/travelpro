
<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open_multipart('services/save/'.$info->service_id,array('id'=>'form_services'));
?>
<fieldset id="new_service_info">
<legend class="name-forms-popup"><?php echo $this->lang->line("services_nuevo_servicio"); ?></legend>

<div class="field_row_name_client">
<?php echo form_label($this->lang->line('cobros_cliente').':', 'nom_cliente',array('class'=>'name-client-field')); ?>
	
	<?php 
	
	if(empty($info->nom_cliente))
	{
		echo form_label($customer, 'nom_cliente',array('class'=>'name-client-field')); 
	}
	else
	{
		echo form_label($info->nom_cliente, 'nom_cliente',array('class'=>'name-client-field')); 
	}
	
	?>
	
	
	
	
</div>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('services_motivo'), 'motivo', array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'motivo',
		'id'=>'motivo',
		'value'=>$info->motivo)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('services_date'), 'fecha', array('class'=>'required')); ?>
		<div id='report_date_range_complex'>
		
		<div class="box-inputs-rep">
		<?php echo form_dropdown('month',$months, $selected_month, 'id="month"'); ?>
		<?php echo form_dropdown('day',$days, $selected_day, 'id="day"'); ?>
		<?php echo form_dropdown('year',$years, $selected_year, 'id="year"'); ?>
		<span id="line-fecha">Hora</span>
		<?php echo form_dropdown('hour',$hours, $selected_hour, 'id="hour"'); ?>
		<?php echo form_dropdown('minute',$minutes, $selected_minute, 'id="minute"'); ?>
		</div>
	</div>
</div>
<?php echo form_input(array(
		'name'=>'customer',
		'id'=>'customer',
		'type'=>'hidden',
		'value'=>$customer)
	);?>
<div class="field_row clearfix">
  <?php echo form_label($this->lang->line('services_asignado'), 'nom_asignado', array('class'=>'required')); ?>
    <div class='form_field'>
	<?php echo form_dropdown('nom_asignado',$empleados,'');?>
	</div>
</div>

<div class="field_row clearfix">
 <?php echo form_label($this->lang->line('services_accion').':', 'accion',array('class'=>'required wide')); ?>
    <div class='form_field'>
	<?php echo form_textarea(array(
		'name'=>'accion',
		'id'=>'accion',
		'rows'=>'4',
        'cols'=>'10',
		'value'=>$info->accion)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('services_duracion'), 'duracion', array('class'=>'required')); ?>
    <div class='form_field'>
	<?php echo form_input(array(
		'name'=>'duracion',
		'id'=>'duracion',
		'value'=>$info->duracion)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('services_reporte').':', 'company_name',array('class'=>'required wide')); ?>
    <div class='form_field'>
	<?php echo form_textarea(array(
		'name'=>'reporte',
		'id'=>'reporte',
		'value'=>$info->reporte)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('services_estado').':', 'estado', array('class'=>'required')); ?>
    <div class='form_field'>
	<?php $estados=array(
		'Pendiente'=>$this->lang->line('services_pendiente'),
		'Cancelado'=>$this->lang->line('services_cancelado'),
		'Hecho'=>$this->lang->line('services_hecho')); 
		echo form_dropdown('estado',$estados,'');?>
	</div>
</div>	
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_right')
);
?>
        
	


</fieldset>
<?php
echo form_close();
?>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{
	$('#form_services').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			motivo:"required",
			
		},
		messages:
		{
			name:"<?php echo $this->lang->line('items_name_required'); ?>",
			category:"<?php echo $this->lang->line('items_category_required'); ?>"
		}
	});
});

</script>
<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	$("#generate_report").click(function()
	{		
		var sale_type = $("#sale_type").val();
		
		if ($("#simple_radio").attr('checked'))
		{
			window.location = window.location+'/'+$("#report_date_range_simple option:selected").val() + '/' + sale_type;
		}
		else
		{
			var start_date = $("#start_year").val()+'-'+$("#start_month").val()+'-'+$('#start_day').val();
			var end_date = $("#end_year").val()+'-'+$("#end_month").val()+'-'+$('#end_day').val();
	
			window.location = window.location+'/'+start_date + '/'+ end_date+ '/' + sale_type;
		}
	});
	
	$("#start_month, #start_day, #start_year, #end_month, #end_day, #end_year").click(function()
	{
		$("#complex_radio").attr('checked', 'checked');
	});
	
	$("#report_date_range_simple").click(function()
	{
		$("#simple_radio").attr('checked', 'checked');
	});
	
});
</script>