<?php $this->load->view("partial/header"); ?>
<script type="text/javascript">
function filtrar_x_mes(){
    $('#form_months').submit();
}
$(document).ready(function()
{
    init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo $this->lang->line("common_confirm_search")?>');
    enable_delete('<?php echo $this->lang->line($controller_name."_confirm_delete")?>','<?php echo $this->lang->line($controller_name."_none_selected")?>');
	

	
	$('#customer').click(function()
    {
    	$(this).attr('value','');
    });

    $("#customer").autocomplete('<?php echo site_url("cobros/customer_search"); ?>',
    {
    	minChars:0,
    	delay:0,
    	max:50,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#customer").result(function(event, data, formatted)
    {
		$("#select_customer_form_cobros").submit();
    });
    
});

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter section').length >1)
	{
		$("#sortable_table").tablesorter(
		{
			sortList: [[1,0]],
			headers:
			{
				0: { sorter: false},
				3: { sorter: false}
			}
		});
	}
}

function post_cobros_form_submit(response)
{
	if(!response.success)
	{	
		
		hightlight_row(response.cobro_id);
		set_feedback(response.message,'error_message',false);
		
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.cobro_id,get_visible_checkbox_ids()) !=-1)
		{
			update_row(response.cobro_id,'<?php echo site_url("$controller_name/get_row")?>');
			update_custom('<?php echo site_url("$controller_name/remove_customer")?>');
			set_feedback(response.message,'success_message',false);
			
			
		}
		else //refresh entire table
		{
			
				do_search(true,function()
		{
				hightlight_row(response.cobros_data);
				set_feedback(response.message,'success_message',false);
				});
				
		}
	
	}
			
	
}


</script>

<div id="title_bar">
	<div id="title" ><?php echo $this->lang->line('common_list_of').' '.$this->lang->line('module_'.$controller_name); ?></div>
	<div id="cover_buttons_top_manage">
		<?php echo anchor("$controller_name/view/-1/",
		"<div class='buttons_top_manage' ><img src='".base_url()."images/ico/ico-cobros.png'/><span>".$this->lang->line($controller_name.'_new')."</span></div>",
		array('class'=>'thickbox none','title'=>$this->lang->line($controller_name.'_new')));
		?>
	</div>
</div>


<div id="table_action_header">
	<ul>
		<li  id="box-buscar" >
	
		<img src="images/ico/icon-lupa.png"/>
		<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
		<input type="text" name ='search' id='search'/>
		</form>
		</li>
	
		<li id="btn-borrar-buscar" ><span><?php echo anchor("$controller_name/delete",$this->lang->line("common_delete"),array('id'=>'delete')); ?></span></li>
		<li id="box_months" >
		<?php
			echo form_open('cobros/filtro/',array('id'=>'form_months'));
		?>
		
		<?php echo form_dropdown('meses',$meses, $selected_month,'id="meses" class ="box-inputs-rep" onChange="filtrar_x_mes();"'); ?>
		</form>
		</li>
		<li id="box_cant_cobros" >
		<span id="name-cuenta">
		<b>Total Cobros Pendientes: </b><?php echo to_currency($total_undone); ?><br>
		<b>Total Cobros Hechos: </b><?php echo to_currency($total_done); ?><br>
		
		</span>
		</li> 
		<li id="box-buscar-cliente-servicios"  >
		<?php
	if(isset($customer))
	{
		echo '<span id="word-cliente">'.$this->lang->line("sales_customer").' :</span><span id="name-customer-servicios">'.$customer. '</span><br />';
		echo anchor("cobros/remove_customer",'['.$this->lang->line('common_remove').' '.$this->lang->line('customers_customer').']',array('id'=>'delete-customer-servicios'));
	}
	else
	{
		echo form_open("cobros/select_customer",array('id'=>'select_customer_form_cobros')); ?>
	
		<?php echo form_input(array('name'=>'customer','id'=>'customer','size'=>'30','value'=>$this->lang->line('cobros_selecciona_cliente')));?>
		</form>
		<div id="box-btn-cli-ser">
		
		<?php echo anchor("customers/view/-1/",
		"<div id='btn-cliente-servicios'><span>".$this->lang->line('cobros_cliente_nuevo')."</span></div>",
		array('class'=>'thickbox none','title'=>$this->lang->line('sales_new_customer')));
		?>
		</div>

		<div class="clearfix">&nbsp;</div>
		<?php
	}
	?>
		</li>
		
	</ul>
</div>

<div id="table_holder">
<?php echo $manage_table; ?>
</div>
<div class="box-pagination">
<?php echo $this->pagination->create_links();?>
</div>
<div id="feedback_bar"></div>
</div>
</div>


<?php $this->load->view("partial/footer"); ?>




