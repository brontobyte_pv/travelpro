<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open_multipart('cobros/save/'.$info->cobro_id,array('id'=>'cobros_form'));
?>
<fieldset id="cobro_info">
<legend class="name-forms-popup"><?php echo $this->lang->line("cobros_nuevo"); ?></legend>

<div class="field_row_name_client clearfix">
<?php echo form_label($this->lang->line('cobros_cliente').':', 'nom_cliente',array('class'=>'name-client-field')); ?>
	
	<?php 
	
	if(empty($info->nom_cliente))
	{
		echo form_label($customer, 'nom_cliente',array('class'=>'name-client-field')); 
	}
	else
	{
		echo form_label($info->nom_cliente, 'nom_cliente',array('class'=>'name-client-field')); 
	}
	
	?>
	
</div>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('cobros_date').':', 'fecha',array('class'=>'wide required')); ?>
	<div id='report_date_range_complex'>
		
		<div class="box-inputs-rep">
		<?php echo form_dropdown('month',$months, $selected_month, 'id="month"'); ?>
		<?php echo form_dropdown('day',$days, $selected_day, 'id="day"'); ?>
		<?php echo form_dropdown('year',$years, $selected_year, 'id="year"'); ?>
		<span id="line-fecha">Hora</span>
		<?php echo form_dropdown('hour',$hours, $selected_hour, 'id="hour"'); ?>
		<?php echo form_dropdown('minute',$minutes, $selected_minute, 'id="minute"'); ?>
		</div>
	</div>
</div>

<?php echo form_input(array(
		'name'=>'customer',
		'id'=>'customer',
		'type'=>'hidden',
		'value'=>$customer)
	);?>
<div class="field_row clearfix">

	<?php 
	if($info->sale_id!=-1 && !empty($info->cantidad))
	{
		?>
		<?php echo form_label($this->lang->line('cobros_saldo').': '.form_label(to_currency($info->cantidad), 'cantidad_l',array('class'=>'name-client-field')), 'cantidad',array('class'=>'wide required')); ?>
	<div class='form_field'><?php
		
		
		
	}
	elseif(empty($info->cantidad))
	{ ?>
		<?php echo form_label($this->lang->line('cobros_cantidad').':', 'cantidad',array('class'=>'wide required')); ?>
	<div class='form_field'><?php 
		echo form_input(array(
		'name'=>'cantidad',
		'id'=>'cantidad',
		'value'=>''));
	}
	else
	{
		?>
		<?php echo form_label($this->lang->line('gastos_cantidad').':', 'cantidad',array('class'=>'wide required')); ?>
	<div class='form_field'><?php
		echo form_input(array(
		'name'=>'cantidad',
		'id'=>'cantidad',
		'value'=>to_currency($info->cantidad)));
	}
	
	
	
	?>
	</div>
</div>
<?php if($info->sale_id!=-1 && !empty($info->cantidad))
	{ ?>
<div class="field_row clearfix">

	<div class='form_field'>
	<?php
	echo form_label('Adeudo de la cuenta: '.form_label($info->sale_id, 'cantidad_l',array('class'=>'name-client-field')), 'concepto',array('class'=>'wide required'));
	
	
		
	
	
	?>
	</div>
</div>
<?php
}
	else
	{	?>
<div class="field_row clearfix">

	<div class='form_field'>
		<?php
		echo form_label($this->lang->line('gastos_concepto').':', 'concepto',array('class'=>'wide required'));
		echo form_input(array(
		'name'=>'concepto',
		'id'=>'concepto',
		'value'=>$info->concepto));
		
		
		
		?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('gastos_recurso').':', 'recurso',array('class'=>'wide required')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('recurso',$recursos,'');?>
	</div>
</div>
<?php
		}?>
<div class="field_row clearfix">

<?php if($info->sale_id!=-1 && !empty($info->cantidad))
	{
	echo form_label($this->lang->line('cobros_cuentas_pendientes').':', 'nom_cliente',array('class'=>'wide required'));
	}
 ?>
	
</div>
<div class="field_row clearfix">
  <?php  echo form_label($this->lang->line('gastos_modo').':', 'modo',array('class'=>'wide'));?>  
    <div class='form_field'>
	<?php echo form_dropdown('modo',$modo,'');?>
	</div>
</div>
 <?php if($info->sale_id!=-1 && !empty($info->cantidad))
	{?>
<div class="field_row clearfix">
  <?php  echo form_label($this->lang->line('gastos_estado').': Pendiente', 'estado',array('class'=>'wide'));?>  
 
    
</div>
<?php
	}else{
 ?>
 <div class="field_row clearfix">
  <?php  echo form_label($this->lang->line('gastos_estado').':', 'estado',array('class'=>'wide'));?>  
 
    <div class='form_field'>
	<?php echo form_dropdown('estado',$estado,'');?>
	</div>
</div>
 
 
 <?php
	}
 ?> 
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_right')
);
?>





</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{
	$('#cobros_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_cobros_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			cantidad:"required"
			
		},
		messages:
		{
			cantidad:"<?php echo $this->lang->line('items_name_required'); ?>"
			
		}
	});
});

</script>
