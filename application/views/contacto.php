<!DOCTYPE html>
<html lang="es">
<head> 
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Author" content="Adrián Ramirez | brontobytemx.com" />
<meta name="Subject" content="Diseño web Mexico" />
<meta name="GOOGLEBOT" content="INDEX, FOLLOW, ALL" />
<meta name="robots" content="index, follow" />
<meta name="GOOGLEBOT" content="NOARCHIVE" />
<meta name="Generator" content="html" />
<meta name="Language" content="Spanish" />
<meta name="Revisit" content="1 day" />
<meta name="Distribution" content="Global" />
<meta name="Robots" content="All" />
	<meta property="og:url"           content="" />
    <meta property="og:type"          content="" />
    <meta property="og:title"         content="" />
    <meta property="og:description"   content="" />
    <meta property="og:image"         content="" />

<title>TravelPro | Home</title>
<meta name="Description" content=""/>
<meta name="Keywords" content="" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/style-header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/style-all.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/dispositivos.css"/>

<!--web-font-->
<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,600,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Cabin:400,600,500' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=BenchNine" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Crete+Round" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oleo+Script" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Suez+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">






<!--//web-font-->

<script src="http://code.jquery.com/jquery.js"></script>
 
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="<?php echo base_url();?>images/ico/favicon.ico">

 

</head>
<body id="body-contact">
<div class="wrapper w-sections" id="w-contac">

	<!--navigation-->
	<?php $this->load->view("header");?>
	<!--navigation-->
	
		<div id="sb-site" class="box-contenido">
				
	<!--MAIN MOVIL-->
	<?php $this->load->view("menu-movil");?>
	<!--MAIN MOVIL-->
			
		
			   <div class="banner-section">
					<img class="img-paquete-detail" src="<?php echo base_url();?>images/reserva-img-contact/banner-contacto.jpg"/>
					<div class="cover-subtitles">
						<h2 class="subtitles-bg">CONTACTO</h2>
						<div id="nav-sub"><a href="<?php echo base_url();?>">HOME/</a> <span>CONTACTO</span></div>
					</div>
				</div>
		<section  class="cover_standar" id="section-contact">
			
			
				<article class="article-contact">
				<h5 class="titles-contact">ENVIANOS UN MENSAJE!!</h5>
					<?php 
					
					$attributes = array('id' => 'formu-contact');
					echo form_open('front/contacto',$attributes); ?>
						<label>Nombre</label>
						<input type="text"  name="nombre" value="<?php echo set_value('nombre'); ?>" >
						<label>Correo</label>
						<input type="email" name="correo" value="<?php echo set_value('correo'); ?>"  >
						<label>Mensaje</label>
						<textarea type="text" name="mensaje"><?php echo set_value('mensaje'); ?></textarea>
						<label>Escribe el código</label>
						<input class="input-contact"   id="captcha" name="captcha" type="text" />
						<div id="img-captcha"><?php echo $img; ?></div>
						<label><?php echo $error_captcha; ?></label>
					
						<input type="submit" value="ENVIAR" id="btn-send-contacto" >
					</form>
						
				</article>	
				<article class="article-contact">
				<h5 class="titles-contact">CONTACTANOS</h5>
				<div id="cover-redes-contac">
					<a href="https://www.facebook.com/TravelPro.mx" target="_blank"><img id="r-fa" src="<?php echo base_url();?>images/ico/face-ico.png" alt=""> </a>
					<a href="https://twitter.com/TRAVELPROIRAPUA?lang=es" target="_blank" ><img id="r-tw" src="<?php echo base_url();?>images/ico/tw-ico.png" alt=""></a>
					<a href="https://www.instagram.com/travelpro.mx/" target="_blank" ><img id="r-in" src="<?php echo base_url();?>images/ico/insta-ico.png" alt=""> </a>
					<a href="https://es.pinterest.com/t_pro/pins/" target="_blank" ><img id="r-pin" src="<?php echo base_url();?>images/ico/red-pin.png" alt=""> </a>
				</div>
				
				<div id="cover-all-cont">
				
					<div class="cover-ubi-contact">
						<img class="ico-ubica-contact" src="<?php echo base_url();?>images/ico/ico-ubicacion-travels.png" alt="">
						
						<span class="ubi-txt-contact">Plaza Delta Avenida Héroes de Nacozari 1655 Int. 14-A</span>
					</div>
					<div class="cover-ubi-contact">
						<img class="ico-ubica-contact" src="<?php echo base_url();?>images/ico/ico-phone-travels.png" alt="">
						<span class="ubi-txt-contact">(+52) 462 114 2525</span>
					</div>
					<div class="cover-ubi-contact">
						<img class="ico-ubica-contact" src="<?php echo base_url();?>images/ico/ico-wat-travels.png" alt="">
						<span class="ubi-txt-contact">(462) 133 49 30</span>
					</div>
					<div class="cover-ubi-contact">			
						<img class="ico-ubica-contact" src="<?php echo base_url();?>images/ico/ico-mail-travels.png" alt="">
						<span class="ubi-txt-contact">contacto@travelpro.mx</span>
				
					</div>
				</div>
				<h5 class="titles-contact">FORMAS DE PAGO</h5>
				<div id="cover-all-pagos">
				
					<img class="ico-pay" src="<?php echo base_url();?>images/ico/paypal_verified
					.png" alt="">
					<span class="ubi-txt-pay">TRANSFERENCIA BBVA : 01 22 22 00 44 59 04 595 4</span>
					<span class="ubi-txt-pay">DEPOSITO BBVA: 044 59 04 595</span>
					
				</div>
			</article>
	
	
		<div id="cover-mapa">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3732.7233589285383!2d-101.37845158550209!3d20.68083088618794!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842c7fbab8b14bef%3A0xd1831b842779744c!2sPlaza+Delta!5e0!3m2!1ses!2sco!4v1474917135785"  frameborder="0" style="border:0" allowfullscreen></iframe>
		
		</div>
	
		
		</section>
		</div>	
	

</div>
	<?php $this->load->view("footer");?>
	
	<!--footer-->
	<!-- bootstrop-->
  <script src="<?php echo base_url();?>js/fjs/bootstrap.js"></script>
     <script>
      !function ($) {
        $(function(){
          // carousel demo
          $('#myCarousel').carousel()
        })
      }(window.jQuery)
    </script>
 <!-- bootstrop-->
	
</body>
</html>