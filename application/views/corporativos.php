<!DOCTYPE html>
<html lang="es">
<head> 
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Author" content="Adrián Ramirez | brontobytemx.com" />
<meta name="Subject" content="Diseño web Mexico" />
<meta name="GOOGLEBOT" content="INDEX, FOLLOW, ALL" />
<meta name="robots" content="index, follow" />
<meta name="GOOGLEBOT" content="NOARCHIVE" />
<meta name="Generator" content="html" />
<meta name="Language" content="Spanish" />
<meta name="Revisit" content="1 day" />
<meta name="Distribution" content="Global" />
<meta name="Robots" content="All" />
	<meta property="og:url"           content="" />
    <meta property="og:type"          content="" />
    <meta property="og:title"         content="" />
    <meta property="og:description"   content="" />
    <meta property="og:image"         content="" />

<title>TravelPro | Home</title>
<meta name="Description" content=""/>
<meta name="Keywords" content="" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/style-header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/style-all.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/dispositivos.css"/>


<!--web-font-->
<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,600,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Cabin:400,600,500' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=BenchNine" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Crete+Round" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oleo+Script" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Suez+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">





<!--//web-font-->

<script src="http://code.jquery.com/jquery.js"></script>
 
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="<?php echo base_url();?>images/ico/favicon.ico">

 

</head>
<body>
	<div class="wrapper w-sections" id="w-corp">

		<!--navigation-->
		<?php $this->load->view("header");?>
		<!--navigation-->
		
		<div id="sb-site" class="box-contenido">
			<!--MAIN MOVIL-->
			<?php $this->load->view("menu-movil");?>
			<!--MAIN MOVIL-->
					
				<section id="cover-paq" class="cover_standar">
					<div class="cover_titles_half">
					<span class="title-big title-big-color">TravelPro</span> <span class="fx-border "></span>
					</div>
					
					<div class="box-txt-travel">
						<h4 class="subtitles-corp">DESCRIPCIÓN DE TRAVEL PRO</h4>
						<p>
						travel pro es una prestadora de servicios y asesoría de viajes, filial de viajes
						moragrega s.a. de c.v. con mas de 25 años de experiencia.
						travel pro actúa como intermediario entre los sujetos de servicio y otros
							prestadores de servicio turístico.

						</p>
						
						<h4 class="subtitles-corp">NOMBRE COMERCIAL:</h4>
						<p>TRAVEL PRO</p>
						<h4 class="subtitles-corp">RAZÓN SOCIAL:</h4>
						<p>VIAJES MORAGREGA S.A. DE C.V.</p>
						
						<h4 class="subtitles-corp">SERVICIOS TRAVEL PRO:</h4>
					

						<ul class="ul-corp">
							<li>Reservación de boletos de avión y cambios de itinerario.</li>
							<li>Negociación y administración de convenios con hoteles y aerolineas.</li>
							<li>Reservación de hoteles con pago en el destino y de prepago.</li>
							<li>Renta de autos con pago en el destino y de prepago.</li>
							<li>Reservación de cruceros y trenes.</li>
							<li>Expedición de boletos de avión y reexpedición de boletos de avión.</li>
							<li>Reconfirmación de vuelos.</li>
							<li>Sistemas de reservaciones y globalizadores con tecnología de punta y su modulo corporativo, en línea, disponible 24 horas la dia los 365 días del año, con ahorros comprobados, diseñadoespecialmente para empresas.</li>
							<li>Reservaciones de hoteles locales y los cambios que sean necesarios en boletos de avión para el personal y visitantes de su empresa.</li>
							<li>Reservación de limusinas y/o autos con chofe r en algunas ciudades.</li>
							<li>Reservación de traslados al aeropuerto del bajío.</li>
							<li>Asesoria y venta de seguros de viajero.</li>
							<li>Programa de tarifas negociadas de renta de autos.</li>
							<li>Estricto apego a las políticas y reglas de su empresa.</li>
							<li>Contamos con consultores de viaje altamente calificados en constante capacitación</li>
							<li>Programa “walk, click & call” en nuestras oficinas, en medios electrónicos y por teléfono.</li>
							<li>Coordinación reservación y venta de viajes de incentivo.</li>
							<li>Coordinación y asesoría en eventos y convenciones, así como renta de carpas y moviliario.</li>
							<li>Servicio de paqueteria urgent e y logistica a cualquier parte de la republica en unidades de diferentes tamaños</li>
					
						</ul>
						
						
					</div>
						
					<div id="box-img-corp" class="box-txt-travel" >
					<img class="img-bg-promo" src="<?php echo base_url();?>images/banner/bg-corpo.jpg"/>
					<img id="logo-corp" src="<?php echo base_url();?>images/banner/logo-travel-bco2.png" alt="">
					</div>
					<div id="cover-info-corp">
					<hr id="linea-directorio"/>
							<div class="box-info-corp">
								<img src="<?php echo base_url();?>images/ico/ico-ubicacion-travels.png" alt="">
								<span>Plaza Delta
									Av. Héroe de Nacozari #1655 Int. 14-A
									Colonia San Miguelito, Irapuato, Gto.
									C.P. 36557
								</span>
							</div>
							<div class="box-info-corp">
								<img src="<?php echo base_url();?>images/ico/ico-phone-travels.png" alt="">
								<span>Teléfono 462 114.25.25</span>
							</div>
							<div class="box-info-corp">
								<img src="<?php echo base_url();?>images/ico/ico-time-travel.png" alt="">
								<span>LUNES A VIERNES DE 09:30 A 19:30 HRS.
								SABADOS DE 10:00 A 14:00 HRS
								</span>
							</div>
							<div id="box-info-puestos" class="box-info-corp">
								<img src="<?php echo base_url();?>images/ico/ico-directorio-travels.png" alt=""><span>
									Director General:
									Ricardo Olivas
									ricardo@travel-pro.com.mx</span>
									<span>
									Gerente de Ventas:
									Maru Moragrega
									maru@travel-pro.com.mx
									</span>
									<span>
									Consejeros de Viajes:
									Natalia Mosqueda natalia@travel-pro.com.mx
									Sofia Nuñez sofia@travel-pro.com.mx</span>
							</div>
						
					</div>
					
				
				</section>
		</div>
	</div>
	<?php $this->load->view("footer");?>
	
	<!--footer-->
	<!-- bootstrop-->
  <script src="<?php echo base_url();?>js/fjs/bootstrap.js"></script>
     <script>
      !function ($) {
        $(function(){
          // carousel demo
          $('#myCarousel').carousel()
        })
      }(window.jQuery)
    </script>
 <!-- bootstrop-->
	
</body>
</html>