<?php $this->load->view("partial/header"); ?>
<div id="page_title2" style="margin-bottom:8px;"><?php echo $this->lang->line('sales_register'); ?></div>
<?php
if(isset($error))
{
	echo "<div class='error_message'>".$error."</div>";
}

if (isset($warning))
{
	echo "<div class='warning_mesage'>".$warning."</div>";
}

if (isset($success))
{
	echo "<div class='success_message'>".$success."</div>";
}
?>
<div id="register_wrapper">
<div id="mode_form_div" class="oculta">
<?php echo form_open("sales/change_mode",array('id'=>'mode_form')); ?>
	<span id='mode_form_span'><?php echo $this->lang->line('sales_mode') ?></span>
<?php echo form_dropdown('mode',$modes,$mode,'onchange="$(\'#mode_form\').submit();"'); ?>



</form>

<?php echo form_open("sales/cancel_sale",array('id'=>'cancel_sale_form')); ?>
			<div id="cover_btn_new_ctas_cre">
	<?php echo anchor("sales/suspended/width:425",
	"<div id='btn_new_ctas_cre'  class='btn_ok'><span>".$this->lang->line('sales_unsuspend')."</span></div>",
	array('class'=>'thickbox none','title'=>$this->lang->line('sales_suspended_sales')));
	?>
</div>
			<div id="cover_btn_new_ctas_cre">
				<div id='btn_new_ctas_cre'  class='btn_cancel'><span><?php echo $this->lang->line('sales_cancel_sale'); ?></span></div>
				</div>
				</form>
				

</div>

<?php echo form_open("sales/add",array('id'=>'add_item_form')); ?>
<label id="item_label" for="item">

<?php
if($mode=='sale')
{
	echo $this->lang->line('sales_find_or_scan_item');
}
else
{
	echo $this->lang->line('sales_find_or_scan_item_or_receipt');
}
?>
<div id="mas"></div>
</label>
<?php echo form_input(array('name'=>'item','id'=>'item'));?>



</form>
<section id="cover_register">



<?php
if(count($cart)==0)
{
?>

<div class='warning_message' id='warning_message_reg'><?php echo $this->lang->line('sales_no_items_in_cart'); ?>

<?php
}


    
//////////////////////////////////////////////
///////////////////////////////////////////////
////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
else
    
{?> 
<div id="filas-head">
<div class="head-filas-c"><?php echo $this->lang->line('sales_quantity'); ?></div>
<div class="head-filas"  id="item-name"> <?php echo $this->lang->line('sales_item_name'); ?></div>
<div class="head-filas"><?php echo $this->lang->line('sales_price'); ?></div>
<div class="head-filas"><?php echo $this->lang->line('sales_total'); ?></div>
<div class="head-filas-o">Borrar</div>


</div>
<?php 
	
	foreach(array_reverse($cart, true) as $line=>$item)
	{
		?>
		<div id="filas-contenido">
		<?php
		$cur_item_info = $this->Item->get_info($item['item_id']);
		echo form_open("sales/edit_item/$line");
	?>
		<div id="filas-head-r"><?php echo $this->lang->line('items_quantity'); ?></div>
                    
			<div class="filas-items-reg-c"><?php
			
        	if($item['is_serialized']==1)
        	{
        		echo $item['quantity'];
        		echo form_hidden(array('name'=>'quantity','value'=>$item['quantity']));
				
        	}
        	else
        	{
        		echo form_input(array('name'=>'quantity','value'=>$item['quantity']));
        	}
		?></div>
		<div id="filas-head-r"><?php echo $this->lang->line('sales_item_name'); ?></div>
		<div  id="name-reg_r" class="filas-items-reg"><span><b><?php echo $item['name']; ?></b><br/>[  <?php echo $cur_item_info->quantity; ?> existencias ]</span></div>
       <?php  
	   
	   echo form_input(array('name'=>'name_item','type'=>'hidden','value'=>$item['name']));
	   ?>
			
		

		

		<?php if ($items_module_allowed)
		{
			
		?>
			<div id="filas-head-r"><?php echo $this->lang->line('sales_price'); ?></div>
			<div class="filas-items-reg"><?php echo form_input(array('name'=>'price','value'=>$item['price']));?></div>
		<?php
		}
		else
		{
		?>
		<div id="filas-head-r"><?php echo $this->lang->line('sales_price'); ?></div>
			<div class="filas-items-reg"><?php echo to_currency($item['price']); ?></div>
			<?php echo form_hidden('price',$item['price']); ?>
		<?php
		}
		?>
	<div id="filas-head-r"><?php echo $this->lang->line('sales_total'); ?></div>
	<div  class="filas-items-reg">
		<b>
		<?php echo to_currency($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100); ?></b></div>
		<div class="filas-items-reg-o"><b><?php echo anchor("sales/delete_item/$line",'['.$this->lang->line('sales_quit_item').']');?></b>
			</div>
	
		
		  
		
		
	
		<?php
        	if($item['is_serialized']==1)
        	{
        		echo form_input(array('name'=>'serialnumber','value'=>$item['serialnumber'],'size'=>'20'));
			}
			else
			{
				echo form_hidden('serialnumber', '');
			}
		?>	<?php echo form_submit(array('id'=>'edit_item'),'');?>
			</form>
	<?php
	}
}
?>
</div>
</section>

</div>




<div id="overall_sale">
		

	<?php
	// Only show this part if there are Items already in the sale.
	if(count($cart) > 0)
	{
	?>
	 <div class="cover_overall_process">
		 
	<div id="finish_sale">
<div  id="sale_details" class="cover_overall_process">
<div class="boxs-cover-sub-and-total" >
				<div class="price-subtotal"><b><?php echo $this->lang->line('sales_sale_for_customer'); ?></b></div>
				<div class="price-subtotal subtotal_total" > <?php
		if(isset($customer))
		{
			echo '<b>'.$customer. '</b>';
			echo anchor("sales/remove_customer",'  ['.$this->lang->line('sales_remove_customer').']');
		}else
		{?>
			<?php echo $this->lang->line('sales_no_customer'); ?>
		<?php	
			
		}?></div>
			</div>
			<div class="boxs-cover-sub-and-total" >
				<div class="price-subtotal"><?php echo $this->lang->line('sales_sub_total'); ?>:</div>
				<div class="price-subtotal subtotal_total" ><?php echo to_currency($subtotal); ?></div>
			</div>
				<?php foreach($taxes as $name=>$value) { ?>
				<div class="descw"><?php echo $name; ?>:</div>
				<div class="descw"><?php echo to_currency($value); ?></div>
				<?php }; ?>
			<div class="boxs-cover-sub-and-total" >
				<div class="price-total"><?php echo $this->lang->line('sales_total'); ?>:</div>
				<div class="price-total"   id="total_total"  ><?php echo to_currency($total); ?></div>
			</div>
			
			<div class="boxs-cover-sub-and-total" id="boxs-cover-recibida">
				<div class="price-subtotal"><?php echo $this->lang->line('sales_amount_tendered'); ?>:</div>
				<div class="price-subtotal subtotal_total" ><?php echo to_currency($payments_total); ?></div>
		   </div>	
		 <div class="boxs-cover-sub-and-total" id="boxs-cover-pago">
		 <div class="price-subtotal"><?php echo $this->lang->line('sales_payment'); ?>:</div>
		  <?php
		  foreach($payments as $payment_id=>$payment)
					{
					if(empty($payment['payment_amount']))
					{$payment['payment_amount']=0;}
					?>
					
				 
		  

				

					
					
				<div class="price-subtotal subtotal_total" ><?php echo $payment['payment_type'].' '.to_currency($payment['payment_amount']); ?></div>
				   	
				<?php
					}
					?>
				</div>
		
		   <?php 
		   $restante=$total;
							
							if($payments_total>=$total){
							$restante=0;
							$cambio=$payments_total-$total;
							}elseif($payments_total<=$total){
							$restante=($total)-($payments_total);
							$cambio=0;
							}
		   if($payments_total < $total ){
		   ?>
		   <div class="boxs-cover-sub-and-total" id="boxs-cover-restante">
					<div class="price-total"><?php echo $this->lang->line('sales_amount_rest'); ?>:</div>
					<div class="price-total" id="total_cambio"><?php
							
					
					

					echo to_currency($restante); ?>
					</div>
			 </div>
		   <?php }
		   else
		   {
		   ?>
			 <div class="boxs-cover-sub-and-total" id="boxs-cover-cambio">
					<div class="price-total"><?php echo $this->lang->line('sales_change_due'); ?>:</div>
					<div class="price-total" id="total_cambio"><?php echo to_currency($cambio); ?></div>
			 </div>
		   <?php }?>
	
	</div>
    	
	<div   id="Payment_Types" >
	<div class="cover_overall_process">
			<?php echo form_open("sales/$func",array('id'=>'giftcards_form')); ?>
			<div class="cover_label_input_ptypes">
			<?php 
				if (isset($card_id))
				{
					echo $card_id;
				}
				
				if (isset($saldo))
				{
					echo "<span>Saldo: <b>".$saldo."<b></span>";
				
					
			
			?>
			<?php
				}
				else
				{?>
					
				
				
				
		<?php	}
				?>
			</div>
			
			
			
        
			</form>

			<?php echo form_open("sales/add_payment",array('id'=>'add_payment_form')); ?>
			<div class="cover_label_input_ptypes">
			
				<span><label id="amount_tendered_label"><?php echo $this->lang->line('sales_amount_tendered').': ';?></span>
			
			
				<?php echo form_input(array('name'=>'amount_tendered','id'=>'amount_tendered','value'=>to_currency_no_money($restante),'size'=>'10'));	?>
			
			</div>
			
			<div class="cover_label_input_ptypes">
			
			<span>
			<?php ?>
				<?php echo $this->lang->line('sales_payment').':   ';?>
			</span>
			
				<?php echo form_dropdown('payment_type',$payment_options,array(), 'id="payment_type"');?>
			
			</div>
			
        
			</form>
			<div class="cover_label_input_ptypes">
			
			
			
		
				
				<div id="cover_btns_venta">
			
				
				<?php
				
				if(!empty($customer_email))
				{
					echo $this->lang->line('sales_email_receipt'). ': '. form_checkbox(array(
					    'name'        => 'email_receipt',
					    'id'          => 'email_receipt',
					    'value'       => '1',
					    'checked'     => (boolean)$email_receipt,
					    )).'<br />('.$customer_email.')<br />';
				}
				
				 
				echo "<div class='btn_ok_venta' id='btn_susp_sale_group'><span>".$this->lang->line('sales_add_payment')."</span></div>";
				echo "<div class='btn_pausar' id='btn_cancel_sale_group'><span>".$this->lang->line('sales_suspend_sale')."</span></div>";
		
				
				?>
				
				</div>
			
			</div>
			
			
			
		
		
			
		
		
		
		</div>
		
			</div>
	<div class="cover_overall_process_2">
			<div class="cover_label_input_ptypes">
			
			
			
			<?php echo form_open("sales/complete",array('id'=>'finish_sale_form')); ?>
				
				
				<div id="cover_btns_venta">
				<?php
				
				if(!empty($customer_email))
				{
					echo $this->lang->line('sales_email_receipt'). ': '. form_checkbox(array(
					    'name'        => 'email_receipt',
					    'id'          => 'email_receipt',
					    'value'       => '1',
					    'checked'     => (boolean)$email_receipt,
					    )).'<br />('.$customer_email.')<br />';
				}
				
				 if($payments_total < $total && isset($customer)){
				echo"<label id='comment_label' for='comment'>[ ".$this->lang->line('sales_coment')." ]</label><label id='sales_add_pago' for='comment'>[ ".$this->lang->line('sales_add_pago')." ]</label>";
				echo form_textarea(array('name'=>'comment', 'id' => 'comment', 'value'=>$comment,'rows'=>'2','cols'=>'23'));
				echo "<div class='btn_credito' id='btn_susp_sale_group'><span>".$this->lang->line('sales_credit_mode')."</span></div>";
				echo "<div class='btn_corregir' id='btn_cancel_sale_group'><span>".anchor("sales/delete_payment/$payment_id",$this->lang->line('sales_change_payment'))."</span></div>";
				 }
				elseif($payments_total >= $total)
				{	echo"<label id='comment_label' for='comment'>[ ".$this->lang->line('sales_coment')." ]</label>";
					echo form_textarea(array('name'=>'comment', 'id' => 'comment', 'value'=>$comment,'rows'=>'2','cols'=>'23'));
					
					echo "<div class='btn_complete' id='btn_susp_sale_group'><span>".$this->lang->line('sales_complete_sale')."</span></div>";
					echo "<div class='btn_corregir' id='btn_cancel_sale_group'><span>".anchor("sales/delete_payment/$payment_id",$this->lang->line('sales_change_payment'))."</span></div>";
				}
				else
				{
				echo"<label id='sales_add_pago' for='comment'>[ ".$this->lang->line('sales_add_pago')." ]</label>";
				echo "<div class='btn_corregir' id='btn_cancel_sale_group'><span>".anchor("sales/delete_payment/$payment_id",$this->lang->line('sales_change_payment'))."</span></div>";
				}
				
				?>
				
			</form>
			</div>
		
		</div>
			
			</div>	

		<?php
		// Only show this part if there is at least one payment entered.
		/*if(count($payments) > 0)
		{*/
		?>
	
		<?php
		// Only show this part if there is at least one payment entered.
		if(count($payments) > 0)
		{
		?>
	    	<div id="small-table-pay">
				
			
				
			</div>
	
		<?php
		}
		if(!isset($customer))
		{	echo'<div class="cover_overall_process_1">';
			echo form_open("sales/select_customer",array('id'=>'select_customer_form')); ?>
			<label class="overall_label"   for="customer"><?php echo $this->lang->line('sales_select_customer'); ?></label>
			<?php echo form_input(array('name'=>'customer','id'=>'input_empieza','size'=>'30','value'=>$this->lang->line('sales_start_typing_customer_name')));?>
			</form>
			<div class="cover_overall_process" id="box-or-btn">
			
			<?php echo anchor("customers/view/-1/width:350",
			"<div class='btn_ok'  id='btn_new_cliente'><span>".$this->lang->line('sales_new_customer')."</span></div>",
			array('class'=>'thickbox none','title'=>$this->lang->line('sales_new_customer')));
			?>
		</div>
			</div>
<?php	}
	
		
	
	?>
		
				
				
	

		
	 </div>

	<?php
	}
	else
	{
	?>
	<div class="cover_overall_process">
   <div class="cover_overall_process_1">
		   <?php
		if(isset($customer))
		{	
			echo $this->lang->line("sales_customer").': <b>'.$customer. '</b><br />';
			echo anchor("sales/remove_customer",'['.$this->lang->line('common_remove').' '.$this->lang->line('customers_customer').']');
			
		}
		else
		{
			echo form_open("sales/select_customer",array('id'=>'select_customer_form')); ?>
			<label class="overall_label"   for="customer"><?php echo $this->lang->line('sales_select_customer'); ?></label>
			<?php echo form_input(array('name'=>'customer','id'=>'input_empieza','size'=>'30','value'=>$this->lang->line('sales_start_typing_customer_name')));?>
			</form>
		
		
		<div class="cover_overall_process" id="box-or-btn">
			
			<?php echo anchor("customers/view/-1/width:350",
			"<div class='btn_ok'  id='btn_new_cliente'><span>".$this->lang->line('sales_new_customer')."</span></div>",
			array('class'=>'thickbox none','title'=>$this->lang->line('sales_new_customer')));
			?>
		</div>
		
		
		<?php
		}?>
	</div>
	</div>
	</div></div>
	
<?php	}
	?>                 


</div></div>

<?php $this->load->view("partial/footer"); ?>


<script type="text/javascript" language="javascript">
$(document).ready(function()
{	
	$(".oculta").css("display","none");
	$("#comment").css("display","none");
	if ( $("#small-table-pay").length > 0 )
	{
		
		$(".cover_overall_process_1").css("display","none");
		giftcards_form
		$(".cover_overall_process_2").css("display","block");
		$("#boxs-cover-pago").css("display","block");
		$("#boxs-cover-recibida").css("display","block");
		$("#boxs-cover-cambio").css("display","block");
		$("#boxs-cover-restante").css("display","block");
		$("#Payment_Types").css("display","none");
		$("#giftcards_form").css("display","none");
		// hacer algo aquí si el elemento existe
	}
	
	$('#sales_add_pago').click(function()
    {
		$("#Payment_Types").css("display","block");
		$(".cover_overall_process_2").css("display","none");
	});
	$('#comment_label').click(function()
    {
		$("#comment").css("display","block");
	});

	$('#mas').click(function()
    {
    	var disp =$(".oculta").css("display");
		//Comparamos si disp esta oculto lo hacemos visible
		if (disp == 'none')
		{
			//Cambia la propiedad css del elemento html con el id encerrado en los primeros parentesis
			$(".oculta").css("display","block");
			$("#mas").css("background-image","url('images/menubar/menos.png')");
			
			
		}//De lo contrario lo ocultamos
		else
		{
			//Cambia la propiedad css del elemento html con el id encerrado en los primeros parentesis
			$(".oculta").css("display","none");
			$("#mas").css("background-image","url('images/menubar/mas.png')");
		}
    });
	
    $("#item").autocomplete('<?php echo site_url("sales/item_search"); ?>',
    {
    	minChars:0,
    	max:50,
    	selectFirst: false,
       	delay:1,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#item").result(function(event, data, formatted)
    {	
		$("#add_item_form").submit();
		$('#item').attr('value','');
    });

	$('#item').focus();

	

	$('#item,#input_empieza').click(function()
    {
    	$(this).attr('value','');
    });

    $("#input_empieza").autocomplete('<?php echo site_url("sales/customer_search"); ?>',
    {
    	minChars:0,
    	delay:0,
    	max:50,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#input_empieza").result(function(event, data, formatted)
    {
		$("#select_customer_form").submit();
    });

    
	
	$('#comment').change(function() 
	{
		$.post('<?php echo site_url("sales/set_comment");?>', {comment: $('#comment').val()});
	});
	
	$('#email_receipt').change(function() 
	{
		$.post('<?php echo site_url("sales/set_email_receipt");?>', {email_receipt: $('#email_receipt').is(':checked') ? '1' : '1'});
	});
	
	
    $(".btn_complete").click(function()
    {
    	
    	$('#finish_sale_form').submit();
    	
    });
	  $(".btn_credito").click(function()
    {
    	if (confirm('<?php echo $this->lang->line("sales_confirm_finish_sale"); ?>'))
    	{
    		$('#finish_sale_form').submit();
    	}
    });
	$(".btn_pausar").click(function()
	{
		if (confirm('<?php echo $this->lang->line("sales_confirm_suspend_sale"); ?>'))
    	{
			$('#finish_sale_form').attr('action', '<?php echo site_url("sales/suspend"); ?>');
    		$('#finish_sale_form').submit();
    	}
	});

    $(".btn_cancel").click(function()
    {
    	if (confirm('<?php echo $this->lang->line("sales_confirm_cancel_sale"); ?>'))
    	{
    		$('#cancel_sale_form').submit();
    	}
    });

	$("#btn_susp_sale_group").click(function()
	{
		
	   $('#add_payment_form').submit();
	   
	   
    });

	$("#payment_type").change(checkPaymentTypeGiftcard).ready(checkPaymentTypeGiftcard)
});
function post_item_form_submit(response)
{
	if(response.success)
	{
		
		$("#item").attr("value",response.item_id);
		
		$("#add_item_form").submit();
		
	}
}

function post_person_form_submit(response)
{
	if(response.success)
	{
		$("#customer").attr("value",response.person_id);
		$("#select_customer_form").submit();
	}
}

function checkPaymentTypeGiftcard()
{
	
	if ($("#payment_type").val() == "<?php echo $this->lang->line('sales_giftcard'); ?>")
	{	
		$("#add_payment_form").css("display","block");
		$("#giftcards_form").css("display","block");
	
		
		$("#amount_tendered_label").html("<?php echo $this->lang->line('sales_giftcard_number'); ?>");
		$("#amount_tendered").val('');
		
		$("#amount_tendered").focus();
	}
	else
	{
		$("#amount_tendered_label").html("<?php echo $this->lang->line('sales_amount_tendered'); ?>");	
		
	}
}

</script>



