<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>

<?php
echo form_open_multipart('item_kits/save/'.$item_kit_info->item_kit_id,array('id'=>'kit_form'));
?>
<fieldset id="item_kit_info">
<legend class="name-forms-popup"><?php echo $item_kit_info->name; ?></legend>



<div class="field_row clearfix">
  <?php  echo form_label($this->lang->line('items_item_number').':', 'name',array('class'=>'wide'));?>  
    <div class='form_field'>
	<?php echo form_input(array(
		'name'=>'kit_number',
		'id'=>'kit_number',
		'value'=>$item_kit_info->kit_number)
        );?>
	</div>
</div>


<div class="field_row clearfix">
<?php echo form_label($this->lang->line('item_kits_name').': <div id="error_name_message_box" class="wide required"></div>', 'name',array('class'=>'wide required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'kit_name',
		'id'=>'kit_name',
		'value'=>$item_kit_info->name)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('item_kits_category').':  <div id="error_category_message_box" class="wide required"></div>', 'name',array('class'=>'wide required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'category',
		'id'=>'category',
		'value'=>$item_kit_info->category)
	);?>
	</div>
</div>

<div class="field_row clearfix">
  <?php  echo form_label($this->lang->line('item_kits_unit_price').':  <div id="error_price_message_box" class="wide required"></div>', 'kit_price',array('class'=>'wide required'));?>  
    <div class='form_field'>
	<?php echo form_input(array(
		'width'=>'30px',
        'name'=>'kit_price',
		'id'=>'kit_price',
		'value'=>$item_kit_info->kit_price)
                
        );?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('item_kits_expire').':', 'fecha',array('class'=>'wide')); ?>
	<div id='report_date_range_complex'>
		
		<div class="box-inputs-rep">
		<?php echo form_dropdown('month',$months, $selected_month, 'id="month"'); ?>
		<?php echo form_dropdown('day',$days, $selected_day, 'id="day"'); ?>
		<?php echo form_dropdown('year',$years, $selected_year, 'id="year"'); ?>
		
		</div>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('item_kits_description').':', 'description',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>$item_kit_info->description,
		'rows'=>'5',
		'cols'=>'17')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('item_kits_imagesT'), 'uploadfile[]',array('class'=>'wide')); ?>
	<div class='form_field'>
	 <?php echo form_upload(array(
		'multiple'=>'multiple',
		'accept'=>'gif|jpg|png',
		'class'=>'form-control',
		'name'=>'uploadfile[]',
		'id'=>'btn-uploadfile',
		'size'=>'18')
	 );?>
	
	<img src="<?php echo base_url();?>images/menubar/forms/thun_paq.jpg"/>
	
</div>
</div>
<div class="field_row_checkbox clearfix">
<?php echo form_label($this->lang->line('items_publish').':', 'is_serialized',array('class'=>'wide')); ?>

	<?php echo form_checkbox(array(
		'name'=>'publicar',
		'id'=>'publicar',
		 'class' => 'checkbox_form',
		'value'=>1,
		'checked'=>($item_kit_info->publicar)? 1 : 0)
	);?>
	
</div>



<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>


//validation and enviar handling
$(document).ready(function()
{
	
	$("#category").autocomplete("<?php echo site_url('item_kits/suggest_category');?>",{max:100,minChars:0,delay:10});
    $("#category").result(function(event, data, formatted){});
	$("#category").search();
	$('#kit_formw').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_item_kit_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_name_message_box #error_category_message_box #error_price_message_box",
 		wrapper: "span",
		rules:
		{
			kit_name:"required",
			category:"required",
			kit_price:"required"
		},
		messages:
		{
			kit_name:"<?php echo $this->lang->line('item_kits_name_required'); ?>",
			category:"<?php echo $this->lang->line('item_kits_category_required'); ?>",
			kit_price:"<?php echo $this->lang->line('item_kits_price_required'); ?>"
		}
	});
});


</script>