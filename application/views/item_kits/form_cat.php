<?php
echo form_open_multipart('item_kits/upload_cat_images/'.$categoria,array('id'=>'form_cat'));
?>
<fieldset id="item_kit_info">
<legend class="name-forms-popup"><?php echo $categoria; ?></legend>









<div class="field_row clearfix">
<?php echo form_label($this->lang->line('item_kits_imagesBC').'', 'uploadfile[]',array('class'=>'wide')); ?>
	<div class='form_field'>
	 <?php echo form_upload(array(
		'multiple'=>'multiple',
		'accept'=>'gif|jpg|png',
		'class'=>'form-control',
		'name'=>'uploadfile[]',
		'id'=>'btn-uploadfile',
		'size'=>'18')
	 );?>
	<img src="<?php echo base_url();?>images/menubar/forms/ban_cat.jpg"/>
</div>
</div>



<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>


//validation and enviar handling
$(document).ready(function()
{
	
	$("#category").autocomplete("<?php echo site_url('item_kits/suggest_category');?>",{max:100,minChars:0,delay:10});
    $("#category").result(function(event, data, formatted){});
	$("#category").search();
	$('#kit_formw').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_item_kit_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_name_message_box #error_category_message_box #error_price_message_box",
 		wrapper: "span",
		rules:
		{
			kit_name:"required",
			category:"required",
			kit_price:"required"
		},
		messages:
		{
			kit_name:"<?php echo $this->lang->line('item_kits_name_required'); ?>",
			category:"<?php echo $this->lang->line('item_kits_category_required'); ?>",
			kit_price:"<?php echo $this->lang->line('item_kits_price_required'); ?>"
		}
	});
});


</script>