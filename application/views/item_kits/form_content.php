<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>

<?php
echo form_open_multipart('item_kits/edit_content/'.$item_kit_info->item_kit_id,array('id'=>'items_kit_form'));
?>
<fieldset id="item_basic_info" class="fieldset_form_popup">
<legend class="name-forms-popup"><?php echo $item_kit_info->name; ?></legend>

<?php
echo form_hidden('name_lang',url_title($item_kit_info->name, '_'));
// echo form_hidden('current_idiom',"asd );

 foreach($lang as $clave =>$campo) { 
 
$vals_lang=explode('_', $clave);
$format_lang=$vals_lang[2];
$field =explode('.', $campo);

?>
<div class="field_row clearfix">
<?php echo form_label(ucwords($format_lang), 'name',array('class'=>'wide')); ?>
<div class='form_field'>
 <?php 	echo form_textarea(array(
		'name'=>$clave,
		'id'=>$clave,
		'value'=>$campo,
		'rows'=>count($field),
		'cols'=>'17'));
		?>
	</div>
</div>

<?php } ?>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('item_kits_imagesBP').'', 'uploadfile[]',array('class'=>'wide')); ?>
	<div class='form_field'>
	 <?php echo form_upload(array(
		'multiple'=>'multiple',
		'accept'=>'gif|jpg|png',
		'class'=>'form-control',
		'name'=>'uploadfile[]',
		'id'=>'btn-uploadfile',
		'size'=>'18')
	 );?>
	<img src="<?php echo base_url();?>images/menubar/forms/ban_paq.jpg"/>
</div>
</div>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
/*
var i=0;
$("#item").autocomplete('<?php echo site_url("items/item_search"); ?>',
{
	minChars:0,
	max:100,
	selectFirst: false,
   	delay:10,
	formatItem: function(row) {
		return row[1];
	}
});

$("#item").result(function(event, data, formatted)
{
	
	$("#item").val("");
	i++;
	if ($("#item_kit_item_"+data[0]).length ==1)
	{
		$("#item_kit_item_"+data[0]).val(parseFloat($("#item_kit_item_"+data[0]).val()) + 1);
	}
	else
	{
		
		
		$("#item_kit_items").append("<tr><td><a href='#' onclick='return deleteItemKitRow(this);'>X</a></td><td>"+data[1]+"</td><td><input class='quantity' id='item_kit_item_"+data[0]+"' type='text' size='3' name=item_kit_item["+data[0]+"] value='1'/></td></tr>");
		
		
	}
});


//validation and submit handling
$(document).ready(function()
{
	$('#items_kit_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_items_kit_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			
			
		},
		messages:
		{
			
			
		}
	});
});

function deleteItemKitRow(link)
{
	$(link).parent().parent().remove();
	return false;
}*/
</script>