<!DOCTYPE html>
<html lang="es">
<head> 
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Author" content="Adrián Ramirez | brontobytemx.com" />
<meta name="Subject" content="Diseño web Mexico" />
<meta name="GOOGLEBOT" content="INDEX, FOLLOW, ALL" />
<meta name="robots" content="index, follow" />
<meta name="GOOGLEBOT" content="NOARCHIVE" />
<meta name="Generator" content="html" />
<meta name="Language" content="Spanish"/>
<meta name="Revisit" content="1 day" />
<meta name="Distribution" content="Global" />
<meta name="Robots" content="All" />
	<meta property="og:url"           content="" />
    <meta property="og:type"          content="" />
    <meta property="og:title"         content="" />
    <meta property="og:description"   content="" />
    <meta property="og:image"         content="" />

<title>TravelPro | Home</title>
<meta name="Description" content=""/>
<meta name="Keywords" content="" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/style-header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/style-all.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/newcss/bootstrap.css">


<!--web-font-->
<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,600,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Cabin:400,600,500' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=BenchNine" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Crete+Round" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oleo+Script" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Suez+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans|Open+Sans+Condensed:300" rel="stylesheet">






<!--//web-font-->

<script src="http://code.jquery.com/jquery.js"></script>
 
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/dispositivos.css"/>

<link rel="shortcut icon" href="<?php echo base_url();?>images/ico/favicon.ico">

 

</head>
<body>
	<div class="wrapper w-sections" id="w-paque">

	<!--navigation-->
	<?php $this->load->view("header");?>
	<!--navigation-->
	
		<div id="sb-site" class="box-contenido">
				<div class="banner-section">
					<img class="img-paquete-detail" src="<?php echo base_url();?>images/paquetes/banner-img-paquetes/banner_detail_x.jpg"/>
					<div class="cover-subtitles">
						<h2 class="subtitles-bg">Mega Italia Promocional</h2>
						<div id="nav-sub"><a href="<?php echo base_url();?>">PAQUETES /</a> <a href="">CATEGORIA /</a> <span>MEGA ITALIA PROMOCIONAL</span></div>
					</div>
				</div>
				<div class="cover_standar">
				
				<section id="section-paquete" >
					
					
				
					<article class="box-detail-paquete">
						<img class="img-paquete-detail" src="<?php echo base_url();?>images/paquetes/paquetes-img-details/img_detail_x.jpg">
						<div id="cover-volamos-con">
							<span id="span-volamos">Volamos con:</span>
							<img id="logo-linea-viaje" src="<?php echo base_url();?>images/paquetes/xtras/british-airways-logo.png">
						</div>
						<div class="cover-iconos-paquete">
						<div class="box-iconos-paq">
							<img src="<?php echo base_url();?>images/ico/ico-ubica-travel-blue.png">
							<span>Ciudades visitadas: Venecia – Florencia – Roma, Venecia – Florencia – Roma</span>
						</div>
						<div class="box-iconos-paq">
							<img src="<?php echo base_url();?>images/ico/ico-time-travel-blue.png">
							<span>Duración: 8 días y 6 noches.</span>
						</div>
						<div class="box-iconos-paq">
							<img src="<?php echo base_url();?>images/ico/ico-price-travel-blue.png">
							<span>Costo: 999.00 USD</span>
						</div>
						
					
						</div>
					</article>
					<span id="txt-vigencia"><b>Vigencia:</b> 26 de noviembre del 2016</span>
				
					<div class="box-detail-paquete" id="box-detail-paquete-complete" >
					<h4 class="subtitles-paquetes-in">ITINERARIO:</h4>
					<span id="word_itinerario">*ITINERARIO SUJETO A CAMBIO</span>
					<span class="sub-in-paq">26 NOV	MÉXICO – LONDRES</span>
					<p>
						Presentarse en TERMINAL No. 1 del aeropuerto de la ciudad de México 3hrs. 
						antes de la salida del vuelo trasatlántico de BRITISH AIRWAYS No. 242 con salida A LAS 21:00 HRS, con destino a Londres. Noche a bordo. 
					</p>
					<span class="sub-in-paq">27 NOV	LONDRES – VENECIA </span>
					<p>
					Llegada a Londres A LAS 13:05 HRS conexión con el vuelo de BRITISH AIRWAYS No. 598 con salida A LAS 18:45 HRS, con destino a Venecia. Llegada A LAS 21:55 HRS. Traslado al hotel. Alojamiento. 
					</p>
					<span class="sub-in-paq">28 NOV	VENECIA</span>
					<p>
					Desayuno. Día libre para actividades personales. Alojamiento. 
					</p>
					<span class="sub-in-paq">29 NOV	VENECIA – FLORENCIA </span>
					<p>
					Desayuno y visita panorámica a pie de esta singular ciudad construida sobre 118 islas con románticos puentes y canales para conocer la Plaza de San Marcos con el Campanario y el Palacio Ducal, el famoso Puente de los Suspiros. Tiempo libre y posibilidad de realizar un paseo opcional en Góndola. Posteriormente salida hacia Florencia, capital de la Toscana y cuna del Renacimiento. Alojamiento.
					</p>
			
					</div>		
						
<div id="table_info_paquetes">
<div class="cover_filas_paq_titles">
	<div class="box-conten-paq" >
		<span class="resalt-table">CATEGORIA</span>
	</div>
	<div class="box-conten-paq" >
		<span>TURISTA 3*</span>
	</div>
	<div class="box-conten-paq" >
		<span>PRIMERA 4*</span>
	</div>
	<div class="box-conten-paq" >
		<span>LUJO 5*</span>
	</div>
</div>
<div class="cover_filas_paq">
	<div class="box-conten-paq" >
		<span class="resalt-table">DOBLE</span>
	</div>
	<div class="box-conten-paq" >
		<span></span>
	</div>
	<div class="box-conten-paq" >
		<span></span>
	</div>
	<div class="box-conten-paq" >
		<span></span>
	</div>
</div>



</div>						
		
		
				</section>
				<div  id="box-aside-paquetes">
					<aside id="aside-paquetes">
						<h3>CATEGORIAS</h3>
						<ul id="nav-aside">
							<li><a href="">OFERTAS</a></li>
							<li><a href="">EUROPA</a></li>
							<li><a href="">SUDAMÉRICA</a></li>
							<li><a href="">ESTADOS UNIDOS</a></li>
							<li><a href="">MEDIO ORIENTE</a></li>
							<li><a href="">CENTROAMERICA</a></li>
							<li><a href="">CARIBE</a></li>
							<li><a href="">CANADÁ</a></li>
							<li><a href="">ASIA</a></li>
							<li><a href="">PACIFICO</a></li>
							<li><a href="">ÁFRICA</a></li>
							<li><a href="">NACIONALES</a></li>
							<li><a href="">EVENTOS ESPECIALES</a></li>
						</ul>
					</aside>
				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view("footer");?>
	
	<!--footer-->
	<!-- bootstrop-->
  <script src="<?php echo base_url();?>js/fjs/bootstrap.js"></script>
     <script>
      !function ($) {
        $(function(){
          // carousel demo
          $('#myCarousel').carousel()
        })
      }(window.jQuery)
    </script>
 <!-- bootstrop-->
	
</body>
</html>