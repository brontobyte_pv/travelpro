<!DOCTYPE html>
<html lang="es">

<head>
<meta charset="UTF-8">
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="noindex, follow" />
<meta name="description" content="Venta de hardware y Reparación de equipo computacional, Configuraci&oacute;n de Redes, C&aacute;maras de Seguridas... Reparaci&oacute;n de Computadoras en Puerto Vallarta">
<meta name="keywords" content="Venta de hardware y Reparaci&oacute;n Computadoras en Puerto Vallarta, Configuración de Redes y C&aacute;maras de Seguridas, Refacciones de Computadoras, cargadores para laptop accesorios para celulares , venta de computadoras, Instalaci&oacute;n de programas, Intalaci&oacute;n de Aplicaciones">
<meta name="author" content="Brontobyte Computaci&oacute;n">

<title>Cat&aacute;logo de Productos</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style-header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style-contenido.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/responsive.css" />
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
</head>

<body>
<div class="wrapper" itemscope itemtype="http://schema.org/LocalBusiness" >
<?php $this->load->view("header"); ?>
	<section itemscope itemtype="http://schema.org/Store" class="contenido-secciones" id="seccion-catal" >
	<?php echo form_open('registro/login') ?>
<div id="container">
<?php echo validation_errors(); ?>
	<div id="top">
	<span>Inicia sesion antes de comprar.</span> <a href="<?php echo base_url();?>index.php/registro/">Login</a><br>
	<span>Si no aun no estas registrado da click en </span> <a href="<?php echo base_url();?>index.php/registro/form">Registro</a><br>
		<span>No recuerdas tu contraseña? </span> <a href="<?php echo base_url();?>index.php/registro/recuperar">Recuperar</a><br>
	</div>
	<div id="login_form">
		
		
		<div class="form_field_label"><?php echo $this->lang->line('login_username'); ?>: </div>
		<div class="form_field">
		<?php echo form_input(array(
		'name'=>'username', 
		'value'=> $_SERVER['HTTP_HOST'] == 'ospos.pappastech.com' ? 'admin' : '',
		'size'=>'20')); ?>
		</div>

		<div class="form_field_label"><?php echo $this->lang->line('login_password'); ?>: </div>
		<div class="form_field">
		<?php echo form_password(array(
		'name'=>'password', 
		'value'=>$_SERVER['HTTP_HOST'] == 'ospos.pappastech.com' ? 'pointofsale' : '',
		'size'=>'20')); ?>
		
		</div>
		
		<div id="submit_button">
		<?php echo form_submit('loginButton','Go'); ?>
		</div>
	</div>
</div>
<?php echo form_close(); ?>
				
	</section>
<?php include('footer.php'); ?>
</div>

</body>