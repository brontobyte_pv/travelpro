
<!DOCTYPE html>
<html>
<head>
<title>Travelpro | detalles</title>
<link href="<?php echo base_url();?>css/fcss/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="<?php echo base_url();?>css/fcss/style.css" type="text/css" rel="stylesheet" media="all">
<!--web-font-->
<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
<!--//web-font-->
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Excursion Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //Custom Theme files -->
<!-- js -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>  
<!-- //js -->	
<!-- start-smoth-scrolling-->
<script type="text/javascript" src="<?php echo base_url();?>js/fjs/move-top.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/fjs/easing.js"></script>	
<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
</script>
<!--//end-smoth-scrolling-->
</head>


<body>
	
	<!--navigation-->
	<?php $this->load->view("header");?>
	<!--navigation-->

	
	<!--single-page-->
	<div class="single-page">
		<div class="container">
			<div class="work-title sngl-title">
				<ol class="breadcrumb">
					<li><a href="index.html">Inicio</a></li>
					<li class="active">Costa Rica</li>
				</ol>
			</div>
			<div class="col-md-8 single-page-left">
				<div class="single-page-info">
					<img src="images/costa1.jpg" alt=""/>
					<h5>COSTA RICA FIN DE AÑO</h5>
					<p>08 DIAS 07 NOCHES
                   AVION CHARTER VIAJE REDONDO DESDE LA CIUDAD DE MEXICO TRASLADOS ALOJAMIENTO VISITA DE CIUDAD CON ALMUERZOS EXCURSION AL VOLCAN ARENAL CON TERMAS ALMUERZOS Y CENAS.</p>  
					<h5>
                    DESDE $865 USD</h5>
					<img src="images/logo1.jpg" alt=""/> 
					<img src="images/costa2.jpg" alt=""/>
				</div>	
				<!--related-posts-->
				
				<!--//related-posts-->
				<br> <br>
				<div class="coment-form">
					<h4>QUIERO RESERVAR 01 462 1142525</h4>
					<form>
						<input type="text" value="Name " onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}" required="">
						<input type="email" value="Email (will not be published)*" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email (will not be published)*';}" required="">
						<input type="text" value="Website" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Website';}" required="">
						<textarea type="text"  onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Your Comment...';}" required="">Your Comment...</textarea>
						<input type="submit" value="Submit Comment" >
					</form>
				</div>		
			</div>	
			<div class="col-md-4 single-page-right">
				<div class="category">
					<h4>Escoge tu destino favorito!!</h4>
					<div class="list-group">
						<a href="singlepage.html" class="list-group-item">Ofertas</a>
						<a href="singlepage.html" class="list-group-item">Europa</a>
						<a href="singlepage.html" class="list-group-item">Sudamérica</a>
						<a href="singlepage.html" class="list-group-item">Estados Unidos</a>
						<a href="singlepage.html" class="list-group-item">Medio Oriente</a>
						<a href="singlepage.html" class="list-group-item">Centoamérica</a>
						<a href="singlepage.html" class="list-group-item">Cribe</a>
						<a href="singlepage.html" class="list-group-item">Canadá</a>
						<a href="singlepage.html" class="list-group-item">Asia</a>
						<a href="singlepage.html" class="list-group-item">Pacífico</a>
						<a href="singlepage.html" class="list-group-item">África</a>
						<a href="singlepage.html" class="list-group-item">Nacionales</a>
						<a href="singlepage.html" class="list-group-item">Eventos Especiales</a>
						<a href="singlepage.html" class="list-group-item">Cruceros</a>
					</div>
				</div>	
				
				
				
	
			</div>
			<div class="clearfix"> </div>
		</div>	
	</div>
	<!--//single-page-->
	
	<!--footer-->
	
	<?php $this->load->view("footer");?>
	
	<!--footer-->
	
		<script type="text/javascript">
			$(document).ready(function() {
				/*
				var defaults = {
					containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
				};
				*/
				
				$().UItoTop({ easingType: 'easeOutQuart' });
				
			});
		</script>
		<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<!--//smooth-scrolling-of-move-up-->
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.js"></script>
</body>
</html>