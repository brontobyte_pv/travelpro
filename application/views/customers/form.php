<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul><?php
echo form_open('customers/save/'.$person_info->person_id,array('id'=>'customer_form'));
?>

<fieldset id="customer_basic_info" class="fieldset_form_popup">
<legend class="name-forms-popup"><?php echo $this->lang->line("customers_basic_information"); ?></legend>
<div class="field_row clearfix">	
<?php echo form_label($this->lang->line('customers_company').':'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'company',
		'id'=>'company',
		'value'=>$person_info->company)
	);?>
	</div>
</div>
<?php $this->load->view("people/form_basic_info"); ?>
<div class="field_row clearfix">	
<?php echo form_label($this->lang->line('customers_account_number').':', 'account_number'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'account_number',
		'id'=>'account_number',
		'value'=>$person_info->account_number)
	);?>
	</div>
</div>

<div class="field_row_checkbox clearfix">	
<?php echo form_label($this->lang->line('customers_taxable').':', 'taxable'); ?>
	
	<?php echo form_checkbox(array(
    'name'        => 'taxable',
    'class'          => 'checkbox_form',
    'value'       => '1',
    'checked'     => $person_info->taxable == '' ? TRUE : (boolean)$person_info->taxable)
    );
	?>
	
</div>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button')
);
?>
</fieldset>
<?php 
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	$('#customer_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_person_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			first_name: "required",
			last_name: "required",
    		
			
   		},
		messages: 
		{
     		first_name: "<?php echo $this->lang->line('common_first_name_required'); ?>",
     		last_name: "<?php echo $this->lang->line('common_last_name_required'); ?>",
     		
		}
	});
});
</script>