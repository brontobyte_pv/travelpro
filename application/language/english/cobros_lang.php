<?php
$lang['cobros_new']='Nuevo Cobro';
$lang['cobros_date']='Fecha';
$lang['cobros_error_cliente']='Selecciona un cliente antes de hacer un cobro';
$lang['cobros_cliente']='Cliente';
$lang['cobros_cliente_nuevo']='Nuevo cliente';
$lang['cobros_selecciona_cliente']='1.-Selecciona un cliente';
$lang['cobros_cuentas_pendientes']='Historial de pagos';
$lang['cobros_successful_adding']='Cobro Agregado o editado correctamente';
$lang['cobros_error_adding']='Error agregando o editando Cobro';
$lang['cobros_confirm_delete']='¿Estás seguro(a) de querer borrar este(os) cobro(s)? Esta acción no se puede deshacer.';
$lang['cobros_none_selected']='No has seleccionado ningun cobro para borrar';
?>