<?php
$lang["contenido_172_ciudades"]="";
$lang["contenido_172_incluye"]="";
$lang["contenido_172_lugares"]="";
$lang["contenido_172_actividades"]="";
$lang["contenido_172_recomendaciones"]="";
$lang["contenido_172_observaciones"]="";
$lang["contenido_172_salida_vuelo"]="";
$lang["contenido_1_ciudades"]="Puerto Vallarta - Sayulita - Nuevo Vallarta";
$lang["contenido_1_incluye"]=" Alojamiento-Vuelo-Desayunos";
$lang["contenido_1_lugares"]="Venecia – Florencia – Roma, Venecia – Florencia – Roma";
$lang["contenido_1_actividades"]="Desayuno y visita panorámica a pie de esta singular ciudad construida sobre 118 islas con románticos puentes y canales para conocer la Plaza de San Marcos con el Campanario y el Palacio Ducal, el famoso Puente de los Suspiros. Tiempo libre y posibilidad de realizar un paseo opcional en Góndola. Posteriormente salida hacia Florencia, capital de la Toscana y cuna del Renacimiento.";
$lang["contenido_1_recomendaciones"]="Acudir a buscadores online que nos permitan comparar precios, distancias, estrellas y servicios entre distintos hoteles de la ciudad. ";
$lang["contenido_1_observaciones"]="*No niños";
$lang["contenido_1_salida_vuelo"]="Presentarse en TERMINAL No. 1 del Aeropuerto Internacional Días Ordaz de la ciudad de Puerto Vallarta Jal 3hrs. antes de la salida del vuelo trasatlántico de BRITISH AIRWAYS No. 242 con salida A LAS 21:00 HRS, con destino a Londres. Noche a bordo.";
$lang["contenido_173_ciudades"]="";
$lang["contenido_173_incluye"]="";
$lang["contenido_173_lugares"]="";
$lang["contenido_173_actividades"]="";
$lang["contenido_173_recomendaciones"]="";
$lang["contenido_173_observaciones"]="";
$lang["contenido_173_salida_vuelo"]="";
$lang["contenido_189_ciudades"]="";
$lang["contenido_189_incluye"]="";
$lang["contenido_189_lugares"]="";
$lang["contenido_189_actividades"]="";
$lang["contenido_189_recomendaciones"]="";
$lang["contenido_189_observaciones"]="";
$lang["contenido_189_salida_vuelo"]="";
$lang["contenido_190_ciudades"]="";
$lang["contenido_190_incluye"]="";
$lang["contenido_190_lugares"]="";
$lang["contenido_190_actividades"]="";
$lang["contenido_190_recomendaciones"]="";
$lang["contenido_190_observaciones"]="";
$lang["contenido_190_salida_vuelo"]="";
?>