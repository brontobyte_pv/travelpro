<?php


$lang['login_correo']='E-mail';
$lang['login_nombre']='Name';
$lang['login_password_confirm']='Password Confirm';
$lang['login_login']='Login';
$lang['login_username']='Username';
$lang['login_password']='Password';
$lang['login_go']='Go';
$lang['login_invalid_username_and_password']='Invalid username/password';
$lang['login_welcome_message']='Welcome to the Open Source Point of Sale System. To continue, please login using your username and password below.';
?>
