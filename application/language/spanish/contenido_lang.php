<?php
$lang["contenido_1_ciudades"]="Argentina";
$lang["contenido_1_incluye"]="Boleto de avión México – Buenos Aires – Bariloche – Buenos Aires – México.<br />
03 noches de alojamiento en Buenos Aires con desayuno incluido.<br />
02 noches de alojamiento en Bariloche con desayuno incluido.<br />
02 noches de alojamiento en Isla Victoria con pensión completa.(Las habitaciones no poseen teléfono, televisor, ni otro artefacto eléctrico)<br />
Traslados de llegada y salida.<br />
Visitas indicadas en el itinerario<br />
Guía de habla hispana.<br />
Bolsa de viaje";
$lang["contenido_1_lugares"]="Buenos Aires – Bariloche - Isla Victoria";
$lang["contenido_1_actividades"]="Boleto de avión México – Buenos Aires – Bariloche – Buenos Aires – México.<br />
03 noches de alojamiento en Buenos Aires con desayuno incluido.<br />
02 noches de alojamiento en Bariloche con desayuno incluido.<br />
02 noches de alojamiento en Isla Victoria con pensión completa.(Las habitaciones no poseen teléfono, televisor, ni otro artefacto eléctrico)<br />
Traslados de llegada y salida.<br />
Visitas indicadas en el itinerario<br />
Guía de habla hispana.<br />
Bolsa de viaje";
$lang["contenido_1_recomendaciones"]="Reserva Experiencias";
$lang["contenido_1_observaciones"]="El viaje NO incluye:<br />
Tarjeta de Asistencia Turística: Costo por persona usd 27.00<br />
Ningún servicio no especificado.<br />
Gastos personales.<br />
IMPUESTOS aéreos pagaderos en México: USD 540.00 (sujetos a cambio)";
$lang["contenido_1_salidaVuelo"]="Salidas diarias";
$lang["contenido_1_metaPalabrasUno"]="";
$lang["contenido_1_metaPalabrasDos"]="";
$lang["contenido_2_ciudades"]="Estados Unidos";
$lang["contenido_2_incluye"]="Vuelo redondo Clase Turista MEXICO-ORLANDO-MEXICO<br />
Traslados de llegada y salida en servicio regular con asistencias (APTO-HTL-APTO)<br />
07 Noches de alojamiento en el Hotel seleccionado<br />
06 días Pase Básico para los parques de: Disney´s Animal Kigdome, Disney´s Magic Kindom, Epcot & Disney´s Hollywood Studios.<br />
Guía de Viaje con Mapa<br />
Cuponera de descuentos para compras<br />
Maletín de Viaje e identificador de maletas.<br />
ORLANDO MARAVILLOSO MAS UNIVERSAL ESTUDIOS<br />
Vuelo redondo Clase Turista MEXICO-ORLANDO-MEXICO<br />
Traslados de llegada y salida en servicio regular con asistencias<br />
07 Noches de alojamiento en el Hotel seleccionado<br />
04 Días Pase Básico para los parques de: Disney´s Animal Kigdome, Disney´s Magic Kindom, Epcot & Disney´s Hollywood Studios.<br />
02 Días de pase para visitar: Isla de la Aventura & Estudios Universales. Con traslados<br />
Guía de Viaje con Mapa<br />
Cuponera de descuentos para compras<br />
Maletín de Viaje e identificador de maletas.";
$lang["contenido_2_lugares"]="Orlando";
$lang["contenido_2_actividades"]="Días libres para disfrutar de los maravillosos parques de Disney.<br />
Si usted se hospeda en hoteles Disney´s puede hacer uso de la trasportación gratuita entre su hotel y los 4 parques temáticos:";
$lang["contenido_2_recomendaciones"]="Reserva Experiencias";
$lang["contenido_2_observaciones"]="";
$lang["contenido_2_salidaVuelo"]="08, 09 Y 15 abril 2017";
$lang["contenido_2_metaPalabrasUno"]="";
$lang["contenido_2_metaPalabrasDos"]="";
$lang["contenido_3_ciudades"]="Estados Unidos";
$lang["contenido_3_incluye"]="Vuelo redondo clase Turista, (vuelos con conexión).<br />
07 Noches de alojamiento como se especifica en el programa.<br />
Desayuno Americano *** Excepto en Boston y New York.<br />
Traslados Aeropuerto-Hotel-Aeropuerto.<br />
Vehículo con aire acondicionado.<br />
Guía hispano (Castellano).<br />
Visita de ciudades Niágara Falls, Boston, Newport. New York, Washington DC.";
$lang["contenido_3_lugares"]="New York – Newport – Boston – Niagara Falls – Washington Dc-Washington Dc – Niagara Falls – Boston – Newport – New York";
$lang["contenido_3_actividades"]="Sugerimos la excursión a la Estatua de la Libertad y el Museo de la Inmigración de Ellis Island (no incluida), ambos a corta distancia de Battery Park por ferry. Para ver el interior de la Estatua de la Libertad es necesario hacer la reserva en la taquilla principal. Las reservas se tendrán que realizar con anticipación y tienen un costo. <br />
Ó bien se recomienda visite alguna de las siguientes atracciones (no incluidas): Empire State, Museo de Historia Natural, Museo de arte Moderno (MoMA), Museo de la Ciudad, Museo Metropolitano de Arte, o bien, recorrer central park, la famosa 5ta avenida y Park Avene, no olvide ser participe conocedor de las espectaculares tiendas en Nueva York.";
$lang["contenido_3_recomendaciones"]="Reserva Experiencias";
$lang["contenido_3_observaciones"]="IMPUESTOS $355 USD por persona.<br />
Alimentos, visitas y servicios no especificados en el programa.<br />
Gastos de índole personal.<br />
Desayunos en Boston y New York";
$lang["contenido_3_salidaVuelo"]="08,15 ABRIL 2017<br />
01,08,15,22,29 JULIO 2017";
$lang["contenido_3_metaPalabrasUno"]="";
$lang["contenido_3_metaPalabrasDos"]="";
$lang["contenido_4_ciudades"]="CANADA";
$lang["contenido_4_incluye"]="*Vuelo redondo México -Toronto// Montreal-México 
*7 noches de alojamiento en hoteles categoría turista 
*Traslado aeropuerto – hotel- aeropuerto (Vehículo con a/c) 
*Guía hispano (Castellano). 
*Desayunos Continentales 
*Visitas de Ciudades (Toronto, Niagara, Montreal, Quebec, Ottawa) 
*Visita a Mont Tremblant, Túneles escénicos, Visita al viñedo 
*Actividades descritas como incluidas 
*Bolsa de viaje";
$lang["contenido_4_lugares"]="Toronto - Niagara - Mil Islas - Ottawa - Quebec - Montreal";
$lang["contenido_4_actividades"]="Desayuno en el hotel.
Visita panorámica de Toronto.
Salida hacia la Capital Nacional de Canadá.
Salida hacia Montreal, gran urbe canadiense que después de París es la ciudad más grande de habla francesa del mundo y en la cual tendrá la oportunidad de ver su Estadio Olímpico, sede de los juegos olímpicos de 1976.
Visita a pie de la ciudad subterránea, con modernos pasajes a través de los cuales se llega a variados comercios, restaurantes, cines y tiendas de todo tipo.";
$lang["contenido_4_recomendaciones"]="CONSULTAR ITINERARIO POR CADA FECHA DE SALIDA ALGUNAS SALIDAS PUEDEN TENER VARIACIONES.
Salida desde Montreal";
$lang["contenido_4_observaciones"]="No Incluye 
*IMPUESTOS $359 USD 
*Tarjeta Assist Card (Consultar planes y coberturas de seguro de asistencia) 
*Ningún servicio no especificado 
*Gastos personales 
*Propinas";
$lang["contenido_4_salidaVuelo"]="Air Canada";
$lang["contenido_4_metaPalabrasUno"]="";
$lang["contenido_4_metaPalabrasDos"]="";
$lang["contenido_5_ciudades"]="Canada";
$lang["contenido_5_incluye"]="Vuelo redondo MÉXICO – YYZ / YUL – MÉXICO 
Hospedaje 
Admisión al barco de Mil Islas - Opera de mayo 1 a octubre 31 - Fuera de temporada se visita el Museo de la Civilización en Ottawa 
8 Desayunos Americanos 
Almuerzo en Tadoussac 
Admisión al Safari fotográfico de las ballenas 
Hornblower Niagara Opera de Mayo a octubre - Fuera de temporada será substituido por los túneles escénicos. 
Traslados de llegada y salida";
$lang["contenido_5_lugares"]="Toronto – Niagara Falls – Mil Islas – Ottawa – Mont Tremblant – Quebec – Montreal – Charlevoix - Montreal";
$lang["contenido_5_actividades"]="Desayuno Americano. 07:00 hrs encuentro en el lobby del hotel para desayunar y conocer al resto del grupo. Iniciamos con una visita panorámica de la ciudad: la alcaldía, el parlamento provincial, la universidad de Toronto, el barrio bohemio de Yorkville y el barrio donde se encuentra el estadio de Baseball y la torre CN (Torre auto portante más alta del mundo).
Visitaremos Table Rock con su inolvidable panorámica. 
Durante nuestro viaje a la capital federal de Canadá tomaremos el auto ruta Transcanadiense, haremos una parada para tomar un crucero que recorre esta hermosa región de veraneo del archipiélago de Mil Islas donde el lago Ontario se transforma en el rio San Lorenzo.";
$lang["contenido_5_recomendaciones"]="(Nota: el horario de entrada a las habitaciones es después de las 16:00 hrs.)";
$lang["contenido_5_observaciones"]="";
$lang["contenido_5_salidaVuelo"]="VUELO INCLUIDO";
$lang["contenido_5_metaPalabrasUno"]="";
$lang["contenido_5_metaPalabrasDos"]="";
$lang["contenido_6_ciudades"]="Inglaterra – Francia";
$lang["contenido_6_incluye"]="BOLETO DE AVIÓN MÉXICO – LONDRES / PARÍS – MÉXICO VOLANDO EN CLASE TURISTA. 
06 NOCHES DE ALOJAMIENTO EN CATEGORÍA INDICADA. 
RÉGIMEN ALIMENTICIO SEGÚN ITINERARIO. 
VISITAS SEGÚN ITINERARIO. 
SEGURO DE ASISTENCIA DEL OPERADOR. 
GUÍA PROFESIONAL DE HABLA HISPANA. 
TRASLADOS LOS INDICADOS. 
TRANSPORTE EN AUTOCAR TURÍSTICO. 
BOLSA DE VIAJE.";
$lang["contenido_6_lugares"]="Londres – Canal De La Mancha – París";
$lang["contenido_6_actividades"]="Visita de la ciudad, recorriendo entre otros Piccadilly Circus, Oxford Street, Casas del Parlamento, Reloj del Big Ben, Residencia Real de Buckingham Palace con el cambio de la Guardia Real (si el tiempo lo permite), Abadía de Westminster,";
$lang["contenido_6_recomendaciones"]="";
$lang["contenido_6_observaciones"]="GASTOS PERSONALES. 
PROPINAS. 
NINGÚN SERVICIO NO ESPECIFICADO. 
EXCURSIONES OPCIONALES. 
PUEDEN APLICAR IMPUESTOS LOCALES HOTELEROS A PAGAR DIRECTO EN DESTINO.";
$lang["contenido_6_salidaVuelo"]="21 de Enero 2017";
$lang["contenido_6_metaPalabrasUno"]="Londres – Canal De La Mancha – París";
$lang["contenido_6_metaPalabrasDos"]="";
$lang["contenido_7_ciudades"]="Peru";
$lang["contenido_7_incluye"]="Boleto de avión viaje redondo México – Lima Cusco – Lima – México
07 noches de alojamiento en los hoteles mencionados con desayunos diarios incluidos.
Lima
Traslado aeropuerto / hotel / aeropuerto en Lima en servicio compartido en idioma español
Tour a la ciudad de Lima en servicio compartido en idioma español con entradas incluidas
Cusco
Traslado aeropuerto / hotel / aeropuerto en servicio compartido en idioma español
Tour a la ciudad de Cusco en servicio compartido en idioma español con entradas incluidas
Tour al Valle Sagrado en servicio compartido en idioma español con entradas incluidas
Tour a Machu Picchu en servicio compartido en idioma español
Traslado hotel/estación de tren/hotel
Ticket de tren ida/retorno en tren Inka Rail
01 bus ida/retorno de Machu Picchu
01 entrada a Machu Picchu con visita guiada
01 almuerzo en restaurante local en Aguas Calientes (no incluye bebidas)
Guía de habla hispana
Bolsa de viaje";
$lang["contenido_7_lugares"]="Lima – Cusco – Valle Sagrado – Machu Picchu";
$lang["contenido_7_actividades"]="A la llegada, asistencia y traslado al hotel. En la tarde, recorrido exclusivo de la ciudad que inicia con una visita a la Plaza de San Cristóbal para disfrutar de una vista panorámica de la ciudad. Luego, visitaremos el Mercado de San Pedro, donde nos empaparemos del sabor local y conoceremos más de cerca los productos de la zona en este mercado que lo tiene todo y abastece a la ciudad completa.";
$lang["contenido_7_recomendaciones"]="HABITACIONES TRIPLES CUPO LIMITADO, FAVOR DE CONSULTAR";
$lang["contenido_7_observaciones"]="";
$lang["contenido_7_salidaVuelo"]="Diciembre 03 y 10";
$lang["contenido_7_metaPalabrasUno"]="";
$lang["contenido_7_metaPalabrasDos"]="";
$lang["contenido_8_ciudades"]="Alemania – Rep. Checa – Eslovaquia – Hungría – Austria";
$lang["contenido_8_incluye"]="BOLETO DE AVIÓN MÉXICO – BERLÍN / VIENA – MÉXICO VOLANDO EN CLASE TURISTA CON IBERIA 
08 NOCHES DE ALOJAMIENTO EN CATEGORIA INDICADA 
REGIMEN ALIMENTICIO SEGÚN ITINERARIO. 
VISITAS SEGÚN ITINERARIO. 
SEGURO DE ASISTENCIA CON COBERTURA DE 15,000€. 
GUIA PROFESIONAL DE HABLA HISPANA 
TRASLADOS LOS INDICADOS. 
TRANSPORTE EN AUTOCAR TURISTICO. 
BOLSA DE VIAJE.";
$lang["contenido_8_lugares"]="Berlín – Dresden – Praga – Bratislava – Budapest – Viena";
$lang["contenido_8_actividades"]="Visita panorámica de la ciudad para familiarizarse con los principales monumentos, recorriendo los lugares más importantes de ésta ciudad.
Tiempo libre para conocer la ciudad, posibilidad de asistir a excursiones opcionales para conocer más la ciudad.
Visita panorámica de la ciudad que se divide en dos zonas, “Buda” donde se encuentra la ciudad vieja, y “Pest” zona moderna comercial.";
$lang["contenido_8_recomendaciones"]="*ITINERARIO SUJETO A CAMBIO";
$lang["contenido_8_observaciones"]="";
$lang["contenido_8_salidaVuelo"]="10 de Abril, viajando con IBERIA";
$lang["contenido_8_metaPalabrasUno"]="Berlín – Dresden – Praga – Bratislava – Budapest – Viena";
$lang["contenido_8_metaPalabrasDos"]="paquetes-de-Europa-economicos-viaje-a-europa-todo-incluido-boletos-de-avion-para-viajar-a-europa";
$lang["contenido_9_ciudades"]="Estados Unidos";
$lang["contenido_9_incluye"]="Vuelo en viaje redondo clase turista México-San Francisco /// Las Vegas –México.<br />
07 Noches de alojamiento de acuerdo al programa<br />
Desayunos todos los días (excepto en Las Vegas)<br />
Visitas de acuerdo al programa<br />
Seguro en cortesía Mega Asist<br />
Transportacion y guía en español como se indica abajo en detalles de día a día";
$lang["contenido_9_lugares"]="San Francisco - Monterey - Carmel - Santa Mónica - Los Ángeles - Beverly Hills - Hollywood - Las Vegas";
$lang["contenido_9_actividades"]="A la hora indicada salida del Hotel para tour guiado por San Francisco, una de las ciudades más fotografiadas, filmadas y documentadas en el mundo, San Francisco es una mezcla seductora de la historia y la sofisticación moderna. Construido sobre varias colinas, la ciudad es la joya de la magnífica bahía al pie de estas montañas. Durante su visita podrá ver el distrito financiero, Union Square, Chinatown, a continuación, cruzar el conocido puente Golden Gate en la pintoresca localidad de Sausalito del otro lado de la bahía.Tarde libre para actividades por su cuenta. Alojamiento.";
$lang["contenido_9_recomendaciones"]="Reserva Experiencias";
$lang["contenido_9_observaciones"]="No incluye impuestos";
$lang["contenido_9_salidaVuelo"]="08 ABRIL <br />
6, 20 MAYO <br />
17 JUNIO <br />
15,22,29 JULIO<br />
5 AGOSTO<br />
9, 16, 30 SEPTIEMBRE<br />
21 OCTUBRE<br />
11, 25 NOVIEMBRE";
$lang["contenido_9_metaPalabrasUno"]="";
$lang["contenido_9_metaPalabrasDos"]="";
$lang["contenido_10_ciudades"]="Canada";
$lang["contenido_10_incluye"]="BASICO
Vuelo México- Toronto – Montreal – México
Traslado APTO-HTL-APTO
7 noches de alojamiento de acuerdo a itinerario.
7 desayunos una mezcla de continental y de americano.
Transporte descrito en itinerario. Día 1 y 8 traslado solamente. Día 5 y 7 transporte no incluido excepto en las excursiones opcionales.
Guía acompañante de habla hispana durante todo el recorrido.
Las visitas de Toronto, Niágara, Ottawa, Quebec y Montreal comentadas por su guía acompañante o por un guía local.
Todas las visitas mencionadas en el itinerario salvo cuando está indicado que son visitas opcionales. Incluye la embarcación « Hornblower» y el crucero por Mil Islas.
SUPERIOR
Vuelo México- Toronto – Montreal – México
Traslado aeropuerto – hotel – aeropuerto.
7 noches de alojamiento de acuerdo a itinerario.
7 desayunos buffet americano o a la carta.
Transporte en bus de acuerdo a itinerario. Día 1 y 8 traslado solamente. Día 5 y 7 transporte NO incluido excepto en las excursiones opcionales.
Guía acompañante de habla hispana durante todo el recorrido.
Las visitas de Toronto, Niágara, Ottawa, Quebec y Montreal comentadas por su guía acompañante o por un guía local.
Todas las visitas mencionadas en el itinerario salvo cuando indicado que son visitas opcionales. Incluye la embarcación « Hornblower » y el crucero por Mil Islas.
Manejo de 1 maleta por persona en los hoteles durante toda la estadía.";
$lang["contenido_10_lugares"]="Toronto - Niágara - Mil Islas – Ottawa – Quebec – Montreal";
$lang["contenido_10_actividades"]="El recorrido empieza visitando Toronto, capital económica del país: recorrido por el antiguo y nuevo City Hall, el Parlamento, el barrio Chino, la Universidad de Toronto, la Torre CN (subida opcional). Continuaremos nuestro paseo para llegar a las Cataratas del Niágara. El recorrido de nuestro tour continúa hacia Mil Islas. Crucero por las Islas de una hora donde pueden apreciar diversos paisajes (disponible de mayo a octubre)";
$lang["contenido_10_recomendaciones"]="";
$lang["contenido_10_observaciones"]="";
$lang["contenido_10_salidaVuelo"]="Domingo - 23 Abril al 29 Octubre 2017";
$lang["contenido_10_metaPalabrasUno"]="";
$lang["contenido_10_metaPalabrasDos"]="";
$lang["contenido_11_ciudades"]="";
$lang["contenido_11_incluye"]="";
$lang["contenido_11_lugares"]="";
$lang["contenido_11_actividades"]="";
$lang["contenido_11_recomendaciones"]="";
$lang["contenido_11_observaciones"]="";
$lang["contenido_11_salidaVuelo"]="";
$lang["contenido_11_metaPalabrasUno"]="";
$lang["contenido_11_metaPalabrasDos"]="";
$lang["contenido_12_ciudades"]="Bruno";
$lang["contenido_12_incluye"]="";
$lang["contenido_12_lugares"]="";
$lang["contenido_12_actividades"]="";
$lang["contenido_12_recomendaciones"]="";
$lang["contenido_12_observaciones"]="";
$lang["contenido_12_salidaVuelo"]="";
$lang["contenido_12_metaPalabrasUno"]="hello";
$lang["contenido_12_metaPalabrasDos"]="yes";
$lang["contenido_13_ciudades"]="Punta Cana";
$lang["contenido_13_incluye"]="Avión viaje redondo México - Punta Cana - México vía Panamá
Traslados Aeropuerto Hotel Aeropuerto en cada ciudad.
04 Noches de alojamiento en Punta Cana en hotel seleccionado.
Plan Todo Incluido en Punta Cana";
$lang["contenido_13_lugares"]="Republica Dominicana";
$lang["contenido_13_actividades"]="Alojamiento en Plan Todo Incluido y día libre para disfrutar de las maravillosas playas del Caribe.

DIA 03 PUNTA CANA 
Alojamiento en Plan Todo Incluido y día libre para disfrutar de las maravillosas playas del Caribe.

DIA 04 PUNTA CANA 
Alojamiento en Plan Todo Incluido y día libre para disfrutar de las maravillosas playas del Caribe.";
$lang["contenido_13_recomendaciones"]="";
$lang["contenido_13_observaciones"]="";
$lang["contenido_13_salidaVuelo"]="Salidas Diarias";
$lang["contenido_13_metaPalabrasUno"]="";
$lang["contenido_13_metaPalabrasDos"]="";
$lang["contenido_14_ciudades"]="Cuba";
$lang["contenido_14_incluye"]="Avión viaje redondo México-La Habana-Mexico
02 Noches de alojamiento en La Habana
01 Noche de alojamiento en Cienfuegos
01 Noche de alojamiento en Sancti Spiritus
01 Noche de alojamiento en Santa Clara
02 Noches de alojamiento en Varadero plan todo Incluido
Alimentos y visitas según programa
Guía durante todo el recorrido y ómnibus climatiza";
$lang["contenido_14_lugares"]="La Habana - Cienfuegos - Sancti Spiritus - Santa Clara - Varadero";
$lang["contenido_14_actividades"]="Recorrido de ciudad con visita al casco histórico Patrimonio de La Humanidad. Visita a las plazas de La Habana. Plaza de Armas, Plaza de San Francisco de Asís, Plaza Vieja.";
$lang["contenido_14_recomendaciones"]="";
$lang["contenido_14_observaciones"]="";
$lang["contenido_14_salidaVuelo"]="Sabados";
$lang["contenido_14_metaPalabrasUno"]="";
$lang["contenido_14_metaPalabrasDos"]="Paquetes-economicos-cuba-";
$lang["contenido_15_ciudades"]="Egipto";
$lang["contenido_15_incluye"]="Boleto de avión viaje redondo MEXICO – EL CAIRO - MEXICO.<br />
Boleto de Tren EL CAIRO – ASWAN / LUXOR – EL CAIRO<br />
02 noches en El Cairo en hotel de categoría 5*<br />
03 noches de Crucero por el Nilo en barco categoría 5*<br />
02 noches a bordo del tren.<br />
Régimen alimenticio según itinerario<br />
Traslados indicados.<br />
Visitas indicadas.<br />
Guías de habla hispana.<br />
Autocar con aire acondicionado de lujo<br />
Bolsa de viaje";
$lang["contenido_15_lugares"]="El Cairo - Aswan - Kom Ombo - Edfu - Luxor - El Cairo";
$lang["contenido_15_actividades"]="Por la mañana salida para visitar las pirámides de Giza, complejo funerario formado por las pirámides de Keops, una de las siete maravillas del Mundo, Kefrén, Micerinos, la Esfinge de Kefrén y el Templo del Valle.<br />
Visita al templo dedicado a los dioses Sobek y Haroeris. También veremos un nilómetro, utilizado por los antiguos egipcios para medir el nivel de las aguas de este rio. Navegación a Edfu. Noche abordo.";
$lang["contenido_15_recomendaciones"]="";
$lang["contenido_15_observaciones"]="";
$lang["contenido_15_salidaVuelo"]="Febrero 12/ Marzo 12 2017";
$lang["contenido_15_metaPalabrasUno"]="";
$lang["contenido_15_metaPalabrasDos"]="";
$lang["contenido_16_ciudades"]="";
$lang["contenido_16_incluye"]="Transportacion aérea saliendo desde el Bajio<br />
Traslados Aeropuerto-Hotel-Aeropuerto<br />
4 noches de hospedaje en el Hotel Westin Resort de Lujo<br />
Desayunos bufet diariamente";
$lang["contenido_16_lugares"]="Cancun";
$lang["contenido_16_actividades"]="";
$lang["contenido_16_recomendaciones"]="";
$lang["contenido_16_observaciones"]="";
$lang["contenido_16_salidaVuelo"]="Aeropuerto del bajio";
$lang["contenido_16_metaPalabrasUno"]="";
$lang["contenido_16_metaPalabrasDos"]="";
$lang["contenido_17_ciudades"]="";
$lang["contenido_17_incluye"]="";
$lang["contenido_17_lugares"]="";
$lang["contenido_17_actividades"]="";
$lang["contenido_17_recomendaciones"]="";
$lang["contenido_17_observaciones"]="";
$lang["contenido_17_salidaVuelo"]="";
$lang["contenido_17_metaPalabrasUno"]="";
$lang["contenido_17_metaPalabrasDos"]="";
$lang["contenido_18_ciudades"]="";
$lang["contenido_18_incluye"]="";
$lang["contenido_18_lugares"]="";
$lang["contenido_18_actividades"]="";
$lang["contenido_18_recomendaciones"]="";
$lang["contenido_18_observaciones"]="";
$lang["contenido_18_salidaVuelo"]="";
$lang["contenido_18_metaPalabrasUno"]="";
$lang["contenido_18_metaPalabrasDos"]="";
$lang["contenido_19_ciudades"]="Mexico";
$lang["contenido_19_incluye"]="Vuelo con Interjet Mexico-Villahermosa//Tuxtla-Mexico<br />
Traslados apto-htl-apt y recorridos terrestres de acuerdo al itinerario en vehículos de lujo con aire acondicionado.<br />
Chofer – guía (Español) todo el recorrido<br />
Todas las entradas a Parques y Monumentos descritos en el itinerario.<br />
Tour en lancha en el Cañón del Sumidero (Lancha Compartida)<br />
5 noches de alojamientohoteles 4 estrellas y 3 estrellas superior<br />
Desayuno diario tipo Americano";
$lang["contenido_19_lugares"]="Chiapas";
$lang["contenido_19_actividades"]="A la hora indicada tomar su vuelo para el destino de Villahermosa. A su llegada nos trasladaremos hacia Palenque. Ésta ciudad destaca por su acervo arquitectónico y escultórico y en donde podremos admirar varias construcciones: El Palacio, El Templo de la Cruz Foliada, El Templo del Sol y otras más. Al terminar visitaremos el museo de sitio considerado como uno de los museos arqueológicos más notables del área maya ya que reúne alrededor de 234 objetos que atestiguan y constituyen fuentes de información sobre la organización de la sociedad Palencana. Al terminar nos dirigiremos al hotel, registro y alojamiento.";
$lang["contenido_19_recomendaciones"]="Reserva Experiencias";
$lang["contenido_19_observaciones"]="El viaje NO incluye:<br />
IMPUESTOS de $1,050 MXN<br />
Propinas a guía y chofer<br />
Ningún Servicio no especificado.<br />
Gastos Personales.";
$lang["contenido_19_salidaVuelo"]="11 y 18 Abril";
$lang["contenido_19_metaPalabrasUno"]="";
$lang["contenido_19_metaPalabrasDos"]="";
$lang["contenido_20_ciudades"]="Argentina";
$lang["contenido_20_incluye"]="Boleto de avión México – Buenos Aires (vía Sao Paulo) – Bariloche – Buenos Aires – Iguazú México<br />
04 noches en Buenos Aires en hotel de categoría elegida<br />
02 noches en Puerto Iguazú en hotel de categoría elegida<br />
02 noches en Bariloche en hotel de categoría elegida<br />
Desayunos diarios<br />
Guías de habla hispana<br />
Visitas indicadas en el itinerario<br />
Traslados de llegada y salida<br />
Bolsa de viaje";
$lang["contenido_20_lugares"]="Buenos Aires - Iguazú – Bariloche";
$lang["contenido_20_actividades"]="Visitaremos Plaza de Mayo, centro político, social e histórico donde encontraremos la Casa Rosada sede del Poder Ejecutivo el Cabildo la Catedral Metropolitana y la Pirámide de Mayo. De allí atravesaremos parte de la Avenida de Mayo, rica en variedad de estilos arquitectónicos dónde se destacan los edificios de estilo europeo. Esta avenida une los Palacios Ejecutivo y el Legislativo, además fue y sigue siendo un punto de reunión de la comunidad española. Luego nos dirigiremos a visitar el barrio de San Telmo, dónde conoceremos su historia. Luego continuaremos al barrio de La Boca, característico por sus viviendas y por haber sido el primer Puerto de Buenos Aires y centro de la colectividad italiana, en especial la genovesa. Posteriormente visitaremos la parte norte de la ciudad: Palermo, un barrio que cuenta con grandes mansiones, su Parque Tres de Febrero y sus innumerables plazas e importantes avenidas. En esta zona veremos el monumento a la Carta Magna, comúnmente llamado De los Españoles. De allí nos dirigiremos a la zona de La Recoleta, uno de los barrios más elegantes y aristocráticos de la ciudad donde veremos el Paseo del Pilar, repleto de restaurantes y bares, la Iglesia del Pilar y los importantes edificios que se encuentran en este sitio";
$lang["contenido_20_recomendaciones"]="Reserva Experiencias";
$lang["contenido_20_observaciones"]="El viaje NO incluye:<br />
Seguro de viajero<br />
Ningún servicio no especificado.<br />
IMPUESTOS y Q’s pagaderos en México 699.00 USD (sujetos a cambio)<br />
Gastos personales.<br />
Entrada al Parque Nacionales Iguazú lado ARG. USD 35.00, lado BRA USD 40.00<br />
Medios de elevación en Bariloche.";
$lang["contenido_20_salidaVuelo"]="Salidas especiales: Febrero 09** Marzo 09 Abril 20 Mayo 04 y 18 Junio 01, 15 y 29 **Suplemento terrestre 99.00 usd por pasajero.";
$lang["contenido_20_metaPalabrasUno"]="";
$lang["contenido_20_metaPalabrasDos"]="";
$lang["contenido_21_ciudades"]="";
$lang["contenido_21_incluye"]="";
$lang["contenido_21_lugares"]="";
$lang["contenido_21_actividades"]="";
$lang["contenido_21_recomendaciones"]="";
$lang["contenido_21_observaciones"]="";
$lang["contenido_21_salidaVuelo"]="";
$lang["contenido_21_metaPalabrasUno"]="";
$lang["contenido_21_metaPalabrasDos"]="";
$lang["contenido_22_ciudades"]="";
$lang["contenido_22_incluye"]="";
$lang["contenido_22_lugares"]="";
$lang["contenido_22_actividades"]="";
$lang["contenido_22_recomendaciones"]="";
$lang["contenido_22_observaciones"]="";
$lang["contenido_22_salidaVuelo"]="";
$lang["contenido_22_metaPalabrasUno"]="";
$lang["contenido_22_metaPalabrasDos"]="";
$lang["contenido_23_ciudades"]="";
$lang["contenido_23_incluye"]="";
$lang["contenido_23_lugares"]="";
$lang["contenido_23_actividades"]="";
$lang["contenido_23_recomendaciones"]="";
$lang["contenido_23_observaciones"]="";
$lang["contenido_23_salidaVuelo"]="";
$lang["contenido_23_metaPalabrasUno"]="";
$lang["contenido_23_metaPalabrasDos"]="";
$lang["contenido_24_ciudades"]="Alaska";
$lang["contenido_24_incluye"]="Todo Incluido  y Cabina cuadruple";
$lang["contenido_24_lugares"]="Alaska Radiante";
$lang["contenido_24_actividades"]="";
$lang["contenido_24_recomendaciones"]="Reserva Experiencias";
$lang["contenido_24_observaciones"]="Pregunta salidas para el verano. <br />
Vuelo no incluido";
$lang["contenido_24_salidaVuelo"]="17 de Mayo, de Vancouver a Seward";
$lang["contenido_24_metaPalabrasUno"]="";
$lang["contenido_24_metaPalabrasDos"]="";
$lang["contenido_25_ciudades"]="Caribe";
$lang["contenido_25_incluye"]="Todo Incluido y cabina cuadruple";
$lang["contenido_25_lugares"]="Ft. Lauderdale , San Marteen . Puerto Rico , Haiti";
$lang["contenido_25_actividades"]="";
$lang["contenido_25_recomendaciones"]="Reserva Experiencias";
$lang["contenido_25_observaciones"]="Desde 13,359. <br />
Vuelo no incluido";
$lang["contenido_25_salidaVuelo"]="7 de Mayo 2017";
$lang["contenido_25_metaPalabrasUno"]="";
$lang["contenido_25_metaPalabrasDos"]="";
$lang["contenido_26_ciudades"]="";
$lang["contenido_26_incluye"]="";
$lang["contenido_26_lugares"]="";
$lang["contenido_26_actividades"]="";
$lang["contenido_26_recomendaciones"]="";
$lang["contenido_26_observaciones"]="";
$lang["contenido_26_salidaVuelo"]="";
$lang["contenido_26_metaPalabrasUno"]="";
$lang["contenido_26_metaPalabrasDos"]="";
$lang["contenido_27_ciudades"]="";
$lang["contenido_27_incluye"]="";
$lang["contenido_27_lugares"]="";
$lang["contenido_27_actividades"]="";
$lang["contenido_27_recomendaciones"]="";
$lang["contenido_27_observaciones"]="";
$lang["contenido_27_salidaVuelo"]="";
$lang["contenido_27_metaPalabrasUno"]="";
$lang["contenido_27_metaPalabrasDos"]="";
$lang["contenido_28_ciudades"]="Turquia";
$lang["contenido_28_incluye"]="Boleto de avión viaje redondo MÉXICO – ESTAMBUL – MÉXICO<br />
Vuelo Doméstico Estambul – Esmirna (La Política de Equipaje incluye solamente una pieza de 15 Kg Para este vuelo)<br />
03 noches en Estambul en hoteles de 4*<br />
01 noche en Kusadasi en hoteles de 4*<br />
01 noche en Pamukkale en hoteles de 4*<br />
02 noches en Antalya en hoteles de 4*<br />
02 noches en Capadocia en hoteles 4*<br />
Régimen alimenticio según itinerario<br />
Traslados indicados<br />
Visitas indicadas<br />
Guías de habla hispana<br />
Autocar con aire acondicionado<br />
Maleta de viaje";
$lang["contenido_28_lugares"]="Estambul – Kusadasi – Éfeso – Pamukkale – Antalya – Capadocia – Ankara - Estambul";
$lang["contenido_28_actividades"]="Puede realizar una Excursión guiada opcional en Estambul con almuerzo incluido en restaurante de comidas típicas: Topkapi Palace, fue la residencia de los sultanes otomanos del siglo XV al XIX y hoy presenta muestras del tesoro real otomano y otras reliquias religiosas. Santa Sofía una obra maestra de la arquitectura mundial desde el siglo V, hoy presenta referencias religiosas cristianas y musulmanas mezcladas después de la conversión en museo. El Hipódromo Romano, decorado con obeliscos, columnas y fuentes. La Mezquita Azul con sus seis minaretes y decorado con azulejos de color turquesa. Seguimos al famoso Gran Bazar, un mercado de paredes y cúpulas seculares. Alojamiento.";
$lang["contenido_28_recomendaciones"]="Reserva experiencias";
$lang["contenido_28_observaciones"]="";
$lang["contenido_28_salidaVuelo"]="Enero 29<br />
Feb 05, 17 y 19<br />
Mar 12 y 17 2017";
$lang["contenido_28_metaPalabrasUno"]="";
$lang["contenido_28_metaPalabrasDos"]="";
$lang["contenido_29_ciudades"]="Dubai";
$lang["contenido_29_incluye"]="Boleto de avión viaje redondo MEXICO – DUBAI – MEXICO. (vía USA)<br />
06 noches de Alojamiento en el hotel mencionado o similar-<br />
06 desayunos<br />
Traslados aeropuerto/hotel/aeropuerto<br />
Medio día de visita con guía de habla hispana<br />
Recuerdo de visita a Emiratos Árabes";
$lang["contenido_29_lugares"]="Dubai";
$lang["contenido_29_actividades"]="Recorrido hasta las magníficas vistas de la ensenada de Dubái Creek, pasando por el área de patrimonio de Bastakiya y sus fascinantes casas antiguas con características torres de viento construidas por ricos mercaderes. Continuación de visita a la fortaleza de Al Fahidi de 225 años de antigüedad. Es aquí donde el museo de Dubái conserva valiosos archivos acerca del pasado de la ciudad, así como crónicas de sus diferentes fases de desarrollo. Posteriormente subirán a bordo de un barco tradicional Abra para atravesar la ensenada y visitar el mercado de especias y el zoco del oro. De camino al Burj Al Arab, el hotel más lujoso del mundo, habrá una parada fotográfica junto a la mezquita de Jumeirah, un monumento arquitectónico de Dubái. <br />
Por la tarde posibilidad de realizar la visita opcional de subida al a torre de Burj Khalifa, tiempo libre en Dubái. Alojamiento.";
$lang["contenido_29_recomendaciones"]="Reserva experiencias";
$lang["contenido_29_observaciones"]="";
$lang["contenido_29_salidaVuelo"]="Diarias";
$lang["contenido_29_metaPalabrasUno"]="";
$lang["contenido_29_metaPalabrasDos"]="";
$lang["contenido_30_ciudades"]="Egipto";
$lang["contenido_30_incluye"]="Boleto de avión viaje redondo MEXICO – EL CAIRO – LONDRES - MEXICO.<br />
Boleto de Tren EL CAIRO – ASWAN / LUXOR – EL CAIRO<br />
02 noches en El Cairo en hotel de categoría 4*<br />
02 noches en tren coche-cama.<br />
03 noches de Crucero por el Nilo en barco categoría 5* (visitas en el crucero Templo de Luxor y paseo por feluca en Aswan<br />
01 noche en Londres hotel de aeropuerto<br />
Medio día de visita en El Cairo a las pirámides<br />
Traslados indicados.<br />
Visitas indicadas.<br />
Guías de habla hispana.<br />
Autocar con aire acondicionado de lujo<br />
Bolsa de viaje";
$lang["contenido_30_lugares"]="El Cairo – Aswan – Kom Ombo – Edfu – Esna – Luxor";
$lang["contenido_30_actividades"]="Por la mañana salida para visitar las pirámides de Giza, complejo funerario formado por las pirámides de Keops, una de las siete maravillas del Mundo, Kefrén, Mikerinos, la Esfinge de Kefrén y el Templo del Valle. Por la tarde traslado a la estación de ferrocarril para salir en tren hacia Aswan. Cena y noche a bordo del tren.";
$lang["contenido_30_recomendaciones"]="Reserva experiencias";
$lang["contenido_30_observaciones"]="";
$lang["contenido_30_salidaVuelo"]="";
$lang["contenido_30_metaPalabrasUno"]="";
$lang["contenido_30_metaPalabrasDos"]="";
$lang["contenido_31_ciudades"]="";
$lang["contenido_31_incluye"]="";
$lang["contenido_31_lugares"]="";
$lang["contenido_31_actividades"]="";
$lang["contenido_31_recomendaciones"]="";
$lang["contenido_31_observaciones"]="";
$lang["contenido_31_salidaVuelo"]="";
$lang["contenido_31_metaPalabrasUno"]="";
$lang["contenido_31_metaPalabrasDos"]="";
$lang["contenido_32_ciudades"]="El barco mas nuevo , mas grande y mas atrevido. ¡Descrubelo!";
$lang["contenido_32_incluye"]="Todo Incluido , nos ajustamos a tu presupuesto.";
$lang["contenido_32_lugares"]="La mejores vacaciones para ti y toda tu familia al completo ,";
$lang["contenido_32_actividades"]="Tienes las actividades mas adrenalinicas asi como las mas relajante.";
$lang["contenido_32_recomendaciones"]="INFINITAS OPCIONES DE ENTRETENIMIENTO BAJO LA LUZ DE LAS GRANDES ESTRELLAS DE BROADWAY";
$lang["contenido_32_observaciones"]="Las tentaciones mas deliciosas de la mano de la alta cocina.";
$lang["contenido_32_salidaVuelo"]="Salidas diarias";
$lang["contenido_32_metaPalabrasUno"]="";
$lang["contenido_32_metaPalabrasDos"]="";
$lang["contenido_33_ciudades"]="";
$lang["contenido_33_incluye"]="";
$lang["contenido_33_lugares"]="";
$lang["contenido_33_actividades"]="";
$lang["contenido_33_recomendaciones"]="";
$lang["contenido_33_observaciones"]="";
$lang["contenido_33_salidaVuelo"]="";
$lang["contenido_33_metaPalabrasUno"]="";
$lang["contenido_33_metaPalabrasDos"]="";
$lang["contenido_34_ciudades"]="";
$lang["contenido_34_incluye"]="";
$lang["contenido_34_lugares"]="";
$lang["contenido_34_actividades"]="";
$lang["contenido_34_recomendaciones"]="";
$lang["contenido_34_observaciones"]="";
$lang["contenido_34_salidaVuelo"]="";
$lang["contenido_34_metaPalabrasUno"]="";
$lang["contenido_34_metaPalabrasDos"]="";
$lang["contenido_35_ciudades"]="Estados Unidos";
$lang["contenido_35_incluye"]="Vuelo redondo clase Turista MEXICO- LAS VEGAS - MEXICO. ¡Vuelo Directo!<br />
03 Noches de alojamiento en Las Vegas en hotel seleccionado.<br />
Traslado de llegada Aeropuerto - Hotel.<br />
Cuponera de descuentos.";
$lang["contenido_35_lugares"]="La Vegas";
$lang["contenido_35_actividades"]="A la hora indicada, presentarse en el aeropuerto de la Ciudad de México para DÍA 01. MÉXICO-LAS VEGAS.<br />
A la hora indicada, presentarse en el aeropuerto de la Ciudad de México para tomar vuelo con destino a Las Vegas. Al llegar a su destino, traslado a su Hotel. Alojamiento.<br />
<br />
DÍAS 02-03. LAS VEGAS.<br />
Días libres para actividades personales, Alojamiento.<br />
<br />
Actividades recomendadas:<br />
CIRQUE DU SOLEI O, KA, Le Reve, Love, Zumanity, Blue Man Group, Mystere, Viva Elvis, Criss Ángel.<br />
<br />
Visitas a: Gran Cañón, Presa Hoover, Aventura por el desierto, Las vegas de noche en helicóptero, Rafting por el Cañón Negro del Río Colorado, etc. (CONSULTAR TARIFAS)<br />
<br />
DÍA 04. LAS VEGAS–MÉXICO.<br />
A la hora indicada trasladarse por su cuenta al aeropuerto para tomar el vuelo con destino a la ciudad de México.";
$lang["contenido_35_recomendaciones"]="Reserva Experiencias";
$lang["contenido_35_observaciones"]="El viaje NO incluye:<br />
IMPUESTOS 260 USD por persona<br />
Alimentos y gastos personales<br />
Ningún servicio no especificado<br />
Resort Fee: Algunos hoteles pueden cobrar resort Fee, este es un impuesto que cobra directamente el hotel al pasajero al momento de hacer el Check Out, es por noche por habitación, dependiendo de cada hotel puede ser entre 15 y hasta 40 usd por habitación por noche.";
$lang["contenido_35_salidaVuelo"]="12, 13, 20 de Abril 2017";
$lang["contenido_35_metaPalabrasUno"]="";
$lang["contenido_35_metaPalabrasDos"]="";
$lang["contenido_36_ciudades"]="";
$lang["contenido_36_incluye"]="";
$lang["contenido_36_lugares"]="";
$lang["contenido_36_actividades"]="";
$lang["contenido_36_recomendaciones"]="";
$lang["contenido_36_observaciones"]="";
$lang["contenido_36_salidaVuelo"]="";
$lang["contenido_36_metaPalabrasUno"]="";
$lang["contenido_36_metaPalabrasDos"]="";
$lang["contenido_37_ciudades"]="Cuba – Jamaica – Islas Caiman – México";
$lang["contenido_37_incluye"]="07 noches de crucero<br />
Hospedaje en la categoría seleccionada del crucero<br />
01 noche post crucero en La Habana<br />
Todos los alimentos a bordo (desayuno, comida y cena en bufete) en el restaurante principal<br />
Acceso a las áreas públicas del barco<br />
Bolsa de viaje";
$lang["contenido_37_lugares"]="La Habana – Montego Bay – Georgetown – Cozumel";
$lang["contenido_37_actividades"]="Mañana libre para realizar actividades personales.<br />
Recorrer La Habana es un verdadero atractivo. La belleza de su arquitectura, sus edificaciones, los tesoros de sus museos invitan a adentrarse en sus calles.<br />
Usted puede visitar El Centro Histórico de La Habana Vieja, con sus museos, galerías de arte, centros recreativos y culturales sistema de fortificaciones: el Castillo de la Real Fuerza, el Castillo de los Tres Reyes del Morro y la Fortaleza de San Carlos de la Cabaña Plaza de la Revolución, vinculada a los momentos más relevantes de la revolución cubana Barrio Chino, los restaurantes La Bodeguita del Medio, y el Floridita Centro de Convenciones de La Habana, marinas internacionales, el cabaret Tropicana y se puede disfrutar de más de 14 km de playas.";
$lang["contenido_37_recomendaciones"]="Reserva Experiencias";
$lang["contenido_37_observaciones"]="El viaje NO incluye:<br />
Restaurantes de especialidades<br />
Gastos personales<br />
IMPUESTOS portuarios y aéreos<br />
Propinas<br />
Excursiones<br />
Visa para cuba<br />
Gastos extras en el crucero, como llamadas telefónicas, lavandería, spa, internet, etc.<br />
Ningún servicio no especificado";
$lang["contenido_37_salidaVuelo"]="Abril 15";
$lang["contenido_37_metaPalabrasUno"]="";
$lang["contenido_37_metaPalabrasDos"]="";
$lang["contenido_38_ciudades"]="México";
$lang["contenido_38_incluye"]="Vuelo viaje redondo con Aeroméxico<br />
Todos los traslados del itinerario<br />
4 noches de hospedaje: 1 noche en Chihuahua, 1 noche en Divisadero, 1 noche en el Fuerte y 2 en Los Mochis, en hoteles 4 estrellas.<br />
Desayunos, y cena en Divisadero.<br />
Ferrocarril panorámico CHEPE, 1ªclase del tramo Divisadero – El Fuerte<br />
Traslado Chihuahua – Divisadero por carretera<br />
Traslado Estación de tren – Hotel en El Fuerte<br />
Traslado El Fuerte – Los Mochis por carretera<br />
Tour en Chihuahua<br />
Visita a comunidades menonitas<br />
Caminata guiada a Barrancas del Cobre, Urique y Piedra Volada<br />
Caminata en El Fuerte<br />
Caminata en el jardín botánico en Los Mochis<br />
Tour de bahía en Topolobampo<br />
**Vista a la barranca en el hotel Divisadero estará sujeta a disponibilidad**";
$lang["contenido_38_lugares"]="Chihuahua - Creel - Divisadero - Barrancas Del Cobre - Urike - Piedra Volada - el Fuerte - Los Mochis";
$lang["contenido_38_actividades"]="Cita en el Aeropuerto de la ciudad de México con destino a Chihuahua. Llegada y traslado a su hotel desayuno (no incluido) para posteriormente hacer la caminata al Centro de la Ciudad donde se recorrerá los murales del Palacio de Gobierno, así como la Catedral, Centro Cultural Universitario antes Quinta Gameros, la Casa de Pancho Villa- Hoy Museo de la Revolución, el Acueducto Colonial y zona Residencial (entradas a los museos no incluidas). Tarde libre. Alojamiento.";
$lang["contenido_38_recomendaciones"]="Reserva Experiencias";
$lang["contenido_38_observaciones"]="El viaje NO incluye:<br />
IMPUESTOS de $1,050 mxn<br />
Gastos personales<br />
Teleférico en Barrancas<br />
Propinas a maleteros, camaristas, meseros, guía y chofer<br />
Ningún servicio no especificado<br />
v NOTA 1: Para su comodidad, algunas veces el equipaje será movido por los maleteros hasta sus cuartos asignados en los hoteles por lo cual les recomendamos que no guarden ningún objeto de valor en los mismos ya que no nos hacemos responsables por estos.<br />
NOTA 2: En caso de causas de fuerza mayor y para su seguridad el itinerario podrá sufrir alguna modificación o cambio.";
$lang["contenido_38_salidaVuelo"]="12 y 19 Abril 2017";
$lang["contenido_38_metaPalabrasUno"]="";
$lang["contenido_38_metaPalabrasDos"]="";
$lang["contenido_39_ciudades"]="Las bahamas";
$lang["contenido_39_incluye"]="Todo Incluido y Cabina cuadruple";
$lang["contenido_39_lugares"]="Miami, Nassau, Cococay y Key West";
$lang["contenido_39_actividades"]="";
$lang["contenido_39_recomendaciones"]="Reserva experiencias";
$lang["contenido_39_observaciones"]="";
$lang["contenido_39_salidaVuelo"]="8 de Mayo 2017<br />
Vuelo no incluido";
$lang["contenido_39_metaPalabrasUno"]="";
$lang["contenido_39_metaPalabrasDos"]="";
$lang["contenido_40_ciudades"]="";
$lang["contenido_40_incluye"]="";
$lang["contenido_40_lugares"]="";
$lang["contenido_40_actividades"]="";
$lang["contenido_40_recomendaciones"]="";
$lang["contenido_40_observaciones"]="";
$lang["contenido_40_salidaVuelo"]="";
$lang["contenido_40_metaPalabrasUno"]="";
$lang["contenido_40_metaPalabrasDos"]="";
$lang["contenido_41_ciudades"]="canada";
$lang["contenido_41_incluye"]="Boleto de avión clase Turista México – Toronto - Montreal – México<br />
08 noches de alojamiento<br />
Admisión al barco de Mil Islas - Opera de Mayo 1 a Octubre 31 - Fuera de temporada se visita el Museo de la Civilización en Ottawa<br />
Es responsabilidad del pasajero de tener la documentación necesaria para ingresar a Canadá. Para mayor información contactar al Consulado Canadiense.<br />
Incluye 8 Desayunos Americanos<br />
Incluye Crucero Maid of the Mist - Opera de Mayo a Octubre. Fuera de temporada será substituido por los túneles escénicos.<br />
Incluye manejo de 1 maleta por pasajero durante el recorrido, maletas adicionales serán cobradas<br />
Traslados apto-hotel-apto";
$lang["contenido_41_lugares"]="Toronto - Niagara Falls - Mil Islas Otawa - Mt Tremblan Quebec - Montreal";
$lang["contenido_41_actividades"]="Desayuno americano. 07:00 hrs encuentro en el lobby del hotel para desayunar y conocer al resto del grupo. Iniciamos con una visita panorámica de la ciudad: la alcaldía, el parlamento provincial, la universidad de Toronto, el barrio bohemio de Yorkville y el barrio donde se encuentra el estadio de Baseball y la torre CN (Torre autoportante más alta del mundo) donde pararemos y daremos tiempo para subir (admisión no incluida). Luego partimos hacia Niagara Falls, una vez allí navegaremos por el rio Niagara en el barco Maid of the Mist, que nos llevara hasta la misma caída de las cataratas. Tiempo libre para almuerzo (no incluido). Visitaremos Table Rock con su inolvidable panorámica. Más tarde continuamos el recorrido por la ruta del vino hasta llegar al bellísimo pueblo de Niagara en the Lake, antigua capital del Alto Canada. Regreso a Toronto. Alojamiento";
$lang["contenido_41_recomendaciones"]="Reserva Experencias";
$lang["contenido_41_observaciones"]="";
$lang["contenido_41_salidaVuelo"]="Enero 23<br />
Febrero 27<br />
Marzo 20<br />
Abril 10";
$lang["contenido_41_metaPalabrasUno"]="";
$lang["contenido_41_metaPalabrasDos"]="";
?>