<?php
$lang['item_kits_name'] = 'Nombre del Paquete';
$lang['item_kits_paquete'] = 'Paquete';
$lang['item_kits_description'] = 'Descripción del Paquete';
$lang['item_kits_no_item_kits_to_display'] = 'No hay Paquetes para mostrar';
$lang['item_kits_update'] = 'Modificar el contenido del paquete';
$lang['item_kits_cats'] = 'Subir imagen de la categoria';
$lang['item_kits_new'] = 'Nuevo Paquete';
$lang['item_kits_info'] = 'Editar datos detallados del paquete';
$lang['item_kits_productos'] = 'Agregar/Quitar productos del paquete';
$lang['item_kits_none_selected'] = "No has seleccionado Paquetes";
$lang['item_kits_successful_adding'] = 'Has agregado satisfactoriamente un Paquete';
$lang['item_kits_successful_updating'] = 'Has actualizado satisfactoriamente un Paquete';
$lang['item_kits_error_adding_updating'] = 'Error agregando/actualizando el Paquete';
$lang['item_kits_successful_deleted'] = 'Has borrado satisfactoriamente';
$lang['item_kits_confirm_delete'] = '¿Estás seguro(a) de querer borrar los Paquetes seleccionados?';
$lang['item_kits_one_or_multiple'] = 'Paquete(s)';
$lang['item_kits_cannot_be_deleted'] = 'No pude borrar el/los Paquete(s) ';
$lang['item_kits_add_item'] = 'Agregar Producto';
$lang['item_kits_items'] = 'Productos';
$lang['item_kits_item'] = 'Producto';
$lang['item_kits_quantity'] = 'Cantidad';
$lang['item_kits_expire'] = 'Expira';
$lang['item_kits_category'] = 'Categoria';
$lang['item_kits_cost_price'] = 'Costo';
$lang['item_kits_unit_price'] = 'Precio';
$lang['item_kits_category_required'] = 'Se necesita la categoría del paquete';
$lang['item_kits_name_required'] = 'Se necesita el nombre del paquete';
$lang['item_kits_price_required'] = 'Se necesita el precio del paquete';
$lang['item_kits_imagesT'] = 'Esta es la imagen miniatura de los paquetes debe ser cuadrada minimo 300px por 300px, maximo 1000px por 1000px';
$lang['item_kits_imagesBP'] = 'Esta es la imagen del banner en los detalles del paquete debe ser 1366px por 500px, otro tamaño puede descuadrar el diseño de la página';
$lang['item_kits_imagesBC'] = 'Esta es la imagen del banner de las categorías debe ser 1366px por 500px, otro tamaño puede descuadrar el diseño de la página';
$lang['item_kits_imagesM'] = 'Esta es la imagen principal de los detalles del paquete debe ser 700px por 406px otro tamaño puede descuadrar el diseño de la página';
?>
