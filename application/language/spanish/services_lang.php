<?php
$lang['services_cliente']='Cliente';
$lang['services_motivo']='Motivo';
$lang['services_date']='Fecha';
$lang['services_estado']='Estado';
$lang['services_asignado']='Asignado';
$lang['services_accion']='Acción a realizar';
$lang['services_reporte']='Reporte';
$lang['services_duracion']='Duracion';
$lang['services_cambiar']='Cambiar';
$lang['services_new']='Nuevo Servicio';
$lang['services_serv']='Servicio';
$lang['services_hecho']='Hecho';
$lang['services_pendiente']='Pendiente';
$lang['services_cancelado']='Cancelado';
$lang['services_confirm_delete']='Esta seguro que desea cancelar este servicio';
$lang['services_none_selected']='No ha seleccionado ningun servicio';
$lang['services_cancel_success']='El servicio se ha cancelado de manera exitosa';
$lang['services_save']='El servicio se ha generado o editado de manera exitosa';
$lang['services_save_error']='El servicio no se ha generado o editado correctamente';
$lang['services_empty']='No hay servicios para mostrar';
$lang['services_cancel']='El servicio se ha cancelado correctamente';
$lang['services_no_cancel']='El servicio no se puede cancelar';
?>


