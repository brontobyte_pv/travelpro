<?php
$lang['module_home']='Inicio';

$lang['module_customers']='Clientes';
$lang['module_customers_desc']='Agregar, Actualizar, Borrar y Buscar clientes';

$lang['module_suppliers']='Proveedor';
$lang['module_suppliers_desc']='Agregar, Actualizar, Borrar y Buscar proveedores';

$lang['module_employees']='Empleados';
$lang['module_employees_desc']='Agregar, Actualizar, Borrar y Buscar empleados';

$lang['module_sales']='Ventas';
$lang['module_sales_desc']='Procesar ventas y reintegros';

$lang['module_reports']='Reportes';
$lang['module_reports_desc']='Ver y generar reportes';

$lang['module_items']='Productos';
$lang['module_items_desc']='Agregar, Actualizar, Borrar y Buscar Productos';

$lang['module_config']='Configuración';
$lang['module_config_desc']='Cambiar la configuración de la tienda';

$lang['module_receivings']='Entradas';
$lang['module_receivings_desc']='Process Purchase orders';

$lang['module_giftcards']='Tarjetas';
$lang['module_giftcards_desc']='Agregar, Actualizar, Borrar y Buscar Tarjetas de Regalo';

$lang['module_item_kits']='Paquetes';
$lang['module_item_kits_desc']='Agregar, Actualizar, Borrar y Buscar Paquetes';

$lang['module_services']='Servicios';
$lang['module_services_desc']='Agregar, Actualizar, Borrar y Buscar Servicios';

$lang['module_cobros']='Cobros';
$lang['module_cobros_desc']='Agregar, Actualizar, Borrar y Buscar Cobros';

$lang['module_gastos']='Gastos';
$lang['module_gastos_desc']='Agregar, Actualizar, Borrar y Buscar Gastos';

$lang['module_pedidos']='Reservaciones';
$lang['module_pedidos_desc']='Agregar, Actualizar, Borrar y Buscar Reservaciones';

$lang['module_recursos']='Recursos';
$lang['module_recursos_desc']='Agregar, Actualizar, Borrar y Buscar Recursos';

$lang['module_home']='Inicio';


?>
